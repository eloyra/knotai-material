"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
const path = require("path");
const url = require("url");
require('electron-reload')(path.join(__dirname, `@root/dist/knotai-material/index.html`));
let win;
electron_1.app.on('ready', createWindow);
electron_1.app.on('activate', () => {
    if (win === null) {
        createWindow();
    }
});
function createWindow() {
    win = new electron_1.BrowserWindow({ show: false, frame: true, icon: path.join(__dirname, 'assets/256x256.png') });
    win.maximize();
    win.once('ready-to-show', () => {
        win.show();
    });
    // win.setMenu(null);
    // win.removeMenu();
    win.setMenuBarVisibility(false);
    win.loadURL(url.format({
        pathname: path.join(__dirname, `../../dist/knotai-material/index.html`),
        protocol: 'file:',
        slashes: true,
    }));
    // win.loadURL('http://localhost:4200/');
    // win.webContents.openDevTools();
    win.on('closed', () => {
        win = null;
    });
}
/*function openModal() {
  const { BrowserWindow } = require('electron');
  const modal = new BrowserWindow({ parent: win, modal: true, show: false });
  modal.loadURL('https://www.sitepoint.com');
  modal.once('ready-to-show', () => {
    modal.show();
  });
}

ipcMain.on('openModal', (event, arg) => {
  openModal();
});*/
//# sourceMappingURL=main.js.map