--=====================================================================================================================
--                          KNOTAI's DATABASE CREATION AND CONFIGURATION SCRIPT                                       --
--=====================================================================================================================

-- FIRST WE MAKE SURE THE REQUIRED SCHEMAS EXIST AND ARE CLEAN
DROP SCHEMA IF EXISTS knotai;
CREATE SCHEMA IF NOT EXISTS knotai;
DROP SCHEMA IF EXISTS knotai_private;
CREATE SCHEMA IF NOT EXISTS knotai_private;

-- WE REVOKE PUBLIC ACCESS FROM ALL OUR FUNCTIONS
alter default privileges revoke execute on functions from public;

-- WE INSTALL REQUIRED EXTENSIONS IN BOTH SCHEMAS
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";

-- WE START USING OUR PRIVATE SCHEMA
SET search_path TO knotai_private;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "pgcrypto";


--//////////////////////////////
-- DATA TYPES
--//////////////////////////////
CREATE TYPE state AS ENUM (
    'new',
--     'pending',
    'ongoing',
    'blocked',
--     'review',
    'finished'
    );

CREATE TYPE priority AS ENUM (
    'high',
    'medium',
    'low'
    );

CREATE TYPE task_type AS ENUM (
    'management',
    'analysis',
    'design',
    'frontend',
    'backend',
    'testing',
    'systems'
    );


-- WE START USING OUR PUBLIC SCHEMA AND DEFINE OUR DB STRUCTURE
SET search_path TO knotai;


--//////////////////////////////
-- DB STRUCTURE
--//////////////////////////////

-- PROJECT
create table project
(
    id             uuid                    default knotai_private.uuid_generate_v4() primary key,
    created_at     timestamp               default now() not null,
    updated_at     timestamp               default now() not null,
    name           varchar(20)                           not null,
    description    text,
    start          timestamp,
    deadline       timestamp,
    finish         timestamp,
    wiki           text,
    sprintDuration int                     default 2,
    config         jsonb,
    state          knotai_private.state    default 'new' not null,
    priority       knotai_private.priority default 'medium',
    repository     varchar(120),
    weight         int
);

comment on table knotai.project is 'A project is the abstract representation of a product to develop and deliver.';
comment on column knotai.project.id is 'Unique identifier.';
comment on column knotai.project.created_at is 'Indicates the time of creation.';
comment on column knotai.project.updated_at is 'Indicates the time of last change.';
comment on column knotai.project.name is 'Human readable name.';
comment on column knotai.project.description is 'Short description.';
comment on column knotai.project.start is 'Scheduled start time.';
comment on column knotai.project.deadline is 'Scheduled end of project.';
comment on column knotai.project.finish is 'Actual project finish date.';
comment on column knotai.project.wiki is 'Project wiki page.';
comment on column knotai.project.config is 'Configuration object.';
comment on column knotai.project.state is 'Lifecycle status.';
comment on column knotai.project.priority is 'Priority.';
comment on column knotai.project.repository is 'URL to the VCS repository.';
comment on column knotai.project.weight is 'Weight in an ordered list.';

create unique index project_name_uindex
    on knotai.project (name);


-- ROLE
create table role
(
    id         uuid      default knotai_private.uuid_generate_v4() primary key,
    created_at timestamp default now() not null,
    updated_at timestamp default now() not null,
    name       varchar(30)             not null unique
);

comment on table role is 'A role used to fence permissions.';
comment on column role.id is 'Unique identifier.';
comment on column role.created_at is 'Indicates the time of creation.';
comment on column role.updated_at is 'Indicates the time of last change.';
comment on column role.name is 'Unique name.';


-- USER PROFILE
create table user_profile
(
    id          uuid      default knotai_private.uuid_generate_v4() primary key,
    created_at  timestamp default now() not null,
    updated_at  timestamp default now() not null,
    first_name  VARCHAR(80),
    last_name   VARCHAR(120)            not null,
    picture     text,
    proficiency jsonb,
    role        uuid                    references knotai.role (id) on update cascade on delete set null
);

comment on table knotai.user_profile is 'Public information of a user.';
comment on column knotai.user_profile.id is 'Unique identifier.';
comment on column knotai.user_profile.created_at is 'Time of creation.';
comment on column knotai.user_profile.updated_at is 'Time of last change.';
comment on column knotai.user_profile.first_name is 'First name.';
comment on column knotai.user_profile.last_name is 'Surname.';
comment on column knotai.user_profile.proficiency is 'Information about the skills of the user.';
comment on column knotai.user_profile.role is 'User assigned role.';


-- MEMBERS - A MANY-TO-MANY RELATION OF USERS AND PROJECTS
create table members
(
    id           uuid default knotai_private.uuid_generate_v4() primary key,
    project      uuid not null references knotai.project (id) on update cascade on delete cascade,
    user_profile uuid not null references knotai.user_profile (id) on update cascade on delete cascade
);

comment on table members is 'Project membership.';
comment on column members.project is 'Project reference.';
comment on column members.user_profile is 'User reference.';


-- SPRINT
create table sprint
(
    id              uuid      default knotai_private.uuid_generate_v4() primary key,
    created_at      timestamp default now() not null,
    updated_at      timestamp default now() not null,
    name            varchar(30)             not null,
    description     text,
    duration        int,
    active          bool      default false not null,
    activation_time timestamp default null,
    closed          timestamp default null,
    weight          int,
    project         uuid references knotai.project (id) on update cascade on delete cascade
);

comment on table sprint is 'A sprint specifies a time frame for the delivery of a set amount of story points.';
comment on column sprint.id is 'Sprint unique identifier.';
comment on column sprint.created_at is 'Indicates the time of creation.';
comment on column sprint.created_at is 'Indicates the time of last change.';
comment on column sprint.name is 'Name.';
comment on column sprint.description is 'Description.';
comment on column sprint.duration is 'Duration in weeks.';
comment on column sprint.active is 'Determines if this is the current working sprint.';
comment on column sprint.activation_time is 'Time of activation.';
comment on column sprint.closed is 'Timestamp when this sprint was closed.';
comment on column sprint.weight is 'Weight in an ordered list.';
comment on column sprint.project is 'Project the sprint is associated to.';


-- STORY
create table story
(
    id            uuid                 default knotai_private.uuid_generate_v4() primary key,
    created_at    timestamp            default now() not null,
    updated_at    timestamp            default now() not null,
    name          varchar(120)                       not null,
    description   text,
    points        int,
    state         knotai_private.state default 'new' not null,
    task_type     knotai_private.task_type,
    finished_date timestamp            default null,
    weight        int,
    project       uuid                               not null references knotai.project (id) on update cascade on delete cascade,
    sprint        uuid                               references knotai.sprint (id) on update cascade on delete set null,
    assignee      uuid                               references user_profile (id) on update cascade on delete set null
);

comment on table story is 'A story defines a use case to implement.';
comment on column story.id is 'Story unique identifier.';
comment on column story.created_at is 'Indicates the time of creation.';
comment on column story.created_at is 'Indicates the time of last change.';
comment on column story.name is 'Name.';
comment on column story.description is 'Description.';
comment on column story.points is 'Duration in agile points.';
comment on column story.state is 'Story state.';
comment on column story.task_type is 'Classification of the story by task type.';
comment on column story.weight is 'Weight in an ordered list.';
comment on column story.project is 'Project the story is associated to.';
comment on column story.sprint is 'Sprint the story is associated to.';
comment on column story.assignee is 'Project member tasked with the story.';


-- TASK
create table task
(
    id          uuid                 default knotai_private.uuid_generate_v4() primary key,
    created_at  timestamp            default now() not null,
    updated_at  timestamp            default now() not null,
    name        varchar(120)                       not null,
    description text,
    points      int,
    state       knotai_private.state default 'new' not null,
    task_type   knotai_private.task_type,
    weight      int,
    project     uuid                               not null references knotai.project (id) on update cascade on delete cascade,
    sprint      uuid                               references knotai.sprint (id) on update cascade on delete set null,
    story       uuid                               not null references knotai.story (id) on update cascade on delete cascade,
    assignee    uuid                               references knotai.user_profile (id) on update cascade on delete set null
);

comment on table task is 'A task defines a specific body of work inside a story.';
comment on column task.id is 'Task unique identifier.';
comment on column task.created_at is 'Indicates the time of creation.';
comment on column task.created_at is 'Indicates the time of last change.';
comment on column task.name is 'Name.';
comment on column task.description is 'Description.';
comment on column task.points is 'Duration in agile points.';
comment on column task.state is 'Task state.';
comment on column task.task_type is 'Classification of the task by task type.';
comment on column task.weight is 'Weight in an ordered list.';
comment on column task.project is 'Project the task is associated to.';
comment on column task.sprint is 'Sprint the task is associated to.';
comment on column task.story is 'Story the task is associated to.';
comment on column task.assignee is 'Project member tasked with the story.';


-- TAG
create table tag
(
    id          uuid      default knotai_private.uuid_generate_v4() primary key,
    created_at  timestamp default now() not null,
    updated_at  timestamp default now() not null,
    label       varchar(30) unique      not null,
    description text
);

comment on table tag is 'A tag classifies a story.';
comment on column tag.id is 'Story unique identifier.';
comment on column tag.created_at is 'Indicates the time of creation.';
comment on column tag.created_at is 'Indicates the time of last change.';
comment on column tag.label is 'Name.';
comment on column tag.description is 'Description.';


-- TAGBOOK - A MANY-TO-MANY RELATION OF TAGS AND STORIES
create table tagbook
(
    id    uuid default knotai_private.uuid_generate_v4() primary key,
    tag   uuid not null references knotai.tag (id) on update cascade on delete cascade,
    story uuid not null references knotai.story (id) on update cascade on delete cascade
);

comment on table tagbook is 'A registry of stories and tags relationships.';
comment on column tagbook.id is 'Tagbook entry unique identifier.';
comment on column tagbook.tag is 'UUID of the associated tag.';
comment on column tagbook.story is 'UUID of the associated story.';


-- COMMENT
create table comment
(
    id         uuid      default knotai_private.uuid_generate_v4() primary key,
    created_at timestamp default now() not null,
    updated_at timestamp default now() not null,
    content    text                    not null,
    author     uuid                    references knotai.user_profile (id) on update cascade on delete set null,
    task       uuid references knotai.task (id) on update cascade on delete cascade
);

comment on table comment is 'A rich text fragment used to communicate inside stories.';
comment on column comment.id is 'Comment unique identifier.';
comment on column comment.created_at is 'Indicates the time of creation.';
comment on column comment.created_at is 'Indicates the time of last change.';
comment on column comment.content is 'Content.';
comment on column comment.author is 'User who created the comment.';
comment on column comment.task is 'Task where the comment is created.';


-- WORK UNIT
create table time_entry
(
    id          uuid      default knotai_private.uuid_generate_v4() primary key,
    created_at  timestamp default now() not null,
    updated_at  timestamp default now() not null,
    hours       decimal                 not null,
    progress    decimal                 not null,
    description text                    not null,
    time        timestamp default now() not null,
    author      uuid                    not null references knotai.user_profile (id) on update cascade on delete set null,
    task        uuid                    not null references knotai.task (id) on update cascade on delete cascade
);

comment on table time_entry is 'Time spent in an specific task inside a story.';
comment on column time_entry.id is 'Entry unique identifier.';
comment on column time_entry.created_at is 'Indicates the time of creation.';
comment on column time_entry.created_at is 'Indicates the time of last change.';
comment on column time_entry.hours is 'Time spent in the task.';
comment on column time_entry.progress is 'Relative percentage of work done for the parent story.';
comment on column time_entry.description is 'Description of the task.';
comment on column time_entry.time is 'The day the work was done.';
comment on column time_entry.author is 'User who created the entry.';
comment on column time_entry.task is 'Task where the time entry is created.';


-- SYSTEM
create table system
(
    id         uuid      default knotai_private.uuid_generate_v4() primary key,
    created_at timestamp default now() not null,
    updated_at timestamp default now() not null,
    config     jsonb                   not null
);

-- WE INDEX ALL FOREIGN KEYS FOR BD ACCESS PERFORMANCE
CREATE INDEX ON "knotai"."comment" ("author");
CREATE INDEX ON "knotai"."comment" ("task");
CREATE INDEX ON "knotai"."members" ("project");
CREATE INDEX ON "knotai"."members" ("user_profile");
CREATE INDEX ON "knotai"."sprint" ("project");
CREATE INDEX ON "knotai"."story" ("project");
CREATE INDEX ON "knotai"."user_profile" ("role");
CREATE INDEX ON "knotai"."story" ("sprint");
CREATE INDEX ON "knotai"."story" ("assignee");
CREATE INDEX ON "knotai"."tagbook" ("story");
CREATE INDEX ON "knotai"."time_entry" ("task");
CREATE INDEX ON "knotai"."tagbook" ("tag");
CREATE INDEX ON "knotai"."time_entry" ("author");
CREATE INDEX ON "knotai"."task" ("project");
CREATE INDEX ON "knotai"."task" ("sprint");
CREATE INDEX ON "knotai"."task" ("story");
CREATE INDEX ON "knotai"."task" ("assignee");

--WE INDEX SPECIFIC FIELD FOR ORDERING
CREATE INDEX ON "knotai"."project" ("weight");
CREATE INDEX ON "knotai"."sprint" ("weight");
CREATE INDEX ON "knotai"."story" ("weight");
CREATE INDEX ON "knotai"."comment" ("created_at");
CREATE INDEX ON "knotai"."task" ("created_at");
CREATE INDEX ON "knotai"."task" ("weight");


-- AGAIN WE USE OUR PRIVATE SCHEMA
SET search_path TO knotai_private;


-- FOR CREATING A USER PRIVATE TABLE
create table user_account
(
    user_id       uuid primary key references knotai.user_profile (id) on update cascade on delete cascade,
    email         varchar(60)  not null unique check (email ~* '^.+@.+\..+$'),
    password_hash varchar(100) not null
);

comment on table user_account is 'Private information of a user.';
comment on column user_account.user_id is 'User profile associated to this information.';
comment on column user_account.email is 'Email as username for the user.';
comment on column user_account.password_hash is 'Hashed password.';



--//////////////////////////////
-- FUNCTIONS AND TRIGGERS
--//////////////////////////////


-- WE DEFINE A FUNCTION FOR REFRESHING THE UPDATED_AT FIELD OF OUR TABLES
create function set_updated_at() returns trigger as
$$
begin
    new.updated_at := current_timestamp;
    return new;
end;
$$ language plpgsql;
comment on function knotai_private.set_updated_at() is 'Function that returns current time as timestamp.';


-- WE RETURN TO THE PUBLIC SCHEMA
SET search_path TO knotai;


-- WE DEFINE ALL TRIGGERS FOR UPDATE_AT
create trigger project_updated_at
    before update
    on knotai.project
    for each row
execute procedure knotai_private.set_updated_at();
create trigger role_updated_at
    before update
    on knotai.role
    for each row
execute procedure knotai_private.set_updated_at();
create trigger user_updated_at
    before update
    on knotai.user_profile
    for each row
execute procedure knotai_private.set_updated_at();
create trigger sprint_updated_at
    before update
    on knotai.sprint
    for each row
execute procedure knotai_private.set_updated_at();
create trigger story_updated_at
    before update
    on knotai.story
    for each row
execute procedure knotai_private.set_updated_at();
create trigger task_updated_at
    before update
    on knotai.task
    for each row
execute procedure knotai_private.set_updated_at();
create trigger tag_updated_at
    before update
    on knotai.tag
    for each row
execute procedure knotai_private.set_updated_at();
create trigger comment_updated_at
    before update
    on knotai.comment
    for each row
execute procedure knotai_private.set_updated_at();
create trigger time_entry_updated_at
    before update
    on knotai.time_entry
    for each row
execute procedure knotai_private.set_updated_at();
create trigger system_updated_at
    before update
    on knotai.system
    for each row
execute procedure knotai_private.set_updated_at();


-- DEFINE A FUNCTION FOR GETTING USER FULL NAME
create function user_profile_full_name("user" knotai.user_profile) returns text as
$$
select "user".first_name || ' ' || "user".last_name
$$ language sql stable
                security definer;
grant execute on function knotai.user_profile_full_name(knotai.user_profile) to anonymous, dev, manager, "admin";
comment on function knotai.user_profile_full_name(knotai.user_profile) is 'A user full name which is a concatenation of their first and last names.';



-- AND DEFINE A FUNCTION FOR REGISTERING A USER
create function register_user(first_name_input varchar(80),
                              last_name_input varchar(120),
                              email_input varchar(60),
                              password varchar(20)) returns knotai.user_profile as
$$
declare
    "user" knotai.user_profile;
begin

    insert into knotai.user_profile (first_name, last_name)
    values (first_name_input, last_name_input)
    returning * into "user";

    insert into knotai_private.user_account (user_id, email, password_hash)
    values ("user".id, email_input, knotai_private.crypt(password, knotai_private.gen_salt('bf')));

    return "user";
end;
$$
    language plpgsql strict
                     security definer;
grant execute on function knotai.register_user(varchar, varchar, varchar, varchar) to anonymous, dev, manager, "admin";
comment on function knotai.register_user(varchar, varchar, varchar, varchar) is 'Registers a user creating their private account and hashing the password.';



--//////////////////////////////
-- ROLES AND AUTHENTICATION
--//////////////////////////////


-- WE CREATE A GENERAL PURPOSE DB ACCESS ROLE FOR POSTGRAPHILE API
create role postgraphile login password '90042f76-2fb1-4891-892a-63f1d72192c9';


-- WE CREATE AN ANONYMOUS ROLE
create role anonymous;
grant anonymous to postgraphile;
-- AN EXTERNAL USER ROLE
create role "external";
grant "external" to postgraphile;
-- A DEVELOPER ROLE
create role dev;
grant dev to postgraphile;
-- A PROJECT MANAGER ROLE
create role manager;
grant manager to postgraphile;
-- AN APP ADMIN ROLE
create role "admin";
grant "admin" to postgraphile;


-- NOW WE DEFINE OUR JWT TOKEN
create type jwt_token as
(
    role_id   uuid,
    role_name varchar(30),
    user_id   uuid,
    user_name varchar(80),
    exp       bigint
);


-- AND THE FUNCTION THAT WILL RETURN IT
create function authenticate(email varchar(60),
                             password varchar(20)) returns knotai.jwt_token as
$$
declare
    account knotai_private.user_account;
    profile knotai.user_profile;
    role    knotai.role;
begin
    select a.*
    into account
    from knotai_private.user_account as a
    where a.email = $1;

    select p.*
    into profile
    from knotai.user_profile as p
    where p.id = account.user_id;

    select r.*
    into role
    from knotai.role as r
    where r.id = profile.role;

    if account.password_hash = knotai_private.crypt($2, account.password_hash) then
        return (
                role.id,
                role.name,
                account.user_id,
                profile.first_name,
                extract(epoch from (now() + interval '2 days'))
            )::knotai.jwt_token;
    else
        return null;
    end if;
end;
$$ language plpgsql strict
                    security definer;
grant execute on function knotai.authenticate(varchar, varchar) to anonymous;
comment on function knotai.authenticate(varchar, varchar) is 'Creates a JWT token to safely identify a user and give them access to the application.';


-- FUNCTION FOR RETRIEVING CURRENT AUTHENTICATED USER
create function "current_user"() returns knotai.user_profile as
$$
declare
    profile knotai.user_profile;
begin
    select p.*
    into profile
    from knotai.user_profile as p
    where p.id = nullif(current_setting('jwt.claims.user_id', true), '')::uuid;

    return (profile)::knotai.user_profile;

end;
$$ language plpgsql stable
                    security definer;
grant execute on function knotai."current_user"() to anonymous, dev, manager, "admin";
comment on function knotai."current_user"() is 'Get the user who is currently authenticated.';


create function check_project_membership(user_id uuid,
                                         project_id uuid) returns boolean as
$$
declare
    cup integer;
begin
    select count(up.*)
    into cup
    from knotai.members as up
    where up.user_profile = user_id
      and up.project = project_id;

    if cup = 1 then
        return true;
    else
        return false;
    end if;
end;
$$ language plpgsql strict
                    security definer;
grant execute on function knotai.check_project_membership(uuid, uuid) to anonymous, dev, manager, "admin";


-- WE FINE-GRAIN OUR ACCESS PERMISSIONS
grant usage on schema knotai to anonymous, dev, manager, "admin";


-- PROJECT PERMISSIONS
grant select on table knotai.project to anonymous, dev, manager, "admin";
grant insert, update, delete on table knotai.project to manager, "admin";
alter table knotai.project
    enable row level security;
create policy select_project_any on knotai.project for select to "admin"
    using (true);
create policy select_project_ifmember on knotai.project for select to anonymous, dev, manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                id
            )
        );
create policy insert_project_any on knotai.project for insert to "admin"
    with check (true);
create policy update_project_any on knotai.project for update to "admin"
    using (true);
create policy update_project_ifmember on knotai.project for update to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                id
            )
        );
create policy delete_project_any on knotai.project for delete to "admin"
    using (true);
create policy delete_project_current on knotai.project for delete to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                id
            )
        );


-- ROLE PERMISSIONS
grant select on table knotai.role to anonymous, dev, manager, "admin";
grant insert, update, delete on table knotai.role to manager, "admin";


-- USER_PROFILE PERMISSIONS
grant select on table knotai.user_profile to anonymous, dev, manager, "admin";
grant update on table knotai.user_profile to dev, manager, "admin";
grant insert, delete on table knotai.user_profile to "admin";
alter table knotai.user_profile
    enable row level security;
create policy select_user_profile on knotai.user_profile for select
    using (true);
create policy insert_user_profile on knotai.project for insert to "admin"
    with check (true);
create policy update_user_profile_any on knotai.user_profile for update to "admin"
    using (true);
create policy update_user_profile_current on knotai.user_profile for update to dev, manager
    using (id = nullif(current_setting('jwt.claims.user_id', true), '')::uuid);
create policy delete_user_profile on knotai.user_profile for delete to "admin"
    using (true);

-- MEMBERS PERMISSIONS
grant select on table knotai.members to anonymous, dev, manager, "admin";
grant insert, update, delete on table knotai.members to manager, "admin";
alter table knotai.members
    enable row level security;
create policy select_members on knotai.members for select
    using (true);
create policy insert_members_any on knotai.members for insert to "admin"
    with check (true);
create policy insert_members_ifmember on knotai.members for insert to manager
    with check
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );
create policy update_members_any on knotai.members for update to "admin"
    using (true);
create policy update_members_ifmember on knotai.members for update to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );
create policy delete_members_any on knotai.members for delete to "admin"
    using (true);
create policy delete_members_ifmember on knotai.members for delete to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );

-- SPRINT PERMISSIONS
grant select on table knotai.sprint to anonymous, dev, manager, "admin";
grant insert, update, delete on table knotai.sprint to manager, "admin";
alter table knotai.sprint
    enable row level security;
create policy select_sprint_any on knotai.sprint for select to "admin"
    using (true);
create policy select_sprint_ifmember on knotai.sprint for select to anonymous, dev, "manager"
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );
create policy insert_sprint_any on knotai.sprint for insert to "admin"
    with check (true);
create policy insert_sprint_ifmember on knotai.sprint for insert to manager
    with check
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );
create policy update_sprint_any on knotai.sprint for update to "admin"
    using (true);
create policy update_sprint_ifmember on knotai.sprint for update to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );
create policy delete_sprint_any on knotai.sprint for delete to "admin"
    using (true);
create policy delete_sprint_ifmember on knotai.sprint for delete to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );


-- STORY PERMISSIONS
grant select on table knotai.story to anonymous, dev, manager, "admin";
grant insert, update, delete on table knotai.story to manager, "admin";
alter table knotai.story
    enable row level security;
create policy select_story_any on knotai.story for select to "admin"
    using (true);
create policy select_story_ifmember on knotai.story for select to anonymous, dev, "manager"
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );
create policy insert_story_any on knotai.story for insert to "admin"
    with check (true);
create policy insert_story_ifmember on knotai.story for insert to manager
    with check
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );
create policy update_story_any on knotai.story for update to "admin"
    using (true);
create policy update_story_ifmember on knotai.story for update to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );
create policy delete_story_any on knotai.story for delete to "admin"
    using (true);
create policy delete_story_ifmember on knotai.story for delete to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );

-- TASK PERMISSIONS
grant select on table knotai.task to anonymous, dev, manager, "admin";
grant insert, update, delete on table knotai.task to manager, "admin";
alter table knotai.task
    enable row level security;
create policy select_task_any on knotai.task for select to "admin"
    using (true);
create policy select_task_ifmember on knotai.task for select to anonymous, dev, "manager"
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );
create policy insert_task_any on knotai.task for insert to "admin"
    with check (true);
create policy insert_task_ifmember on knotai.task for insert to manager
    with check
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );
create policy update_task_any on knotai.task for update to "admin"
    using (true);
create policy update_task_ifmember on knotai.task for update to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );
create policy delete_task_any on knotai.task for delete to "admin"
    using (true);
create policy delete_task_ifmember on knotai.task for delete to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                project
            )
        );

-- TAG PERMISSIONS
grant select on table knotai.tag to anonymous, dev, manager, "admin";
grant insert, update, delete on table knotai.tag to "admin";

-- TAGBOOK PERMISSIONS
grant select on table knotai.tagbook to anonymous, dev, manager, "admin";
grant insert, update, delete on table knotai.tagbook to manager, "admin";
alter table knotai.tagbook
    enable row level security;
create policy select_tagbook_any on knotai.tagbook for select to "admin"
    using (true);
create policy select_tagbook_ifmember on knotai.tagbook for select to anonymous, dev, "manager"
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                (SELECT project
                 FROM knotai.story
                 WHERE story.id = tagbook.story)
            )
        );
create policy insert_tagbook_any on knotai.tagbook for insert to "admin"
    with check (true);
create policy insert_tagbook_ifmember on knotai.tagbook for insert to manager
    with check
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                (SELECT project
                 FROM knotai.story
                 WHERE story.id = tagbook.story)
            )
        );
create policy update_tagbook_any on knotai.tagbook for update to "admin"
    using (true);
create policy update_tagbook_ifmember on knotai.tagbook for update to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                (SELECT project
                 FROM knotai.story
                 WHERE story.id = tagbook.story)
            )
        );
create policy delete_tagbook_any on knotai.tagbook for delete to "admin"
    using (true);
create policy delete_tagbook_ifmember on knotai.tagbook for delete to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                (SELECT project
                 FROM knotai.story
                 WHERE story.id = tagbook.story)
            )
        );

-- COMMENT PERMISSIONS
grant select, insert, update, delete on table knotai.comment to anonymous, dev, manager, "admin";
alter table knotai.comment
    enable row level security;
create policy select_comment_any on knotai.comment for select to "admin"
    using (true);
create policy select_comment_ifmember on knotai.comment for select to anonymous, dev, "manager"
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                (SELECT project
                 FROM knotai.task
                 WHERE task.id = comment.task)
            )
        );
create policy insert_comment_any on knotai.comment for insert to "admin"
    with check (true);
create policy insert_comment_ifmember on knotai.comment for insert to anonymous, dev, manager
    with check
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                (SELECT project
                 FROM knotai.task
                 WHERE task.id = comment.task)
            )
        );
create policy update_comment_any on knotai.comment for update to "admin"
    using (true);
create policy update_comment_ifmember on knotai.comment for update to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                (SELECT project
                 FROM knotai.task
                 WHERE task.id = comment.task)
            )
        );
create policy update_comment_ifauthor on knotai.comment for update to anonymous, dev
    using
    (author = nullif(current_setting('jwt.claims.user_id', true), '')::uuid);
create policy delete_comment_any on knotai.comment for delete to "admin"
    using (true);
create policy delete_comment_ifmember on knotai.comment for delete to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                (SELECT project
                 FROM knotai.task
                 WHERE task.id = comment.task)
            )
        );
create policy delete_comment_ifauthor on knotai.comment for delete to anonymous, dev
    using
    (author = nullif(current_setting('jwt.claims.user_id', true), '')::uuid);


-- TIME ENTRY
grant select, insert, update, delete on table knotai.time_entry to anonymous, dev, manager, "admin";
alter table knotai.time_entry
    enable row level security;
create policy select_time_entry_any on knotai.time_entry for select to "admin"
    using (true);
create policy select_time_entry_ifmember on knotai.time_entry for select to anonymous, dev, "manager"
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                (SELECT project
                 FROM knotai.task
                 WHERE task.id = time_entry.task)
            )
        );
create policy insert_time_entry_any on knotai.time_entry for insert to "admin"
    with check (true);
create policy insert_time_entry_ifmember on knotai.time_entry for insert to anonymous, dev, manager
    with check
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                (SELECT project
                 FROM knotai.task
                 WHERE task.id = time_entry.task)
            )
        );
create policy update_time_entry_any on knotai.time_entry for update to "admin"
    using (true);
create policy update_time_entry_ifmember on knotai.time_entry for update to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                (SELECT project
                 FROM knotai.task
                 WHERE task.id = time_entry.task)
            )
        );
create policy update_time_entry_ifauthor on knotai.time_entry for update to anonymous, dev
    using
    (author = nullif(current_setting('jwt.claims.user_id', true), '')::uuid);
create policy delete_time_entry_any on knotai.time_entry for delete to "admin"
    using (true);
create policy delete_time_entry_ifmember on knotai.time_entry for delete to manager
    using
    (
        knotai.check_project_membership(
                nullif(current_setting('jwt.claims.user_id', true), '')::uuid,
                (SELECT project
                 FROM knotai.task
                 WHERE task.id = time_entry.task)
            )
        );
create policy delete_time_entry_ifauthor on knotai.time_entry for delete to anonymous, dev
    using
    (author = nullif(current_setting('jwt.claims.user_id', true), '')::uuid);

-- SYSTEM CONFIGURATION
grant select on table knotai.system to anonymous, dev, manager, "admin";
grant insert, update, delete on table knotai.system to "admin";

-- USER_ACCOUNT PERMISSIONS
grant select, insert, update, delete on table knotai_private.user_account to "admin";
