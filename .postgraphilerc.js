module.exports = {
  options: {
    connection: "postgres://eloyra:admin@localhost/knotai-material",
    schema: ["knotai"],
    token: "knotai.jwt_token",
    secret: "030d6dbe-0993-4559-9950-24b52eac3456",
    defaultRole: "eloyra",
    appendPlugins: "@graphile-contrib/pg-simplify-inflector",
    exportSchemaJson: "/home/eloyra/Projects/knotai-material-docs/bd/schema-json.gql",
    exportSchemaGraphql: "/home/eloyra/Projects/knotai-material-docs/bd/schema.gql",
    cors: true,
    watch: true,
    dynamicJson: true,
    showErrorStack: true,
    extendedErrors: ["hint", "detail", "errcode"],
    enableQueryBatching: true,
    legacyRelations: "omit"
  },
};
