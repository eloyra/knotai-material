#!/usr/bin/env zsh

sudo -S /etc/init.d/postgresql restart
postgraphile \
  --no-setof-functions-contain-nulls \
  --no-ignore-rbac \
  --no-ignore-indexes \
  --enhance-graphiql \
  --allow-explain \
  &
