// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  mock: true,
  deployUrl: '',
  imagesPath: 'assets/images/',
  defaultAvatar: 'default-avatar.png',
  storyPointDuration: 4,
  adminRole: 'admin',
  managerRole: 'manager',
  storyDescriptionTemplate:
    '**Statement**\n' +
    'As a <...> I want to <...>\n' +
    '\n' +
    '**Scope**\n' +
    '<!-- What services and projects does this story involve? -->\n' +
    'N/A\n' +
    '\n' +
    '**KPI**\n' +
    '<!-- Does this aim to improve a specific KPI? -->\n' +
    'N/A\n',
  taskDescriptionTemplate:
    '**Description**\n' +
    '<!-- Describe briefly what this task aims to achieve -->\n' +
    'N/A\n' +
    '\n' +
    '**DoD**\n' +
    "<!-- Add the task's definition of done if necessary -->\n" +
    'N/A\n' +
    '\n' +
    '**Dependencies**\n' +
    'N/A\n' +
    '\n' +
    '**Known issues**\n' +
    'N/A,',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
