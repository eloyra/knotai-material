import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { environment } from 'src/environments';

@Component({
  selector: 'k-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginPageComponent implements OnInit {
  public logo = environment.deployUrl + environment.imagesPath + 'logo.png';

  constructor() {}

  ngOnInit(): void {}
}
