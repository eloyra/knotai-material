import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';

@Component({
  selector: 'k-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginFormComponent implements OnInit {
  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private auth: AuthenticationService,
    private router: Router
  ) {}

  public email: string;
  public password: string;
  public fetching = false;
  public wrongCredentials = false;
  public form = new FormGroup({});
  public model: any = { email: '', password: '' };
  public fields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      className: 'field',
      templateOptions: {
        type: 'text',
        label: 'Email',
        required: true,
        appearance: 'outline',
        color: 'accent',
      },
    },
    {
      key: 'password',
      type: 'input',
      className: 'field',
      templateOptions: {
        type: 'password',
        label: 'Password',
        minLength: 5,
        maxLength: 20,
        required: true,
        appearance: 'outline',
        color: 'accent',
      },
    },
  ];

  ngOnInit(): void {}

  onSubmit(model) {
    this.auth
      .authenticate(model)
      .then((success: boolean) => {
        this.wrongCredentials = !success;
        this.changeDetectorRef.markForCheck();
        if (success) {
          this.router.navigate(['/home']).then();
        }
      })
      .catch((reason) => {
        console.error('login-form', reason);
      });
  }
}
