import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { SharedModule } from 'src/app/shared/shared.module';

import { AdministrationRoutingModule } from './administration-routing.module';
import { AdminProjectsComponent } from './pages/admin-projects/admin-projects.component';
import { AdminSprintsComponent } from './pages/admin-sprints/admin-sprints.component';
import { AdminStoriesComponent } from './pages/admin-stories/admin-stories.component';
import { AdminTasksComponent } from './pages/admin-tasks/admin-tasks.component';
import { AdminUsersComponent } from './pages/admin-users/admin-users.component';
import { AdminProjectsTableComponent } from './components/admin-projects-table/admin-projects-table.component';
import { AdminSprintsTableComponent } from './components/admin-sprints-table/admin-sprints-table.component';
import { AdminStoriesTableComponent } from './components/admin-stories-table/admin-stories-table.component';
import { AdminTasksTableComponent } from './components/admin-tasks-table/admin-tasks-table.component';
import { AdminUsersTableComponent } from './components/admin-users-table/admin-users-table.component';

@NgModule({
  declarations: [
    AdminProjectsComponent,
    AdminSprintsComponent,
    AdminStoriesComponent,
    AdminTasksComponent,
    AdminUsersComponent,
    AdminProjectsTableComponent,
    AdminSprintsTableComponent,
    AdminStoriesTableComponent,
    AdminTasksTableComponent,
    AdminUsersTableComponent,
  ],
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    SharedModule,
    MatButtonModule,
    MatProgressSpinnerModule,
  ],
  exports: [
    AdminProjectsTableComponent,
    AdminSprintsTableComponent,
    AdminStoriesTableComponent,
    AdminTasksTableComponent,
    AdminUsersTableComponent,
  ],
})
export class AdministrationModule {}
