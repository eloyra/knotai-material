import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { DeleteSprintGQL, GetSprintsGQL, Sprint, SprintsConnection, SprintsEdge } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { SprintFormComponent } from 'src/app/shared/forms/sprint-form/sprint-form.component';

@Component({
  selector: 'k-admin-sprints',
  templateUrl: './admin-sprints.component.html',
  styleUrls: ['./admin-sprints.component.scss'],
})
export class AdminSprintsComponent implements OnInit {
  public error: any = null;
  public loading = false;
  public sprints: Sprint[] = [];

  constructor(
    private titleService: Title,
    private graphql: GraphqlService,
    private getSprints: GetSprintsGQL,
    private deleteSprint: DeleteSprintGQL,
    public dialog: MatDialog
  ) {
    this.titleService.setTitle('Sprint administration');
  }

  ngOnInit(): void {
    this.loading = true;
    this.graphql.query(this.getSprints).subscribe(
      (results: SprintsConnection) => {
        if (results) {
          this.sprints = results.edges.map((edge: SprintsEdge) => edge.node);
          this.loading = false;
        }
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  openAddSprintDialog(): void {
    this.dialog
      .open(SprintFormComponent, {
        width: '60%',
        height: 'auto',
      })
      .afterClosed()
      .subscribe((newSprint: Sprint) => {
        if (newSprint) {
          this.sprints = [...this.sprints, newSprint];
        }
      }, console.error);
  }

  onEdit(id: string): void {
    this.dialog
      .open(SprintFormComponent, {
        width: '60%',
        height: 'auto',
        data: this.sprints.find((sprint: Sprint) => sprint.id === id),
      })
      .afterClosed()
      .subscribe((updatedSprint: Sprint) => {
        if (updatedSprint) {
          this.sprints.splice(
            this.sprints.findIndex((sprint: Sprint) => sprint.id === updatedSprint.id),
            1,
            updatedSprint
          );
          this.sprints = [...this.sprints];
        }
      }, console.error);
  }

  onDelete(id: string): void {
    this.dialog
      .open(ConfirmationDialogComponent, {
        width: '40%',
        height: 'auto',
        data: {
          title: `Deleting sprint`,
          text: 'Are you sure you want to remove this sprint? This action cannot be undone. ',
        },
      })
      .afterClosed()
      .subscribe((actionTaken: boolean) => {
        if (actionTaken) {
          this.graphql
            .mutate(this.deleteSprint, {
              id,
            })
            .toPromise()
            .then(() => {
              this.sprints.splice(
                this.sprints.findIndex((sprint: Sprint) => sprint.id === id),
                1
              );
              this.sprints = [...this.sprints];
            })
            .catch(console.error);
        }
      }, console.error);
  }
}
