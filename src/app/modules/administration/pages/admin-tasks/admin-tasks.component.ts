import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { DeleteTaskGQL, GetTasksInDepthGQL, Task, TasksConnection, TasksEdge } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { TaskFormComponent } from 'src/app/shared/forms/task-form/task-form.component';

@Component({
  selector: 'k-admin-tasks',
  templateUrl: './admin-tasks.component.html',
  styleUrls: ['./admin-tasks.component.scss'],
})
export class AdminTasksComponent implements OnInit {
  public error: any = null;
  public loading = false;
  public tasks: Task[] = [];

  constructor(
    private titleService: Title,
    private graphql: GraphqlService,
    private getTasks: GetTasksInDepthGQL,
    private deleteTask: DeleteTaskGQL,
    public dialog: MatDialog
  ) {
    this.titleService.setTitle('Task administration');
  }

  ngOnInit(): void {
    this.loading = true;
    this.graphql.query(this.getTasks).subscribe(
      (results: TasksConnection) => {
        if (results) {
          this.tasks = results.edges.map((edge: TasksEdge) => edge.node);
          this.loading = false;
        }
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  openAddTaskDialog(): void {
    this.dialog
      .open(TaskFormComponent, {
        width: '40%',
        height: 'auto',
      })
      .afterClosed()
      .subscribe((newTask: Task) => {
        if (newTask) {
          this.tasks = [...this.tasks, newTask];
        }
      }, console.error);
  }

  onEdit(id: string): void {
    this.dialog
      .open(TaskFormComponent, {
        width: '60%',
        height: 'auto',
        data: { task: this.tasks.find((task: Task) => task.id === id) },
      })
      .afterClosed()
      .subscribe((updatedTask: Task) => {
        if (updatedTask) {
          this.tasks.splice(
            this.tasks.findIndex((task: Task) => task.id === updatedTask.id),
            1,
            updatedTask
          );
          this.tasks = [...this.tasks];
        }
      }, console.error);
  }

  onDelete(id: string): void {
    this.dialog
      .open(ConfirmationDialogComponent, {
        width: '60%',
        height: 'auto',
        data: {
          title: `Deleting task`,
          text: 'Are you sure you want to remove this task? This action cannot be undone. ',
        },
      })
      .afterClosed()
      .subscribe((actionTaken: boolean) => {
        if (actionTaken) {
          this.graphql
            .mutate(this.deleteTask, {
              id,
            })
            .toPromise()
            .then(() => {
              this.tasks.splice(
                this.tasks.findIndex((task: Task) => task.id === id),
                1
              );
              this.tasks = [...this.tasks];
            })
            .catch(console.error);
        }
      }, console.error);
  }
}
