import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import {
  DeleteProjectGQL,
  GetProjectsInDepthGQL,
  Project,
  ProjectsConnection,
  ProjectsEdge,
} from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { ProjectFormComponent } from 'src/app/shared/forms/project-form/project-form.component';

@Component({
  selector: 'k-admin-projects',
  templateUrl: './admin-projects.component.html',
  styleUrls: ['./admin-projects.component.scss'],
})
export class AdminProjectsComponent implements OnInit {
  public error: any = null;
  public loading = false;
  public projects: Project[] = [];

  constructor(
    private titleService: Title,
    private graphql: GraphqlService,
    private getProjects: GetProjectsInDepthGQL,
    private deleteProject: DeleteProjectGQL,
    public dialog: MatDialog
  ) {
    this.titleService.setTitle('Project administration');
  }

  ngOnInit(): void {
    this.loading = true;
    this.graphql.query(this.getProjects).subscribe(
      (results: ProjectsConnection) => {
        if (results) {
          this.projects = results.edges.map((edge: ProjectsEdge) => edge.node);
          this.loading = false;
        }
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  openAddProjectDialog(): void {
    this.dialog
      .open(ProjectFormComponent, {
        width: '60%',
        height: 'auto',
      })
      .afterClosed()
      .subscribe((newProject: Project) => {
        if (newProject) {
          this.projects = [...this.projects, newProject];
        }
      }, console.error);
  }

  onEdit(id: string): void {
    this.dialog
      .open(ProjectFormComponent, {
        width: '60%',
        height: 'auto',
        data: this.projects.find((project: Project) => project.id === id),
      })
      .afterClosed()
      .subscribe((updatedProject: Project) => {
        if (updatedProject) {
          this.projects.splice(
            this.projects.findIndex((project: Project) => project.id === updatedProject.id),
            1,
            updatedProject
          );
          this.projects = [...this.projects];
        }
      }, console.error);
  }

  onDelete(id: string): void {
    this.dialog
      .open(ConfirmationDialogComponent, {
        width: '40%',
        height: 'auto',
        data: {
          title: `Deleting project`,
          text: 'Are you sure you want to remove this project? This action cannot be undone. ',
        },
      })
      .afterClosed()
      .subscribe((actionTaken: boolean) => {
        if (actionTaken) {
          this.graphql
            .mutate(this.deleteProject, {
              id,
            })
            .toPromise()
            .then(() => {
              this.projects.splice(
                this.projects.findIndex((project: Project) => project.id === id),
                1
              );
              this.projects = [...this.projects];
            })
            .catch(console.error);
        }
      }, console.error);
  }
}
