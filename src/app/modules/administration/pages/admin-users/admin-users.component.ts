import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import {
  DeleteUserProfileGQL,
  GetUserProfilesGQL,
  UserProfilesConnection,
  UserProfilesEdge,
  UserProfile,
} from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { UserFormComponent } from 'src/app/shared/forms/user-form/user-form.component';

@Component({
  selector: 'k-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.scss'],
})
export class AdminUsersComponent implements OnInit {
  public error: any = null;
  public loading = false;
  public users: UserProfile[] = [];

  constructor(
    private titleService: Title,
    private graphql: GraphqlService,
    private getUserProfiles: GetUserProfilesGQL,
    private deleteUserProfile: DeleteUserProfileGQL,
    public dialog: MatDialog
  ) {
    this.titleService.setTitle('Users administration');
  }

  ngOnInit(): void {
    this.loading = true;
    this.graphql.query(this.getUserProfiles).subscribe(
      (results: UserProfilesConnection) => {
        if (results) {
          this.users = results.edges.map((edge: UserProfilesEdge) => edge.node);
          this.loading = false;
        }
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  openAddUserDialog(): void {
    this.dialog
      .open(UserFormComponent, {
        width: '60%',
        height: 'auto',
      })
      .afterClosed()
      .subscribe((newUser: UserProfile) => {
        if (newUser) {
          this.users = [...this.users, newUser];
        }
      }, console.error);
  }

  onEdit(id: string): void {
    this.dialog
      .open(UserFormComponent, {
        width: '60%',
        height: 'auto',
        data: { user: this.users.find((user: UserProfile) => user.id === id) },
      })
      .afterClosed()
      .subscribe((updatedUser: UserProfile) => {
        if (updatedUser) {
          this.users.splice(
            this.users.findIndex((user: UserProfile) => user.id === updatedUser.id),
            1,
            updatedUser
          );
          this.users = [...this.users];
        }
      }, console.error);
  }

  onDelete(id: string): void {
    this.dialog
      .open(ConfirmationDialogComponent, {
        width: '40%',
        height: 'auto',
        data: {
          title: `Deleting user`,
          text: 'Are you sure you want to remove this user? This action cannot be undone. ',
        },
      })
      .afterClosed()
      .subscribe((actionTaken: boolean) => {
        if (actionTaken) {
          this.graphql
            .mutate(this.deleteUserProfile, {
              id,
            })
            .toPromise()
            .then(() => {
              this.users.splice(
                this.users.findIndex((user: UserProfile) => user.id === id),
                1
              );
              this.users = [...this.users];
            })
            .catch(console.error);
        }
      }, console.error);
  }
}
