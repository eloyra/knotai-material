import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { DeleteStoryGQL, Story, StoriesConnection, StoriesEdge, GetStoriesGQL } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { StoryFormComponent } from 'src/app/shared/forms/story-form/story-form.component';

@Component({
  selector: 'k-admin-stories',
  templateUrl: './admin-stories.component.html',
  styleUrls: ['./admin-stories.component.scss'],
})
export class AdminStoriesComponent implements OnInit {
  public error: any = null;
  public loading = false;
  public stories: Story[] = [];

  constructor(
    private titleService: Title,
    private graphql: GraphqlService,
    private getStories: GetStoriesGQL,
    private deleteStory: DeleteStoryGQL,
    public dialog: MatDialog
  ) {
    this.titleService.setTitle('Story administration');
  }

  ngOnInit(): void {
    this.loading = true;
    this.graphql.query(this.getStories).subscribe(
      (results: StoriesConnection) => {
        if (results) {
          this.stories = results.edges.map((edge: StoriesEdge) => edge.node);
          this.loading = false;
        }
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  openAddStoryDialog(): void {
    this.dialog
      .open(StoryFormComponent, {
        width: '40%',
        height: 'auto',
      })
      .afterClosed()
      .subscribe((newStory: Story) => {
        if (newStory) {
          this.stories = [...this.stories, newStory];
        }
      }, console.error);
  }

  onEdit(id: string): void {
    this.dialog
      .open(StoryFormComponent, {
        width: '60%',
        height: 'auto',
        data: { story: this.stories.find((story: Story) => story.id === id) },
      })
      .afterClosed()
      .subscribe((updatedStory: Story) => {
        if (updatedStory) {
          this.stories.splice(
            this.stories.findIndex((story: Story) => story.id === updatedStory.id),
            1,
            updatedStory
          );
          this.stories = [...this.stories];
        }
      }, console.error);
  }

  onDelete(id: string): void {
    this.dialog
      .open(ConfirmationDialogComponent, {
        width: '60%',
        height: 'auto',
        data: {
          title: `Deleting story`,
          text: 'Are you sure you want to remove this story? This action cannot be undone. ',
        },
      })
      .afterClosed()
      .subscribe((actionTaken: boolean) => {
        if (actionTaken) {
          this.graphql
            .mutate(this.deleteStory, {
              id,
            })
            .toPromise()
            .then(() => {
              this.stories.splice(
                this.stories.findIndex((story: Story) => story.id === id),
                1
              );
              this.stories = [...this.stories];
            })
            .catch(console.error);
        }
      }, console.error);
  }
}
