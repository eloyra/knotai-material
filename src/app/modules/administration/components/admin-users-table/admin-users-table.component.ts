import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserProfile } from 'src/app/services/api';

@Component({
  selector: 'k-admin-users-table',
  templateUrl: './admin-users-table.component.html',
  styleUrls: ['./admin-users-table.component.scss'],
})
export class AdminUsersTableComponent implements OnInit {
  @Input() users: UserProfile[] = null;
  @Input() displayedColumns: string[] = ['picture', 'fullName', 'role', 'actions'];

  @Output() handleEdit: EventEmitter<string> = new EventEmitter<string>();
  @Output() handleDelete: EventEmitter<string> = new EventEmitter<string>();

  public tableSource: MatTableDataSource<UserProfile>;
  public tableFilter: string = '';

  @ViewChild(MatSort) sort: MatSort;

  constructor() {}

  ngOnInit(): void {
    this.tableSource = new MatTableDataSource(this.users);
  }

  ngAfterViewInit() {
    if (this.tableSource) {
      this.tableSource.sort = this.sort;
    }
  }

  ngOnChanges(): void {
    this.tableSource = new MatTableDataSource(this.users);
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableSource.filter = filterValue.trim().toLowerCase();
  }

  onEdit(projectId: string): void {
    this.handleEdit.emit(projectId);
  }

  onDelete(projectId: string) {
    this.handleDelete.emit(projectId);
  }
}
