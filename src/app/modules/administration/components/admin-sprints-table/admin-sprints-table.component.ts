import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Sprint } from 'src/app/services/api';

@Component({
  selector: 'k-admin-sprints-table',
  templateUrl: './admin-sprints-table.component.html',
  styleUrls: ['./admin-sprints-table.component.scss'],
})
export class AdminSprintsTableComponent implements OnInit {
  @Input() sprints: Sprint[] = null;
  @Input() displayedColumns: string[] = ['name', 'duration', 'active', 'closed', 'actions'];

  @Output() handleEdit: EventEmitter<string> = new EventEmitter<string>();
  @Output() handleDelete: EventEmitter<string> = new EventEmitter<string>();

  public tableSource: MatTableDataSource<Sprint>;
  public tableFilter: string = '';

  @ViewChild(MatSort) sort: MatSort;

  constructor() {}

  ngOnInit(): void {
    this.tableSource = new MatTableDataSource(this.sprints);
  }

  ngAfterViewInit() {
    if (this.tableSource) {
      this.tableSource.sort = this.sort;
    }
  }

  ngOnChanges(): void {
    this.tableSource = new MatTableDataSource(this.sprints);
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableSource.filter = filterValue.trim().toLowerCase();
  }

  onEdit(projectId: string): void {
    this.handleEdit.emit(projectId);
  }

  onDelete(projectId: string) {
    this.handleDelete.emit(projectId);
  }
}
