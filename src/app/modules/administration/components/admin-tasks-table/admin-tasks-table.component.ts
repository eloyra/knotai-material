import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Task } from 'src/app/services/api';

@Component({
  selector: 'k-admin-tasks-table',
  templateUrl: './admin-tasks-table.component.html',
  styleUrls: ['./admin-tasks-table.component.scss'],
})
export class AdminTasksTableComponent implements OnInit {
  @Input() tasks: Task[] = null;
  @Input() displayedColumns: string[] = ['name', 'state', 'task_type', 'actions'];

  @Output() handleEdit: EventEmitter<string> = new EventEmitter<string>();
  @Output() handleDelete: EventEmitter<string> = new EventEmitter<string>();

  public tableSource: MatTableDataSource<Task>;
  public tableFilter: string = '';

  @ViewChild(MatSort) sort: MatSort;

  constructor() {}

  ngOnInit(): void {
    this.tableSource = new MatTableDataSource(this.tasks);
  }

  ngAfterViewInit() {
    if (this.tableSource) {
      this.tableSource.sort = this.sort;
    }
  }

  ngOnChanges(): void {
    this.tableSource = new MatTableDataSource(this.tasks);
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableSource.filter = filterValue.trim().toLowerCase();
  }

  onEdit(projectId: string): void {
    this.handleEdit.emit(projectId);
  }

  onDelete(projectId: string) {
    this.handleDelete.emit(projectId);
  }
}
