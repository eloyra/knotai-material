import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Story } from 'src/app/services/api';

@Component({
  selector: 'k-admin-stories-table',
  templateUrl: './admin-stories-table.component.html',
  styleUrls: ['./admin-stories-table.component.scss'],
})
export class AdminStoriesTableComponent implements OnInit {
  @Input() stories: Story[] = null;
  @Input() displayedColumns: string[] = ['name', 'points', 'state', 'actions'];

  @Output() handleEdit: EventEmitter<string> = new EventEmitter<string>();
  @Output() handleDelete: EventEmitter<string> = new EventEmitter<string>();

  public tableSource: MatTableDataSource<Story>;
  public tableFilter: string = '';

  @ViewChild(MatSort) sort: MatSort;

  constructor() {}

  ngOnInit(): void {
    this.tableSource = new MatTableDataSource(this.stories);
  }

  ngAfterViewInit() {
    if (this.tableSource) {
      this.tableSource.sort = this.sort;
    }
  }

  ngOnChanges(): void {
    this.tableSource = new MatTableDataSource(this.stories);
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableSource.filter = filterValue.trim().toLowerCase();
  }

  onEdit(projectId: string): void {
    this.handleEdit.emit(projectId);
  }

  onDelete(projectId: string) {
    this.handleDelete.emit(projectId);
  }
}
