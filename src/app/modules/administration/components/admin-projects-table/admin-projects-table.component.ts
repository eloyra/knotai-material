import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Project } from 'src/app/services/api';

@Component({
  selector: 'k-admin-projects-table',
  templateUrl: './admin-projects-table.component.html',
  styleUrls: ['./admin-projects-table.component.scss'],
})
export class AdminProjectsTableComponent implements OnInit {
  @Input() projects: Project[] = null;
  @Input() displayedColumns: string[] = ['name', 'start', 'state', 'priority', 'velocity', 'actions'];

  @Output() handleEdit: EventEmitter<string> = new EventEmitter<string>();
  @Output() handleDelete: EventEmitter<string> = new EventEmitter<string>();

  public tableSource: MatTableDataSource<Project>;
  public tableFilter: string = '';

  @ViewChild(MatSort) sort: MatSort;

  constructor() {}

  ngOnInit(): void {
    this.tableSource = new MatTableDataSource(this.projects);
  }

  ngAfterViewInit() {
    if (this.tableSource) {
      this.tableSource.sort = this.sort;
    }
  }

  ngOnChanges(): void {
    this.tableSource = new MatTableDataSource(this.projects);
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableSource.filter = filterValue.trim().toLowerCase();
  }

  onEdit(projectId: string): void {
    this.handleEdit.emit(projectId);
  }

  onDelete(projectId: string) {
    this.handleDelete.emit(projectId);
  }
}
