import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminProjectsComponent } from 'src/app/modules/administration/pages/admin-projects/admin-projects.component';
import { AdminSprintsComponent } from 'src/app/modules/administration/pages/admin-sprints/admin-sprints.component';
import { AdminStoriesComponent } from 'src/app/modules/administration/pages/admin-stories/admin-stories.component';
import { AdminTasksComponent } from 'src/app/modules/administration/pages/admin-tasks/admin-tasks.component';
import { AdminUsersComponent } from 'src/app/modules/administration/pages/admin-users/admin-users.component';
import { AdminAuthGuardService } from 'src/app/services/auth/admin-auth-guard.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'projects',
    pathMatch: 'full',
  },
  {
    path: 'projects',
    component: AdminProjectsComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'sprints',
    component: AdminSprintsComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'stories',
    component: AdminStoriesComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'tasks',
    component: AdminTasksComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: 'users',
    component: AdminUsersComponent,
    canActivate: [AdminAuthGuardService],
  },
  {
    path: '**',
    component: AdminProjectsComponent,
    canActivate: [AdminAuthGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministrationRoutingModule {}
