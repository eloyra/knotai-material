import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color } from 'ng2-charts';

@Component({
  selector: 'k-new-sprint-points-chart',
  templateUrl: './new-sprint-points-chart.component.html',
  styleUrls: ['./new-sprint-points-chart.component.scss'],
})
export class NewSprintPointsChartComponent implements OnInit {
  @Input() chartData: ChartDataSets[] = [];

  public chartOptions: ChartOptions = {
    title: {
      display: true,
      text: 'Workload',
      fontSize: 24,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{ type: 'linear', stacked: true }],
      yAxes: [{ type: 'category', labels: ['Recommended', 'Selection'] }],
    },
    plugins: {
      deferred: {
        xOffset: '100%',
        yOffset: 50,
        delay: 500,
      },
    },
  };
  public chartColors: Color[] = [
    {
      borderColor: 'black',
    },
  ];
  public chartLegend = false;
  public chartType: ChartType = 'horizontalBar';
  public chartPlugins = [];

  constructor() {}

  ngOnInit(): void {}
}
