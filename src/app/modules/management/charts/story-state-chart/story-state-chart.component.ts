import { Component, Input, OnInit } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import { Label, SingleDataSet } from 'ng2-charts';
import { State, Story } from 'src/app/services/api';

@Component({
  selector: 'k-story-state-chart',
  templateUrl: './story-state-chart.component.html',
  styleUrls: ['./story-state-chart.component.scss'],
})
export class StoryStateChartComponent implements OnInit {
  @Input() allStories: Story[] = null;

  public ready = false;

  public polarAreaChartLabels: Label[] = [];
  public polarAreaChartData: SingleDataSet = [];
  public polarChartOptions: ChartOptions = {
    title: {
      display: true,
      text: 'Stories by state',
      fontSize: 24,
    },
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      deferred: {
        xOffset: '100%',
        yOffset: 50,
        delay: 2000,
      },
    },
  };

  public polarAreaChartType: ChartType = 'polarArea';

  constructor() {}

  ngOnInit() {
    if (this.allStories) {
      this.classifyStories();
      this.ready = true;
    }
  }

  private classifyStories(): void {
    Object.keys(State).forEach((key: string) => {
      this.polarAreaChartLabels.push(key);
      this.polarAreaChartData.push(this.allStories.filter((story: Story) => story.state === State[key]).length);
    });
  }
}
