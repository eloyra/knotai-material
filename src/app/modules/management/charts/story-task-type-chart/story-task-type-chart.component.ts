import { Component, Input, OnInit } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import { Label, SingleDataSet } from 'ng2-charts';
import { Task, TaskType } from 'src/app/services/api';

@Component({
  selector: 'k-story-task-type-chart',
  templateUrl: './story-task-type-chart.component.html',
  styleUrls: ['./story-task-type-chart.component.scss'],
})
export class StoryTaskTypeChartComponent implements OnInit {
  @Input() allTasks: Task[] = null;

  public ready = false;

  public polarAreaChartLabels: Label[] = [];
  public polarAreaChartData: SingleDataSet = [];
  public polarChartOptions: ChartOptions = {
    title: {
      display: true,
      text: 'Tasks by type',
      fontSize: 24,
    },
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      deferred: {
        xOffset: '100%',
        yOffset: 50,
        delay: 2000,
      },
    },
  };

  public polarAreaChartType: ChartType = 'polarArea';

  constructor() {}

  ngOnInit() {
    if (this.allTasks) {
      this.classifyStories();
      this.ready = true;
    }
  }

  private classifyStories(): void {
    Object.keys(TaskType).forEach((key: string) => {
      this.polarAreaChartLabels.push(key);
      this.polarAreaChartData.push(this.allTasks.filter((task: Task) => task.taskType === TaskType[key]).length);
    });
  }
}
