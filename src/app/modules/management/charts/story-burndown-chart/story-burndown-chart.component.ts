import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color } from 'ng2-charts';
import { Project, Sprint } from 'src/app/services/api';
import { BurndownService } from 'src/app/services/statistics/burndown.service';

@Component({
  selector: 'k-story-burndown-chart',
  templateUrl: './story-burndown-chart.component.html',
  styleUrls: ['./story-burndown-chart.component.scss'],
})
export class StoryBurndownChartComponent implements OnInit {
  @Input() project: Project = null;
  @Input() sprint: Sprint = null;
  @Input() scope: 'project' | 'sprint' = 'project';

  public ready = false;

  public lineChartData: ChartDataSets[] = [];
  public lineChartOptions: ChartOptions = {
    title: {
      display: true,
      text: ' burndown chart',
      fontSize: 24,
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{ type: 'time', time: { minUnit: 'day' }, ticks: { autoSkip: false } }],
      yAxes: [{ type: 'linear', ticks: { autoSkip: false } }],
    },
    plugins: {
      deferred: {
        xOffset: '100%',
        yOffset: 50,
        delay: 500,
      },
    },
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];

  constructor() {}

  ngOnInit() {
    this.lineChartOptions.title.text =
      (this.scope === 'project' ? 'Project' : 'Sprint') + this.lineChartOptions.title.text;
    this.buildData();
    this.ready = true;
  }

  private buildData(): void {
    if (this.scope === 'project') {
      const { ideal, real, activeSprint } = BurndownService.forProject({ project: this.project }) ?? {
        ideal: null,
        real: null,
        activeSprint: null,
      };

      this.lineChartData.push(
        {
          label: 'Ideal',
          data: ideal,
          pointRadius: 5,
          pointHitRadius: 5,
        },
        {
          label: 'Real',
          data: real,
        },
        {
          label: 'Active sprint',
          data: activeSprint,
        }
      );
    }

    if (this.scope === 'sprint') {
      const { ideal, real } = BurndownService.forSprint({ sprint: this.sprint });

      this.lineChartData.push(
        {
          label: 'Ideal',
          data: ideal,
        },
        {
          label: 'Real',
          data: real,
        }
      );
    }
  }

  /*private buildData(): void {
    const startDate = new Date(this.startDate);
    const deadline = new Date(this.deadline);

    const idealBurndown: ChartDataSets = {
      label: 'Ideal',
      data: [],
    };
    const totalBurndown: ChartDataSets = {
      label: 'Total',
      data: [],
    };
    const currentBurndown: ChartDataSets = {
      label: 'Current sprint',
      data: [],
    };

    this.allStories = this.allStories.sort((a: Story, b: Story) => {
      return a.createdAt - b.createdAt;
    });

    /!**
     * TODO Ahora mismo se esta dando igual tiempo a todas las historias para el burndown ideal - hay que tener en cuenta story points
     *!/
    const availableTimePerStory = (deadline.getTime() - startDate.getTime()) / this.allStories.length;
    let completedStories = 0;
    let idealAccumulatedTime = startDate.getTime();
    let realAccumulatedTime = startDate.getTime();
    idealBurndown.data.push({ t: idealAccumulatedTime, y: this.allStories.length } as any);
    totalBurndown.data.push({ t: realAccumulatedTime, y: this.allStories.length } as any);
    this.allStories.forEach((story: Story, index: number) => {
      idealAccumulatedTime += availableTimePerStory;
      idealBurndown.data.push({ t: idealAccumulatedTime, y: this.allStories.length - (index + 1) } as any);
      if (story.state === State.Finished) {
        completedStories++;
        totalBurndown.data.push({
          t: new Date(story.finishedDate),
          y: this.allStories.length - completedStories,
        } as any);
        console.warn(this.currentSprint, story.sprint);
        if (this.currentSprint && story.sprint === this.currentSprint) {
          currentBurndown.data.push({
            t: new Date(story.finishedDate),
            y: this.allStories.length - completedStories,
          } as any);
        }
      }
    });

    this.lineChartData.push(totalBurndown, idealBurndown);
    if (this.scope === 'project' && this.currentSprint) {
      this.lineChartData.push(currentBurndown);
    }
  }*/
}
