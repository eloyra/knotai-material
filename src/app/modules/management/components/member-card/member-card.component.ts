import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Project, Story, TimeEntry, UserProfile } from 'src/app/services/api';

@Component({
  selector: 'k-member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MemberCardComponent implements OnInit {
  @Input() project: Project;
  @Input() member: UserProfile;
  public memberStories: Story[] = [];
  public memberTimeEntries: TimeEntry[] = [];
  public totalHours: number;
  public lastTimeEntry: TimeEntry;
  public loading = false;

  constructor() {}

  ngOnInit(): void {
    this.loading = true;
    if (this.project?.storiesByProject?.totalCount) {
      this.memberStories = this.project.storiesByProject.nodes.filter(
        (story: Story) => story.assignee === this.member.id
      );
    }
    // TODO FIx
    /*if (this.memberStories.length && this.member?.timeEntriesByAuthor?.totalCount) {
      this.memberTimeEntries = this.member.timeEntriesByAuthor.nodes
        .filter((entry: TimeEntry) => this.memberStories.some((story: Story) => entry.story === story.id))
        .sort((a: TimeEntry, b: TimeEntry) => {
          const timeDifference = new Date(a.time).getTime() - new Date(b.time).getTime();
          if (timeDifference === 0) {
            return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
          }
          return timeDifference;
        });
      if (this.memberTimeEntries.length) {
        this.lastTimeEntry = this.memberTimeEntries[this.memberTimeEntries.length - 1];
        this.totalHours = this.memberTimeEntries.reduce(
          (accumulator: number, currentValue: TimeEntry) => accumulator + +currentValue.hours,
          0
        );
      }
    }*/
    this.loading = false;
    console.warn(this.member, this.memberStories, this.memberTimeEntries, this.lastTimeEntry);
  }
}
