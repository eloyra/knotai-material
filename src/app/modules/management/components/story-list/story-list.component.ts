import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, Input, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { DeleteStoryGQL, Sprint, Story, UpdateStoryGQL } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProgressService } from 'src/app/services/statistics/progress.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { StoryFormComponent } from 'src/app/shared/forms/story-form/story-form.component';
import { TaskFormComponent } from 'src/app/shared/forms/task-form/task-form.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'k-story-list',
  templateUrl: './story-list.component.html',
  styleUrls: ['./story-list.component.scss'],
})
export class StoryListComponent {
  @ViewChild(ContextMenuComponent, { static: true }) public contextualMenu: ContextMenuComponent;
  @Input() listTitle: string;
  @Input() dragNDrop = true;
  @Input() stories: Story[];
  @Input() defaultSprint: Sprint;
  @Input() isListOpen = false;
  @Input() withSprintTag = false;

  private storiesToUpdateAfterDrag: Story[] = [];

  public openedDetails: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private projectStorage: ProjectStorageService,
    private graphql: GraphqlService,
    private updateStory: UpdateStoryGQL,
    private deleteStory: DeleteStoryGQL,
    public dialog: MatDialog
  ) {}

  toggleList() {
    this.isListOpen = !this.isListOpen;
  }

  // @TODO: Tras invalidar la cache del proyecto, mover elementos provoca una recarga del proyecto ANTES de la mutacion
  onDrop(event: CdkDragDrop<string[]>) {
    if (this.dragNDrop && this.stories) {
      if (event.previousContainer === event.container) {
        moveItemInArray(this.stories, event.previousIndex, event.currentIndex);
        this.updateWeightOnDrag(this.stories, event.item.data, event.currentIndex);
      }
    }
  }

  onClickItem(event: MouseEvent) {
    const id = (event.target as any)?.id;
    this.openedDetails = this.openedDetails === id ? null : id;
  }

  getProgressAmount(story: Story): number {
    return ProgressService.forStory({ story });
  }

  goToDetails(selectedStory: Story) {
    if (this.route.snapshot.params?.project && selectedStory?.id) {
      this.router
        .navigate(['management', this.route.snapshot.params.project, 'stories', selectedStory.id], {
          state: { redirect: this.router.url },
        })
        .catch(console.error);
    }
  }

  handleAddTask(selectedStory: Story) {
    this.dialog
      .open(TaskFormComponent, {
        width: '60%',
        height: 'auto',
        data: { story: this.stories.find((story: Story) => story.id === selectedStory.id) },
      })
      .afterClosed()
      .toPromise()
      .catch(console.error);
  }

  handleEdit(selectedStory: Story) {
    this.dialog
      .open(StoryFormComponent, {
        width: '60%',
        height: 'auto',
        data: {
          story: this.stories.find((story: Story) => story.id === selectedStory.id),
          defaultSprint: this.defaultSprint,
        },
      })
      .afterClosed();
  }

  handleDelete(selectedStory: Story) {
    this.dialog
      .open(ConfirmationDialogComponent, {
        width: '40%',
        height: 'auto',
        data: {
          title: `Deleting story "${this.stories.find((story: Story) => story.id === selectedStory.id).name}"`,
          text: 'Are you sure you want to delete this story? This action cannot be undone. ',
        },
      })
      .afterClosed()
      .subscribe((actionTaken: boolean) => {
        if (actionTaken) {
          this.graphql
            .mutate(this.deleteStory, { id: selectedStory.id })
            .toPromise()
            .then(() => {
              this.stories.splice(
                this.stories.findIndex((story: Story) => story.id === selectedStory.id),
                1
              );
              this.projectStorage.invalidate();
            })
            .catch(console.error);
        }
      });
  }

  private updateWeightOnDrag(list: Story[], item: Story, newIndex: number) {
    const newWeight = this.calcWeight(list, newIndex);

    if (item.weight !== newWeight) {
      item.weight = newWeight;
      this.storiesToUpdateAfterDrag.push(item);
    }

    this.propagateWeight(list, newIndex + 1, newWeight);
    this.triggerUpdates();
  }

  // noinspection JSMethodCanBeStatic
  private calcWeight(list, index) {
    return index > 0 ? list[index - 1].weight + 1 : 0;
  }

  private propagateWeight(list: Story[], index: number, weight: number) {
    if (index < list.length) {
      if (list[index].weight === weight) {
        list[index].weight = weight + 1;
        this.storiesToUpdateAfterDrag.push(list[index]);
        this.propagateWeight(list, index + 1, weight + 1);
      }
    }
  }

  private triggerUpdates() {
    const promisePool = [];
    this.storiesToUpdateAfterDrag.forEach((story: Story) => {
      promisePool.push(
        this.graphql.mutate(this.updateStory, { id: story.id, patch: { weight: story.weight } }).toPromise()
      );
    });
    Promise.all(promisePool).then(() => {
      this.projectStorage.invalidate();
      this.storiesToUpdateAfterDrag = [];
    });
  }
}
