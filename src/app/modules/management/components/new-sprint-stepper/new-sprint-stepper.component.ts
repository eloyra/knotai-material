import { CdkDrag, CdkDragDrop, CdkDropList } from '@angular/cdk/drag-drop';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';
import { ChartDataSets } from 'chart.js';
import {
  CreateSprintGQL,
  CreateStoryGQL,
  Project,
  Sprint,
  State,
  Story,
  UpdateProjectGQL,
  UpdateSprintGQL,
  UpdateStoryGQL,
} from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProgressService } from 'src/app/services/statistics/progress.service';
import { ActiveSprintStorageService } from 'src/app/services/storage/active-sprint-storage.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { StoryFormComponent } from 'src/app/shared/forms/story-form/story-form.component';
import { fakeDateHandling } from 'src/app/shared/utils';

@Component({
  selector: 'k-new-sprint-stepper',
  templateUrl: './new-sprint-stepper.component.html',
  styleUrls: ['./new-sprint-stepper.component.scss'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { showError: true },
    },
  ],
})
export class NewSprintStepperComponent implements OnInit {
  @Input() project: Project = null;

  @ViewChild('stepper') private sprintStepper: MatStepper;

  public fetching = false;

  // Initialization
  private previousSprint: Sprint = null;
  private newVelocity: number = null;

  // Step One Properties - New Sprint Data
  public stepOneCompleted = true;
  public stepOneHasError = false;
  private newSprint: Sprint = null;
  public recommendedStoryPoints: number = null;

  //Step Two Properties - Story Selection
  public stepTwoCompleted = true;
  public stepTwoHasError = false;

  public backlog: Story[] = [];
  public selectedStories: Story[] = [];
  public unfinishedStories: Story[] = [];
  public storiesForNewSprint: Story[] = [];

  public includeUnfinishedStories = false;
  public allowToggleUnfinishedStories = true;

  private selectedStoryPoints = {
    ok: 0,
    warn: 0,
    danger: 0,
  };
  public chartData: ChartDataSets[] = [];

  // Step Three Properties - Sprint Confirmation
  public stepThreeCompleted = true;
  public stepThreeHasError = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private activeSprintStorage: ActiveSprintStorageService,
    private projectStorage: ProjectStorageService,
    private graphql: GraphqlService,
    private createSprint: CreateSprintGQL,
    private updateSprint: UpdateSprintGQL,
    private createStory: CreateStoryGQL,
    private updateStory: UpdateStoryGQL,
    private updateProject: UpdateProjectGQL,
    private dialog: MatDialog
  ) {}

  // INITIALIZATION
  ngOnInit(): void {
    this.previousSprint =
      this.activeSprintStorage.getSprintSnapshot() || this.activeSprintStorage.findActiveSprintInProject(this.project);

    this.calculateNewVelocity();

    if (this.project?.storiesByProject?.totalCount) {
      this.backlog = this.project.storiesByProject.nodes
        .filter((story: Story) => !story.sprint)
        .filter((story: Story) => story.state !== State.Finished);

      this.unfinishedStories = this.project.storiesByProject.nodes
        .filter((story: Story) => story.sprint)
        .filter((story: Story) => story.state !== State.Finished);
    }
  }

  stepperGoBack(): void {
    this.sprintStepper.previous();
  }

  stepperGoNext(): void {
    this.sprintStepper.next();
  }

  // FIRST STEP
  onSprintCreated(model: Sprint) {
    if (model) {
      this.newSprint = {
        ...model,
        active: true,
        activationTime: fakeDateHandling(new Date()),
        closed: null,
        duration: this.project?.sprintDuration || 2,
      };

      if (this.newVelocity) {
        this.recommendedStoryPoints = Math.round((this.newSprint.duration * 5 * 8) / this.newVelocity);
        if (this.chartData.length < 1) {
          this.chartData.push({
            label: 'Ideal workload',
            stack: 'ideal',
            data: [this.recommendedStoryPoints, 0],
            backgroundColor: 'rgb(54, 162, 235)',
            hoverBackgroundColor: 'rgba(54, 162, 235, 0.35)',
          });
        }
      }
      this.stepperGoNext();
    } else {
      this.stepOneCompleted = false;
      this.stepOneHasError = true;
    }
  }

  // SECOND STEP
  openNewStoryDialog() {
    const ref = this.dialog.open(StoryFormComponent, {
      width: '60%',
      height: 'auto',
    });
    ref.componentInstance.hideSprint = true;
    ref
      .afterClosed()
      .toPromise()
      .then((newStory) => {
        if (newStory) {
          this.backlog.push(newStory);
          this.projectStorage.invalidate();
        }
      })
      .catch(console.error);
  }

  checkEntry(drag: CdkDrag, drop: CdkDropList): boolean {
    return drag.dropContainer.id === 'backlog' && drop.id === 'dropzone';
  }

  onDrop(event: CdkDragDrop<string[]>) {
    if (event.item.data) {
      const movedStory = event.item.data as Story;
      this.selectedStories.push({ ...movedStory });
      this.storiesForNewSprint.push({ ...movedStory });
      this.backlog.splice(
        this.backlog.findIndex((story: Story) => story.id === movedStory.id),
        1
      );
      this.updateStoryPointCount();
    }
  }

  toggleUnfinishedStories(): void {
    if (this.includeUnfinishedStories) {
      this.includeUnfinishedStories = false;
      this.removeUnfinishedStories();
    } else {
      this.includeUnfinishedStories = true;
      this.addUnfinishedStories();
    }
  }

  processStorySelection(): void {
    console.warn(this.storiesForNewSprint);
    if (this.storiesForNewSprint?.length) {
      this.stepperGoNext();
    } else {
      this.stepTwoCompleted = false;
      this.stepTwoHasError = true;
    }
  }

  // THIRD STEP
  confirmNewSprint(): void {
    this.fetching = true;

    // I try to create the new sprint
    this.graphql
      .mutate(this.createSprint, {
        item: {
          ...this.newSprint,
          project: this.project.id,
        },
      })
      .subscribe(
        (newSprint: Sprint) => {
          if (newSprint) {
            this.newSprint = newSprint;
            // I try to update the selected stories so that the new sprint is their sprint
            Promise.all(
              this.storiesForNewSprint.map((story: Story) =>
                this.graphql
                  .mutate(this.updateStory, {
                    id: story.id,
                    patch: { sprint: newSprint.id },
                  })
                  .toPromise()
              )
            )
              .then((response: Story[]) => {
                if (response?.length) {
                  if (this.previousSprint) {
                    // I set the previous sprint as inactive and closed
                    this.graphql
                      .mutate(this.updateSprint, {
                        id: this.previousSprint.id,
                        patch: { active: false, closed: fakeDateHandling(new Date()) },
                      })
                      .toPromise()
                      .then(() => {
                        this.graphql
                          .mutate(this.updateProject, {
                            id: this.project.id,
                            patch: { config: { ...this.project.config, velocity: this.newVelocity } },
                          })
                          .toPromise()
                          .then(() => {
                            this.successfulCreationAftermath();
                          });
                      })
                      .catch((error) => {
                        console.error(error);
                        this.stepThreeHasError = true;
                        this.fetching = false;
                      });
                  } else {
                    this.successfulCreationAftermath();
                  }
                } else {
                  this.stepThreeHasError = true;
                  this.fetching = false;
                }
              })
              .catch((reason) => {
                console.error(reason);
                this.stepThreeHasError = true;
                this.fetching = false;
              });
          } else {
            this.stepThreeHasError = true;
            this.fetching = false;
          }
        },
        (error) => {
          console.error(error);
          this.stepThreeHasError = true;
          this.fetching = false;
        }
      );
  }

  // AUX FUNCTIONS
  private calculateNewVelocity(): void {
    if (this.previousSprint && this.project?.config?.velocity) {
      const previousSprintVelocity = ProgressService.calculateSprintVelocity(this.previousSprint);
      this.newVelocity = (this.project.config.velocity + previousSprintVelocity) / 2;
    } else if (this.project?.config?.velocity) {
      this.newVelocity = this.project.config.velocity;
    } else {
      this.newVelocity = 4;
    }
  }

  private updateStoryPointCount(): void {
    if (this.recommendedStoryPoints) {
      const points = this.storiesForNewSprint.reduce(
        (points: number, currentStory: Story) => points + +currentStory.points,
        0
      );

      this.selectedStoryPoints.ok = Math.min(points, this.recommendedStoryPoints);
      if (points > this.recommendedStoryPoints) {
        this.selectedStoryPoints.warn = Math.min(
          points - this.recommendedStoryPoints,
          Math.round(this.recommendedStoryPoints * 0.25)
        );
      } else {
        this.selectedStoryPoints.warn = 0;
      }
      if (points - this.recommendedStoryPoints > this.recommendedStoryPoints * 0.25) {
        this.selectedStoryPoints.danger = Math.max(
          0,
          points - this.recommendedStoryPoints - Math.round(this.recommendedStoryPoints * 0.25)
        );
      } else {
        this.selectedStoryPoints.danger = 0;
      }

      if (this.chartData.length < 2) {
        this.chartData.push({
          label: 'Good workload',
          stack: 'real',
          data: [0, this.selectedStoryPoints.ok],
          backgroundColor: 'rgb(144, 238, 144)',
          hoverBackgroundColor: 'rgba(144, 238, 144, 0.35)',
        });
      } else {
        this.chartData[1].data = [0, this.selectedStoryPoints.ok];
      }

      if (this.chartData.length < 3) {
        this.chartData.push({
          label: 'Now you are pushing it',
          stack: 'real',
          data: [0, this.selectedStoryPoints.warn],
          backgroundColor: 'rgb(255, 206, 86)',
          hoverBackgroundColor: 'rgba(255, 206, 86, 0.35)',
        });
      } else {
        this.chartData[2].data = [0, this.selectedStoryPoints.warn];
      }

      if (this.chartData.length < 4) {
        this.chartData.push({
          label: `Don't chew more than you can swallow!`,
          stack: 'real',
          data: [0, this.selectedStoryPoints.danger],
          backgroundColor: 'rgb(220, 20, 60)',
          hoverBackgroundColor: 'rgba(220, 20, 60, 0.35)',
        });
      } else {
        this.chartData[3].data = [0, this.selectedStoryPoints.danger];
      }
    }
  }

  private addUnfinishedStories(): void {
    this.storiesForNewSprint = [...this.selectedStories, ...this.unfinishedStories];
    this.updateStoryPointCount();
  }

  private removeUnfinishedStories(): void {
    this.storiesForNewSprint = [...this.selectedStories];
    this.updateStoryPointCount();
  }

  private successfulCreationAftermath(): void {
    this.projectStorage.invalidate();
    this.projectStorage.loadFromRoute(this.route);
    this.stepThreeCompleted = true;
    this.fetching = false;
    this.router.navigate(['management', this.project.id, 'sprints', 'active']).catch(console.error);
  }
}
