import { CdkDrag, CdkDragDrop, CdkDropList, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { ActivatedRoute, Router } from '@angular/router';
import { ContextMenuComponent } from 'ngx-contextmenu';
import {
  DeleteStoryGQL,
  DeleteTaskGQL,
  Project,
  Sprint,
  State,
  Story,
  Task,
  UpdateStoryGQL,
  UpdateTaskGQL,
} from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { StoryFormComponent } from 'src/app/shared/forms/story-form/story-form.component';
import { TaskFormComponent } from 'src/app/shared/forms/task-form/task-form.component';
import { TimeEntryFormComponent } from 'src/app/shared/forms/time-entry-form/time-entry-form.component';
import { fakeDateHandling } from 'src/app/shared/utils';

@Component({
  selector: 'k-active-board',
  templateUrl: './active-board.component.html',
  styleUrls: ['./active-board.component.scss'],
})
export class ActiveBoardComponent implements OnInit {
  @ViewChild(ContextMenuComponent, { static: true }) public contextualMenu: ContextMenuComponent;
  @Input() project: Project = null;
  @Input() sprint: Sprint = null;
  @Input() stories: {
    [State.New]: Story[];
    [State.Ongoing]: Story[];
    [State.Blocked]: Story[];
    [State.Finished]: Story[];
  } = null;
  @Input() tasks: {
    [State.New]: Task[];
    [State.Ongoing]: Task[];
    [State.Blocked]: Task[];
    [State.Finished]: Task[];
  } = null;

  private storiesToUpdateAfterDrag = [];
  private tasksToUpdateAfterDrag = [];

  public showTasks: boolean = false;
  public allStories: Story[] = [];
  public tasksByStory: { storyId: string; tasks: Task[] } = null;
  public secondLaneSelectedState = State.Ongoing;
  public selectStoryFormControl: FormControl = new FormControl();
  public selectedStory: string = null;

  constructor(
    private projectStorage: ProjectStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private graphql: GraphqlService,
    private updateStory: UpdateStoryGQL,
    private deleteStory: DeleteStoryGQL,
    private updateTask: UpdateTaskGQL,
    private deleteTask: DeleteTaskGQL,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    if (this.stories) {
      this.secondLaneSelectedState = this.stories[State.Blocked]?.length ? State.Blocked : State.Ongoing;
      Object.values(this.stories).forEach((collection: Story[]) => {
        this.allStories.push(...collection);
      });
    }
  }

  ngOnChanges() {
    if (this.stories) {
      this.allStories = [];
      Object.values(this.stories).forEach((collection: Story[]) => {
        this.allStories.push(...collection);
      });
    }
  }

  private static calcWeight(list, index) {
    return index > 0 ? list[index - 1].weight + 1 : 0;
  }

  checkEntry(drag: CdkDrag, drop: CdkDropList): boolean {
    return drag?.data?.state !== drop.element.nativeElement.attributes.getNamedItem('data-state').value;
  }

  onDrop(event: CdkDragDrop<string[]>) {
    if (!this.showTasks) {
      if (event.previousContainer === event.container) {
        moveItemInArray(
          this.stories[event.container.element.nativeElement.attributes.getNamedItem('data-state').value],
          event.previousIndex,
          event.currentIndex
        );
        this.updateWeightOnDrag(
          this.stories[event.container.element.nativeElement.attributes.getNamedItem('data-state').value],
          event.item.data,
          event.currentIndex
        );
      } else {
        /*const targetState =
          event.container.element.nativeElement.attributes.getNamedItem('data-state').value === State.Blocked
            ? State.Ongoing
            : event.container.element.nativeElement.attributes.getNamedItem('data-state').value;*/
        transferArrayItem(
          this.stories[event.previousContainer.element.nativeElement.attributes.getNamedItem('data-state').value],
          this.stories[event.container.element.nativeElement.attributes.getNamedItem('data-state').value],
          event.previousIndex,
          event.currentIndex
        );
        this.updateStateOnDrag(
          event.item.data,
          event.container.element.nativeElement.attributes.getNamedItem('data-state').value as State
        );
      }
    } else if (this.showTasks) {
      if (event.previousContainer === event.container) {
        moveItemInArray(
          this.tasks[event.container.element.nativeElement.attributes.getNamedItem('data-state').value],
          event.previousIndex,
          event.currentIndex
        );
        this.updateWeightOnDragTask(
          this.tasks[event.container.element.nativeElement.attributes.getNamedItem('data-state').value],
          event.item.data,
          event.currentIndex
        );
      } else {
        /*const targetState =
          event.container.element.nativeElement.attributes.getNamedItem('data-state').value === State.Blocked
            ? State.Ongoing
            : event.container.element.nativeElement.attributes.getNamedItem('data-state').value;*/
        transferArrayItem(
          this.tasks[event.previousContainer.element.nativeElement.attributes.getNamedItem('data-state').value],
          this.tasks[event.container.element.nativeElement.attributes.getNamedItem('data-state').value],
          event.previousIndex,
          event.currentIndex
        );
        this.updateStateOnDragTask(
          event.item.data,
          event.container.element.nativeElement.attributes.getNamedItem('data-state').value as State
        );
      }
    }
  }

  goToDetails({ story, task }: { story: Story; task?: Task }) {
    if (this.project?.id && story?.id) {
      this.router
        .navigate(['management', this.project.id, 'stories', story.id], {
          state: { redirect: this.router.url },
        })
        .catch(console.error);
    } else if (this.project?.id && task?.id) {
      this.router
        .navigate(['management', this.project.id, 'stories', task.story, 'tasks', task.id], {
          state: { redirect: this.router.url },
        })
        .catch(console.error);
    }
  }

  handleAddEntry({ story, task }: { story: Story; task?: Task }) {
    if (task) {
      this.dialog
        .open(TimeEntryFormComponent, {
          width: '60%',
          height: 'auto',
          data: { task },
        })
        .afterClosed()
        .subscribe(console.log, console.error);
    }
  }

  handleEdit({ story, task }: { story: Story; task?: Task }) {
    if (task) {
      this.dialog
        .open(TaskFormComponent, {
          width: '60%',
          height: 'auto',
          data: {
            story: this.allStories.find((story: Story) => story.id === task.story),
            task,
            defaultSprint: this.sprint,
          },
        })
        .afterClosed()
        .subscribe((value) => {
          console.log(value);
        });
    } else if (story) {
      this.dialog
        .open(StoryFormComponent, {
          width: '60%',
          height: 'auto',
          data: { story, defaultSprint: this.sprint },
        })
        .afterClosed()
        .subscribe((value) => {
          console.log(value);
        });
    }
  }

  handleDelete({ story, task }: { story: Story; task?: Task }) {
    if (task) {
      this.dialog
        .open(ConfirmationDialogComponent, {
          width: '40%',
          height: 'auto',
          data: {
            title: `Deleting task "${task.name}"`,
            text: 'Are you sure you want to delete this task? This action cannot be undone. ',
          },
        })
        .afterClosed()
        .subscribe((actionTaken: boolean) => {
          if (actionTaken) {
            this.graphql
              .mutate(this.deleteTask, { id: task.id })
              .toPromise()
              .then(() => {
                this.tasks[task.state].splice(
                  this.tasks[task.state].findIndex((insideTask: Task) => task.id === insideTask.id),
                  1
                );
                this.projectStorage.invalidate();
              })
              .catch(console.error);
          }
        });
    } else if (story) {
      this.dialog
        .open(ConfirmationDialogComponent, {
          width: '40%',
          height: 'auto',
          data: {
            title: `Deleting story "${story.name}"`,
            text: 'Are you sure you want to delete this story? This action cannot be undone. ',
          },
        })
        .afterClosed()
        .subscribe((actionTaken: boolean) => {
          if (actionTaken) {
            this.graphql
              .mutate(this.deleteStory, { id: story.id })
              .toPromise()
              .then(() => {
                this.stories[story.state].splice(
                  this.stories[story.state].findIndex((insideStory: Story) => story.id === insideStory.id),
                  1
                );
                const index = this.allStories.findIndex((insideStory: Story) => story.id === insideStory.id);
                if (index) {
                  this.allStories.splice(index, 1);
                }
                this.projectStorage.invalidate();
              })
              .catch(console.error);
          }
        });
    }
  }

  handleSlider(event: MatSlideToggleChange) {
    this.showTasks = event.checked;
  }

  /*handleStorySelection(event: MatAutocompleteSelectedEvent) {
    this.selectedStory = event.option.id;
    if (this.selectedStory) {
      this.selectStoryFormControl.setValue(event.option.value);
    }
  }*/

  /*getTasksFromSelectedStoryByState(state: State): Task[] {
    const tasks = [];
    if (this.stories?.[state]?.length) {
      this.stories[state].forEach((story: Story) => {
        if (story?.tasksByStory?.totalCount) {
          tasks.push(...story.tasksByStory.nodes);
        }
      });
    }
    return tasks;
  }*/

  private updateWeightOnDrag(list: Story[], item: Story, newIndex: number) {
    const newWeight = ActiveBoardComponent.calcWeight(list, newIndex);

    if (item.weight !== newWeight) {
      item.weight = newWeight;
      this.storiesToUpdateAfterDrag.push(item);
    }

    this.propagateWeight(list, newIndex + 1, newWeight);
    this.triggerUpdates();
  }

  private updateWeightOnDragTask(list: Task[], item: Task, newIndex: number) {
    const newWeight = ActiveBoardComponent.calcWeight(list, newIndex);

    if (item.weight !== newWeight) {
      item.weight = newWeight;
      this.tasksToUpdateAfterDrag.push(item);
    }

    this.propagateWeightTask(list, newIndex + 1, newWeight);
    this.triggerUpdates();
  }

  private propagateWeight(list: Story[], index: number, weight: number) {
    if (index < list.length) {
      if (list[index].weight === weight) {
        list[index].weight = weight + 1;
        this.storiesToUpdateAfterDrag.push(list[index]);
        this.propagateWeight(list, index + 1, weight + 1);
      }
    }
  }

  private propagateWeightTask(list: Task[], index: number, weight: number) {
    if (index < list.length) {
      if (list[index].weight === weight) {
        list[index].weight = weight + 1;
        this.tasksToUpdateAfterDrag.push(list[index]);
        this.propagateWeightTask(list, index + 1, weight + 1);
      }
    }
  }

  private updateStateOnDrag(item: Story, newState: State) {
    item.state = newState;
    this.storiesToUpdateAfterDrag.push(item);
    this.triggerUpdates();
  }

  private updateStateOnDragTask(item: Task, newState: State) {
    item.state = newState;
    this.tasksToUpdateAfterDrag.push(item);
    this.triggerUpdates();
  }

  private triggerUpdates() {
    const promisePool = [];
    this.storiesToUpdateAfterDrag.forEach((story: Story) => {
      promisePool.push(
        this.graphql
          .mutate(this.updateStory, {
            id: story.id,
            patch: {
              weight: story.weight,
              state: story.state,
              finishedDate: story.state === State.Finished ? fakeDateHandling(new Date()) : null,
            },
          })
          .toPromise()
      );
    });
    this.tasksToUpdateAfterDrag.forEach((task: Task) => {
      promisePool.push(
        this.graphql
          .mutate(this.updateTask, { id: task.id, patch: { weight: task.weight, state: task.state } })
          .toPromise()
      );
    });
    Promise.all(promisePool).then(() => {
      this.projectStorage.invalidate();
      this.projectStorage.loadFromRoute(this.route);
      this.storiesToUpdateAfterDrag = [];
      this.tasksToUpdateAfterDrag = [];
    });
  }

  public getStoryByTask(task: Task): Story {
    return this.allStories.find((story: Story) => story.id === task.story);
  }
}
