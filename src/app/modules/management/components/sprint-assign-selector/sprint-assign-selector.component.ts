import { Component, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { Sprint, Story, UpdateStoryGQL } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';

@Component({
  selector: 'k-sprint-assign-selector',
  templateUrl: './sprint-assign-selector.component.html',
  styleUrls: ['./sprint-assign-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintAssignSelectorComponent {
  public form = new FormGroup({});
  public model: any = {};
  public fields: FormlyFieldConfig[] = [];
  public loading = false;

  constructor(
    private graphql: GraphqlService,
    private updateStory: UpdateStoryGQL,
    private dialogRef: MatDialogRef<SprintAssignSelectorComponent>,
    @Inject(MAT_DIALOG_DATA) private data: { storyId: string; sprints: Sprint[] }
  ) {
    this.fields = [
      {
        key: 'sprint',
        type: 'select',
        className: 'field',
        templateOptions: {
          label: 'Sprint',
          options: [
            { value: null, label: '-' },
            ...data.sprints.map((sprint) => ({ value: sprint.id, label: sprint.name })),
          ],
          appearance: 'outline',
          color: 'accent',
        },
      },
    ];
  }

  onSubmit(model) {
    this.dialogRef.close({ id: model.sprint });
    this.loading = true;
    this.graphql
      .mutate(this.updateStory, {
        id: this.data.storyId,
        patch: model,
      })
      .subscribe(
        (newStory: Story) => {
          this.loading = false;
          this.dialogRef.close({
            ...newStory,
            sprintBySprint: {
              id: model.sprint,
              name: this.data.sprints.find((sprint: Sprint) => sprint.id === model.sprint).name,
            },
          });
        },
        (reason) => {
          console.error(reason);
          this.loading = false;
          this.dialogRef.close(false);
        }
      );
  }
}
