import { Component, Input } from '@angular/core';
import { Story, Task } from 'src/app/services/api';

@Component({
  selector: 'k-task-card',
  templateUrl: './task-card.component.html',
  styleUrls: ['./task-card.component.scss'],
})
export class TaskCardComponent {
  @Input() story: Story = null;
  @Input() task: Task = null;

  constructor() {}
}
