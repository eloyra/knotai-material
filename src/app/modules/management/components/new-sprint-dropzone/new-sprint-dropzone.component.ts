import { CdkDrag, CdkDragDrop, CdkDropList, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { CreateSprintGQL, Project, Sprint, Story, UpdateStoryGQL } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';

@Component({
  selector: 'k-new-sprint-dropzone',
  templateUrl: './new-sprint-dropzone.component.html',
  styleUrls: ['./new-sprint-dropzone.component.scss'],
})
export class NewSprintDropzoneComponent implements OnInit {
  @Input() project: Project;
  @Input() freeStories: Story[];

  public fetching = false;
  public form = new FormGroup({});
  public model: any = {};
  public fields: FormlyFieldConfig[] = [];
  public selectedStories: Story[] = [];

  constructor(
    private projectStorage: ProjectStorageService,
    private graphql: GraphqlService,
    private createSprint: CreateSprintGQL,
    private updateStory: UpdateStoryGQL
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  onSubmit(model) {
    this.create(model).then((result) => {});
  }

  checkEntry(drag: CdkDrag, drop: CdkDropList): boolean {
    return drag.dropContainer.id === 'backlog' && drop.id === 'devStories';
  }

  onDrop(event: CdkDragDrop<string[]>) {
    /*const movedStory = event.item.data as Story;
      this.graphql
        .mutate(this.updateStory, {
          id: movedStory.id,
          patch: { sprint: this.currentSprint.id },
        })
        .subscribe(
          (newStory: Story) => {
            this.freeStories.splice(
              this.freeStories.findIndex((story: Story) => story.id === movedStory.id),
              1
            );
            this.currentSprintStories.push({
              ...movedStory,
              sprint: newStory.sprint,
              sprintBySprint: newStory.sprintBySprint,
            });
            this.projectStorage.invalidate();
          },
          (reason) => {
            console.error(reason);
          }
        );*/
  }

  private buildForm() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        className: 'field field-small',
        templateOptions: {
          type: 'text',
          label: 'Sprint name',
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'duration',
        type: 'input',
        className: 'field field-small',
        templateOptions: {
          type: 'number',
          label: 'Duration (in weeks)',
          appearance: 'outline',
          color: 'accent',
          min: 0,
        },
      },
      {
        key: 'status',
        type: 'select',
        className: 'field field-small',
        templateOptions: {
          multiple: false,
          label: 'Status',
          options: [
            {
              value: 'active',
              label: 'Active',
            },
            {
              value: 'closed',
              label: 'Closed',
            },
            { value: null, label: 'In backlog' },
          ],
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'description',
        type: 'textarea',
        className: 'field',
        templateOptions: {
          label: 'Description',
          multiline: true,
          rows: 5,
          appearance: 'outline',
          color: 'accent',
        },
      },
    ];
  }

  private create(model: Sprint): Promise<boolean> {
    this.fetching = true;
    const selectedStories = (model as any)?.stories;
    const active = (model as any)?.status === 'active';
    const activationTime = active ? new Date() : null;
    const closed = (model as any)?.status === 'closed' ? new Date() : null;
    delete (model as any).stories;
    delete (model as any).status;
    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.createSprint, { item: { ...model, active, activationTime, closed, project: this.project.id } })
        .subscribe(
          (newSprint: Sprint) => {
            if (!selectedStories?.length) {
              resolve(this.resolveMutation(newSprint));
            } else {
              this.processStories(selectedStories, newSprint.id)
                .then((storiesInSprint: Story[]) => {
                  resolve(this.resolveMutation(newSprint, storiesInSprint));
                })
                .catch((error) => {
                  console.error(error);
                  this.fetching = false;
                  reject(false);
                });
            }
          },
          (error) => {
            console.error(error);
            this.fetching = false;
            reject(false);
          }
        );
    });
  }

  private processStories(selectedStories: string[], newSprintId: string): Promise<Story[]> {
    let storiesToAddSprint: string[] = [...selectedStories];
    const promisesToWait = storiesToAddSprint.map((story: string) =>
      this.graphql.mutate(this.updateStory, { id: story, patch: { sprint: newSprintId } }).toPromise()
    );
    return new Promise((resolve, reject) => {
      Promise.all(promisesToWait)
        .then((response: Story[]) => {
          resolve(response);
        })
        .catch((reason) => {
          console.error(reason);
          reject(reason);
        });
    });
  }

  private resolveMutation(newSprint: Sprint, storiesInSprint: Story[] = []): boolean {
    if (storiesInSprint.length) {
      if (!newSprint.storiesBySprint) (newSprint.storiesBySprint as any) = {};
      if (!newSprint.storiesBySprint.nodes) newSprint.storiesBySprint.nodes = [];
      newSprint.storiesBySprint.nodes.push(...storiesInSprint);
      newSprint.storiesBySprint.totalCount = newSprint.storiesBySprint.totalCount + storiesInSprint.length;
    }
    this.project.sprintsByProject.nodes.push(newSprint);
    this.project.sprintsByProject.totalCount += 1;
    this.projectStorage.flush();
    this.fetching = false;
    return true;
  }
}
