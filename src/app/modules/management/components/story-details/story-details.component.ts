import { Component, Input, OnInit } from '@angular/core';
import { Story } from 'src/app/services/api';
import { ProgressService } from 'src/app/services/statistics/progress.service';

@Component({
  selector: 'k-story-details',
  templateUrl: './story-details.component.html',
  styleUrls: ['./story-details.component.scss'],
})
export class StoryDetailsComponent implements OnInit {
  @Input() story: Story;
  @Input() disableDescription: boolean = false;
  @Input() variant: 'regular' | 'reduced' = 'regular';

  public mdeDescriptionContent: string = '*No description available...*';

  constructor() {}

  ngOnInit(): void {
    if (this.story?.description) {
      this.mdeDescriptionContent = this.story.description;
    }

    setTimeout(() => {
      (document
        .getElementById(`simplemde-${this.story.id}`)
        .querySelector(`.editor-toolbar .smdi-eye`) as HTMLElement).click();
    }, 500);
  }

  getProgressAmount(story: Story): number {
    return ProgressService.forStory({ story });
  }
}
