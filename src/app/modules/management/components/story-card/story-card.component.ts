import { Component, Input } from '@angular/core';
import { Story } from 'src/app/services/api';
import { ProgressService } from 'src/app/services/statistics/progress.service';

@Component({
  selector: 'k-story-card',
  templateUrl: './story-card.component.html',
  styleUrls: ['./story-card.component.scss'],
})
export class StoryCardComponent {
  @Input() story: Story = null;

  constructor() {}

  getStoryPoints() {
    return this.story.points ?? 0;
  }

  getProgressAmount(story: Story): number {
    return ProgressService.forStory({ story });
  }
}
