import { Component, Input } from '@angular/core';
import { Task, TimeEntry } from 'src/app/services/api';
import { MatDialog } from '@angular/material/dialog';
import { TimeEntryFormComponent } from 'src/app/shared/forms/time-entry-form/time-entry-form.component';

@Component({
  selector: 'k-time-entry-list',
  templateUrl: './time-entry-list.component.html',
  styleUrls: ['./time-entry-list.component.scss'],
})
export class TimeEntryListComponent {
  @Input() task: Task;
  @Input() withoutNewEntryButton: boolean = false;

  constructor(private dialog: MatDialog) {}

  openNewTimeEntryDialog() {
    this.dialog
      .open(TimeEntryFormComponent, {
        width: '60%',
        height: 'auto',
        data: { task: this.task },
      })
      .afterClosed()
      .subscribe((newEntry: TimeEntry) => {
        if (newEntry) {
          this.task.timeEntriesByTask.totalCount++;
          this.task.timeEntriesByTask.nodes.unshift(newEntry);
        }
      });
  }

  openEditTimeEntryDialog(timeEntry: TimeEntry): void {
    this.dialog.open(TimeEntryFormComponent, {
      width: '60%',
      height: 'auto',
      data: { task: this.task, timeEntry },
    });
    // todo: check why when updating we lose the author
  }

  preventEditionOnTextSelection(event: MouseEvent) {
    event.stopPropagation();
  }
}
