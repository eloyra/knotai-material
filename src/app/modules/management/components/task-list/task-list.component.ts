import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { DeleteTaskGQL, Task, TimeEntry } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { TaskFormComponent } from 'src/app/shared/forms/task-form/task-form.component';
import { TimeEntryFormComponent } from 'src/app/shared/forms/time-entry-form/time-entry-form.component';

@Component({
  selector: 'k-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
})
export class TaskListComponent implements OnInit {
  @ViewChild(ContextMenuComponent, { static: true }) public contextualMenu: ContextMenuComponent;
  @Input() listTitle: string;
  @Input() tasks: Task[];
  @Input() isListOpen = false;
  @Input() variant: 'regular' | 'compact' | 'extended' = 'regular';

  public openedDetails: string;
  public timeSpent: any = {};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private projectStorage: ProjectStorageService,
    private graphql: GraphqlService,
    private deleteTask: DeleteTaskGQL,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    if (this.tasks.length) {
      this.tasks.forEach((task: Task) => {
        this.timeSpent[task.id] = this.getTimeSpent(task);
      });
    }
  }

  toggleList() {
    this.isListOpen = !this.isListOpen;
  }

  onClickItem(event: MouseEvent) {
    if (this.variant === 'regular') {
      const id = (event.target as any)?.id;
      this.openedDetails = this.openedDetails === id ? null : id;
    }
  }

  getTimeSpent(task: Task): number {
    if (task?.timeEntriesByTask?.totalCount) {
      return task.timeEntriesByTask.nodes.reduce(
        (accumulatedTime: number, currentTimeEntry: TimeEntry) => accumulatedTime + +currentTimeEntry?.hours,
        0
      );
    }

    return 0;
  }

  goToDetails(selectedTask: Task) {
    console.warn(selectedTask);
    if (this.route.snapshot.params?.project && this.route.snapshot.params?.story && selectedTask?.id) {
      this.router
        .navigate(
          [
            'management',
            this.route.snapshot.params.project,
            'stories',
            this.route.snapshot.params.story,
            'tasks',
            selectedTask.id,
          ],
          {
            state: { redirect: this.router.url },
          }
        )
        .catch(console.error);
    } else {
      this.router
        .navigate(
          ['management', this.route.snapshot.params.project, 'stories', selectedTask.story, 'tasks', selectedTask.id],
          {
            state: { redirect: this.router.url },
          }
        )
        .catch(console.error);
    }
  }

  handleAddEntry(selectedTask: Task) {
    this.dialog
      .open(TimeEntryFormComponent, {
        width: '60%',
        height: 'auto',
        data: { task: this.tasks.find((task: Task) => task.id === selectedTask.id) },
      })
      .afterClosed()
      .toPromise()
      .then((newTimeEntry: TimeEntry) => {
        if (newTimeEntry) {
          this.timeSpent[selectedTask.id] += +newTimeEntry?.hours;
        }
      });
  }

  handleEdit(selectedTask: Task) {
    this.dialog
      .open(TaskFormComponent, {
        width: '60%',
        height: 'auto',
        data: { task: this.tasks.find((task: Task) => task.id === selectedTask.id) },
      })
      .afterClosed();
  }

  handleDelete(selectedTask: Task) {
    this.dialog
      .open(ConfirmationDialogComponent, {
        width: '40%',
        height: 'auto',
        data: {
          title: `Deleting story "${this.tasks.find((task: Task) => task.id === selectedTask.id).name}"`,
          text: 'Are you sure you want to delete this task? This action cannot be undone. ',
        },
      })
      .afterClosed()
      .subscribe((actionTaken: boolean) => {
        if (actionTaken) {
          this.graphql
            .mutate(this.deleteTask, { id: selectedTask.id })
            .toPromise()
            .then(() => {
              this.tasks.splice(
                this.tasks.findIndex((task: Task) => task.id === selectedTask.id),
                1
              );
              this.projectStorage.invalidate();
            })
            .catch(console.error);
        }
      });
  }
}
