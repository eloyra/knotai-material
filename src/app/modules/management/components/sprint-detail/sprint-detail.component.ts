import { Component, ChangeDetectionStrategy, Input, OnChanges } from '@angular/core';
import { Sprint, Story, UserProfile } from 'src/app/services/api';

@Component({
  selector: 'k-sprint-detail',
  templateUrl: './sprint-detail.component.html',
  styleUrls: ['./sprint-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintDetailComponent implements OnChanges {
  @Input() sprint: Sprint;
  @Input() disabled = false;
  public stories: Story[];
  public assignees: UserProfile[];

  constructor() {}

  ngOnChanges(): void {
    if (this.stories) {
      this.stories = [...(this.sprint as any).stories];
      this.assignees = this.stories
        .filter((story) => !!story.userProfileByAssignee)
        .map((story: Story) => story.userProfileByAssignee);
    }
  }
}
