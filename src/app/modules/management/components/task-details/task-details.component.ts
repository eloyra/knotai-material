import { Component, Input, OnInit } from '@angular/core';
import { Task, TimeEntry } from 'src/app/services/api';

type AddonType = 'TIME_ENTRIES' | 'COMMENTS';

@Component({
  selector: 'k-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.scss'],
})
export class TaskDetailsComponent implements OnInit {
  @Input() task: Task;
  @Input() disableDescription: boolean = false;
  @Input() variant: 'regular' | 'reduced' = 'regular';

  public activeAddon: AddonType = 'TIME_ENTRIES';
  public mdeDescriptionContent: string = '*No description available...*';

  constructor() {}

  ngOnInit(): void {
    if (this.task?.description) {
      this.mdeDescriptionContent = this.task.description;
    }

    setTimeout(() => {
      (document
        .getElementById(`simplemde-${this.task.id}`)
        .querySelector(`.editor-toolbar .smdi-eye`) as HTMLElement).click();
    }, 500);
  }

  getTimeSpent(): number {
    if (this.task?.timeEntriesByTask?.totalCount) {
      return this.task.timeEntriesByTask.nodes.reduce(
        (accumulatedHours: number, currentEntry: TimeEntry) => accumulatedHours + +currentEntry?.hours,
        0
      );
    }

    return 0;
  }
}
