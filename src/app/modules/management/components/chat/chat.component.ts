import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CreateCommentGQL, Task, Comment, UserProfile } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';

@Component({
  selector: 'k-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {
  @Input() task: Task = null;

  public comments: Comment[] = [];
  public newCommentContent: string = '';

  public loading = true;
  public fetching = false;

  private author: UserProfile = null;

  constructor(
    private auth: AuthenticationService,
    private projectStorage: ProjectStorageService,
    private route: ActivatedRoute,
    private graphql: GraphqlService,
    private createComment: CreateCommentGQL
  ) {}

  ngOnInit(): void {
    this.comments =
      this.task?.commentsByTask?.nodes.sort(
        (a: Comment, b: Comment) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
      ) || [];
    console.warn(this.comments);
    this.auth.getCurrentUser().subscribe(
      (author: UserProfile) => {
        this.author = author;
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.loading = false;
      }
    );
  }

  handleCommentCreation(commentInput: HTMLTextAreaElement): void {
    this.fetching = true;
    this.graphql
      .mutate(this.createComment, {
        item: { content: commentInput.value, author: this.author.id, task: this.task.id },
      })
      .subscribe(
        (newComment: Comment) => {
          commentInput.value = '';
          this.comments = [...this.comments, newComment];
          this.projectStorage.invalidate();
          this.projectStorage.loadFromRoute(this.route).catch(console.error);
          this.fetching = false;
        },
        (error) => {
          console.error(error);
          this.fetching = false;
        }
      );
  }
}
