import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserProfile } from 'src/app/services/api';

@Component({
  selector: 'k-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.scss'],
})
export class MemberListComponent implements OnInit, AfterViewInit {
  @Input() members: UserProfile[] = null;
  @Input() displayedColumns: string[] = ['picture', 'workload', 'fullName', 'role', 'actions'];

  @Output() memberDeleted: EventEmitter<string> = new EventEmitter<string>();
  @Output() workloadEdited: EventEmitter<string> = new EventEmitter<string>();

  public tableSource: MatTableDataSource<UserProfile>;
  public tableFilter: string = '';

  @ViewChild(MatSort) sort: MatSort;

  constructor() {}

  ngOnInit(): void {
    this.tableSource = new MatTableDataSource(this.members);

    this.tableSource.sortingDataAccessor = (item: UserProfile, property: string) => {
      switch (property) {
        case 'role':
          return item?.roleByRole?.name;
        default:
          return item[property];
      }
    };
  }

  ngAfterViewInit() {
    if (this.tableSource) {
      this.tableSource.sort = this.sort;
    }
  }

  ngOnChanges(): void {
    this.tableSource = new MatTableDataSource(this.members);
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.tableSource.filter = filterValue.trim().toLowerCase();
  }

  editWorkload(userId: string): void {
    this.workloadEdited.emit(userId);
  }

  handleDelete(userId: string) {
    this.memberDeleted.emit(userId);
  }
}
