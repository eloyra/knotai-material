import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActiveSprintComponent } from 'src/app/modules/management/pages/active-sprint/active-sprint.component';
import { BacklogComponent } from 'src/app/modules/management/pages/backlog/backlog.component';
import { BoardComponent } from 'src/app/modules/management/pages/board/board.component';
import { NewSprintInteractiveComponent } from 'src/app/modules/management/pages/new-sprint-interactive/new-sprint-interactive.component';
import { ProjectOverviewComponent } from 'src/app/modules/management/pages/project-overview/project-overview.component';
import { ProjectsViewComponent } from 'src/app/modules/management/pages/projects-view/projects-view.component';
import { SprintHistoryComponent } from 'src/app/modules/management/pages/sprint-history/sprint-history.component';
import { TaskDetailComponent } from 'src/app/modules/management/pages/task-detail/task-detail.component';
import { AuthGuardService } from 'src/app/services/auth/auth-guard.service';
import { MembersComponent } from './pages/members/members.component';
import { StoryDetailComponent } from './pages/story-detail/story-detail.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'projects',
        component: ProjectsViewComponent,
        canActivate: [AuthGuardService],
      },
      {
        path: ':project',
        canActivate: [AuthGuardService],
        canActivateChild: [AuthGuardService],
        children: [
          {
            path: '',
            component: ProjectOverviewComponent,
          },
          {
            path: 'backlog',
            component: BacklogComponent,
          },
          {
            path: 'board',
            component: BoardComponent,
          },
          {
            path: 'sprints/active',
            component: ActiveSprintComponent,
          },
          {
            path: 'sprints/history',
            component: SprintHistoryComponent,
          },
          {
            path: 'sprints/new',
            component: NewSprintInteractiveComponent,
          },
          {
            path: 'stories/:story/tasks/:task',
            component: TaskDetailComponent,
          },
          {
            path: 'stories/:story',
            component: StoryDetailComponent,
          },
          {
            path: 'members',
            children: [
              {
                path: '',
                component: MembersComponent,
              },
            ],
          },
        ],
      },
      {
        path: '**',
        component: ProjectsViewComponent,
        canActivate: [AuthGuardService],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagementRoutingModule {}
