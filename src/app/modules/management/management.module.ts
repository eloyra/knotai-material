import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatOptionModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { FormlyModule } from '@ngx-formly/core';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ContextMenuModule } from 'ngx-contextmenu';
import { SimplemdeModule } from 'ngx-simplemde';
import { ProjectCardComponent } from 'src/app/modules/management/components/project-card/project-card.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxObserveModule } from 'ngx-observe';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ChartsModule } from 'ng2-charts';

import { ManagementRoutingModule } from './management-routing.module';
import { ProjectsViewComponent } from 'src/app/modules/management/pages/projects-view/projects-view.component';
import { BacklogComponent } from './pages/backlog/backlog.component';
import { ProjectOverviewComponent } from './pages/project-overview/project-overview.component';
import { StoryListComponent } from './components/story-list/story-list.component';
import { SprintAssignSelectorComponent } from './components/sprint-assign-selector/sprint-assign-selector.component';
import { SprintsComponent } from './pages/sprints/sprints.component';
import { SprintDetailComponent } from './components/sprint-detail/sprint-detail.component';
import { StoryBurndownChartComponent } from './charts/story-burndown-chart/story-burndown-chart.component';
import { StoryStateChartComponent } from './charts/story-state-chart/story-state-chart.component';
import { StoryTaskTypeChartComponent } from './charts/story-task-type-chart/story-task-type-chart.component';
import { BoardComponent } from './pages/board/board.component';
import { StoryCardComponent } from './components/story-card/story-card.component';
import { MembersComponent } from './pages/members/members.component';
import { MemberCardComponent } from './components/member-card/member-card.component';
import { TimeEntryListComponent } from './components/time-entry-list/time-entry-list.component';
import { StoryDetailComponent } from './pages/story-detail/story-detail.component';
import { TimeagoModule } from 'ngx-timeago';
import { TaskDetailComponent } from './pages/task-detail/task-detail.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { ActiveSprintComponent } from './pages/active-sprint/active-sprint.component';
import { ActiveBoardComponent } from './components/active-board/active-board.component';
import { NewSprintDropzoneComponent } from './components/new-sprint-dropzone/new-sprint-dropzone.component';
import { NewSprintInteractiveComponent } from './pages/new-sprint-interactive/new-sprint-interactive.component';
import { StoryDetailsComponent } from './components/story-details/story-details.component';
import { TaskDetailsComponent } from './components/task-details/task-details.component';
import { MemberListComponent } from './components/member-list/member-list.component';
import { NewSprintStepperComponent } from './components/new-sprint-stepper/new-sprint-stepper.component';
import { NewSprintPointsChartComponent } from './charts/new-sprint-points-chart/new-sprint-points-chart.component';
import { TaskCardComponent } from './components/task-card/task-card.component';
import { SprintHistoryComponent } from './pages/sprint-history/sprint-history.component';
import { ChatComponent } from './components/chat/chat.component';

@NgModule({
  declarations: [
    ProjectsViewComponent,
    ProjectCardComponent,
    BacklogComponent,
    ProjectOverviewComponent,
    StoryListComponent,
    SprintAssignSelectorComponent,
    SprintsComponent,
    SprintDetailComponent,
    StoryBurndownChartComponent,
    StoryStateChartComponent,
    StoryTaskTypeChartComponent,
    BoardComponent,
    StoryCardComponent,
    MembersComponent,
    MemberCardComponent,
    TimeEntryListComponent,
    StoryDetailComponent,
    TaskDetailComponent,
    TaskListComponent,
    ActiveSprintComponent,
    ActiveBoardComponent,
    NewSprintDropzoneComponent,
    NewSprintInteractiveComponent,
    StoryDetailsComponent,
    TaskDetailsComponent,
    MemberListComponent,
    NewSprintStepperComponent,
    NewSprintPointsChartComponent,
    TaskCardComponent,
    SprintHistoryComponent,
    ChatComponent,
  ],
  imports: [
    CommonModule,
    ManagementRoutingModule,
    MatIconModule,
    MatOptionModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    SharedModule,
    NgxObserveModule,
    MatProgressSpinnerModule,
    DragDropModule,
    MatListModule,
    MatProgressBarModule,
    MatChipsModule,
    NgCircleProgressModule.forRoot(),
    ContextMenuModule,
    MatButtonModule,
    FormlyModule,
    ChartsModule,
    MatTabsModule,
    MatButtonToggleModule,
    TimeagoModule.forChild(),
    MatSlideToggleModule,
    MatCheckboxModule,
    MatTableModule,
    MatSortModule,
    SimplemdeModule,
    MatStepperModule,
    MatSelectModule,
  ],
  exports: [
    BacklogComponent,
    ProjectOverviewComponent,
    StoryListComponent,
    SprintAssignSelectorComponent,
    SprintsComponent,
    SprintDetailComponent,
    StoryBurndownChartComponent,
    BoardComponent,
    StoryCardComponent,
    MembersComponent,
    MemberCardComponent,
    TimeEntryListComponent,
    StoryDetailComponent,
    TaskDetailComponent,
    TaskListComponent,
    ActiveSprintComponent,
    ActiveBoardComponent,
    NewSprintDropzoneComponent,
    NewSprintInteractiveComponent,
    StoryDetailsComponent,
    TaskDetailsComponent,
    MemberListComponent,
    ChatComponent,
  ],
})
export class ManagementModule {}
