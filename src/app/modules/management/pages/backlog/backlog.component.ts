import { CdkDrag, CdkDragDrop, CdkDropList, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Project, Sprint, State, Story, UpdateStoryGQL } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { StoryFormComponent } from 'src/app/shared/forms/story-form/story-form.component';

// noinspection JSUnusedLocalSymbols
@Component({
  selector: 'k-backlog',
  templateUrl: './backlog.component.html',
  styleUrls: ['./backlog.component.scss'],
})
export class BacklogComponent implements OnInit {
  public error: any = null;
  public loading = false;
  public project: Project = null;
  public currentSprint: { sprint: Sprint; stories: Story[]; points: number } = {
    sprint: null,
    stories: [],
    points: 0,
  };
  public otherSprints: { [id: string]: { sprint: Sprint; stories: Story[]; points: number } } = {};
  public stories: Story[] = null;
  public freeStories: Story[] = [];

  private storiesToUpdateAfterDrag: Story[] = [];

  constructor(
    private titleService: Title,
    private router: Router,
    private route: ActivatedRoute,
    private projectStorage: ProjectStorageService,
    private graphql: GraphqlService,
    private updateStory: UpdateStoryGQL,
    public dialog: MatDialog
  ) {
    this.titleService.setTitle('Backlog');
  }

  ngOnInit(): void {
    this.loading = true;
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        if (project) {
          this.processProjectData(project);
        }
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  openNewStoryDialog() {
    this.dialog
      .open(StoryFormComponent, {
        width: '60%',
        height: 'auto',
        data: { withoutSprint: true },
      })
      .afterClosed()
      .toPromise()
      .then((newStory) => {
        /*if (newStory) {
          this.stories.push(newStory);
          this.freeStories.push(newStory);
        }*/
      })
      .catch(console.error);
  }

  goToNewSprint() {
    if (this.route.snapshot.params?.project) {
      this.router
        .navigate(['management', this.route.snapshot.params.project, 'sprints', 'new'], {
          state: { redirect: this.router.url },
        })
        .catch(console.error);
    }
  }

  checkEntry(drag: CdkDrag, drop: CdkDropList): boolean {
    return true;
  }

  onDrop(event: CdkDragDrop<string[]>) {
    console.log(event);
    // Same container movement
    if (event.previousContainer.id === event.container.id) {
      // Current sprint movements
      if (event.previousContainer.id === this.currentSprint?.sprint?.id) {
        moveItemInArray(this.currentSprint.stories, event.previousIndex, event.currentIndex);
        this.updateWeightOnDrag(this.currentSprint.stories, event.item.data, event.currentIndex);
        // Backlog movements
      } else if (event.previousContainer.id === 'backlog') {
        moveItemInArray(this.freeStories, event.previousIndex, event.currentIndex);
        this.updateWeightOnDrag(this.freeStories, event.item.data, event.currentIndex);
        // Other sprint movements
      } else {
        moveItemInArray(this.otherSprints[event.previousContainer.id].stories, event.previousIndex, event.currentIndex);
        this.updateWeightOnDrag(
          this.otherSprints[event.previousContainer.id].stories,
          event.item.data,
          event.currentIndex
        );
      }
      // Different container movements
    } else {
      let movedStory: Story;
      const previousSprint: string = (event.item.data as Story).sprint;
      let newSprint: string;

      if (event.container.id === 'backlog') {
        newSprint = null;
      } else {
        newSprint = event.container.id;
      }

      if (previousSprint === this.currentSprint?.sprint?.id) {
        movedStory = this.currentSprint.stories.find((story: Story) => story.id === (event.item.data as Story).id);
      } else if (previousSprint) {
        movedStory = this.otherSprints[previousSprint].stories.find(
          (story: Story) => story.id === (event.item.data as Story).id
        );
      } else {
        movedStory = this.freeStories.find((story: Story) => story.id === (event.item.data as Story).id);
      }

      this.graphql
        .mutate(this.updateStory, {
          id: movedStory.id,
          patch: { sprint: newSprint },
        })
        .subscribe(
          (newStory: Story) => {
            // Remove from previous story list
            if (previousSprint === this.currentSprint?.sprint?.id) {
              this.currentSprint.points -= newStory.points;
              this.currentSprint.stories.splice(
                this.currentSprint.stories.findIndex((story: Story) => story.id === newStory.id),
                1
              );
            } else if (previousSprint) {
              this.otherSprints[previousSprint].points -= newStory.points;
              this.otherSprints[previousSprint].stories.splice(
                this.otherSprints[previousSprint].stories.findIndex((story: Story) => story.id === newStory.id),
                1
              );
            } else {
              this.freeStories.splice(
                this.freeStories.findIndex((story: Story) => story.id === newStory.id),
                1
              );
            }

            // Add to new story list
            if (newSprint === this.currentSprint?.sprint?.id) {
              this.currentSprint.points += newStory.points;
              this.currentSprint.stories.push(newStory);
            } else if (newSprint) {
              this.otherSprints[newSprint].points += newStory.points;
              this.otherSprints[newSprint].stories.push(newStory);
            } else {
              this.freeStories.push(newStory);
            }

            this.projectStorage.invalidate();
          },
          (reason) => {
            console.error(reason);
          }
        );
    }
  }

  private processProjectData(project: Project) {
    this.project = project;
    this.stories = [];
    this.freeStories = [];

    if (this.project?.sprintsByProject?.totalCount && this.project?.storiesByProject?.totalCount) {
      this.project.sprintsByProject.nodes
        .filter((sprint: Sprint) => !sprint.closed)
        .sort((a: Sprint, b: Sprint) => a.createdAt - b.createdAt)
        .forEach((sprint: Sprint) => {
          if (sprint.active) {
            this.currentSprint.sprint = sprint;
            this.currentSprint.stories = [];
            this.currentSprint.points = 0;
            this.project.storiesByProject.nodes
              .filter((story: Story) => story.sprint === sprint.id)
              .forEach((story: Story) => {
                this.currentSprint['stories'].push(story);
                this.currentSprint['points'] += +story.points;
              });
          } else {
            this.otherSprints[sprint.id] = {
              sprint: null,
              stories: [],
              points: 0,
            };
            this.otherSprints[sprint.id].sprint = sprint;
            this.project.storiesByProject.nodes
              .filter((story: Story) => story.sprint === sprint.id)
              .forEach((story: Story) => {
                this.otherSprints[sprint.id]['stories'].push(story);
                this.otherSprints[sprint.id]['points'] += +story.points;
              });
          }
        });
    }

    if (this.project?.storiesByProject?.totalCount) {
      this.stories = this.project.storiesByProject.nodes;
      this.freeStories = this.project.storiesByProject.nodes
        .filter((story: Story) => !story.sprint)
        .filter((story: Story) => story.state !== State.Finished);
    }
  }

  // noinspection DuplicatedCode
  private updateWeightOnDrag(list: Story[], item: Story, newIndex: number) {
    const newWeight = this.calcWeight(list, newIndex);

    if (item.weight !== newWeight) {
      item.weight = newWeight;
      this.storiesToUpdateAfterDrag.push(item);
    }

    this.propagateWeight(list, newIndex + 1, newWeight);
    this.triggerUpdates();
  }

  // noinspection JSMethodCanBeStatic
  private calcWeight(list, index) {
    return index > 0 ? list[index - 1].weight + 1 : 0;
  }

  // noinspection DuplicatedCode
  private propagateWeight(list: Story[], index: number, weight: number) {
    if (index < list.length) {
      if (list[index].weight === weight) {
        list[index].weight = weight + 1;
        this.storiesToUpdateAfterDrag.push(list[index]);
        this.propagateWeight(list, index + 1, weight + 1);
      }
    }
  }

  // noinspection DuplicatedCode
  private triggerUpdates() {
    const promisePool = [];
    this.storiesToUpdateAfterDrag.forEach((story: Story) => {
      promisePool.push(
        this.graphql.mutate(this.updateStory, { id: story.id, patch: { weight: story.weight } }).toPromise()
      );
    });
    Promise.all(promisePool).then(() => {
      this.projectStorage.invalidate();
      this.storiesToUpdateAfterDrag = [];
    });
  }
}
