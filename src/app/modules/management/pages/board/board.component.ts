import { CdkDrag, CdkDragDrop, CdkDropList, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { DeleteStoryGQL, Project, Sprint, State, Story, UpdateStoryGQL } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { StoryFormComponent } from 'src/app/shared/forms/story-form/story-form.component';
import { TimeEntryFormComponent } from 'src/app/shared/forms/time-entry-form/time-entry-form.component';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'k-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BoardComponent implements OnInit {
  constructor(
    private titleService: Title,
    private projectStorage: ProjectStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private graphql: GraphqlService,
    private updateStory: UpdateStoryGQL,
    private deleteStory: DeleteStoryGQL,
    private dialog: MatDialog
  ) {
    this.titleService.setTitle('Board');
  }

  @ViewChild(ContextMenuComponent, { static: true }) public contextualMenu: ContextMenuComponent;
  public project: Project = null;
  public loading = true;
  public error = null;
  public selectSprintFormControl: FormControl = new FormControl();
  public sprints: { id: string; name: string; hasStories: boolean }[] = [];
  public selectedSprint: string = null;
  public storyRegistry: {
    [sprintId: string]: {
      NEW: Story[];
      ONGOING: Story[];
      BLOCKED: Story[];
      FINISHED: Story[];
    };
  } = {};
  private allStoriesFakeSprint = {
    id: 'any_sprint',
    name: 'All sprints',
  };
  private storiesToUpdateAfterDrag = [];
  public firstLaneSelectedState = State.New;
  public secondLaneSelectedState = State.Ongoing;
  public thirdLaneSelectedState = State.Finished;

  private static calcWeight(list, index) {
    return index > 0 ? list[index - 1].weight + 1 : 0;
  }

  ngOnInit(): void {
    this.loading = true;
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        if (project) {
          this.project = project;
          this.buildProjectData();
          if (this.sprints.length) {
            this.selectedSprint = this.sprints[0].id;
            this.selectSprintFormControl.setValue(this.sprints[0].name);
          }
        }
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  getSprintsWithStories(): any[] {
    return this.sprints.filter((sprint) => sprint.hasStories);
  }

  handleSprintSelection(event: MatAutocompleteSelectedEvent) {
    this.selectedSprint = event.option.id;
    if (this.selectedSprint) {
      this.selectSprintFormControl.setValue(event.option.value);
    }
  }

  checkEntry(drag: CdkDrag, drop: CdkDropList): boolean {
    return drag?.data?.state !== drop.element.nativeElement.attributes.getNamedItem('data-state').value;
  }

  onDrop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        this.storyRegistry[this.selectedSprint][
          event.container.element.nativeElement.attributes.getNamedItem('data-state').value
        ],
        event.previousIndex,
        event.currentIndex
      );
      moveItemInArray(
        this.storyRegistry[this.allStoriesFakeSprint.id][
          event.container.element.nativeElement.attributes.getNamedItem('data-state').value
        ],
        event.previousIndex,
        event.currentIndex
      );
      this.updateWeightOnDrag(
        this.storyRegistry[this.allStoriesFakeSprint.id][
          event.container.element.nativeElement.attributes.getNamedItem('data-state').value
        ],
        event.item.data,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        this.storyRegistry[this.selectedSprint][
          event.previousContainer.element.nativeElement.attributes.getNamedItem('data-state').value
        ],
        this.storyRegistry[this.selectedSprint][
          event.container.element.nativeElement.attributes.getNamedItem('data-state').value
        ],
        event.previousIndex,
        event.currentIndex
      );
      transferArrayItem(
        this.storyRegistry[this.allStoriesFakeSprint.id][
          event.previousContainer.element.nativeElement.attributes.getNamedItem('data-state').value
        ],
        this.storyRegistry[this.allStoriesFakeSprint.id][
          event.container.element.nativeElement.attributes.getNamedItem('data-state').value
        ],
        event.previousIndex,
        event.currentIndex
      );
      this.updateStateOnDrag(
        event.item.data,
        event.container.element.nativeElement.attributes.getNamedItem('data-state').value as State
      );
    }
  }

  goToDetails(selectedStory: Story) {
    if (this.project?.id && selectedStory?.id) {
      this.router
        .navigate(['management', this.project.id, 'stories', selectedStory.id], {
          state: { redirect: this.router.url },
        })
        .catch(console.error);
    }
  }

  handleAddEntry(selectedStory: Story) {
    this.dialog
      .open(TimeEntryFormComponent, {
        width: '60%',
        height: 'auto',
        data: { story: selectedStory },
      })
      .afterClosed()
      .subscribe(console.log);
  }

  handleEdit(selectedStory: Story) {
    this.dialog
      .open(StoryFormComponent, {
        width: '60%',
        height: 'auto',
        data: selectedStory,
      })
      .afterClosed()
      .subscribe((value) => {
        console.log(value);
      });
  }

  handleDelete(selectedStory: Story) {
    this.dialog
      .open(ConfirmationDialogComponent, {
        width: '40%',
        height: 'auto',
        data: {
          title: `Deleting story "${selectedStory.name}"`,
          text: 'Are you sure you want to delete this story? This action cannot be undone. ',
        },
      })
      .afterClosed()
      .subscribe((actionTaken: boolean) => {
        if (actionTaken) {
          this.graphql
            .mutate(this.deleteStory, { id: selectedStory.id })
            .toPromise()
            .then(() => {
              this.storyRegistry[this.selectedSprint][selectedStory.state].splice(
                this.storyRegistry[this.selectedSprint][selectedStory.state].findIndex(
                  (story: Story) => story.id === selectedStory.id
                ),
                1
              );
              this.storyRegistry[this.allStoriesFakeSprint.id][selectedStory.state].splice(
                this.storyRegistry[this.allStoriesFakeSprint.id][selectedStory.state].findIndex(
                  (story: Story) => story.id === selectedStory.id
                ),
                1
              );
              this.projectStorage.invalidate();
            })
            .catch(console.error);
        }
      });
  }

  private buildProjectData(): void {
    this.sprints = [];
    this.storyRegistry = {};
    if (this.project?.storiesByProject?.totalCount) {
      this.sprints.push({ id: this.allStoriesFakeSprint.id, name: this.allStoriesFakeSprint.name, hasStories: true });
      this.storyRegistry[this.allStoriesFakeSprint.id] = {
        NEW: this.project?.storiesByProject?.nodes
          .filter((story: Story) => story?.state === State.New)
          .sort((a: Story, b: Story) => +a - +b),
        ONGOING: this.project?.storiesByProject?.nodes
          .filter((story: Story) => story?.state === State.Ongoing)
          .sort((a: Story, b: Story) => +a - +b),
        BLOCKED: this.project?.storiesByProject?.nodes
          .filter((story: Story) => story?.state === State.Blocked)
          .sort((a: Story, b: Story) => +a - +b),
        FINISHED: this.project?.storiesByProject?.nodes
          .filter((story: Story) => story?.state === State.Finished)
          .sort((a: Story, b: Story) => +a - +b),
      };
      if (this.project?.sprintsByProject?.totalCount) {
        this.project?.sprintsByProject?.nodes?.forEach((sprint: Sprint) => {
          this.sprints.push({
            id: sprint.id,
            name: sprint.name,
            hasStories: this.project?.storiesByProject?.nodes.some((story: Story) => story?.sprint === sprint.id),
          });
          const sprintStories = this.project?.storiesByProject?.nodes?.filter(
            (story: Story) => story?.sprint === sprint.id
          );
          if (sprintStories?.length) {
            this.storyRegistry[sprint.id] = {
              NEW: sprintStories
                .filter((story: Story) => story?.state === State.New)
                .sort((a: Story, b: Story) => +a - +b),
              ONGOING: sprintStories
                .filter((story: Story) => story?.state === State.Ongoing)
                .sort((a: Story, b: Story) => +a - +b),
              BLOCKED: sprintStories
                .filter((story: Story) => story?.state === State.Blocked)
                .sort((a: Story, b: Story) => +a - +b),
              FINISHED: sprintStories
                .filter((story: Story) => story?.state === State.Finished)
                .sort((a: Story, b: Story) => +a - +b),
            };
          }
        });
      }
    }
  }

  private updateWeightOnDrag(list: Story[], item: Story, newIndex: number) {
    const newWeight = BoardComponent.calcWeight(list, newIndex);

    if (item.weight !== newWeight) {
      item.weight = newWeight;
      this.storiesToUpdateAfterDrag.push(item);
    }

    this.propagateWeight(list, newIndex + 1, newWeight);
    this.triggerUpdates();
  }

  private propagateWeight(list: Story[], index: number, weight: number) {
    if (index < list.length) {
      if (list[index].weight === weight) {
        list[index].weight = weight + 1;
        this.storiesToUpdateAfterDrag.push(list[index]);
        this.propagateWeight(list, index + 1, weight + 1);
      }
    }
  }

  private updateStateOnDrag(item: Story, newState: State) {
    item.state = newState;
    this.storiesToUpdateAfterDrag.push(item);
    this.triggerUpdates();
  }

  private triggerUpdates() {
    const promisePool = [];
    this.storiesToUpdateAfterDrag.forEach((story: Story) => {
      promisePool.push(
        this.graphql
          .mutate(this.updateStory, { id: story.id, patch: { weight: story.weight, state: story.state } })
          .toPromise()
      );
    });
    Promise.all(promisePool).then(() => {
      this.projectStorage.invalidate();
      this.storiesToUpdateAfterDrag = [];
    });
  }
}
