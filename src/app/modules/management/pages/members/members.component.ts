import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { DeleteMemberGQL, Member, Project, State, Story, UserProfile } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { MembersSelectorComponent } from 'src/app/shared/forms/members-selector/members-selector.component';
import { WorkloadFormComponent } from 'src/app/shared/forms/workload-form/workload-form.component';

@Component({
  selector: 'k-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
})
export class MembersComponent implements OnInit {
  public error: any = null;
  public loading = false;
  public project: Project = null;
  public members: UserProfile[] = [];

  constructor(
    private titleService: Title,
    private route: ActivatedRoute,
    private projectStorage: ProjectStorageService,
    private graphql: GraphqlService,
    private deleteMember: DeleteMemberGQL,
    public dialog: MatDialog
  ) {
    this.titleService.setTitle('Members');
  }

  ngOnInit(): void {
    this.loading = true;
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        if (project) {
          this.project = project;
          this.processProjectData(this.project);
        }
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  openAddMembersDialog(): void {
    this.dialog
      .open(MembersSelectorComponent, {
        width: '40%',
        height: 'auto',
      })
      .afterClosed()
      .subscribe((newMembers: UserProfile[]) => {
        if (newMembers?.length) {
          this.members = [
            ...this.members,
            ...newMembers.map((newMember: UserProfile) => ({ ...newMember, workload: 0 })),
          ];
          // this.tableSource = new MatTableDataSource(this.members);
          this.projectStorage.invalidate();
          this.projectStorage.loadFromRoute(this.route);
        }
      });
  }

  handleEditWorkload(userId: string): void {
    this.dialog
      .open(WorkloadFormComponent, {
        width: '40%',
        height: 'auto',
        data: {
          user: this.members.find((member: UserProfile) => member.id === userId),
        },
      })
      .afterClosed()
      .subscribe(() => {
        this.members = [...this.members];
      });
  }

  handleDelete(userId: string): void {
    this.dialog
      .open(ConfirmationDialogComponent, {
        width: '40%',
        height: 'auto',
        data: {
          title: `Deleting member`,
          text: 'Are you sure you want to remove this person from this project? This action cannot be undone. ',
        },
      })
      .afterClosed()
      .subscribe((actionTaken: boolean) => {
        if (actionTaken) {
          this.graphql
            .mutate(this.deleteMember, {
              id: this.project.membersByProject.nodes.find(
                (member: Member) => member.userProfileByUserProfile.id === userId
              ).id,
            })
            .toPromise()
            .then(() => {
              this.members.splice(
                this.members.findIndex((user: UserProfile) => user.id === userId),
                1
              );
              this.project.membersByProject.nodes.splice(
                this.project.membersByProject.nodes.findIndex(
                  (member: Member) => member.userProfileByUserProfile.id === userId
                ),
                1
              );
              this.project.membersByProject.totalCount -= 1;
              // this.tableSource = new MatTableDataSource(this.members);
              this.projectStorage.flush(this.project);
            })
            .catch(console.error);
        }
      });
  }

  private processProjectData(project: Project): void {
    if (project?.membersByProject?.totalCount) {
      this.members = project.membersByProject.nodes
        .map((member: Member) => member.userProfileByUserProfile)
        .sort((a: UserProfile, b: UserProfile) => a.firstName.localeCompare(b.firstName, 'en', { sensitivity: 'base' }))
        .map((user: UserProfile) => {
          return {
            ...user,
            workload: this.project?.storiesByProject?.nodes
              ?.filter((story: Story) => story?.assignee === user.id)
              ?.filter((story: Story) => story?.state !== State.Finished)
              ?.reduce(
                (accumulatedPoints: number, currentStory: Story) => accumulatedPoints + +currentStory?.points,
                0
              ),
          };
        });
    }
  }
}
