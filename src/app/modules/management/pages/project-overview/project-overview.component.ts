import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import {
  DeleteMemberGQL,
  GetProjectGQL,
  Member,
  Project,
  Sprint,
  State,
  Story,
  Task,
  UserProfile,
} from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ActiveSprintStorageService } from 'src/app/services/storage/active-sprint-storage.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { WorkloadFormComponent } from 'src/app/shared/forms/workload-form/workload-form.component';

@Component({
  selector: 'k-project-overview',
  templateUrl: './project-overview.component.html',
  styleUrls: ['./project-overview.component.scss'],
})
export class ProjectOverviewComponent implements OnInit {
  public project$: Observable<Project> = null;
  private project: Project = null;
  public stories: Story[] = null;
  public tasks: Task[] = null;
  public activeStories: Story[] = null;
  public currentSprint: string = null;
  public idleMembers: UserProfile[] = null;

  constructor(
    private titleService: Title,
    private projectStorage: ProjectStorageService,
    private activeSprint: ActiveSprintStorageService,
    private route: ActivatedRoute,
    private graphql: GraphqlService,
    private getProject: GetProjectGQL,
    private deleteMember: DeleteMemberGQL,
    public dialog: MatDialog
  ) {
    this.titleService.setTitle('Project details');
  }

  ngOnInit(): void {
    this.project$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.graphql.query(this.getProject, { id: params.get('project'), storiesOrderBy: 'WEIGHT_ASC' })
      ),
      switchMap((project: Project) => {
        if (project) {
          this.projectStorage.store(project);
          return new Promise((resolve) => resolve(project));
        } else {
          return this.projectStorage.getListener();
        }
      }),
      tap((project: Project) => {
        this.project = project;
        this.stories = [...project?.storiesByProject?.nodes].sort((a: Story, b: Story) => {
          return a.updatedAt - b.updatedAt;
        });
        this.tasks = project?.tasksByProject?.nodes;
        this.currentSprint = project?.sprintsByProject?.nodes?.find((sprint: Sprint) => sprint.active)?.id;
        this.activeStories = this.stories.filter((story: Story) => story.sprint === this.currentSprint);
        this.idleMembers = project?.membersByProject?.nodes
          ?.map((member: Member) => member?.userProfileByUserProfile)
          ?.filter((user: UserProfile) => this.isIdle(user.id));
      })
    );
  }

  private isIdle(memberId: string): boolean {
    const activeStories = this.stories?.filter((story: Story) => story?.state !== State.Finished);

    if (activeStories?.filter((story: Story) => story?.assignee === memberId)?.length) {
      return false;
    }

    for (let i = 0; i < activeStories.length; i++) {
      if (activeStories[i]?.tasksByStory?.totalCount) {
        for (let j = 0; j < activeStories[i].tasksByStory.nodes.length; j++) {
          if (activeStories[i].tasksByStory.nodes[j]?.assignee === memberId) {
            return false;
          }
        }
      }
    }

    return true;
  }

  handleEditWorkload(userId: string): void {
    this.dialog
      .open(WorkloadFormComponent, {
        width: '40%',
        height: 'auto',
        data: {
          user: this.idleMembers.find((member: UserProfile) => member.id === userId),
        },
      })
      .afterClosed()
      .subscribe((response: number) => {
        if (response > 0) {
          this.idleMembers.splice(
            this.idleMembers.findIndex((member: UserProfile) => member.id === userId),
            1
          );
        }
      });
  }

  handleDelete(userId: string): void {
    this.dialog
      .open(ConfirmationDialogComponent, {
        width: '40%',
        height: 'auto',
        data: {
          title: `Deleting member`,
          text: 'Are you sure you want to remove this person from this project? This action cannot be undone. ',
        },
      })
      .afterClosed()
      .subscribe((actionTaken: boolean) => {
        if (actionTaken) {
          this.graphql
            .mutate(this.deleteMember, {
              id: this.project.membersByProject.nodes.find(
                (member: Member) => member.userProfileByUserProfile.id === userId
              ).id,
            })
            .toPromise()
            .then(() => {
              this.idleMembers.splice(
                this.idleMembers.findIndex((user: UserProfile) => user.id === userId),
                1
              );
              this.project.membersByProject.nodes.splice(
                this.project.membersByProject.nodes.findIndex(
                  (member: Member) => member.userProfileByUserProfile.id === userId
                ),
                1
              );
              this.project.membersByProject.totalCount -= 1;
              // this.tableSource = new MatTableDataSource(this.members);
              this.projectStorage.flush(this.project);
            })
            .catch(console.error);
        }
      });
  }
}
