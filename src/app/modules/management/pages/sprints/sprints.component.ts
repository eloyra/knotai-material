import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';
import { Project, Sprint, Story } from 'src/app/services/api';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { SprintFormComponent } from 'src/app/shared/forms/sprint-form/sprint-form.component';
import { environment } from 'src/environments';

@Component({
  selector: 'k-sprints',
  templateUrl: './sprints.component.html',
  styleUrls: ['./sprints.component.scss'],
})
export class SprintsComponent implements OnInit {
  public error: any = null;
  public loading = false;
  public project: Project = null;
  public sprints: { id: string; sprint: Sprint } = null;
  public selectSprintFormControl: FormControl = new FormControl();
  public selectedSprint: Sprint = null;
  public selectedSprintDeadline: string = null;
  public activeStoriesForSelectedSprint: Story[] = null;
  public inactiveStoriesForSelectedSprint: Story[] = null;
  public activeTab = 4;

  constructor(
    private titleService: Title,
    private route: ActivatedRoute,
    private router: Router,
    private projectStorage: ProjectStorageService,
    public dialog: MatDialog
  ) {
    this.titleService.setTitle('Sprint details');
  }

  ngOnInit(): void {
    this.loading = true;
    this.route.queryParams.subscribe((params: Params) => {
      if (params?.tab) {
        this.activeTab = params.tab;
      }
    });
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        if (project) {
          this.processProjectData(project);
          this.getSelectedSprint();
          this.selectedSprintDeadline = this.calculateCurrentSprintDeadline();
          this.activeStoriesForSelectedSprint = this.getActiveStoriesForCurrentSprint();
          this.inactiveStoriesForSelectedSprint = this.getInactiveStoriesForCurrentSprint();
          console.log(this.project, this.sprints, this.selectedSprint);
        }
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  openNewSprintDialog() {
    this.dialog
      .open(SprintFormComponent, {
        width: '60%',
        height: 'auto',
      })
      .afterClosed()
      .subscribe(console.log);
  }

  handleSprintSelection(event: MatAutocompleteSelectedEvent) {
    this.loading = true;
    this.router
      .navigate(['management', this.project.id, 'sprints', event.option.id], { queryParams: { tab: this.activeTab } })
      .then(() => {
        window.location.reload();
      })
      .catch(console.error);
  }

  handleTabChange(event: MatTabChangeEvent): void {
    this.activeTab = event.index;
  }

  private processProjectData(project: Project) {
    this.project = project;
    this.sprints = project?.sprintsByProject?.nodes.reduce(
      (object: any, currentValue: Sprint) => ({
        ...object,
        [currentValue.id]: {
          ...currentValue,
          stories: project?.storiesByProject?.nodes.filter((story: Story) => story?.sprint === currentValue.id),
        },
      }),
      {}
    );
  }

  private getSelectedSprint() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      if (this.sprints[params.get('sprint')]) {
        this.selectedSprint = this.sprints[params.get('sprint')];
      } else if (Object.keys(this.sprints).length) {
        this.selectedSprint = Object.values(this.sprints)[0] as Sprint;
      }
      if (this.selectedSprint) {
        this.selectSprintFormControl.setValue(this.selectedSprint.name);
      }
    });
  }

  private calculateCurrentSprintDeadline(): string {
    const totalEstimatedMilliseconds =
      (this.selectedSprint as any)?.stories.reduce((accumulatedValue: number, currentStory: Story) => {
        return accumulatedValue + currentStory.points * environment.storyPointDuration;
      }, 0) *
      60 *
      60 *
      1000;
    // TODO Fix
    return 'needs fix'; //new Date(new Date(this.selectedSprint.start).getTime() + totalEstimatedMilliseconds).toISOString();
  }

  private getActiveStoriesForCurrentSprint(): Story[] {
    return []; /* (this.selectedSprint as any).stories.filter(
      // TODO Fix using STATE, ONGOING is the active state
      (story: Story) => story?.start && (story?.finish === null || new Date(story?.finish).getTime() > Date.now())
    );*/
  }

  private getInactiveStoriesForCurrentSprint(): Story[] {
    return []; /*(this.selectedSprint as any).stories.filter(
      (story: Story) => !story?.start || (story?.finish && new Date(story?.finish).getTime() <= Date.now())
    );*/
  }
}
