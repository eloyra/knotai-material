import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { debounceTime, map } from 'rxjs/operators';
import { GetProjectsInDepthGQL, PageInfo, Project, ProjectsConnection, ProjectsEdge } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProgressService } from 'src/app/services/statistics/progress.service';
import { ActiveSprintStorageService } from 'src/app/services/storage/active-sprint-storage.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { ProjectFormComponent } from 'src/app/shared/forms/project-form/project-form.component';

@Component({
  selector: 'k-projects-view',
  templateUrl: './projects-view.component.html',
  styleUrls: ['./projects-view.component.scss'],
})
export class ProjectsViewComponent implements OnInit {
  public loading = false;
  public error = null;
  public projects: Project[] = null;
  public filteredProjects: Project[] = null;
  public pageInfo: PageInfo = null;
  public totalCount: number = null;
  public projectFilter: FormGroup;

  constructor(
    private titleService: Title,
    private router: Router,
    private getProjects: GetProjectsInDepthGQL,
    private graphql: GraphqlService,
    private projectStorage: ProjectStorageService,
    private activeSprintStorage: ActiveSprintStorageService,
    public dialog: MatDialog
  ) {
    this.titleService.setTitle('Projects list');
  }

  ngOnInit(): void {
    this.loading = true;

    this.projectFilter = new FormGroup({
      filterControl: new FormControl(''),
    });

    this.enableFiltering();

    this.graphql.query(this.getProjects).subscribe(
      (connection: ProjectsConnection) => {
        const rawProjects = connection.edges.map((edge: ProjectsEdge) => edge.node);
        const red = [];
        const yellow = [];
        const green = [];
        const blue = [];
        rawProjects.forEach((project: Project) => {
          if (project?.storiesByProject?.totalCount) {
            const currentSprint = this.activeSprintStorage.findActiveSprintInProject(project);
            const { color } = ProgressService.forSprint({
              sprint: currentSprint,
              velocity: project?.config?.velocity ?? 4,
            });
            if (color === 'red') red.push(project);
            else if (color === 'yellow') yellow.push(project);
            else if (color === 'green') green.push(project);
            else if (color === 'blue') blue.push(project);
          } else {
            blue.push(project);
          }
        });
        this.projects = [].concat(red).concat(yellow).concat(green).concat(blue);
        this.filteredProjects = [...this.projects];
        this.pageInfo = connection.pageInfo;
        this.totalCount = connection.totalCount;
        this.projectStorage.storeProjects(this.projects);
        this.loading = false;
      },
      (error) => {
        const storedProjects = this.projectStorage.getStoredProjects();
        if (storedProjects) {
          this.projects = [...storedProjects];
          this.filteredProjects = [...this.projects];
          this.totalCount = this.projects.length;
          console.warn('funchiono');
        } else {
          this.error = error;
        }
        this.loading = false;
      }
    );
  }

  enableFiltering(): void {
    this.projectFilter.valueChanges
      .pipe(debounceTime(500))
      .pipe(map((value: any) => value.filterControl))
      .subscribe((filterValue: string) => {
        this.filteredProjects = [
          ...this.projects.filter((project: Project) =>
            project.name.toLocaleLowerCase().includes(filterValue.toLocaleLowerCase())
          ),
        ];
      });
  }

  openNewProjectDialog() {
    this.dialog
      .open(ProjectFormComponent, {
        width: '60%',
        height: 'auto',
      })
      .afterClosed()
      .subscribe((newProject: Project) => {
        if (newProject) {
          this.projects.push(newProject);
          this.filteredProjects = [...this.projects];
          this.projectFilter.reset({ filterControl: '' });
        }
      });
  }
}
