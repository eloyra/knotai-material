import { CdkDrag, CdkDragDrop, CdkDropList } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { CreateSprintGQL, Project, Sprint, State, Story, Task, TimeEntry, UpdateStoryGQL } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { StoryFormComponent } from 'src/app/shared/forms/story-form/story-form.component';

@Component({
  selector: 'k-new-sprint-interactive',
  templateUrl: './new-sprint-interactive.component.html',
  styleUrls: ['./new-sprint-interactive.component.scss'],
})
export class NewSprintInteractiveComponent implements OnInit {
  public error: any = null;
  public loading = false;
  public fetching = false;
  public project: Project = null;
  public backlog: Story[] = [];
  public prevUnfinishedStories: Story[];
  public prevUnfinishedStoriesPoints: number;
  public velocity: number = 0;
  public form = new FormGroup({});
  public model: any = {};
  public fields: FormlyFieldConfig[] = [];
  public selectedStories: Story[] = [];
  public includePrevUnfinishedStories = false;

  private arePrevStoriesCurrentlyIncluded = false;
  private manuallySelectedStories: Story[] = [];

  private sprintPointsThresholds = {
    ok: this.velocity * 1.15,
    warning: this.velocity * 1.5,
  };

  constructor(
    private titleService: Title,
    private router: Router,
    private route: ActivatedRoute,
    private projectStorage: ProjectStorageService,
    private graphql: GraphqlService,
    private createSprint: CreateSprintGQL,
    private updateStory: UpdateStoryGQL,
    public dialog: MatDialog
  ) {
    this.titleService.setTitle('New sprint');
  }

  ngOnInit(): void {
    this.loading = true;
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        if (project) {
          this.processProjectData(project);
          this.buildForm();
        }
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  openNewStoryDialog() {
    const ref = this.dialog.open(StoryFormComponent, {
      width: '60%',
      height: 'auto',
    });
    ref.componentInstance.hideSprint = true;
    ref
      .afterClosed()
      .toPromise()
      .then((newStory) => {
        if (newStory) {
          this.projectStorage.invalidate();
        }
      })
      .catch(console.error);
  }

  togglePrevStories() {
    this.includePrevUnfinishedStories = !this.includePrevUnfinishedStories;
    if (this.includePrevUnfinishedStories) {
      this.selectedStories = [...this.selectedStories, ...this.prevUnfinishedStories];
      this.arePrevStoriesCurrentlyIncluded = true;
    } else if (this.arePrevStoriesCurrentlyIncluded) {
      this.selectedStories = [...this.manuallySelectedStories];
      this.arePrevStoriesCurrentlyIncluded = false;
    }
  }

  getCurrentlySelectedStoryPoints(): number {
    return this.selectedStories?.reduce(
      (totalPoints: number, currentStory: Story) => totalPoints + +currentStory.points,
      0
    );
  }

  onSubmit(model) {
    this.create(model).then((success: boolean) => {
      if (success) {
        this.projectStorage.invalidate();
        this.router.navigate(['management', this.project.id, 'backlog']);
      }
    });
  }

  checkEntry(drag: CdkDrag, drop: CdkDropList): boolean {
    return drag.dropContainer.id === 'backlog' && drop.id === 'dropzone';
  }

  onDrop(event: CdkDragDrop<string[]>) {
    if (event.item.data) {
      const movedStory = event.item.data as Story;
      this.manuallySelectedStories.push(movedStory);
      this.selectedStories.push(movedStory);
      this.backlog.splice(
        this.backlog.findIndex((story: Story) => story.id === movedStory.id),
        1
      );
    }
  }

  isStoryPointAmountAcceptable(): boolean {
    return this.sprintPointsThresholds.ok >= this.getCurrentlySelectedStoryPoints();
  }

  private buildForm() {
    this.fields = [
      {
        key: 'name',
        type: 'input',
        className: 'field field-small',
        templateOptions: {
          type: 'text',
          label: 'Sprint name',
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'duration',
        type: 'input',
        className: 'field field-small',
        templateOptions: {
          type: 'number',
          label: 'Duration (in weeks)',
          appearance: 'outline',
          color: 'accent',
          min: 0,
        },
      },
      {
        key: 'description',
        type: 'textarea',
        className: 'field',
        templateOptions: {
          label: 'Description',
          multiline: true,
          rows: 5,
          appearance: 'outline',
          color: 'accent',
        },
      },
    ];
  }

  private processProjectData(project: Project) {
    this.project = project;
    this.velocity = this.calculateVelocity();
    this.backlog = this.getBacklog();

    this.prevUnfinishedStories = this.getPrevUnfinishedStories();
    this.prevUnfinishedStoriesPoints =
      this.prevUnfinishedStories?.reduce((points: number, currentStory: Story) => points + +currentStory.points, 0) ??
      0;
  }

  private getBacklog(): Story[] {
    if (!this.project?.storiesByProject?.totalCount) {
      return [];
    }
    return this.project.storiesByProject.nodes.filter((story: Story) => !story.sprint);
  }

  private getPrevUnfinishedStories(): Story[] {
    if (!this.project?.storiesByProject?.totalCount) {
      return [];
    }
    return this.project.storiesByProject.nodes.filter(
      (story: Story) => story.sprint && story.sprintBySprint.closed && story.state !== State.Finished
    );
  }

  private calculateVelocity(): number {
    let closedSprints = [];
    let points = 0;
    let hours = 0;

    if (this.project?.sprintsByProject?.totalCount) {
      closedSprints = this.project.sprintsByProject.nodes.filter((sprint: Sprint) => sprint.closed);
    }

    if (this.project?.storiesByProject?.totalCount) {
      this.project.storiesByProject.nodes
        .filter((story: Story) => closedSprints.map((sprint: Sprint) => sprint.id).includes(story.sprint))
        .filter((story: Story) => story.state === State.Finished)
        .forEach((story: Story) => {
          points += story.points;

          if (story?.tasksByStory?.totalCount) {
            story.tasksByStory.nodes.forEach((task: Task) => {
              if (task?.timeEntriesByTask?.totalCount) {
                hours += task.timeEntriesByTask.nodes.reduce(
                  (taskHours: number, currentValue: TimeEntry) => taskHours + +currentValue?.hours,
                  0
                );
              }
            });
          }
        });
    }

    const realStoryPointHourValue = points ? hours / points : 0;
    const recommendedSprintLength = closedSprints.length ? Math.floor(points / closedSprints.length) : 0;

    return recommendedSprintLength;
  }

  private create(model: Sprint): Promise<boolean> {
    this.fetching = true;
    return new Promise((resolve, reject) => {
      this.graphql.mutate(this.createSprint, { item: { ...model, project: this.project.id } }).subscribe(
        (newSprint: Sprint) => {
          if (!this.selectedStories?.length) {
            resolve(this.resolveMutation(newSprint));
          } else {
            this.processStories(
              this.selectedStories.map((story: Story) => story.id),
              newSprint.id
            )
              .then((storiesInSprint: Story[]) => {
                resolve(this.resolveMutation(newSprint, storiesInSprint));
              })
              .catch((error) => {
                console.error(error);
                this.fetching = false;
                reject(false);
              });
          }
        },
        (error) => {
          console.error(error);
          this.fetching = false;
          reject(false);
        }
      );
    });
  }

  private processStories(selectedStories: string[], newSprintId: string): Promise<Story[]> {
    let storiesToAddSprint: string[] = [...selectedStories];
    const promisesToWait = storiesToAddSprint.map((story: string) =>
      this.graphql.mutate(this.updateStory, { id: story, patch: { sprint: newSprintId } }).toPromise()
    );
    return new Promise((resolve, reject) => {
      Promise.all(promisesToWait)
        .then((response: Story[]) => {
          resolve(response);
        })
        .catch((reason) => {
          console.error(reason);
          reject(reason);
        });
    });
  }

  private resolveMutation(newSprint: Sprint, storiesInSprint: Story[] = []): boolean {
    if (storiesInSprint.length) {
      if (!newSprint.storiesBySprint) (newSprint.storiesBySprint as any) = {};
      if (!newSprint.storiesBySprint.nodes) newSprint.storiesBySprint.nodes = [];
      newSprint.storiesBySprint.nodes.push(...storiesInSprint);
      newSprint.storiesBySprint.totalCount = newSprint.storiesBySprint.totalCount + storiesInSprint.length;
    }
    this.project.sprintsByProject.nodes.push(newSprint);
    this.project.sprintsByProject.totalCount += 1;
    this.projectStorage.flush();
    this.fetching = false;
    return true;
  }
}
