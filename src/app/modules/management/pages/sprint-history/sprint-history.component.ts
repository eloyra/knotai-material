import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Project, Sprint, State, Story, Task } from 'src/app/services/api';
import { ProgressService } from 'src/app/services/statistics/progress.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';

@Component({
  selector: 'k-sprint-history',
  templateUrl: './sprint-history.component.html',
  styleUrls: ['./sprint-history.component.scss'],
})
export class SprintHistoryComponent implements OnInit {
  public error: any = null;
  public loading = false;
  public project: Project = null;
  public availableSprints: Sprint[] = [];
  public activeSprint: Sprint = null;
  public activeSprintBinder: string = null;
  public sprintStories: Story[] = [];
  public storiesByStatus: any = {
    [State.New]: [],
    [State.Ongoing]: [],
    [State.Blocked]: [],
    [State.Finished]: [],
  };
  public sprintTasks: Task[] = [];
  public activeTab = 0;

  constructor(
    private titleService: Title,
    private route: ActivatedRoute,
    private router: Router,
    private projectStorage: ProjectStorageService,
    public dialog: MatDialog,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    this.titleService.setTitle('Sprint history');
  }

  ngOnInit(): void {
    this.loading = true;
    this.route.queryParams.subscribe((params: Params) => {
      if (params?.tab) {
        this.activeTab = params.tab;
      }
    });
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        if (project) {
          this.project = project;
          this.preBuildData();
          this.buildData();
        }
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  handleSprintChange() {
    this.activeSprint = this.availableSprints.find((sprint: Sprint) => sprint.id === this.activeSprintBinder);
    this.buildData();
    this.changeDetectorRef.detectChanges();
  }

  handleTabChange(event: MatTabChangeEvent): void {
    this.activeTab = event.index;
  }

  calculateCurrentSprintDeadline(): string {
    const deadline = ProgressService.getSprintDeadlineMilliseconds({ sprint: this.activeSprint });

    return deadline ? new Date(deadline).toISOString() : null;
  }

  private preBuildData(): void {
    if (this.project?.sprintsByProject?.totalCount) {
      this.availableSprints = this.project.sprintsByProject.nodes.filter((sprint: Sprint) => !sprint.active);
      this.activeSprint = this.availableSprints?.[0];
      this.activeSprintBinder = this.activeSprint?.id;
    }
  }

  private buildData() {
    if (this.project?.sprintsByProject?.totalCount) {
      if (this.activeSprint && this.project?.storiesByProject?.totalCount) {
        this.sprintStories = this.project.storiesByProject.nodes.filter(
          (story: Story) => story.sprint === this.activeSprint.id
        );
        this.storiesByStatus[State.New] = this.getNewStoriesForCurrentSprint();
        this.storiesByStatus[State.Ongoing] = this.getOngoingStoriesForCurrentSprint();
        this.storiesByStatus[State.Blocked] = this.getBlockedStoriesForCurrentSprint();
        this.storiesByStatus[State.Finished] = this.getFinishedStoriesForCurrentSprint();
      }

      if (this.activeSprint && this.sprintStories.length) {
        this.sprintTasks = [];
        this.sprintStories.forEach((story: Story) => {
          if (story?.tasksByStory?.totalCount) {
            this.sprintTasks.push(...story.tasksByStory.nodes);
          }
        });
      }
    }
  }

  private getNewStoriesForCurrentSprint(): Story[] {
    if (this.sprintStories.length) {
      return this.sprintStories.filter((story: Story) => story?.state === State.New);
    }
    return [];
  }

  private getOngoingStoriesForCurrentSprint(): Story[] {
    if (this.sprintStories.length) {
      return this.sprintStories.filter((story: Story) => story?.state === State.Ongoing);
    }
    return [];
  }

  private getBlockedStoriesForCurrentSprint(): Story[] {
    if (this.sprintStories.length) {
      return this.sprintStories.filter((story: Story) => story?.state === State.Blocked);
    }
    return [];
  }

  private getFinishedStoriesForCurrentSprint(): Story[] {
    if (this.sprintStories.length) {
      return this.sprintStories.filter((story: Story) => story?.state === State.Finished);
    }
    return [];
  }
}
