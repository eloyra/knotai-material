import { Component, OnInit } from '@angular/core';
import { DeleteStoryGQL, Project, Story, Task } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { TaskFormComponent } from 'src/app/shared/forms/task-form/task-form.component';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';

type AddonType = 'TIME_ENTRIES' | 'COMMENTS';

type DetailsMode = 'DISPLAY' | 'EDIT';

@Component({
  selector: 'k-story-detail',
  templateUrl: './story-detail.component.html',
  styleUrls: ['./story-detail.component.scss'],
})
export class StoryDetailComponent implements OnInit {
  public project: Project;
  public story: Story;

  public loading = false;
  public error: any;

  public activeAddon: AddonType = 'TIME_ENTRIES';

  public mode: DetailsMode = 'DISPLAY';

  constructor(
    private titleService: Title,
    public dialog: MatDialog,
    private projectStorage: ProjectStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private graphql: GraphqlService,
    private deleteStory: DeleteStoryGQL
  ) {
    this.titleService.setTitle('Story details');
  }

  ngOnInit(): void {
    this.loading = true;
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        if (project) {
          this.project = project;
          this.story = this.project.storiesByProject.nodes.find(
            (story: Story) => story.id === this.route.snapshot.params.story
          );
          if (this.story?.tasksByStory?.totalCount) {
            this.story.tasksByStory.nodes.sort((a: Task, b: Task) => a.createdAt - b.createdAt);
          }
        }
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  toggleMode() {
    if (this.mode === 'DISPLAY') {
      this.editStory();
    } else {
      this.displayStory();
    }
  }

  editionSubmitted() {
    this.mode = 'DISPLAY';
  }

  openNewTaskDialog() {
    this.dialog
      .open(TaskFormComponent, {
        width: '60%',
        height: 'auto',
        data: { story: this.story },
      })
      .afterClosed()
      .toPromise()
      .catch(console.error);
  }

  handleDelete() {
    this.dialog
      .open(ConfirmationDialogComponent, {
        width: '40%',
        height: 'auto',
        data: {
          title: `Deleting story "${this.story.name}"`,
          text: 'Are you sure you want to delete this story? This action cannot be undone. ',
        },
      })
      .afterClosed()
      .subscribe((actionTaken: boolean) => {
        if (actionTaken) {
          this.graphql
            .mutate(this.deleteStory, { id: this.story.id })
            .toPromise()
            .then(() => {
              this.project.storiesByProject.nodes.splice(
                this.project.storiesByProject.nodes.findIndex((story: Story) => story.id === this.story.id),
                1
              );
              this.project.storiesByProject.totalCount--;
              this.projectStorage.flush(this.project);
              if (window.history?.state?.redirect) {
                this.router.navigateByUrl(window.history.state.redirect).catch(console.error);
              } else {
                this.router.navigate(['management', this.project.id]).catch(console.error);
              }
            })
            .catch(console.error);
        }
      });
  }

  private displayStory() {
    this.mode = 'DISPLAY';
  }

  private editStory() {
    this.mode = 'EDIT';
  }
}
