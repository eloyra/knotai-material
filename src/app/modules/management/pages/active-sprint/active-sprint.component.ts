import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Project, Sprint, State, Story, Task } from 'src/app/services/api';
import { ProgressService } from 'src/app/services/statistics/progress.service';
import { ActiveSprintStorageService } from 'src/app/services/storage/active-sprint-storage.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { StoryFormComponent } from 'src/app/shared/forms/story-form/story-form.component';
import { TaskFormComponent } from 'src/app/shared/forms/task-form/task-form.component';

@Component({
  selector: 'k-active-sprint',
  templateUrl: './active-sprint.component.html',
  styleUrls: ['./active-sprint.component.scss'],
})
export class ActiveSprintComponent implements OnInit {
  public error: any = null;
  public loading = false;
  public project: Project = null;
  public activeSprint: Sprint = null;
  public sprintStories: Story[] = [];
  public storiesByStatus: any = {
    [State.New]: [],
    [State.Ongoing]: [],
    [State.Blocked]: [],
    [State.Finished]: [],
  };
  public sprintTasks: Task[] = [];
  public tasksByStatus: any = {
    [State.New]: [],
    [State.Ongoing]: [],
    [State.Blocked]: [],
    [State.Finished]: [],
  };
  public activeTab = 0;

  constructor(
    private titleService: Title,
    private route: ActivatedRoute,
    private router: Router,
    private projectStorage: ProjectStorageService,
    private activeSprintStorage: ActiveSprintStorageService,
    public dialog: MatDialog
  ) {
    this.titleService.setTitle('Active sprint');
  }

  ngOnInit(): void {
    this.loading = true;
    this.route.queryParams.subscribe((params: Params) => {
      if (params?.tab) {
        this.activeTab = params.tab;
      }
    });
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        if (project) {
          this.project = project;
          this.buildData();
        }
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  goToNewSprint() {
    if (this.route.snapshot.params?.project) {
      this.router
        .navigate(['management', this.route.snapshot.params.project, 'sprints', 'new'], {
          state: { redirect: this.router.url },
        })
        .catch(console.error);
    }
  }

  /*openNewSprintDialog() {
    this.dialog
      .open(SprintFormComponent, {
        width: '60%',
        height: 'auto',
      })
      .afterClosed()
      .subscribe(console.log);
  }*/

  openNewStoryDialog() {
    this.dialog
      .open(StoryFormComponent, {
        width: '60%',
        height: 'auto',
        data: { defaultSprint: this.activeSprint },
      })
      .afterClosed()
      .subscribe(console.log);
  }

  openNewTaskDialog() {
    this.dialog
      .open(TaskFormComponent, {
        width: '60%',
        height: 'auto',
        data: { defaultSprint: this.activeSprint },
      })
      .afterClosed()
      .subscribe(console.log);
  }

  handleTabChange(event: MatTabChangeEvent): void {
    this.activeTab = event.index;
  }

  calculateCurrentSprintDeadline(): string {
    const deadline = ProgressService.getSprintDeadlineMilliseconds({ sprint: this.activeSprint });

    return deadline ? new Date(deadline).toISOString() : null;
  }

  private buildData(activeSprint: Sprint = null) {
    if (this.project?.sprintsByProject?.totalCount) {
      if (activeSprint) {
        this.project.sprintsByProject.nodes.find((sprint: Sprint) => sprint.active).active = false;
        const newActiveSprint = this.project.sprintsByProject.nodes.find(
          (sprint: Sprint) => sprint.id === activeSprint.id
        );
        newActiveSprint.active = activeSprint.active;
        newActiveSprint.activationTime = activeSprint.activationTime;
        this.activeSprint = newActiveSprint;
      } else {
        this.activeSprint = this.activeSprintStorage.findActiveSprintInProject(this.project);
      }

      if (this.activeSprint && this.project?.storiesByProject?.totalCount) {
        this.sprintStories = this.project.storiesByProject.nodes.filter(
          (story: Story) => story.sprint === this.activeSprint.id
        );
        this.storiesByStatus[State.New] = this.getNewStoriesForCurrentSprint();
        this.storiesByStatus[State.Ongoing] = this.getOngoingStoriesForCurrentSprint();
        this.storiesByStatus[State.Blocked] = this.getBlockedStoriesForCurrentSprint();
        this.storiesByStatus[State.Finished] = this.getFinishedStoriesForCurrentSprint();
      }

      if (this.activeSprint && this.sprintStories.length) {
        this.sprintTasks = [];
        this.sprintStories.forEach((story: Story) => {
          if (story?.tasksByStory?.totalCount) {
            this.sprintTasks.push(...story.tasksByStory.nodes);
          }
        });
        this.tasksByStatus[State.New] = this.getNewTasksForCurrentSprint();
        this.tasksByStatus[State.Ongoing] = this.getOngoingTasksForCurrentSprint();
        this.tasksByStatus[State.Blocked] = this.getBlockedTasksForCurrentSprint();
        this.tasksByStatus[State.Finished] = this.getFinishedTasksForCurrentSprint();
      }
    }
  }

  private getNewStoriesForCurrentSprint(): Story[] {
    if (this.sprintStories.length) {
      return this.sprintStories.filter((story: Story) => story?.state === State.New);
    }
    return [];
  }

  private getOngoingStoriesForCurrentSprint(): Story[] {
    if (this.sprintStories.length) {
      return this.sprintStories.filter((story: Story) => story?.state === State.Ongoing);
    }
    return [];
  }

  private getBlockedStoriesForCurrentSprint(): Story[] {
    if (this.sprintStories.length) {
      return this.sprintStories.filter((story: Story) => story?.state === State.Blocked);
    }
    return [];
  }

  private getFinishedStoriesForCurrentSprint(): Story[] {
    if (this.sprintStories.length) {
      return this.sprintStories.filter((story: Story) => story?.state === State.Finished);
    }
    return [];
  }

  private getNewTasksForCurrentSprint(): Task[] {
    if (this.sprintTasks.length) {
      return this.sprintTasks.filter((task: Task) => task?.state === State.New);
    }
    return [];
  }

  private getOngoingTasksForCurrentSprint(): Task[] {
    if (this.sprintTasks.length) {
      return this.sprintTasks.filter((task: Task) => task?.state === State.Ongoing);
    }
    return [];
  }

  private getBlockedTasksForCurrentSprint(): Task[] {
    if (this.sprintTasks.length) {
      return this.sprintTasks.filter((task: Task) => task?.state === State.Blocked);
    }
    return [];
  }

  private getFinishedTasksForCurrentSprint(): Task[] {
    if (this.sprintTasks.length) {
      return this.sprintTasks.filter((task: Task) => task?.state === State.Finished);
    }
    return [];
  }
}
