import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteTaskGQL, Project, Story, Task } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';

type DetailsMode = 'DISPLAY' | 'EDIT';

@Component({
  selector: 'k-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss'],
})
export class TaskDetailComponent implements OnInit {
  public project: Project;
  public task: Task;

  public loading = false;
  public error: any;

  public mode: DetailsMode = 'DISPLAY';

  constructor(
    private titleService: Title,
    public dialog: MatDialog,
    private projectStorage: ProjectStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private graphql: GraphqlService,
    private deleteTask: DeleteTaskGQL
  ) {
    this.titleService.setTitle('Task details');
  }

  ngOnInit(): void {
    this.loading = true;
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        if (project) {
          this.project = project;
          this.task = this.project.storiesByProject.nodes
            .find((story: Story) => story.id === this.route.snapshot.params.story)
            .tasksByStory.nodes.find((task: Task) => task.id === this.route.snapshot.params.task);
        }
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  toggleMode() {
    if (this.mode === 'DISPLAY') {
      this.editTask();
    } else {
      this.displayTask();
    }
  }

  editionSubmitted() {
    this.mode = 'DISPLAY';
  }

  handleDelete() {
    this.dialog
      .open(ConfirmationDialogComponent, {
        width: '40%',
        height: 'auto',
        data: {
          title: `Deleting task "${this.task.name}"`,
          text: 'Are you sure you want to delete this task? This action cannot be undone. ',
        },
      })
      .afterClosed()
      .subscribe((actionTaken: boolean) => {
        if (actionTaken) {
          this.graphql
            .mutate(this.deleteTask, { id: this.task.id })
            .toPromise()
            .then(() => {
              this.project.tasksByProject.nodes.splice(
                this.project.tasksByProject.nodes.findIndex((task: Task) => task.id === this.task.id),
                1
              );
              this.project.tasksByProject.totalCount--;
              this.projectStorage.flush(this.project);
              if (window.history?.state?.redirect) {
                this.router.navigateByUrl(window.history.state.redirect).catch(console.error);
              } else {
                this.router.navigate(['management', this.project.id]).catch(console.error);
              }
            })
            .catch(console.error);
        }
      });
  }

  private displayTask() {
    this.mode = 'DISPLAY';
  }

  private editTask() {
    this.mode = 'EDIT';
  }
}
