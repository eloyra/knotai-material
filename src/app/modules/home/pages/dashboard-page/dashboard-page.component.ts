import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import {
  GetProjectsInDepthGQL,
  GetStoriesGQL,
  GetUserProfilesGQL,
  Project,
  ProjectsConnection,
  ProjectsEdge,
  State,
  StoriesConnection,
  StoriesEdge,
  Story,
  Task,
  UserProfile,
  UserProfilesConnection,
  UserProfilesEdge,
} from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProgressService } from 'src/app/services/statistics/progress.service';
import { ActiveSprintStorageService } from 'src/app/services/storage/active-sprint-storage.service';

@Component({
  selector: 'k-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
})
export class DashboardPageComponent implements OnInit {
  public error: any = null;
  public loading = false;

  public projects: Project[] = [];
  public people: UserProfile[] = [];
  public stories: Story[] = [];

  constructor(
    private titleService: Title,
    private activeSprintService: ActiveSprintStorageService,
    private graphql: GraphqlService,
    private getProjects: GetProjectsInDepthGQL,
    private getUsers: GetUserProfilesGQL,
    private getStories: GetStoriesGQL
  ) {
    this.titleService.setTitle('Dashboard');
  }

  ngOnInit(): void {
    this.loading = true;
    this.graphql.query(this.getProjects).subscribe(
      (connection: ProjectsConnection) => {
        this.projects = connection.edges
          .map((edge: ProjectsEdge) => edge.node)
          .filter((project: Project) => {
            const activeSprint = this.activeSprintService.findActiveSprintInProject(project);

            if (activeSprint) {
              const { color } = ProgressService.forSprint({
                sprint: activeSprint,
                velocity: project.config?.velocity ?? 4,
              });

              return color === 'red' || color === 'yellow';
            }

            return false;
          });

        this.graphql.query(this.getUsers).subscribe(
          (connection: UserProfilesConnection) => {
            this.people = connection.edges
              .map((edge: UserProfilesEdge) => edge.node)
              .filter((user: UserProfile) => user.roleByRole && user.roleByRole.name === 'dev')
              .filter(
                (user: UserProfile) =>
                  !(
                    user?.storiesByAssignee?.totalCount &&
                    user.storiesByAssignee.nodes.filter((story: Story) => story.state !== State.Finished).length
                  ) &&
                  !(
                    user?.tasksByAssignee?.totalCount &&
                    user.tasksByAssignee.nodes.filter((task: Task) => task.state !== State.Finished)
                  )
              );

            this.graphql.query(this.getStories).subscribe(
              (connection: StoriesConnection) => {
                this.stories = connection.edges
                  .map((edge: StoriesEdge) => edge.node)
                  .filter((story: Story) => story?.state === State.Blocked);

                this.loading = false;
              },
              (error) => {
                this.error = error;
                this.loading = false;
              }
            );
          },
          (error) => {
            this.error = error;
            this.loading = false;
          }
        );
      },
      (error) => {
        this.error = error;
        this.loading = false;
      }
    );
  }
}
