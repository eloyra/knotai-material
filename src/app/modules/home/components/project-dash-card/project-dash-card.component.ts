import { Component, Input, OnInit } from '@angular/core';
import { Member, Project, Sprint, State, Story, Task } from 'src/app/services/api';
import { ProgressService } from 'src/app/services/statistics/progress.service';
import { environment } from 'src/environments';

@Component({
  selector: 'k-project-dash-card',
  templateUrl: './project-dash-card.component.html',
  styleUrls: ['./project-dash-card.component.scss'],
})
export class ProjectDashCardComponent implements OnInit {
  @Input() project: Project;
  public currentSprint: Sprint;
  public manager: string = null;
  public consumedHours: number = null;
  public progress: number = null;
  public burndown: string = null;
  public loading = false;

  public githubLogo: string = environment.deployUrl + environment.imagesPath + 'git-logo.png';

  constructor() {}

  ngOnInit() {
    this.loading = true;
    this.currentSprint = this.getActiveSprint(this.project?.sprintsByProject?.nodes);
    this.manager = this.getManagerName(this.project.membersByProject.nodes);
    if (this.project?.storiesByProject?.totalCount) {
      const { hoursDone, progress, color } = this.evaluateProgress(
        this.currentSprint,
        this.project.storiesByProject.nodes,
        // @todo change this default value
        this.project?.config?.velocity ?? 4
      );
      this.consumedHours = hoursDone;
      this.progress = progress;
      this.burndown = color;
    }
    this.loading = false;
  }

  private getManagerName(members: Member[]): string {
    return members.find(
      (member: Member) => member?.userProfileByUserProfile?.roleByRole?.name === environment.managerRole
    )?.userProfileByUserProfile?.fullName;
  }

  private evaluateProgress(sprint: Sprint, stories: Story[], velocity: number): any {
    const { hoursDone } = this.calculateHours(stories);

    const { progress, color } = ProgressService.forSprint({ sprint, velocity });

    return {
      hoursDone: Math.round(hoursDone + Number.EPSILON),
      progress,
      color,
    };
  }

  /*private evaluateProgress(stories: Story[]): any {
    const { hoursDone, hoursPending } = this.calculateHours(stories);

    const idealHoursDone = this.project.start
      ? // TODO Mantener lo de solo 8 horas al dia?
        ((Date.now() - new Date(this.project.start).getTime()) / 60 / 60 / 1000) * 0.333 // only 8 hours of work per day
      : 0;

    return {
      hoursDone: Math.round(hoursDone + Number.EPSILON),
      progress: Math.round(hoursDone / (hoursDone + hoursPending) + Number.EPSILON) * 100,
      burndown: ProjectCardComponent.calculateBurndown(hoursDone, idealHoursDone),
    };
  }*/

  private getActiveSprint(sprints: Sprint[]): Sprint {
    if (!sprints) return null;
    return sprints.find((sprint: Sprint) => sprint.active);
  }

  private calculateHours(stories: Story[]): any {
    let pendingPoints = 0;
    let finishedPoints = 0;

    if (stories.length) {
      stories.forEach((story: Story) => {
        if (story?.tasksByStory?.totalCount) {
          pendingPoints = story.tasksByStory.nodes
            .filter((task: Task) => task.state !== State.Finished)
            .reduce((accumulator: number, currentValue: Task) => accumulator + +currentValue.points, 0);
          finishedPoints = story.tasksByStory.nodes
            .filter((task: Task) => task.state === State.Finished)
            .reduce((accumulator: number, currentValue: Task) => accumulator + +currentValue.points, 0);
        }
      });
    }

    return {
      hoursDone: finishedPoints ? finishedPoints * environment.storyPointDuration : 0,
      hoursPending: pendingPoints ? pendingPoints * environment.storyPointDuration : 0,
    };
  }

  /*private static calculateBurndown(hoursDone: number, idealHoursDone: number): string {
    if (idealHoursDone === null || hoursDone === null) return 'blue';
    if (idealHoursDone * 0.75 > hoursDone) return 'red';
    if (idealHoursDone * 0.9 > hoursDone) return 'yellow';
    return 'green';
  }*/
}
