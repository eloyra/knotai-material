import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatChipsModule } from '@angular/material/chips';
import { MatListModule } from '@angular/material/list';
import { ContextMenuModule } from 'ngx-contextmenu';
import { ManagementModule } from 'src/app/modules/management/management.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { HomeRoutingModule } from './home-routing.module';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { StoryDashListComponent } from './components/story-dash-list/story-dash-list.component';
import { ProjectDashCardComponent } from './components/project-dash-card/project-dash-card.component';

@NgModule({
  declarations: [DashboardPageComponent, StoryDashListComponent, ProjectDashCardComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    MatListModule,
    ManagementModule,
    MatChipsModule,
    DragDropModule,
    ContextMenuModule,
  ],
  exports: [StoryDashListComponent, ProjectDashCardComponent],
})
export class HomeModule {}
