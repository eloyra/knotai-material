import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { filter } from 'rxjs/operators';
import { environment } from 'src/environments';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public title = 'Knotai';
  public logo = environment.deployUrl + environment.imagesPath + 'logo.png';
  public isLoginPage = false;

  constructor(private router: Router) {}

  ngOnInit() {
    console.log(this.router, this.router.routerState.snapshot.url);
    this.router.events
      .pipe(filter((event: RouterEvent) => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        this.isLoginPage = event.url === '/login';
      });
  }
}
