import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from 'src/app/login/login-page/login-page.component';
import { AuthGuardService } from 'src/app/services/auth/auth-guard.service';
import { LoginGuardService } from 'src/app/services/auth/login-guard.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginPageComponent,
    canActivate: [LoginGuardService],
  },
  {
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then((h) => h.HomeModule),
    canLoad: [AuthGuardService],
  },
  {
    path: 'administration',
    loadChildren: () => import('./modules/administration/administration.module').then((a) => a.AdministrationModule),
    canLoad: [AuthGuardService],
  },
  {
    path: 'management',
    loadChildren: () => import('./modules/management/management.module').then((m) => m.ManagementModule),
    canLoad: [AuthGuardService],
  },
  {
    path: '**',
    redirectTo: 'home',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled', enableTracing: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
