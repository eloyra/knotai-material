import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { SimplemdeModule } from 'ngx-simplemde';
import { DefaultImageDirective } from 'src/app/shared/directives/default-image.directive';
import { TopnavComponent } from './navigation/topnav/topnav.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { SidenavComponent } from './navigation/sidenav/sidenav.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconComponent } from './components/mat-icon/mat-icon.component';
import { TaskTypeChipComponent } from './components/task-type-chip/task-type-chip.component';
import { AvatarComponent } from './components/avatar/avatar.component';
import { StoryFormComponent } from './forms/story-form/story-form.component';
import { FormlyMatDatepickerModule } from '@ngx-formly/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { LoadButtonComponent } from './components/load-button/load-button.component';
import { SprintChipComponent } from './components/sprint-chip/sprint-chip.component';
import { CircleProgressComponent } from './components/circle-progress/circle-progress.component';
import { SprintFormComponent } from './forms/sprint-form/sprint-form.component';
import { LoginFormComponent } from '../login/login-form/login-form.component';
import { ProjectFormComponent } from './forms/project-form/project-form.component';
import { TimeEntryFormComponent } from './forms/time-entry-form/time-entry-form.component';
import { FormlyWrapperAddonsComponent } from './forms/formly/formly-wrapper-addons/formly-wrapper-addons.component';
import { TaskFormComponent } from './forms/task-form/task-form.component';
import { ActivateSprintFormComponent } from './forms/activate-sprint-form/activate-sprint-form.component';
import { MembersSelectorComponent } from './forms/members-selector/members-selector.component';
import { StoryChipComponent } from './components/story-chip/story-chip.component';
import { WorkloadFormComponent } from './forms/workload-form/workload-form.component';
import { PageLoaderComponent } from './components/page-loader/page-loader.component';
import { UserFormComponent } from './forms/user-form/user-form.component';

@NgModule({
  declarations: [
    TopnavComponent,
    SidenavComponent,
    MatIconComponent,
    TaskTypeChipComponent,
    DefaultImageDirective,
    AvatarComponent,
    StoryFormComponent,
    ConfirmationDialogComponent,
    LoadButtonComponent,
    SprintChipComponent,
    CircleProgressComponent,
    SprintFormComponent,
    LoginFormComponent,
    ProjectFormComponent,
    TimeEntryFormComponent,
    FormlyWrapperAddonsComponent,
    TaskFormComponent,
    ActivateSprintFormComponent,
    MembersSelectorComponent,
    StoryChipComponent,
    StoryChipComponent,
    WorkloadFormComponent,
    PageLoaderComponent,
    UserFormComponent,
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    RouterModule,
    MatRippleModule,
    MatIconModule,
    MatSidenavModule,
    MatChipsModule,
    ReactiveFormsModule,
    FormlyModule,
    FormlyMaterialModule,
    MatButtonModule,
    FormlyMatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    NgCircleProgressModule,
    SimplemdeModule.forRoot({}),
    FormsModule,
  ],
  exports: [
    TopnavComponent,
    SidenavComponent,
    MatSidenavModule,
    MatIconComponent,
    TaskTypeChipComponent,
    DefaultImageDirective,
    AvatarComponent,
    StoryFormComponent,
    ConfirmationDialogComponent,
    LoadButtonComponent,
    SprintChipComponent,
    CircleProgressComponent,
    SprintFormComponent,
    LoginFormComponent,
    ProjectFormComponent,
    TimeEntryFormComponent,
    FormlyWrapperAddonsComponent,
    TaskFormComponent,
    ActivateSprintFormComponent,
    MembersSelectorComponent,
    StoryChipComponent,
    StoryChipComponent,
    WorkloadFormComponent,
    PageLoaderComponent,
    UserFormComponent,
  ],
})
export class SharedModule {}
