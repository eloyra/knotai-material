import { Component, OnInit, Optional } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import {
  CreateMemberGQL,
  GetUserProfilesGQL,
  Member,
  Project,
  UserProfile,
  UserProfilesConnection,
  UserProfilesEdge,
} from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';

@Component({
  selector: 'k-members-selector',
  templateUrl: './members-selector.component.html',
  styleUrls: ['./members-selector.component.scss'],
})
export class MembersSelectorComponent implements OnInit {
  public loading = false;
  public fetching = false;
  public error: any = null;
  public form = new FormGroup({});
  public model: any = {};
  public fields: FormlyFieldConfig[] = [];

  private project: Project = null;
  private availableUsers: UserProfile[] = [];

  constructor(
    private projectStorage: ProjectStorageService,
    private route: ActivatedRoute,
    @Optional() public dialogRef: MatDialogRef<MembersSelectorComponent>,
    private graphql: GraphqlService,
    private createMember: CreateMemberGQL,
    private getUsers: GetUserProfilesGQL
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        this.project = project;

        const currentMembersIds = this.project?.membersByProject?.nodes?.map(
          (member: Member) => member?.userProfileByUserProfile?.id
        );

        this.graphql.query(this.getUsers).subscribe(
          (connection: UserProfilesConnection) => {
            this.availableUsers = connection.edges
              .map((edge: UserProfilesEdge) => edge.node)
              .filter((user: UserProfile) => !currentMembersIds.includes(user.id));

            this.buildForm();
          },
          (error) => {
            this.error = error;
            this.loading = false;
          }
        );

        this.buildForm();
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      }
    );
  }

  onSubmit(model) {
    this.addMembers(model).then((newMembers: UserProfile[]) => {
      if (this.dialogRef) {
        this.dialogRef.close(newMembers);
      }
    });
  }

  private buildForm() {
    this.fields = [
      {
        key: 'members',
        type: 'select',
        className: 'field',
        templateOptions: {
          multiple: true,
          label: 'Select new members',
          appearance: 'outline',
          color: 'accent',
          options: this.availableUsers.map((user: UserProfile) => ({
            value: user.id,
            label: user.fullName,
          })),
        },
      },
    ];
  }

  private addMembers(model: any): Promise<UserProfile[]> {
    const membersToAdd = model?.members.map((userId: string) =>
      this.graphql
        .mutate(this.createMember, {
          item: { userProfile: userId, project: this.project.id },
        })
        .toPromise()
    );

    return new Promise((resolve, reject) => {
      Promise.all(membersToAdd)
        .then((response: any[]) => {
          resolve(
            response.map((member: any) =>
              this.availableUsers.find((user: UserProfile) => user.id === member.userProfile)
            )
          );
        })
        .catch((reason) => {
          console.error(reason);
          reject([]);
        });
    });
  }
}
