import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Optional,
  Input,
  Output,
  EventEmitter,
  Inject,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { Project, UserProfile, Task, Story, UpdateStoryGQL, UpdateTaskGQL } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';

@Component({
  selector: 'k-workload-form',
  templateUrl: './workload-form.component.html',
  styleUrls: ['./workload-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WorkloadFormComponent implements OnInit {
  @Input() user: UserProfile = null;

  @Output() formSubmitted: EventEmitter<any> = new EventEmitter<any>();

  public loading = false;
  public fetching = false;
  public error: any = null;
  public form = new FormGroup({});
  public model: any = {};
  public fields: FormlyFieldConfig[] = [];

  private project: Project = null;
  private currentTasks: Task[] = [];
  private availableTasks: Task[] = [];
  private currentStories: Story[] = [];
  private availableStories: Story[] = [];

  constructor(
    private projectStorage: ProjectStorageService,
    private route: ActivatedRoute,
    @Optional() public dialogRef: MatDialogRef<WorkloadFormComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public injectedData: { user: UserProfile },
    private graphql: GraphqlService,
    private updateStory: UpdateStoryGQL,
    private updateTask: UpdateTaskGQL
  ) {}

  ngOnInit(): void {
    this.user = this.user ?? this.injectedData?.user;
    this.loading = true;

    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        this.project = project;

        if (this.user && this.project?.storiesByProject?.totalCount) {
          this.project.storiesByProject.nodes.forEach((story: Story) => {
            if (story?.assignee === this.user.id) this.currentStories.push(story);
            else if (!story?.assignee) this.availableStories.push(story);

            if (story?.tasksByStory?.totalCount) {
              story.tasksByStory.nodes.forEach((task: Task) => {
                if (task?.assignee === this.user.id) this.currentTasks.push(task);
                else if (!task?.assignee) this.availableTasks.push(task);
              });
            }
          });
        }

        this.buildForm();
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      }
    );
  }

  onSubmit(model) {
    this.updateWorkload(model).then((response: number) => {
      if (this.dialogRef) {
        this.dialogRef.close(response);
      }
      this.formSubmitted.emit();
    });
  }

  private buildForm() {
    this.fields = [
      {
        key: 'stories',
        type: 'select',
        className: 'field field-medium',
        defaultValue: this.currentStories.map((story: Story) => story.id),
        templateOptions: {
          multiple: true,
          label: 'Select stories',
          appearance: 'outline',
          color: 'accent',
          options: [
            ...this.availableStories.map((story: Story) => ({
              value: story.id,
              label: story.name,
            })),
            ...this.currentStories.map((story: Story) => ({
              value: story.id,
              label: story.name,
            })),
          ],
        },
      },
      {
        key: 'tasks',
        type: 'select',
        className: 'field field-medium',
        defaultValue: this.currentTasks.map((task: Task) => task.id),
        templateOptions: {
          multiple: true,
          label: 'Select tasks',
          appearance: 'outline',
          color: 'accent',
          options: [
            ...this.availableTasks.map((task: Task) => ({
              value: task.id,
              label: task.name,
            })),
            ...this.currentTasks.map((task: Task) => ({
              value: task.id,
              label: task.name,
            })),
          ],
        },
      },
    ];
  }

  private updateWorkload(model: any): Promise<number> {
    const storiesToAssign = this.availableStories.filter((story: Story) => model.stories.includes(story.id));
    const storiesToDeassign = this.currentStories.filter((story: Story) => !model.stories.includes(story.id));
    const tasksToAssign = this.availableTasks.filter((task: Task) => model.tasks.includes(task.id));
    const tasksToDeassign = this.currentTasks.filter((task: Task) => !model.tasks.includes(task.id));

    const promises = []
      .concat(
        storiesToAssign.map((story: Story) =>
          this.graphql
            .mutate(this.updateStory, {
              id: story.id,
              patch: { assignee: this.user.id },
            })
            .toPromise()
        )
      )
      .concat(
        storiesToDeassign.map((story: Story) =>
          this.graphql
            .mutate(this.updateStory, {
              id: story.id,
              patch: { assignee: null },
            })
            .toPromise()
        )
      )
      .concat(
        tasksToAssign.map((task: Task) =>
          this.graphql
            .mutate(this.updateTask, {
              id: task.id,
              patch: { assignee: this.user.id },
            })
            .toPromise()
        )
      )
      .concat(
        tasksToDeassign.map((task: Task) =>
          this.graphql
            .mutate(this.updateTask, {
              id: task.id,
              patch: { assignee: null },
            })
            .toPromise()
        )
      );

    return new Promise((resolve, reject) => {
      Promise.all(promises)
        .then((response: any[]) => {
          this.projectStorage.invalidate();
          resolve(
            response.reduce((workload, currentItem) => {
              if (currentItem?.__typename === 'Task') {
                return workload;
              }
              if (currentItem?.assignee) {
                return workload + currentItem.points;
              } else {
                return workload - currentItem.points;
              }
            }, 0)
          );
        })
        .catch((reason) => {
          console.error(reason);
          reject(0);
        });
    });
  }
}
