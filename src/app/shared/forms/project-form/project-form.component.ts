import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ChangeDetectorRef,
  Optional,
  Inject,
  OnChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { CreateProjectGQL, Priority, Project, State, UpdateProjectGQL } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { fakeDateHandling } from 'src/app/shared/utils';

enum FORM_MODES {
  CREATION = 'creation',
  UPDATE = 'update',
}

type FormMode = FORM_MODES.CREATION | FORM_MODES.UPDATE;

@Component({
  selector: 'k-project-form',
  templateUrl: './project-form.component.html',
  styleUrls: ['./project-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectFormComponent implements OnInit, OnChanges {
  @Input() project: Project;
  @Output() formSubmitted: EventEmitter<any> = new EventEmitter<any>();

  private mode: FormMode;

  public loading = true;
  public fetching = false;
  public error: any = null;
  public form = new FormGroup({});
  public model: any = {};
  public fields: FormlyFieldConfig[] = [];

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private projectStorage: ProjectStorageService,
    private route: ActivatedRoute,
    @Optional() public dialogRef: MatDialogRef<ProjectFormComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public injectedProject: Project,
    private graphql: GraphqlService,
    private createProject: CreateProjectGQL,
    private updateProject: UpdateProjectGQL
  ) {}

  private static getModelFromProject(project: Project): any {
    const model = { ...project };
    delete model.createdAt;
    delete model.updatedAt;
    delete model.weight;
    delete model.membersByProject;
    delete model.sprintsByProject;
    delete model.storiesByProject;
    delete model.__typename;

    return model;
  }

  ngOnInit(): void {
    this.setFormMode();
    this.buildForm();
    this.loading = false;
  }

  ngOnChanges(): void {
    this.buildForm();
  }

  onSubmit(model) {
    if (!this.form.dirty) return;
    if (this.mode === FORM_MODES.CREATION) {
      this.create(model).then((result) => {
        if (this.dialogRef) {
          this.dialogRef.close(result);
        }
        this.formSubmitted.emit();
      });
    } else if (this.mode === FORM_MODES.UPDATE) {
      this.update(model).then((result) => {
        if (this.dialogRef) {
          this.dialogRef.close(result);
        }
        this.formSubmitted.emit();
      });
    }
  }

  private setFormMode() {
    this.project = this.project ?? this.injectedProject;
    if (!this.project) {
      this.mode = FORM_MODES.CREATION;
    } else {
      this.mode = FORM_MODES.UPDATE;
    }
  }

  private buildForm() {
    if (this.mode === FORM_MODES.UPDATE) {
      this.model = ProjectFormComponent.getModelFromProject(this.project);
    }

    this.fields = [
      {
        key: 'name',
        type: 'input',
        className: 'field field-big',
        templateOptions: {
          type: 'text',
          label: 'Project name',
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'sprintDuration',
        type: 'input',
        className: 'field field-small',
        templateOptions: {
          type: 'number',
          min: 1,
          label: 'Sprint duration (in weeks)',
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'state',
        type: 'select',
        className: 'field field-medium',
        templateOptions: {
          label: 'State',
          options: Object.keys(State).map((key: string) => ({ value: State[key], label: key })),
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'priority',
        type: 'select',
        className: 'field field-medium',
        templateOptions: {
          label: 'Priority',
          options: Object.keys(Priority).map((key: string) => ({ value: Priority[key], label: key })),
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'start',
        type: 'datepicker',
        className: 'field field-medium',
        templateOptions: {
          label: 'Start date',
          appearance: 'outline',
          color: 'accent',
        },
        parsers: [fakeDateHandling],
      },
      /*{
        key: 'deadline',
        type: 'datepicker',
        className: 'field field-medium',
        templateOptions: {
          label: 'Deadline',
          appearance: 'outline',
          color: 'accent',
        },
        parsers: [fakeDateHandling],
      },*/
      {
        key: 'finish',
        type: 'datepicker',
        className: 'field field-medium',
        templateOptions: {
          label: 'Finished',
          appearance: 'outline',
          color: 'accent',
        },
        parsers: [fakeDateHandling],
      },
      {
        key: 'repository',
        type: 'input',
        className: 'field',
        templateOptions: {
          type: 'text',
          label: 'Repository',
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'description',
        type: 'textarea',
        className: 'field description-field',
        templateOptions: {
          label: 'Description',
          rows: 5,
          appearance: 'outline',
          color: 'accent',
        },
      },
    ];

    this.changeDetectorRef.markForCheck();
  }

  private create(model: Project): Promise<Project> {
    this.fetching = true;
    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.createProject, { item: model })
        .toPromise()
        .then((newProject: Project) => {
          this.fetching = false;
          resolve(newProject);
        })
        .catch((reason) => {
          console.error(reason);
          this.fetching = false;
          reject(null);
        });
    });
  }

  private update(model: Project): Promise<boolean> {
    this.fetching = true;
    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.updateProject, {
          id: this.project.id,
          patch: model,
        })
        .toPromise()
        .then(() => {
          this.projectStorage.flush();
          this.fetching = false;
          resolve(true);
        })
        .catch((reason) => {
          console.error(reason);
          this.fetching = false;
          reject(false);
        });
    });
  }
}
