import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnInit,
  Optional,
  Output,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormlyFieldConfig } from '@ngx-formly/core';
import {
  DeleteUserProfileGQL,
  GetRolesGQL,
  RegisterUserGQL,
  Role,
  RolesConnection,
  RolesEdge,
  UpdateUserProfileGQL,
  UserProfile,
} from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { environment } from 'src/environments';

enum FORM_MODES {
  CREATION = 'creation',
  UPDATE = 'update',
}

type FormMode = FORM_MODES.CREATION | FORM_MODES.UPDATE;

@Component({
  selector: 'k-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserFormComponent implements OnInit, OnChanges {
  @Input() user: UserProfile;
  @Output() formSubmitted: EventEmitter<any> = new EventEmitter<any>();

  private mode: FormMode;
  private roles: Role[];

  public loading = true;
  public fetching = false;
  public error: any = null;
  public form = new FormGroup({});
  public model: any = {};
  public fields: FormlyFieldConfig[] = [];
  public defaultImage: string = environment.deployUrl + environment.imagesPath + environment.defaultAvatar;
  public currentAvatar: string;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    @Optional() public dialogRef: MatDialogRef<UserFormComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public injectedData: { user: UserProfile },
    private graphql: GraphqlService,
    private registerUser: RegisterUserGQL,
    private updateUser: UpdateUserProfileGQL,
    private deleteUser: DeleteUserProfileGQL,
    private getRoles: GetRolesGQL
  ) {}

  ngOnInit(): void {
    this.user = this.user ?? this.injectedData?.user;
    console.warn(this.user);
    this.currentAvatar = this.user?.picture;
    this.setFormMode();
    this.graphql.query(this.getRoles).subscribe(
      (results: RolesConnection) => {
        if (results) {
          this.roles = results.edges.map((edge: RolesEdge) => edge.node);
          this.buildForm();
          this.loading = false;
        }
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      },
      () => {
        this.loading = false;
      }
    );
  }

  ngOnChanges(): void {
    this.buildForm();
  }

  onSubmit(model) {
    if (this.mode === FORM_MODES.CREATION) {
      this.create({ ...model, picture: this.currentAvatar }).then((result) => {
        if (this.dialogRef) {
          this.dialogRef.close(result);
        }
        this.formSubmitted.emit();
      });
    } else if (this.mode === FORM_MODES.UPDATE) {
      this.update({ ...model, picture: this.currentAvatar }).then((result) => {
        if (this.dialogRef) {
          this.dialogRef.close(result);
        }
        this.formSubmitted.emit();
      });
    }
  }

  private setFormMode() {
    if (!this.user) {
      this.mode = FORM_MODES.CREATION;
    } else {
      this.mode = FORM_MODES.UPDATE;
    }
  }

  private buildForm() {
    if (this.mode === FORM_MODES.UPDATE) {
      this.model = this.user;

      this.fields = [
        {
          key: 'firstName',
          type: 'input',
          className: 'field',
          templateOptions: {
            type: 'text',
            label: 'First Name',
            required: true,
            appearance: 'outline',
            color: 'accent',
          },
        },
        {
          key: 'lastName',
          type: 'input',
          className: 'field',
          templateOptions: {
            type: 'text',
            label: 'Last Name',
            required: true,
            appearance: 'outline',
            color: 'accent',
          },
        },
        {
          key: 'role',
          type: 'select',
          className: 'field field-small',
          defaultValue: this.roles.find((role: Role) => role.id === this.user.role)?.id,
          templateOptions: {
            label: 'Role',
            options: this.roles.map((role: Role) => ({ value: role.id, label: role.name })),
            required: true,
            appearance: 'outline',
            color: 'accent',
          },
        },
      ];
    }

    if (this.mode === FORM_MODES.CREATION) {
      this.fields = [
        {
          key: 'firstName',
          type: 'input',
          className: 'field',
          templateOptions: {
            type: 'text',
            label: 'First Name',
            required: true,
            appearance: 'outline',
            color: 'accent',
          },
        },
        {
          key: 'lastName',
          type: 'input',
          className: 'field',
          templateOptions: {
            type: 'text',
            label: 'Last Name',
            required: true,
            appearance: 'outline',
            color: 'accent',
          },
        },
        {
          key: 'email',
          type: 'input',
          className: 'field',
          templateOptions: {
            type: 'email',
            label: 'Email',
            required: true,
            appearance: 'outline',
            color: 'accent',
          },
        },
        {
          key: 'password',
          type: 'input',
          className: 'field',
          templateOptions: {
            type: 'password',
            label: 'Password',
            required: true,
            appearance: 'outline',
            color: 'accent',
          },
        },
        {
          key: 'role',
          type: 'select',
          className: 'field field-small',
          templateOptions: {
            label: 'Role',
            options: this.roles.map((role: Role) => ({ value: role.id, label: role.name })),
            required: true,
            appearance: 'outline',
            color: 'accent',
          },
        },
      ];
    }

    this.changeDetectorRef.markForCheck();
  }

  public uploadImage(imageInput: any): void {
    const image: File = imageInput.files[0];
    const reader = new FileReader();
    reader.onload = (_) => {
      this.currentAvatar = reader.result.toString();
      this.changeDetectorRef.markForCheck();
    };
    reader.readAsDataURL(image);
  }

  private create(model: any): Promise<UserProfile | any> {
    this.fetching = true;
    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.registerUser, {
          item: {
            firstNameInput: model.firstName,
            lastNameInput: model.lastName,
            emailInput: model.email,
            password: model.password,
          },
        })
        .subscribe(
          (newUser: UserProfile) => {
            if (newUser) {
              this.graphql
                .mutate(this.updateUser, {
                  id: newUser.id,
                  patch: {
                    role: model.role,
                    picture: model.picture,
                  },
                })
                .subscribe(
                  (updatedUser: UserProfile) => {
                    this.fetching = false;
                    resolve(updatedUser);
                  },
                  (error) => {
                    console.error(error);
                    this.error = error;
                    this.fetching = false;
                    reject(error);
                  }
                );
            } else {
              this.fetching = false;
              reject();
            }
          },
          (error) => {
            console.error(error);
            this.error = error;
            this.fetching = false;
            reject(error);
          }
        );
    });
  }

  private update(model: any): Promise<UserProfile | any> {
    this.fetching = true;
    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.updateUser, {
          id: this.user.id,
          patch: {
            firstName: model.firstName,
            lastName: model.lastName,
            role: model.role,
          },
        })
        .subscribe(
          (updatedUser: UserProfile) => {
            this.fetching = false;
            resolve(updatedUser);
          },
          (error) => {
            console.error(error);
            this.error = error;
            this.fetching = false;
            reject(error);
          }
        );
    });
  }
}
