import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ChangeDetectorRef,
  Optional,
  Inject,
  OnChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import {
  CreateTimeEntryGQL,
  TimeEntry,
  UpdateTimeEntryGQL,
  Project,
  Member,
  UserProfile,
  Task,
} from 'src/app/services/api';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { ActivatedRoute } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { AuthenticationService, JwtToken } from 'src/app/services/auth/authentication.service';
import { fakeDateHandling } from 'src/app/shared/utils';
import { environment } from 'src/environments';

enum FORM_MODES {
  CREATION = 'creation',
  UPDATE = 'update',
}

type FormMode = FORM_MODES.CREATION | FORM_MODES.UPDATE;

@Component({
  selector: 'k-time-entry-form',
  templateUrl: './time-entry-form.component.html',
  styleUrls: ['./time-entry-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimeEntryFormComponent implements OnInit, OnChanges {
  @Input() task: Task;
  @Input() timeEntry: TimeEntry;
  @Output() formSubmitted: EventEmitter<any> = new EventEmitter<any>();

  private project: Project;
  private author: UserProfile;
  private mode: FormMode;

  public loading = true;
  public fetching = false;
  public error: any = null;
  public form = new FormGroup({});
  public model: any = {};
  public fields: FormlyFieldConfig[] = [];

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private projectStorage: ProjectStorageService,
    private route: ActivatedRoute,
    private auth: AuthenticationService,
    @Optional() public dialogRef: MatDialogRef<TimeEntryFormComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public injectedData: { task: Task; timeEntry: TimeEntry },
    private graphql: GraphqlService,
    private createTimeEntry: CreateTimeEntryGQL,
    private updateTimeEntry: UpdateTimeEntryGQL
  ) {}

  ngOnInit(): void {
    this.task = this.task ?? this.injectedData?.task;
    this.timeEntry = this.timeEntry ?? this.injectedData?.timeEntry;
    this.setFormMode();
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        this.project = project;
        this.auth.getCurrentUser().subscribe(
          (author: UserProfile) => {
            this.author = author;
            this.buildForm();
            this.loading = false;
          },
          (error) => {
            console.error(error);
            this.error = error;
            this.loading = false;
          }
        );
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      }
    );
  }

  ngOnChanges(): void {
    this.buildForm();
  }

  onSubmit(model) {
    if (this.mode === FORM_MODES.CREATION) {
      this.create(model).then((result) => {
        if (this.dialogRef) {
          this.dialogRef.close(result);
        }
        this.formSubmitted.emit();
      });
    } else if (this.mode === FORM_MODES.UPDATE) {
      this.update(model).then((result) => {
        if (this.dialogRef) {
          this.dialogRef.close(result);
        }
        this.formSubmitted.emit();
      });
    }
  }

  private setFormMode() {
    if (!this.timeEntry) {
      this.mode = FORM_MODES.CREATION;
    } else {
      this.mode = FORM_MODES.UPDATE;
    }
  }

  private buildForm() {
    if (this.mode === FORM_MODES.UPDATE) {
      this.model = this.timeEntry;
    }

    this.fields = [
      {
        key: 'description',
        type: 'input',
        className: 'field',
        templateOptions: {
          type: 'text',
          label: 'Description',
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'time',
        type: 'datepicker',
        className: 'field field-small',
        templateOptions: {
          label: 'Date',
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
        parsers: [fakeDateHandling],
      },
      {
        key: 'hours',
        type: 'input',
        className: 'field field-small',
        templateOptions: {
          type: 'number',
          label: 'Time spent (hours)',
          required: true,
          appearance: 'outline',
          color: 'accent',
          min: 0,
        },
      },
      /*{
        key: 'progress',
        type: 'input',
        className: 'field field-small',
        templateOptions: {
          type: 'number',
          label: 'Progress made (%)',
          required: true,
          appearance: 'outline',
          color: 'accent',
          min: 0,
          max:
            100 -
              this.task?.timeEntriesByTask.nodes.reduce(
                (accumulated: number, currentValue: TimeEntry) => accumulated + +currentValue.progress,
                0
              ) ?? 0,
        },
      },*/
    ];

    if (
      (this.auth.getToken() as JwtToken)?.role_name === environment.adminRole &&
      this.project?.membersByProject?.totalCount
    ) {
      const possibleAuthors = this.project.membersByProject.nodes.map((member: Member) => ({
        value: member.userProfileByUserProfile.id,
        label: member.userProfileByUserProfile.fullName,
      }));
      this.fields.push({
        key: 'author',
        type: 'select',
        className: 'field field-small',
        templateOptions: {
          label: 'Author',
          options: possibleAuthors,
          appearance: 'outline',
          color: 'accent',
        },
      });
      if (possibleAuthors.find((author: any) => author.value === this.author.id)) {
        this.model.author = this.author.id;
      }
    }

    if (this.mode === FORM_MODES.CREATION) {
      this.model.time = Date.now();
    }
    this.changeDetectorRef.markForCheck();
  }

  private create(model: TimeEntry): Promise<TimeEntry> {
    this.fetching = true;
    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.createTimeEntry, {
          item: { ...model, author: model.author || this.author.id, task: this.task.id, progress: 0 },
        })
        .toPromise()
        .then((newEntry: TimeEntry) => {
          this.fetching = false;
          this.projectStorage.invalidate();
          resolve(newEntry);
        })
        .catch((reason) => {
          console.error(reason);
          this.fetching = false;
          reject(null);
        });
    });
  }

  private update(model: TimeEntry): Promise<TimeEntry> {
    delete model?.createdAt;
    delete model?.updatedAt;
    delete model?.userProfileByAuthor;
    delete model?.__typename;

    this.fetching = true;
    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.updateTimeEntry, {
          id: this.timeEntry.id,
          patch: model,
        })
        .toPromise()
        .then((updatedEntry: TimeEntry) => {
          this.fetching = false;
          this.projectStorage.invalidate();
          resolve(updatedEntry);
        })
        .catch((reason) => {
          console.error(reason);
          this.fetching = false;
          reject(null);
        });
    });
  }
}
