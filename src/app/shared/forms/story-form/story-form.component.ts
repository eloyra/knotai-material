import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ChangeDetectorRef,
  Optional,
  Inject,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { map } from 'rxjs/operators';
import {
  CreateStoryGQL,
  CreateTagbookGQL,
  DeleteTagbookGQL,
  GetTagsInDepthGQL,
  Member,
  Project,
  Sprint,
  State,
  Story,
  Tag,
  Tagbook,
  TagsConnection,
  TagsEdge,
  UpdateStoryGQL,
} from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ActiveSprintStorageService } from 'src/app/services/storage/active-sprint-storage.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { fakeDateHandling } from 'src/app/shared/utils';
import { environment } from 'src/environments';

enum FORM_MODES {
  CREATION = 'creation',
  UPDATE = 'update',
}

interface KeyValue {
  value: string;
  label: string;
}

type FormMode = FORM_MODES.CREATION | FORM_MODES.UPDATE;

@Component({
  selector: 'k-story-form',
  templateUrl: './story-form.component.html',
  styleUrls: ['./story-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StoryFormComponent implements OnInit {
  @Input() story: Story;
  @Input() defaultSprint: Sprint = null;
  @Input() withoutSprint = false;
  @Output() formSubmitted: EventEmitter<any> = new EventEmitter<any>();

  private project: Project;
  private activeSprint: Sprint;
  private sprints: KeyValue[] = [];
  private users: KeyValue[] = [];
  private tags: KeyValue[] = [];
  private mode: FormMode;

  public loading = true;
  public fetching = false;
  public error: any = null;
  public form = new FormGroup({});
  public model: any = {};
  public fields: FormlyFieldConfig[] = [];
  public hideSprint = false;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private projectStorage: ProjectStorageService,
    private activeSprintStorage: ActiveSprintStorageService,
    private route: ActivatedRoute,
    @Optional() public dialogRef: MatDialogRef<StoryFormComponent>,
    @Optional()
    @Inject(MAT_DIALOG_DATA)
    public injectedData: { story?: Story; defaultSprint?: Sprint; withoutSprint: boolean },
    private graphql: GraphqlService,
    private createStory: CreateStoryGQL,
    private updateStory: UpdateStoryGQL,
    private getTags: GetTagsInDepthGQL,
    private createTagbook: CreateTagbookGQL,
    private deleteTagbook: DeleteTagbookGQL
  ) {}

  ngOnInit(): void {
    this.setFormMode();
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        this.buildProjectData(project);
        this.loadTags().then(() => {
          this.activeSprint = this.activeSprintStorage.findActiveSprintInProject(project);
          this.buildForm();
          this.loading = false;

          /*this.activeSprintStorage.getListener(project).subscribe(
            (sprint: Sprint) => {
              this.activeSprint = sprint;
              this.buildForm();
              this.loading = false;
            },
            (error) => {
              console.error(error);
              this.buildForm();
              this.loading = false;
            }
          );*/
        });
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      }
    );
  }

  onSubmit(model) {
    if (this.mode === FORM_MODES.CREATION) {
      this.create(model).then((result) => {
        if (this.dialogRef) {
          this.dialogRef.close(result);
        }
        this.formSubmitted.emit();
      });
    } else if (this.mode === FORM_MODES.UPDATE) {
      this.update(model).then((result) => {
        if (this.dialogRef) {
          this.dialogRef.close(result);
        }
        this.formSubmitted.emit();
      });
    }
  }

  private setFormMode() {
    this.story = this.story ?? this.injectedData?.story;
    this.defaultSprint = this.defaultSprint ?? this.injectedData?.defaultSprint;
    this.withoutSprint = this.withoutSprint || this.injectedData?.withoutSprint;
    if (!this.story) {
      this.mode = FORM_MODES.CREATION;
    } else {
      this.mode = FORM_MODES.UPDATE;
    }
  }

  private buildProjectData(project) {
    this.project = project;

    if (project.sprintsByProject?.totalCount) {
      this.sprints = project.sprintsByProject?.nodes.map((sprint: Sprint) => ({
        value: sprint.id,
        label: sprint.name,
      }));
    }

    if (project.membersByProject?.totalCount) {
      this.users = project.membersByProject?.nodes.map((member: Member) => ({
        value: member.userProfileByUserProfile.id,
        label: member.userProfileByUserProfile.fullName,
      }));
    }
  }

  private loadTags(): Promise<void> {
    return new Promise((resolve) => {
      this.graphql
        .query(this.getTags)
        .pipe(map((response: TagsConnection) => response.edges.map((edge: TagsEdge) => edge.node)))
        .pipe(map((tags: Tag[]) => tags.map((tag: Tag) => ({ value: tag.id, label: tag.label }))))
        .subscribe(
          (tags: KeyValue[]) => {
            this.tags = tags;
            resolve();
          },
          () => {
            resolve();
          }
        );
    });
  }

  private buildForm() {
    if (this.mode === FORM_MODES.UPDATE) {
      this.model = this.getModelFromStory(this.story);
      this.model.tagbook = this.story.tagbooksByStory.nodes.map((tagbook: Tagbook) => tagbook.tagByTag.id);
    }

    this.fields = [
      {
        key: 'name',
        type: 'input',
        className: !!this.defaultSprint || !!this.activeSprint || this.withoutSprint ? 'field' : 'field field-medium',
        templateOptions: {
          type: 'text',
          label: 'Story name',
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'points',
        type: 'input',
        className: 'field field-small',
        templateOptions: {
          type: 'number',
          label: 'Points',
          min: 0,
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'sprint',
        type: 'select',
        className: 'field field-small',
        defaultValue: this.story?.sprint ?? this?.defaultSprint?.id ?? null,
        templateOptions: {
          label: 'Sprint',
          options: [{ value: null, label: '-' }].concat(this.sprints),
          appearance: 'outline',
          color: 'accent',
        },
        hideExpression: !!this.defaultSprint || !!this.activeSprint || this.withoutSprint,
      },
      {
        key: 'state',
        type: 'select',
        className: 'field field-small',
        defaultValue: State.New,
        templateOptions: {
          label: 'State',
          options: Object.keys(State).map((key: string) => ({ value: State[key], label: key })),
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },
      /*{
        key: 'taskType',
        type: 'select',
        className: 'field field-small',
        templateOptions: {
          label: 'Task type',
          options: Object.keys(TaskType).map((key: string) => ({ value: TaskType[key], label: key })),
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },*/
      /*{
        key: 'tagbook',
        type: 'select',
        className: 'field field-small',
        templateOptions: {
          multiple: true,
          label: 'Tags',
          options: this.tags,
          appearance: 'outline',
          color: 'accent',
        },
      },*/
      {
        key: 'assignee',
        type: 'select',
        className: 'field field-small',
        templateOptions: {
          label: 'Assignee',
          options: [{ value: null, label: '-' }].concat(this.users),
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'description',
        type: 'textarea',
        className: 'field description-field',
        defaultValue: this.mode === FORM_MODES.CREATION ? environment.storyDescriptionTemplate : '',
        templateOptions: {
          label: 'Description',
          multiline: true,
          rows: 5,
          appearance: 'outline',
          color: 'accent',
        },
      },
    ];

    this.changeDetectorRef.markForCheck();
  }

  private create(model: Story): Promise<Story> {
    this.fetching = true;
    const tagbook = (model as any)?.tagbook;
    const finishedDate = model.state === State.Finished ? fakeDateHandling(new Date()) : null;
    delete (model as any).tagbook;
    return new Promise((resolve, reject) => {
      this.graphql.mutate(this.createStory, { item: { ...model, finishedDate, project: this.project.id } }).subscribe(
        (newStory: Story) => {
          if (!tagbook?.length) {
            resolve(this.resolveMutation(newStory));
          } else {
            this.story = newStory;
            this.processTags(tagbook)
              .then((newTagbook: Tagbook[]) => {
                resolve(this.resolveMutation(newStory, newTagbook));
              })
              .catch((error) => {
                console.error(error);
                this.fetching = false;
                reject(null);
              });
          }
        },
        (error) => {
          console.error(error);
          this.fetching = false;
          reject(null);
        }
      );
    });
  }

  private update(model: Story): Promise<Story> {
    this.fetching = true;
    const tagbook = (model as any)?.tagbook;
    const finishedDate = model.state === State.Finished ? fakeDateHandling(new Date()) : null;
    delete (model as any).tagbook;
    delete (model as any).commentsByStory;
    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.updateStory, {
          id: this.story.id,
          patch: { ...model, finishedDate, project: this.project.id },
        })
        .subscribe(
          (newStory: Story) => {
            if (!tagbook?.length) {
              resolve(this.resolveMutation(newStory));
            } else {
              this.processTags(tagbook)
                .then((newTagbook: Tagbook[]) => {
                  resolve(this.resolveMutation(newStory, newTagbook));
                })
                .catch((error) => {
                  console.error(error);
                  this.fetching = false;
                  reject(null);
                });
            }
          },
          (error) => {
            console.error(error);
            this.fetching = false;
            reject(null);
          }
        );
    });
  }

  private processTags(selectedTags: string[]): Promise<Tagbook[]> {
    let tagsToSave: string[] = [...selectedTags];
    let tagbooksToDelete: string[] = [];
    if (this.mode === FORM_MODES.UPDATE) {
      const currentTagbook: any = this.story.tagbooksByStory.nodes.reduce(
        (currentTagbook: any, newItem: Tagbook) => ({
          ...currentTagbook,
          [newItem.tagByTag.id]: newItem.id,
        }),
        {}
      );
      tagsToSave = selectedTags.filter((tag: string) => !Object.keys(currentTagbook).includes(tag));
      tagbooksToDelete = Object.keys(currentTagbook)
        .filter((tag: string) => !selectedTags.includes(tag))
        .map((tagKey: string) => currentTagbook[tagKey]);
    }
    const promisesToWait = tagsToSave.map((tag) =>
      this.graphql.mutate(this.createTagbook, { item: { story: this.story.id, tag: tag } }).toPromise()
    );
    promisesToWait.concat(
      tagbooksToDelete.map((tagbook: string) => this.graphql.mutate(this.deleteTagbook, { id: tagbook }).toPromise())
    );
    return new Promise((resolve, reject) => {
      Promise.all(promisesToWait)
        .then((response: Tagbook[]) => {
          resolve(response);
        })
        .catch((reason) => {
          console.error(reason);
          reject(reason);
        });
    });
  }

  private resolveMutation(newStory: Story, newTagbook: Tagbook[] = []): Story {
    if (newTagbook.length) {
      if (!newStory.tagbooksByStory.nodes) newStory.tagbooksByStory.nodes = [];
      newStory.tagbooksByStory.nodes.push(...newTagbook);
      newStory.tagbooksByStory.totalCount = newStory.tagbooksByStory.totalCount + newTagbook.length;
    }
    if (this.mode === FORM_MODES.CREATION) {
      this.project.storiesByProject.nodes.push(newStory);
      this.project.storiesByProject.totalCount += 1;
    } else if (this.mode === FORM_MODES.UPDATE) {
      const currentIndex = this.project.storiesByProject.nodes.findIndex((story) => story.id === newStory.id);
      this.project.storiesByProject.nodes.splice(currentIndex, 1, newStory);
    }
    this.projectStorage.flush(this.project);
    this.fetching = false;
    return newStory;
  }

  // noinspection JSMethodCanBeStatic
  private getModelFromStory(story: Story): any {
    const model = { ...story };
    delete model?.sprintBySprint;
    delete model?.tagbooksByStory;
    delete model?.tasksByStory;
    delete model?.userProfileByAssignee;
    delete model?.__typename;
    return model;
  }
}
