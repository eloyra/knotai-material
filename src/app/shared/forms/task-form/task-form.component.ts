import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ChangeDetectorRef,
  Optional,
  Inject,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import {
  CreateTaskGQL,
  Member,
  Project,
  Sprint,
  State,
  Story,
  Task,
  TaskType,
  UpdateTaskGQL,
  UpdateUserProfileGQL,
} from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProgressService } from 'src/app/services/statistics/progress.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { environment } from 'src/environments';

enum FORM_MODES {
  CREATION = 'creation',
  UPDATE = 'update',
}

interface KeyValue {
  value: string;
  label: string;
}

type FormMode = FORM_MODES.CREATION | FORM_MODES.UPDATE;

@Component({
  selector: 'k-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskFormComponent implements OnInit {
  @Input() task: Task;
  @Input() story: Story;
  @Input() defaultSprint: Sprint;
  @Output() formSubmitted: EventEmitter<any> = new EventEmitter<any>();

  private project: Project;
  private stories: KeyValue[] = [];
  private users: KeyValue[] = [];
  private mode: FormMode;

  public loading = true;
  public fetching = false;
  public error: any = null;
  public form = new FormGroup({});
  public model: any = {};
  public fields: FormlyFieldConfig[] = [];

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private projectStorage: ProjectStorageService,
    private route: ActivatedRoute,
    @Optional() public dialogRef: MatDialogRef<TaskFormComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public injectedData: { task: Task; story: Story; defaultSprint: Sprint },
    private graphql: GraphqlService,
    private createTask: CreateTaskGQL,
    private updateTask: UpdateTaskGQL,
    private updateUser: UpdateUserProfileGQL
  ) {}

  ngOnInit(): void {
    this.task = this.task ?? this.injectedData?.task;
    this.story = this.story ?? this.injectedData?.story;
    this.defaultSprint = this.defaultSprint ?? this.injectedData?.defaultSprint;
    this.setFormMode();
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        this.buildProjectData(project);
        this.buildForm();
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      }
    );
  }

  onSubmit(model) {
    if (this.mode === FORM_MODES.CREATION) {
      this.create(model).then((result) => {
        if (this.dialogRef) {
          this.dialogRef.close(result);
        }
        this.formSubmitted.emit();
      });
    } else if (this.mode === FORM_MODES.UPDATE) {
      this.update(model).then((result) => {
        if (this.dialogRef) {
          this.dialogRef.close(result);
        }
        this.formSubmitted.emit();
      });
    }
  }

  private setFormMode() {
    if (!this.task) {
      this.mode = FORM_MODES.CREATION;
    } else {
      this.mode = FORM_MODES.UPDATE;
    }
  }

  private buildProjectData(project) {
    this.project = project;

    if (!this.story && project.storiesByProject?.totalCount) {
      if (this.defaultSprint) {
        this.stories = project.storiesByProject?.nodes
          .filter((story: Story) => story?.sprint === this.defaultSprint.id)
          .map((story: Story) => ({
            value: story.id,
            label: story.name,
          }));
      } else {
        this.stories = project.storiesByProject?.nodes.map((story: Story) => ({
          value: story.id,
          label: story.name,
        }));
      }
    }

    if (project.membersByProject?.totalCount) {
      this.users = project.membersByProject?.nodes.map((member: Member) => ({
        value: member.userProfileByUserProfile.id,
        label: member.userProfileByUserProfile.fullName,
        proficiency: member.userProfileByUserProfile.proficiency || {},
      }));
    }
  }

  private buildForm() {
    if (this.mode === FORM_MODES.UPDATE) {
      this.model = this.getModelFromTask(this.task);
    }

    this.fields = [
      {
        key: 'name',
        type: 'input',
        className: !!this.story ? 'field' : 'field field-big',
        templateOptions: {
          type: 'text',
          label: 'Task name',
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },
      /*{
        key: 'points',
        type: 'input',
        className: 'field field-small',
        templateOptions: {
          type: 'number',
          label: 'Points',
          min: 0,
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },*/
      /*{
        key: 'sprint',
        type: 'select',
        className: 'field field-small',
        templateOptions: {
          label: 'Sprint',
          options: [{ value: null, label: '-' }].concat(this.sprints),
          appearance: 'outline',
          color: 'accent',
        },
      },*/
      {
        key: 'state',
        type: 'select',
        className: 'field field-small',
        defaultValue: State.New,
        templateOptions: {
          label: 'State',
          options: Object.keys(State).map((key: string) => ({ value: State[key], label: key })),
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'taskType',
        type: 'select',
        className: 'field field-small',
        templateOptions: {
          label: 'Task type',
          options: Object.keys(TaskType).map((key: string) => ({ value: TaskType[key], label: key })),
          required: true,
          appearance: 'outline',
          color: 'accent',
        },
      },
      {
        key: 'story',
        type: 'select',
        className: 'field field-small',
        defaultValue: this.mode === FORM_MODES.CREATION ? this.story?.id : this.task?.story,
        templateOptions: {
          label: 'Story',
          options: [{ value: null, label: '-' }].concat(this.stories),
          appearance: 'outline',
          color: 'accent',
          required: true,
        },
        hideExpression: !!this.story,
      },
      {
        key: 'assignee',
        type: 'select',
        className: 'field field-small',
        defaultValue: this.task?.assignee,
        templateOptions: {
          label: 'Assignee',
          options: [{ value: null, label: '-' }].concat(this.users),
          appearance: 'outline',
          color: 'accent',
        },
        hooks: {
          // @todo improve recommendation system
          onInit: (field) => {
            this.form.get('taskType').valueChanges.subscribe((value: TaskType) => {
              field.templateOptions.options = [{ value: null, label: '-' }].concat(
                [...this.users].sort((a: any, b: any) => +b.proficiency?.[value] - +a.proficiency?.[value])
              );
            });
          },
        },
      },
      {
        key: 'description',
        type: 'textarea',
        className: 'field description-field',
        defaultValue: this.mode === FORM_MODES.CREATION ? environment.taskDescriptionTemplate : '',
        templateOptions: {
          label: 'Description',
          multiline: true,
          rows: 5,
          appearance: 'outline',
          color: 'accent',
        },
      },
    ];

    /*if (this.story) {
      this.fields.splice(
        this.fields.findIndex((field: FormlyFieldConfig) => field.key === 'story'),
        1
      );
    }*/

    this.changeDetectorRef.markForCheck();
  }

  private create(model: Task): Promise<Task> {
    this.fetching = true;
    let sprint = null;
    if (this.story) {
      sprint = this.project.storiesByProject.nodes.find((story: Story) => story.id === this.story.id)?.sprint;
    }
    if (model?.story && !this.story) {
      sprint = this.project.storiesByProject.nodes.find((story: Story) => story.id === model.story)?.sprint;
    }

    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.createTask, { item: { story: this.story?.id, ...model, project: this.project.id, sprint } })
        .toPromise()
        .then((newTask: Task) => {
          const newTaskHours = ProgressService.getHoursInTask(newTask);
          if (newTask?.assignee && newTask?.taskType && newTask?.state === State.Finished && newTaskHours !== null) {
            this.graphql.mutate(this.updateUser, {
              id: newTask.assignee,
              patch: {
                proficiency: {
                  ...newTask.userProfileByAssignee.proficiency,
                  [newTask.taskType]: newTaskHours + +newTask.userProfileByAssignee.proficiency[newTask.taskType],
                },
              },
            });
          }

          let story;
          if (this.story) {
            story = this.project.storiesByProject.nodes.find((story: Story) => story.id === this.story.id);
          } else if (model?.story) {
            story = this.project.storiesByProject.nodes.find((story: Story) => story.id === model.story);
          }
          if (story) {
            story.tasksByStory.nodes.push(newTask);
            story.tasksByStory.totalCount++;
            this.projectStorage.flush(this.project);
          }
          this.fetching = false;
          resolve(newTask);
        })
        .catch((reason) => {
          console.error(reason);
          this.fetching = false;
          reject(null);
        });
    });
  }

  private update(model: Task): Promise<Task> {
    this.fetching = true;
    let sprint = this.task.sprint;
    if (model?.story) {
      sprint = this.project.storiesByProject.nodes.find((story: Story) => story.id === model.story)?.sprint;
    }
    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.updateTask, {
          id: this.task.id,
          patch: { ...model, sprint },
        })
        .toPromise()
        .then((updatedTask: Task) => {
          const updatedTaskHours = ProgressService.getHoursInTask(updatedTask);
          if (
            updatedTask?.assignee &&
            updatedTask?.taskType &&
            updatedTask?.state === State.Finished &&
            updatedTaskHours !== null
          ) {
            this.graphql.mutate(this.updateUser, {
              id: updatedTask.assignee,
              patch: {
                proficiency: {
                  ...updatedTask.userProfileByAssignee.proficiency,
                  [updatedTask.taskType]:
                    updatedTaskHours + +updatedTask.userProfileByAssignee.proficiency[updatedTask.taskType],
                },
              },
            });
          }

          let story;
          if (this.story) {
            story = this.project.storiesByProject.nodes.find((story: Story) => story.id === this.story.id);
          } else if (model?.story) {
            story = this.project.storiesByProject.nodes.find((story: Story) => story.id === model.story);
          }
          if (story) {
            const currentTaskIndex = story.tasksByStory.nodes.findIndex((task) => task.id === updatedTask.id);
            story.tasksByStory.nodes.splice(currentTaskIndex, 1, updatedTask);
            this.projectStorage.flush(this.project);
          }
          this.fetching = false;
          resolve(updatedTask);
        })
        .catch((reason) => {
          console.error(reason);
          this.fetching = false;
          reject(null);
        });
    });
  }

  // noinspection JSMethodCanBeStatic
  private getModelFromTask(task: Task): any {
    const model = { ...task };
    delete model?.timeEntriesByTask;
    delete model?.commentsByTask;
    delete model?.userProfileByAssignee;
    delete model?.sprintBySprint;
    delete model?.storyByStory;
    delete model?.__typename;
    return model;
  }
}
