import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ChangeDetectorRef,
  Optional,
  Inject,
  OnChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { CreateSprintGQL, Project, Sprint, Story, UpdateSprintGQL, UpdateStoryGQL } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { fakeDateHandling } from 'src/app/shared/utils';

enum FORM_MODES {
  CREATION = 'creation',
  UPDATE = 'update',
}

type FormMode = FORM_MODES.CREATION | FORM_MODES.UPDATE;

@Component({
  selector: 'k-sprint-form',
  templateUrl: './sprint-form.component.html',
  styleUrls: ['./sprint-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintFormComponent implements OnInit, OnChanges {
  @Input() sprint: Sprint;
  @Input() noStories = false;
  @Input() delegate = false;
  @Input() disabled = false;
  @Output() formSubmitted: EventEmitter<any> = new EventEmitter<any>();

  private project: Project;
  private stories: Story[] = [];
  private mode: FormMode;

  public loading = true;
  public fetching = false;
  public error: any = null;
  public form = new FormGroup({});
  public model: any = {};
  public fields: FormlyFieldConfig[] = [];
  public selectedStories: Story[] = [];

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private projectStorage: ProjectStorageService,
    private route: ActivatedRoute,
    @Optional() public dialogRef: MatDialogRef<SprintFormComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public injectedSprint: Sprint,
    private graphql: GraphqlService,
    private createSprint: CreateSprintGQL,
    private updateSprint: UpdateSprintGQL,
    private updateStory: UpdateStoryGQL
  ) {}

  ngOnInit(): void {
    this.setFormMode();
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        this.buildProjectData(project);
        this.buildForm();
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      }
    );
  }

  ngOnChanges(): void {
    this.buildForm();
  }

  onSubmit(model) {
    if (this.delegate) {
      this.formSubmitted.emit(model);
    } else {
      if (this.mode === FORM_MODES.CREATION) {
        this.create(model).then((result) => {
          if (this.dialogRef) {
            this.dialogRef.close(result);
          }
          this.formSubmitted.emit(result);
        });
      } else if (this.mode === FORM_MODES.UPDATE) {
        this.update(model).then((result) => {
          if (this.dialogRef) {
            this.dialogRef.close(result);
          }
          this.formSubmitted.emit(result);
        });
      }
    }
  }

  private setFormMode() {
    this.sprint = this.sprint ?? this.injectedSprint;
    if (!this.sprint) {
      this.mode = FORM_MODES.CREATION;
    } else {
      this.mode = FORM_MODES.UPDATE;
    }
  }

  private buildProjectData(project) {
    this.project = project;

    if (project.storiesByProject?.totalCount) {
      this.stories = project.storiesByProject?.nodes
        .filter((story: Story) => !story?.sprint || story?.sprint === this.sprint?.id)
        .map((story: Story) => ({
          value: story.id,
          label: story.name,
          selected: story?.sprint === this.sprint?.id,
        }));
    }
  }

  private buildForm() {
    if (this.mode === FORM_MODES.UPDATE) {
      this.model = this.sprint;
    }

    this.fields = [
      {
        key: 'name',
        type: 'input',
        className: 'field',
        templateOptions: {
          type: 'text',
          label: 'Sprint name',
          required: true,
          appearance: 'outline',
          color: 'accent',
          disabled: this.disabled,
        },
      },
      /*{
        key: 'duration',
        type: 'input',
        className: 'field field-small',
        templateOptions: {
          type: 'number',
          label: 'Duration (in weeks)',
          appearance: 'outline',
          color: 'accent',
          min: 1,
          required: true,
        },
      },*/
      /*{
        key: 'status',
        type: 'select',
        className: 'field field-small',
        defaultValue: this.sprint?.active ? 'active' : this.sprint?.closed ? 'closed' : null,
        templateOptions: {
          multiple: false,
          label: 'Status',
          options: [
            {
              value: 'active',
              label: 'Active',
            },
            {
              value: 'closed',
              label: 'Closed',
            },
            { value: null, label: 'In backlog' },
          ],
          appearance: 'outline',
          color: 'accent',
        },
      },*/
      {
        key: 'stories',
        type: 'select',
        className: 'field field-small',
        templateOptions: {
          multiple: true,
          label: 'Stories',
          options: this.stories,
          appearance: 'outline',
          color: 'accent',
          disabled: this.disabled,
        },
      },
      {
        key: 'description',
        type: 'textarea',
        className: 'field description-field',
        templateOptions: {
          label: 'Description',
          multiline: true,
          rows: 5,
          appearance: 'outline',
          color: 'accent',
          disabled: this.disabled,
        },
      },
    ];

    if (this.noStories) {
      this.fields.splice(
        this.fields.findIndex((field) => field.key === 'stories'),
        1
      );
    } else {
      this.model.stories = this.stories.filter((story: any) => story.selected).map((story: any) => story.value);
    }
    this.changeDetectorRef.markForCheck();
  }

  private create(model: Sprint): Promise<boolean> {
    this.fetching = true;
    const selectedStories = !this.noStories ? (model as any)?.stories : [];
    if (!this.noStories) {
      delete (model as any).stories;
    }
    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.createSprint, {
          item: {
            ...model,
            duration: this.project?.sprintDuration || 2,
            active: true,
            activationTime: fakeDateHandling(new Date()),
            closed: null,
            project: this.project.id,
          },
        })
        .subscribe(
          (newSprint: Sprint) => {
            if (!selectedStories?.length) {
              resolve(this.resolveMutation(newSprint));
            } else {
              this.processStories(selectedStories, newSprint.id)
                .then((storiesInSprint: Story[]) => {
                  resolve(this.resolveMutation(newSprint, storiesInSprint));
                })
                .catch((error) => {
                  console.error(error);
                  this.fetching = false;
                  reject(false);
                });
            }
          },
          (error) => {
            console.error(error);
            this.fetching = false;
            reject(false);
          }
        );
    });
  }

  private update(model: Sprint): Promise<boolean> {
    this.fetching = true;
    const selectedStories = (model as any)?.stories;
    // const active = (model as any)?.status === 'active';
    // const activationTime = active ? this.sprint.activationTime : null;
    // const closed = (model as any)?.status === 'closed' ? model.closed : null;
    delete (model as any).stories;
    // delete (model as any).status;
    delete (model as any).storiesBySprint;
    delete (model as any).tasksBySprint;
    delete (model as any).__typename;
    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.updateSprint, {
          id: this.sprint.id,
          patch: { ...model, project: this.project.id },
        })
        .subscribe(
          (newSprint: Sprint) => {
            if (!selectedStories?.length) {
              resolve(this.resolveMutation(newSprint));
            } else {
              this.processStories(selectedStories, newSprint.id)
                .then((storiesInSprint: Story[]) => {
                  resolve(this.resolveMutation(newSprint, storiesInSprint));
                })
                .catch((error) => {
                  console.error(error);
                  this.fetching = false;
                  reject(false);
                });
            }
          },
          (error) => {
            console.error(error);
            this.fetching = false;
            reject(false);
          }
        );
    });
  }

  private processStories(selectedStories: string[], newSprintId: string): Promise<Story[]> {
    let storiesToAddSprint: string[] = [...selectedStories];
    let storiesToDeleteSprint: string[] = [];
    if (this.mode === FORM_MODES.UPDATE) {
      const currentStoriesInSprint: any = this.sprint.storiesBySprint.nodes.map((story: Story) => story.id);
      storiesToAddSprint = selectedStories.filter((story: string) => !currentStoriesInSprint.includes(story));
      storiesToDeleteSprint = currentStoriesInSprint.filter((story: string) => !selectedStories.includes(story));
    }
    const promisesToWait = storiesToAddSprint.map((story: string) =>
      this.graphql.mutate(this.updateStory, { id: story, patch: { sprint: newSprintId } }).toPromise()
    );
    promisesToWait.concat(
      storiesToDeleteSprint.map((story: string) =>
        this.graphql.mutate(this.updateStory, { id: story, patch: { sprint: null } }).toPromise()
      )
    );
    return new Promise((resolve, reject) => {
      Promise.all(promisesToWait)
        .then((response: Story[]) => {
          resolve(response);
        })
        .catch((reason) => {
          console.error(reason);
          reject(reason);
        });
    });
  }

  private resolveMutation(newSprint: Sprint, storiesInSprint: Story[] = []): boolean {
    if (storiesInSprint.length) {
      if (!newSprint.storiesBySprint) (newSprint.storiesBySprint as any) = {};
      if (!newSprint.storiesBySprint.nodes) newSprint.storiesBySprint.nodes = [];
      newSprint.storiesBySprint.nodes.push(...storiesInSprint);
      newSprint.storiesBySprint.totalCount = newSprint.storiesBySprint.totalCount + storiesInSprint.length;
    }
    if (this.mode === FORM_MODES.CREATION) {
      this.project.sprintsByProject.nodes.push(newSprint);
      this.project.sprintsByProject.totalCount += 1;
    } else if (this.mode === FORM_MODES.UPDATE) {
      const currentIndex = this.project.sprintsByProject.nodes.findIndex(
        (sprint: Sprint) => sprint.id === newSprint.id
      );
      this.project.sprintsByProject.nodes.splice(currentIndex, 1, newSprint);
    }
    this.projectStorage.flush();
    this.fetching = false;
    return true;
  }
}
