import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, Optional } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { Project, Sprint, UpdateSprintGQL } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';
import { ActiveSprintStorageService } from 'src/app/services/storage/active-sprint-storage.service';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';
import { fakeDateHandling } from 'src/app/shared/utils';

@Component({
  selector: 'k-activate-sprint-form',
  templateUrl: './activate-sprint-form.component.html',
  styleUrls: ['./activate-sprint-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivateSprintFormComponent implements OnInit {
  public loading = false;
  public fetching = false;
  public error: any = null;
  public form = new FormGroup({});
  public model: any = {};
  public fields: FormlyFieldConfig[] = [];
  public activeSprint: Sprint = null;

  private project: Project = null;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private projectStorage: ProjectStorageService,
    private activeSprintStorage: ActiveSprintStorageService,
    private route: ActivatedRoute,
    @Optional() public dialogRef: MatDialogRef<ActivateSprintFormComponent>,
    private graphql: GraphqlService,
    private updateSprint: UpdateSprintGQL
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.projectStorage.getListener(this.route).subscribe(
      (project: Project) => {
        this.project = project;
        this.activeSprint = this.project?.sprintsByProject?.nodes?.find((sprint: Sprint) => sprint.active);
        this.buildForm();
        this.loading = false;
      },
      (error) => {
        console.error(error);
        this.error = error;
        this.loading = false;
      }
    );
  }

  onSubmit(model) {
    if (model.sprint !== this.activeSprint.id) {
      this.update(model).then((result: Sprint) => {
        if (result) {
          this.activeSprintStorage.invalidate();
        }
        if (this.dialogRef) {
          this.dialogRef.close(result);
        }
      });
    }
    this.dialogRef.close(null);
  }

  private buildForm() {
    this.fields = [
      {
        key: 'sprint',
        type: 'select',
        className: 'field',
        templateOptions: {
          multiple: false,
          label: 'Next active sprint',
          appearance: 'outline',
          color: 'accent',
          options: this.project?.sprintsByProject?.nodes
            ?.filter((sprint: Sprint) => !sprint.active)
            .map((sprint: Sprint) => ({
              value: sprint.id,
              label: sprint.name,
            })),
        },
      },
    ];
  }

  private update(model: any): Promise<Sprint> {
    this.fetching = true;
    return new Promise((resolve, reject) => {
      this.graphql
        .mutate(this.updateSprint, {
          id: model.sprint,
          patch: { active: true, activationTime: fakeDateHandling(new Date()), closed: null },
        })
        .toPromise()
        .then((newSprint: Sprint) => {
          this.graphql
            .mutate(this.updateSprint, {
              id: this.activeSprint.id,
              patch: { active: false, closed: fakeDateHandling(new Date()) },
            })
            .toPromise()
            .then(() => {
              this.fetching = false;
            })
            .catch((error) => {
              console.error(error);
              this.fetching = false;
            });
          resolve(newSprint);
        })
        .catch((error) => {
          console.error(error);
          this.fetching = false;
          reject(null);
        });
    });
  }

  /*private purgeSprint(sprint: Sprint): any {
    const purged = { ...sprint };
    delete purged.__typename;
    delete purged.tasksBySprint;
    delete purged.createdAt;
    delete purged.updatedAt;
    delete (purged as any)?.stories;
    delete (purged as any)?.status;

    return purged;
  }*/
}
