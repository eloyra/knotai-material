import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: 'img[kDefaultImage]',
  host: {
    '[src]': 'src',
  },
})
export class DefaultImageDirective {
  @Input() src: string;
  @Input('kDefaultImage') default: string;

  constructor() {}

  @HostListener('error') onError() {
    this.src = this.default;
  }
}
