import { Component, OnInit, Input } from '@angular/core';
import { Story } from 'src/app/services/api';
import { ProgressService } from 'src/app/services/statistics/progress.service';

@Component({
  selector: 'k-circle-progress',
  templateUrl: './circle-progress.component.html',
  styleUrls: ['./circle-progress.component.scss'],
})
export class CircleProgressComponent implements OnInit {
  @Input() data;
  @Input() showTitle: boolean;
  @Input() titleColor: string;
  @Input() titleFontSize: string;
  @Input() showSubtitle: boolean;
  @Input() showUnits: boolean;
  @Input() showImage: boolean;
  @Input() showBackground: boolean;
  @Input() showInnerStroke: boolean;
  @Input() showZeroOuterStroke: boolean;
  @Input() outerStrokeWidth: number;
  @Input() outerStrokeColor: Function | string;
  @Input() innerStrokeWidth: number;
  @Input() innerStrokeColor: string;
  @Input() space: number;
  @Input() percent: Function | number;
  @Input() radius: number;
  @Input() decoy: boolean;

  public ready = false;
  public calculatedOuterStrokeColor: string;
  public calculatedPercent: number;

  constructor() {}

  ngOnInit(): void {
    if (this.data?.__typename === 'Story') {
      this.calculatedOuterStrokeColor = 'blue';
      this.calculatedPercent = ProgressService.forStory({ story: this.data });
    } else {
      this.calculatedOuterStrokeColor =
        typeof this.outerStrokeColor === 'function' ? this.outerStrokeColor(this.data) : this.outerStrokeColor;
      this.calculatedPercent = typeof this.percent === 'function' ? this.percent(this.data) : this.percent;
    }
    this.ready = true;
  }
}
