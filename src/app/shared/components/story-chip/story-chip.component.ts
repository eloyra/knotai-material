import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'k-story-chip',
  templateUrl: './story-chip.component.html',
  styleUrls: ['./story-chip.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StoryChipComponent {
  @Input() storyId: string;
  @Input() storyName: string;

  constructor(private router: Router, private route: ActivatedRoute) {}

  goto() {
    if (this.storyId) {
      this.router.navigate(['management', this.route.snapshot.params?.project, 'stories', this.storyId]).then();
    }
  }
}
