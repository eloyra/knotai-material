import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { environment } from 'src/environments';

@Component({
  selector: 'k-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AvatarComponent implements OnInit {
  @Input() picture: string = null;
  public defaultImage: string = environment.deployUrl + environment.imagesPath + environment.defaultAvatar;

  constructor() {}

  ngOnInit(): void {}
}
