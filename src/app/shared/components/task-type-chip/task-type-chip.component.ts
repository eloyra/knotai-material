import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

type TaskType = 'MANAGEMENT' | 'ANALYSIS' | 'DESIGN' | 'FRONTEND' | 'BACKEND' | 'TESTING' | 'SYSTEMS';

enum TaskLabel {
  MANAGEMENT = 'Manage',
  ANALYSIS = 'Analyze',
  DESIGN = 'Design',
  FRONTEND = 'Front',
  BACKEND = 'Back',
  TESTING = 'Test',
  SYSTEMS = 'Sys',
}

type Variant = 'icon' | 'chip';

interface TaskChip {
  icon: string;
  label: TaskLabel;
}

type TaskChipRegistry = {
  [key in TaskType]: TaskChip;
};

const TASK_CHIPS: TaskChipRegistry = {
  MANAGEMENT: {
    icon: 'date_range', //'schedule'
    label: TaskLabel.MANAGEMENT,
  },
  ANALYSIS: {
    icon: 'assessment',
    label: TaskLabel.ANALYSIS,
  },
  DESIGN: {
    icon: 'aspect_ratio', //'invert_colors'
    label: TaskLabel.DESIGN,
  },
  FRONTEND: {
    icon: 'important_devices',
    label: TaskLabel.FRONTEND,
  },
  BACKEND: {
    icon: 'code',
    label: TaskLabel.BACKEND,
  },
  TESTING: {
    icon: 'report_problem', //'perm_data_setting'
    label: TaskLabel.TESTING,
  },
  SYSTEMS: {
    icon: 'build',
    label: TaskLabel.SYSTEMS,
  },
};

@Component({
  selector: 'k-task-type-chip',
  templateUrl: './task-type-chip.component.html',
  styleUrls: ['./task-type-chip.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskTypeChipComponent {
  public taskChips: TaskChipRegistry = TASK_CHIPS;
  @Input() taskType: TaskType;
  @Input() variant: Variant;

  constructor() {}
}
