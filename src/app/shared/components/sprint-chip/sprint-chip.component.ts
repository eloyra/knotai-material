import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'k-sprint-chip',
  templateUrl: './sprint-chip.component.html',
  styleUrls: ['./sprint-chip.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SprintChipComponent {
  @Input() sprintId: string;
  @Input() sprintName: string;

  constructor(private router: Router, private route: ActivatedRoute) {}

  goto() {
    if (this.sprintId) {
      this.router.navigate(['management', this.route.snapshot.params?.project, 'sprints', this.sprintId]).then();
    }
  }
}
