import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'k-page-loader',
  templateUrl: './page-loader.component.html',
  styleUrls: ['./page-loader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageLoaderComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
