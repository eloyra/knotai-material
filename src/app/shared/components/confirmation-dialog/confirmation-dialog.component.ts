import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

const DEFAULT_CONFIRMATION_TEXTS = {
  confirm: 'Continue',
  reject: `I'll think about it`,
};

@Component({
  selector: 'k-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmationDialogComponent implements OnInit {
  public title: string;
  public text: string;
  public dialogTexts = DEFAULT_CONFIRMATION_TEXTS;

  constructor(@Inject(MAT_DIALOG_DATA) public entryData: { title: string; text: string }) {}

  ngOnInit() {
    this.title = this.entryData?.title;
    this.text = this.entryData?.text;
  }
}
