import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { ThemePalette } from '@angular/material/core';

@Component({
  selector: 'k-load-button',
  templateUrl: './load-button.component.html',
  styleUrls: ['./load-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadButtonComponent {
  @Input() color: ThemePalette = 'primary';
  @Input() type: string;
  @Input() disabled: boolean;
  @Input() loading: boolean;
  @Input() loaderColor: ThemePalette = 'accent';
  @Input() label: string = 'Submit';
  @Input() form: string = null;

  constructor() {}
}
