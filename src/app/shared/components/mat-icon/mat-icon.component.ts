import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'k-mat-icon',
  templateUrl: './mat-icon.component.html',
  styleUrls: ['./mat-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatIconComponent {
  @Input() icon: string;

  constructor() {}
}
