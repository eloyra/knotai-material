const fakeDateHandling = (date: Date): Date => {
  return new Date(date.valueOf() + 60 * 60 * 1000);
};

const isWeekend = (date?: Date): boolean => {
  let dayOfWeek;

  if (date) {
    dayOfWeek = date.getDay();
  } else {
    dayOfWeek = new Date().getDay();
  }

  return dayOfWeek === 6 || dayOfWeek === 0;
};

export { fakeDateHandling, isWeekend };
