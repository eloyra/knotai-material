import { Component, Input, OnInit } from '@angular/core';
import { LayoutService, LayoutState } from 'src/app/shared/navigation/layout.service';

export interface TopnavOption {
  icon: string;
  text?: string;
  route: string;
}

const TOPNAV_DEFAULT_OPTIONS: TopnavOption[] = [
  {
    icon: 'dashboard',
    text: 'Dashboard',
    route: '/home',
  },
  {
    icon: 'create',
    text: 'My projects',
    route: '/management',
  },
  {
    icon: 'inbox',
    text: 'Administration',
    route: '/administration',
  },
];

@Component({
  selector: 'k-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.scss'],
})
export class TopnavComponent implements OnInit {
  @Input() title: string;
  @Input() logo: URL;
  public topnavOptions: TopnavOption[] = TOPNAV_DEFAULT_OPTIONS;
  public sidenavLayoutState: LayoutState;

  constructor(private layout: LayoutService) {}

  ngOnInit(): void {
    this.layout.getSidenavState().subscribe((state) => (this.sidenavLayoutState = state));
  }
}
