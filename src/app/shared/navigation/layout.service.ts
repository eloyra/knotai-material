import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export enum LayoutState {
  'OPEN' = 'OPEN',
  'CLOSED' = 'CLOSED',
}

@Injectable({
  providedIn: 'root',
})
export class LayoutService {
  private sidenavState = LayoutState.OPEN;
  private sidenavState$$: BehaviorSubject<LayoutState>;

  constructor() {
    this.sidenavState$$ = new BehaviorSubject<LayoutState>(this.sidenavState);
  }

  public setSidenavState($state: LayoutState) {
    this.sidenavState = LayoutState[$state];
    this.sidenavState$$.next(this.sidenavState);
  }

  public toggleSidenav() {
    if (this.sidenavState === LayoutState.OPEN) {
      this.setSidenavState(LayoutState.CLOSED);
    } else if (this.sidenavState === LayoutState.CLOSED) {
      this.setSidenavState(LayoutState.OPEN);
    }
  }

  public getSidenavState() {
    return this.sidenavState$$.asObservable();
  }
}
