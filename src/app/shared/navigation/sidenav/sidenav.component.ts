import { Component, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { LayoutService, LayoutState } from 'src/app/shared/navigation/layout.service';

const UuidLength = 36;

export interface SidenavParams {
  mode?: MatSidenav['mode'];
  opened?: MatSidenav['opened'];
  topGap?: MatSidenav['fixedTopGap'];
}

export interface SidenavOption {
  icon: string;
  name: string;
  route: string[];
}

const SIDENAV_DEFAULT_PARAMS: SidenavParams = {
  mode: 'side',
  opened: true,
  topGap: 50,
};

const VALID_ROUTES = {
  '/home': '/home',
  '/administration': '/administration',
  '/management': '/management',
};

const HOME_SECTIONS: SidenavOption[] = [];

const ADMINISTRATION_SECTIONS: SidenavOption[] = [
  {
    icon: 'all_inbox',
    name: 'Projects',
    route: ['/administration/projects'],
  },
  {
    icon: 'timeline',
    name: 'Sprints',
    route: ['/administration/sprints'],
  },
  {
    icon: 'list',
    name: 'Stories',
    route: ['/administration/stories'],
  },
  {
    icon: 'task',
    name: 'Tasks',
    route: ['/administration/tasks'],
  },
  {
    icon: 'supervised_user_circle',
    name: 'Users',
    route: ['/administration/users'],
  },
];

const MANAGEMENT_SECTIONS: SidenavOption[] = [
  {
    icon: 'home',
    name: 'Projects',
    route: ['management'],
  },
];

const SECTIONS_WITH_PARAMS: { [key: string]: SidenavOption[] } = {
  '/management': [
    {
      icon: 'info',
      name: 'Overview',
      route: ['management', ':project'],
    },
    {
      icon: 'list',
      name: 'Backlog',
      route: ['management', ':project', 'backlog'],
    },
    /*{
      icon: 'view_column',
      name: 'Board',
      route: ['management', ':project', 'board'],
    },*/
    {
      icon: 'label_important',
      name: 'Active sprint',
      route: ['management', ':project', 'sprints', 'active'],
    },
    {
      icon: 'timeline',
      name: 'History',
      route: ['management', ':project', 'sprints', 'history'],
    },
    {
      icon: 'supervised_user_circle',
      name: 'Members',
      route: ['management', ':project', 'members'],
    },
    /*{
      icon: 'settings',
      name: 'Settings',
      route: ['management', ':project', 'settings'],
    },*/
  ],
};

const SIDENAV_OPTIONS: { [key: string]: SidenavOption[] } = {
  default: HOME_SECTIONS,
  '/home': HOME_SECTIONS,
  '/administration': ADMINISTRATION_SECTIONS,
  '/management': MANAGEMENT_SECTIONS,
};

@Component({
  selector: 'k-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit {
  public sidenavMode: SidenavParams['mode'] = SIDENAV_DEFAULT_PARAMS.mode;
  public sidenavState: SidenavParams['opened'] = SIDENAV_DEFAULT_PARAMS.opened;
  public sidenavTopGap: SidenavParams['topGap'] = SIDENAV_DEFAULT_PARAMS.topGap;

  public sidenavOptions: SidenavOption[] = SIDENAV_OPTIONS['/home'];

  public sidenavLayoutState: LayoutState;

  public isHome = true;

  constructor(private router: Router, public layout: LayoutService, private route: ActivatedRoute) {
    // this.route.params.subscribe(console.log);
    /*this.router.events.pipe(filter((e) => e instanceof NavigationStart)).subscribe((e) => {
      // console.log(e);
      // this.route.params.subscribe(console.log);
    });*/
    this.router.events.pipe(filter((e) => e instanceof NavigationEnd)).subscribe((e: NavigationEnd) => {
      // this.handleProjectManagement(this.router.routerState.snapshot.url);
      this.updateSidenav(e.urlAfterRedirects);
      // this.restoreSidenavDefaults();
    });
    this.layout.getSidenavState().subscribe((state) => {
      this.sidenavLayoutState = state;
    });
  }

  ngOnInit(): void {}

  private updateSidenav(location: string): void {
    this.isHome = location.indexOf('/home') !== -1;

    const uuidInUrlRegExp = new RegExp(`\/.*\/([a-z0-9\-]{${UuidLength}})\/?.*`);
    const topLevelLocation = location.substring(
      0,
      location.indexOf('/', 1) !== -1 ? location.indexOf('/', 1) : location.length
    );

    let additionalSections = [];
    if (uuidInUrlRegExp.test(location)) {
      additionalSections = this.handleParams(topLevelLocation, uuidInUrlRegExp.exec(location)[1]);
    }

    VALID_ROUTES[topLevelLocation]
      ? (this.sidenavOptions = SIDENAV_OPTIONS[topLevelLocation].concat(additionalSections))
      : (this.sidenavOptions = SIDENAV_OPTIONS.default);
  }

  private handleParams(currentLocation: string, param: string): SidenavOption[] {
    const paramRegExp = new RegExp(/:.*/);
    return (
      Object.assign([], SECTIONS_WITH_PARAMS[currentLocation]).map((section) => {
        return {
          icon: section.icon,
          name: section.name,
          route: Object.assign(
            [],
            section.route.map((fragment) => {
              return paramRegExp.test(fragment) ? param : fragment;
            })
          ),
        };
      }) || []
    );
  }
}
