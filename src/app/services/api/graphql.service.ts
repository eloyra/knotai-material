import { Injectable } from '@angular/core';
import { Mutation, Query } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import { map } from 'rxjs/operators';

interface QueryVariables {
  id?: string;
  first?: number;
  after?: any;
  before?: any;
  orderBy?: any;
  storiesOrderBy?: any;
}

interface MutationVariables {
  id?: string;
  item?: {};
  patch?: {};
}

@Injectable({
  providedIn: 'root',
})
export class GraphqlService {
  constructor() {}

  public query = (query: Query, variables: QueryVariables = {}, options: {} = {}) =>
    query.watch(variables, options).valueChanges.pipe(
      map((result: ApolloQueryResult<any>) => {
        return Object.values(result.data)[0];
      })
    );

  public mutate = (mutation: Mutation, variables: MutationVariables, options: {} = {}) =>
    mutation.mutate(variables, options).pipe(
      map((result: ApolloQueryResult<any>) => {
        return Object.values(Object.values(result.data)[0])[0];
      })
    );
}
