import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A location in a connection that can be used for resuming pagination. */
  Cursor: any;
  /** A universally unique identifier as defined by [RFC 4122](https://tools.ietf.org/html/rfc4122). */
  UUID: any;
  /** A point in time as described by the [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) standard. May or may not include a timezone. */
  Datetime: any;
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: any;
  /** A floating point number that requires more precision than IEEE 754 binary 64 */
  BigFloat: any;
  /** A JSON Web Token defined by [RFC 7519](https://tools.ietf.org/html/rfc7519) which securely represents claims between two parties. */
  JwtToken: any;
};

/** All input for the `authenticate` mutation. */
export type AuthenticateInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  email: Scalars['String'];
  password: Scalars['String'];
};

/** The output of our `authenticate` mutation. */
export type AuthenticatePayload = {
  __typename?: 'AuthenticatePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  jwtToken?: Maybe<Scalars['JwtToken']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};

/** All input for the `checkProjectMembership` mutation. */
export type CheckProjectMembershipInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  userId: Scalars['UUID'];
  projectId: Scalars['UUID'];
};

/** The output of our `checkProjectMembership` mutation. */
export type CheckProjectMembershipPayload = {
  __typename?: 'CheckProjectMembershipPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  boolean?: Maybe<Scalars['Boolean']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};

/** A rich text fragment used to communicate inside stories. */
export type Comment = Node & {
  __typename?: 'Comment';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  /** Comment unique identifier. */
  id: Scalars['UUID'];
  /** Indicates the time of last change. */
  createdAt: Scalars['Datetime'];
  updatedAt: Scalars['Datetime'];
  /** Content. */
  content: Scalars['String'];
  /** User who created the comment. */
  author?: Maybe<Scalars['UUID']>;
  /** Task where the comment is created. */
  task?: Maybe<Scalars['UUID']>;
  /** Reads a single `UserProfile` that is related to this `Comment`. */
  userProfileByAuthor?: Maybe<UserProfile>;
  /** Reads a single `Task` that is related to this `Comment`. */
  taskByTask?: Maybe<Task>;
};

/** A condition to be used against `Comment` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type CommentCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: Maybe<Scalars['Datetime']>;
  /** Checks for equality with the object’s `author` field. */
  author?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `task` field. */
  task?: Maybe<Scalars['UUID']>;
};

/** An input for mutations affecting `Comment` */
export type CommentInput = {
  /** Comment unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of last change. */
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Content. */
  content: Scalars['String'];
  /** User who created the comment. */
  author?: Maybe<Scalars['UUID']>;
  /** Task where the comment is created. */
  task?: Maybe<Scalars['UUID']>;
};

/** Represents an update to a `Comment`. Fields that are set will be updated. */
export type CommentPatch = {
  /** Comment unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of last change. */
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Content. */
  content?: Maybe<Scalars['String']>;
  /** User who created the comment. */
  author?: Maybe<Scalars['UUID']>;
  /** Task where the comment is created. */
  task?: Maybe<Scalars['UUID']>;
};

/** A connection to a list of `Comment` values. */
export type CommentsConnection = {
  __typename?: 'CommentsConnection';
  /** A list of `Comment` objects. */
  nodes: Array<Comment>;
  /** A list of edges which contains the `Comment` and cursor to aid in pagination. */
  edges: Array<CommentsEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Comment` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `Comment` edge in the connection. */
export type CommentsEdge = {
  __typename?: 'CommentsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `Comment` at the end of the edge. */
  node: Comment;
};

/** Methods to use when ordering `Comment`. */
export enum CommentsOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  AuthorAsc = 'AUTHOR_ASC',
  AuthorDesc = 'AUTHOR_DESC',
  TaskAsc = 'TASK_ASC',
  TaskDesc = 'TASK_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
}

/** All input for the create `Comment` mutation. */
export type CreateCommentInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Comment` to be created by this mutation. */
  comment: CommentInput;
};

/** The output of our create `Comment` mutation. */
export type CreateCommentPayload = {
  __typename?: 'CreateCommentPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Comment` that was created by this mutation. */
  comment?: Maybe<Comment>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `UserProfile` that is related to this `Comment`. */
  userProfileByAuthor?: Maybe<UserProfile>;
  /** Reads a single `Task` that is related to this `Comment`. */
  taskByTask?: Maybe<Task>;
  /** An edge for our `Comment`. May be used by Relay 1. */
  commentEdge?: Maybe<CommentsEdge>;
};

/** The output of our create `Comment` mutation. */
export type CreateCommentPayloadCommentEdgeArgs = {
  orderBy?: Maybe<Array<CommentsOrderBy>>;
};

/** All input for the create `Member` mutation. */
export type CreateMemberInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Member` to be created by this mutation. */
  member: MemberInput;
};

/** The output of our create `Member` mutation. */
export type CreateMemberPayload = {
  __typename?: 'CreateMemberPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Member` that was created by this mutation. */
  member?: Maybe<Member>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Project` that is related to this `Member`. */
  projectByProject?: Maybe<Project>;
  /** Reads a single `UserProfile` that is related to this `Member`. */
  userProfileByUserProfile?: Maybe<UserProfile>;
  /** An edge for our `Member`. May be used by Relay 1. */
  memberEdge?: Maybe<MembersEdge>;
};

/** The output of our create `Member` mutation. */
export type CreateMemberPayloadMemberEdgeArgs = {
  orderBy?: Maybe<Array<MembersOrderBy>>;
};

/** All input for the create `Project` mutation. */
export type CreateProjectInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Project` to be created by this mutation. */
  project: ProjectInput;
};

/** The output of our create `Project` mutation. */
export type CreateProjectPayload = {
  __typename?: 'CreateProjectPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Project` that was created by this mutation. */
  project?: Maybe<Project>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Project`. May be used by Relay 1. */
  projectEdge?: Maybe<ProjectsEdge>;
};

/** The output of our create `Project` mutation. */
export type CreateProjectPayloadProjectEdgeArgs = {
  orderBy?: Maybe<Array<ProjectsOrderBy>>;
};

/** All input for the create `Role` mutation. */
export type CreateRoleInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Role` to be created by this mutation. */
  role: RoleInput;
};

/** The output of our create `Role` mutation. */
export type CreateRolePayload = {
  __typename?: 'CreateRolePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Role` that was created by this mutation. */
  role?: Maybe<Role>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Role`. May be used by Relay 1. */
  roleEdge?: Maybe<RolesEdge>;
};

/** The output of our create `Role` mutation. */
export type CreateRolePayloadRoleEdgeArgs = {
  orderBy?: Maybe<Array<RolesOrderBy>>;
};

/** All input for the create `Sprint` mutation. */
export type CreateSprintInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Sprint` to be created by this mutation. */
  sprint: SprintInput;
};

/** The output of our create `Sprint` mutation. */
export type CreateSprintPayload = {
  __typename?: 'CreateSprintPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Sprint` that was created by this mutation. */
  sprint?: Maybe<Sprint>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Project` that is related to this `Sprint`. */
  projectByProject?: Maybe<Project>;
  /** An edge for our `Sprint`. May be used by Relay 1. */
  sprintEdge?: Maybe<SprintsEdge>;
};

/** The output of our create `Sprint` mutation. */
export type CreateSprintPayloadSprintEdgeArgs = {
  orderBy?: Maybe<Array<SprintsOrderBy>>;
};

/** All input for the create `Story` mutation. */
export type CreateStoryInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Story` to be created by this mutation. */
  story: StoryInput;
};

/** The output of our create `Story` mutation. */
export type CreateStoryPayload = {
  __typename?: 'CreateStoryPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Story` that was created by this mutation. */
  story?: Maybe<Story>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Project` that is related to this `Story`. */
  projectByProject?: Maybe<Project>;
  /** Reads a single `Sprint` that is related to this `Story`. */
  sprintBySprint?: Maybe<Sprint>;
  /** Reads a single `UserProfile` that is related to this `Story`. */
  userProfileByAssignee?: Maybe<UserProfile>;
  /** An edge for our `Story`. May be used by Relay 1. */
  storyEdge?: Maybe<StoriesEdge>;
};

/** The output of our create `Story` mutation. */
export type CreateStoryPayloadStoryEdgeArgs = {
  orderBy?: Maybe<Array<StoriesOrderBy>>;
};

/** All input for the create `System` mutation. */
export type CreateSystemInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `System` to be created by this mutation. */
  system: SystemInput;
};

/** The output of our create `System` mutation. */
export type CreateSystemPayload = {
  __typename?: 'CreateSystemPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `System` that was created by this mutation. */
  system?: Maybe<System>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `System`. May be used by Relay 1. */
  systemEdge?: Maybe<SystemsEdge>;
};

/** The output of our create `System` mutation. */
export type CreateSystemPayloadSystemEdgeArgs = {
  orderBy?: Maybe<Array<SystemsOrderBy>>;
};

/** All input for the create `Tagbook` mutation. */
export type CreateTagbookInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Tagbook` to be created by this mutation. */
  tagbook: TagbookInput;
};

/** The output of our create `Tagbook` mutation. */
export type CreateTagbookPayload = {
  __typename?: 'CreateTagbookPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Tagbook` that was created by this mutation. */
  tagbook?: Maybe<Tagbook>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Tag` that is related to this `Tagbook`. */
  tagByTag?: Maybe<Tag>;
  /** Reads a single `Story` that is related to this `Tagbook`. */
  storyByStory?: Maybe<Story>;
  /** An edge for our `Tagbook`. May be used by Relay 1. */
  tagbookEdge?: Maybe<TagbooksEdge>;
};

/** The output of our create `Tagbook` mutation. */
export type CreateTagbookPayloadTagbookEdgeArgs = {
  orderBy?: Maybe<Array<TagbooksOrderBy>>;
};

/** All input for the create `Tag` mutation. */
export type CreateTagInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Tag` to be created by this mutation. */
  tag: TagInput;
};

/** The output of our create `Tag` mutation. */
export type CreateTagPayload = {
  __typename?: 'CreateTagPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Tag` that was created by this mutation. */
  tag?: Maybe<Tag>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Tag`. May be used by Relay 1. */
  tagEdge?: Maybe<TagsEdge>;
};

/** The output of our create `Tag` mutation. */
export type CreateTagPayloadTagEdgeArgs = {
  orderBy?: Maybe<Array<TagsOrderBy>>;
};

/** All input for the create `Task` mutation. */
export type CreateTaskInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Task` to be created by this mutation. */
  task: TaskInput;
};

/** The output of our create `Task` mutation. */
export type CreateTaskPayload = {
  __typename?: 'CreateTaskPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Task` that was created by this mutation. */
  task?: Maybe<Task>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Project` that is related to this `Task`. */
  projectByProject?: Maybe<Project>;
  /** Reads a single `Sprint` that is related to this `Task`. */
  sprintBySprint?: Maybe<Sprint>;
  /** Reads a single `Story` that is related to this `Task`. */
  storyByStory?: Maybe<Story>;
  /** Reads a single `UserProfile` that is related to this `Task`. */
  userProfileByAssignee?: Maybe<UserProfile>;
  /** An edge for our `Task`. May be used by Relay 1. */
  taskEdge?: Maybe<TasksEdge>;
};

/** The output of our create `Task` mutation. */
export type CreateTaskPayloadTaskEdgeArgs = {
  orderBy?: Maybe<Array<TasksOrderBy>>;
};

/** All input for the create `TimeEntry` mutation. */
export type CreateTimeEntryInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `TimeEntry` to be created by this mutation. */
  timeEntry: TimeEntryInput;
};

/** The output of our create `TimeEntry` mutation. */
export type CreateTimeEntryPayload = {
  __typename?: 'CreateTimeEntryPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `TimeEntry` that was created by this mutation. */
  timeEntry?: Maybe<TimeEntry>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `UserProfile` that is related to this `TimeEntry`. */
  userProfileByAuthor?: Maybe<UserProfile>;
  /** Reads a single `Task` that is related to this `TimeEntry`. */
  taskByTask?: Maybe<Task>;
  /** An edge for our `TimeEntry`. May be used by Relay 1. */
  timeEntryEdge?: Maybe<TimeEntriesEdge>;
};

/** The output of our create `TimeEntry` mutation. */
export type CreateTimeEntryPayloadTimeEntryEdgeArgs = {
  orderBy?: Maybe<Array<TimeEntriesOrderBy>>;
};

/** All input for the create `UserProfile` mutation. */
export type CreateUserProfileInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `UserProfile` to be created by this mutation. */
  userProfile: UserProfileInput;
};

/** The output of our create `UserProfile` mutation. */
export type CreateUserProfilePayload = {
  __typename?: 'CreateUserProfilePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `UserProfile` that was created by this mutation. */
  userProfile?: Maybe<UserProfile>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Role` that is related to this `UserProfile`. */
  roleByRole?: Maybe<Role>;
  /** An edge for our `UserProfile`. May be used by Relay 1. */
  userProfileEdge?: Maybe<UserProfilesEdge>;
};

/** The output of our create `UserProfile` mutation. */
export type CreateUserProfilePayloadUserProfileEdgeArgs = {
  orderBy?: Maybe<Array<UserProfilesOrderBy>>;
};

/** All input for the `deleteCommentByNodeId` mutation. */
export type DeleteCommentByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Comment` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteComment` mutation. */
export type DeleteCommentInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** Comment unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our delete `Comment` mutation. */
export type DeleteCommentPayload = {
  __typename?: 'DeleteCommentPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Comment` that was deleted by this mutation. */
  comment?: Maybe<Comment>;
  deletedCommentNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `UserProfile` that is related to this `Comment`. */
  userProfileByAuthor?: Maybe<UserProfile>;
  /** Reads a single `Task` that is related to this `Comment`. */
  taskByTask?: Maybe<Task>;
  /** An edge for our `Comment`. May be used by Relay 1. */
  commentEdge?: Maybe<CommentsEdge>;
};

/** The output of our delete `Comment` mutation. */
export type DeleteCommentPayloadCommentEdgeArgs = {
  orderBy?: Maybe<Array<CommentsOrderBy>>;
};

/** All input for the `deleteMemberByNodeId` mutation. */
export type DeleteMemberByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Member` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteMember` mutation. */
export type DeleteMemberInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  id: Scalars['UUID'];
};

/** The output of our delete `Member` mutation. */
export type DeleteMemberPayload = {
  __typename?: 'DeleteMemberPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Member` that was deleted by this mutation. */
  member?: Maybe<Member>;
  deletedMemberNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Project` that is related to this `Member`. */
  projectByProject?: Maybe<Project>;
  /** Reads a single `UserProfile` that is related to this `Member`. */
  userProfileByUserProfile?: Maybe<UserProfile>;
  /** An edge for our `Member`. May be used by Relay 1. */
  memberEdge?: Maybe<MembersEdge>;
};

/** The output of our delete `Member` mutation. */
export type DeleteMemberPayloadMemberEdgeArgs = {
  orderBy?: Maybe<Array<MembersOrderBy>>;
};

/** All input for the `deleteProjectByNodeId` mutation. */
export type DeleteProjectByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Project` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteProject` mutation. */
export type DeleteProjectInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** Unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our delete `Project` mutation. */
export type DeleteProjectPayload = {
  __typename?: 'DeleteProjectPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Project` that was deleted by this mutation. */
  project?: Maybe<Project>;
  deletedProjectNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Project`. May be used by Relay 1. */
  projectEdge?: Maybe<ProjectsEdge>;
};

/** The output of our delete `Project` mutation. */
export type DeleteProjectPayloadProjectEdgeArgs = {
  orderBy?: Maybe<Array<ProjectsOrderBy>>;
};

/** All input for the `deleteRoleByName` mutation. */
export type DeleteRoleByNameInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** Unique name. */
  name: Scalars['String'];
};

/** All input for the `deleteRoleByNodeId` mutation. */
export type DeleteRoleByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Role` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteRole` mutation. */
export type DeleteRoleInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** Unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our delete `Role` mutation. */
export type DeleteRolePayload = {
  __typename?: 'DeleteRolePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Role` that was deleted by this mutation. */
  role?: Maybe<Role>;
  deletedRoleNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Role`. May be used by Relay 1. */
  roleEdge?: Maybe<RolesEdge>;
};

/** The output of our delete `Role` mutation. */
export type DeleteRolePayloadRoleEdgeArgs = {
  orderBy?: Maybe<Array<RolesOrderBy>>;
};

/** All input for the `deleteSprintByNodeId` mutation. */
export type DeleteSprintByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Sprint` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteSprint` mutation. */
export type DeleteSprintInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** Sprint unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our delete `Sprint` mutation. */
export type DeleteSprintPayload = {
  __typename?: 'DeleteSprintPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Sprint` that was deleted by this mutation. */
  sprint?: Maybe<Sprint>;
  deletedSprintNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Project` that is related to this `Sprint`. */
  projectByProject?: Maybe<Project>;
  /** An edge for our `Sprint`. May be used by Relay 1. */
  sprintEdge?: Maybe<SprintsEdge>;
};

/** The output of our delete `Sprint` mutation. */
export type DeleteSprintPayloadSprintEdgeArgs = {
  orderBy?: Maybe<Array<SprintsOrderBy>>;
};

/** All input for the `deleteStoryByNodeId` mutation. */
export type DeleteStoryByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Story` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteStory` mutation. */
export type DeleteStoryInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** Story unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our delete `Story` mutation. */
export type DeleteStoryPayload = {
  __typename?: 'DeleteStoryPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Story` that was deleted by this mutation. */
  story?: Maybe<Story>;
  deletedStoryNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Project` that is related to this `Story`. */
  projectByProject?: Maybe<Project>;
  /** Reads a single `Sprint` that is related to this `Story`. */
  sprintBySprint?: Maybe<Sprint>;
  /** Reads a single `UserProfile` that is related to this `Story`. */
  userProfileByAssignee?: Maybe<UserProfile>;
  /** An edge for our `Story`. May be used by Relay 1. */
  storyEdge?: Maybe<StoriesEdge>;
};

/** The output of our delete `Story` mutation. */
export type DeleteStoryPayloadStoryEdgeArgs = {
  orderBy?: Maybe<Array<StoriesOrderBy>>;
};

/** All input for the `deleteSystemByNodeId` mutation. */
export type DeleteSystemByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `System` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteSystem` mutation. */
export type DeleteSystemInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  id: Scalars['UUID'];
};

/** The output of our delete `System` mutation. */
export type DeleteSystemPayload = {
  __typename?: 'DeleteSystemPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `System` that was deleted by this mutation. */
  system?: Maybe<System>;
  deletedSystemNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `System`. May be used by Relay 1. */
  systemEdge?: Maybe<SystemsEdge>;
};

/** The output of our delete `System` mutation. */
export type DeleteSystemPayloadSystemEdgeArgs = {
  orderBy?: Maybe<Array<SystemsOrderBy>>;
};

/** All input for the `deleteTagbookByNodeId` mutation. */
export type DeleteTagbookByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Tagbook` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteTagbook` mutation. */
export type DeleteTagbookInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** Tagbook entry unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our delete `Tagbook` mutation. */
export type DeleteTagbookPayload = {
  __typename?: 'DeleteTagbookPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Tagbook` that was deleted by this mutation. */
  tagbook?: Maybe<Tagbook>;
  deletedTagbookNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Tag` that is related to this `Tagbook`. */
  tagByTag?: Maybe<Tag>;
  /** Reads a single `Story` that is related to this `Tagbook`. */
  storyByStory?: Maybe<Story>;
  /** An edge for our `Tagbook`. May be used by Relay 1. */
  tagbookEdge?: Maybe<TagbooksEdge>;
};

/** The output of our delete `Tagbook` mutation. */
export type DeleteTagbookPayloadTagbookEdgeArgs = {
  orderBy?: Maybe<Array<TagbooksOrderBy>>;
};

/** All input for the `deleteTagByLabel` mutation. */
export type DeleteTagByLabelInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** Name. */
  label: Scalars['String'];
};

/** All input for the `deleteTagByNodeId` mutation. */
export type DeleteTagByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Tag` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteTag` mutation. */
export type DeleteTagInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** Story unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our delete `Tag` mutation. */
export type DeleteTagPayload = {
  __typename?: 'DeleteTagPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Tag` that was deleted by this mutation. */
  tag?: Maybe<Tag>;
  deletedTagNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Tag`. May be used by Relay 1. */
  tagEdge?: Maybe<TagsEdge>;
};

/** The output of our delete `Tag` mutation. */
export type DeleteTagPayloadTagEdgeArgs = {
  orderBy?: Maybe<Array<TagsOrderBy>>;
};

/** All input for the `deleteTaskByNodeId` mutation. */
export type DeleteTaskByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Task` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteTask` mutation. */
export type DeleteTaskInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** Task unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our delete `Task` mutation. */
export type DeleteTaskPayload = {
  __typename?: 'DeleteTaskPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Task` that was deleted by this mutation. */
  task?: Maybe<Task>;
  deletedTaskNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Project` that is related to this `Task`. */
  projectByProject?: Maybe<Project>;
  /** Reads a single `Sprint` that is related to this `Task`. */
  sprintBySprint?: Maybe<Sprint>;
  /** Reads a single `Story` that is related to this `Task`. */
  storyByStory?: Maybe<Story>;
  /** Reads a single `UserProfile` that is related to this `Task`. */
  userProfileByAssignee?: Maybe<UserProfile>;
  /** An edge for our `Task`. May be used by Relay 1. */
  taskEdge?: Maybe<TasksEdge>;
};

/** The output of our delete `Task` mutation. */
export type DeleteTaskPayloadTaskEdgeArgs = {
  orderBy?: Maybe<Array<TasksOrderBy>>;
};

/** All input for the `deleteTimeEntryByNodeId` mutation. */
export type DeleteTimeEntryByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `TimeEntry` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteTimeEntry` mutation. */
export type DeleteTimeEntryInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** Entry unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our delete `TimeEntry` mutation. */
export type DeleteTimeEntryPayload = {
  __typename?: 'DeleteTimeEntryPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `TimeEntry` that was deleted by this mutation. */
  timeEntry?: Maybe<TimeEntry>;
  deletedTimeEntryNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `UserProfile` that is related to this `TimeEntry`. */
  userProfileByAuthor?: Maybe<UserProfile>;
  /** Reads a single `Task` that is related to this `TimeEntry`. */
  taskByTask?: Maybe<Task>;
  /** An edge for our `TimeEntry`. May be used by Relay 1. */
  timeEntryEdge?: Maybe<TimeEntriesEdge>;
};

/** The output of our delete `TimeEntry` mutation. */
export type DeleteTimeEntryPayloadTimeEntryEdgeArgs = {
  orderBy?: Maybe<Array<TimeEntriesOrderBy>>;
};

/** All input for the `deleteUserProfileByNodeId` mutation. */
export type DeleteUserProfileByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `UserProfile` to be deleted. */
  nodeId: Scalars['ID'];
};

/** All input for the `deleteUserProfile` mutation. */
export type DeleteUserProfileInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** Unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our delete `UserProfile` mutation. */
export type DeleteUserProfilePayload = {
  __typename?: 'DeleteUserProfilePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `UserProfile` that was deleted by this mutation. */
  userProfile?: Maybe<UserProfile>;
  deletedUserProfileNodeId?: Maybe<Scalars['ID']>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Role` that is related to this `UserProfile`. */
  roleByRole?: Maybe<Role>;
  /** An edge for our `UserProfile`. May be used by Relay 1. */
  userProfileEdge?: Maybe<UserProfilesEdge>;
};

/** The output of our delete `UserProfile` mutation. */
export type DeleteUserProfilePayloadUserProfileEdgeArgs = {
  orderBy?: Maybe<Array<UserProfilesOrderBy>>;
};

/** Project membership. */
export type Member = Node & {
  __typename?: 'Member';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  id: Scalars['UUID'];
  /** Project reference. */
  project: Scalars['UUID'];
  /** User reference. */
  userProfile: Scalars['UUID'];
  /** Reads a single `Project` that is related to this `Member`. */
  projectByProject?: Maybe<Project>;
  /** Reads a single `UserProfile` that is related to this `Member`. */
  userProfileByUserProfile?: Maybe<UserProfile>;
};

/** A condition to be used against `Member` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type MemberCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `project` field. */
  project?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `userProfile` field. */
  userProfile?: Maybe<Scalars['UUID']>;
};

/** An input for mutations affecting `Member` */
export type MemberInput = {
  id?: Maybe<Scalars['UUID']>;
  /** Project reference. */
  project: Scalars['UUID'];
  /** User reference. */
  userProfile: Scalars['UUID'];
};

/** Represents an update to a `Member`. Fields that are set will be updated. */
export type MemberPatch = {
  id?: Maybe<Scalars['UUID']>;
  /** Project reference. */
  project?: Maybe<Scalars['UUID']>;
  /** User reference. */
  userProfile?: Maybe<Scalars['UUID']>;
};

/** A connection to a list of `Member` values. */
export type MembersConnection = {
  __typename?: 'MembersConnection';
  /** A list of `Member` objects. */
  nodes: Array<Member>;
  /** A list of edges which contains the `Member` and cursor to aid in pagination. */
  edges: Array<MembersEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Member` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `Member` edge in the connection. */
export type MembersEdge = {
  __typename?: 'MembersEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `Member` at the end of the edge. */
  node: Member;
};

/** Methods to use when ordering `Member`. */
export enum MembersOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  ProjectAsc = 'PROJECT_ASC',
  ProjectDesc = 'PROJECT_DESC',
  UserProfileAsc = 'USER_PROFILE_ASC',
  UserProfileDesc = 'USER_PROFILE_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
}

/** The root mutation type which contains root level fields which mutate data. */
export type Mutation = {
  __typename?: 'Mutation';
  /** Creates a single `Comment`. */
  createComment?: Maybe<CreateCommentPayload>;
  /** Creates a single `Member`. */
  createMember?: Maybe<CreateMemberPayload>;
  /** Creates a single `Project`. */
  createProject?: Maybe<CreateProjectPayload>;
  /** Creates a single `Role`. */
  createRole?: Maybe<CreateRolePayload>;
  /** Creates a single `Sprint`. */
  createSprint?: Maybe<CreateSprintPayload>;
  /** Creates a single `Story`. */
  createStory?: Maybe<CreateStoryPayload>;
  /** Creates a single `System`. */
  createSystem?: Maybe<CreateSystemPayload>;
  /** Creates a single `Tag`. */
  createTag?: Maybe<CreateTagPayload>;
  /** Creates a single `Tagbook`. */
  createTagbook?: Maybe<CreateTagbookPayload>;
  /** Creates a single `Task`. */
  createTask?: Maybe<CreateTaskPayload>;
  /** Creates a single `TimeEntry`. */
  createTimeEntry?: Maybe<CreateTimeEntryPayload>;
  /** Creates a single `UserProfile`. */
  createUserProfile?: Maybe<CreateUserProfilePayload>;
  /** Updates a single `Comment` using its globally unique id and a patch. */
  updateCommentByNodeId?: Maybe<UpdateCommentPayload>;
  /** Updates a single `Comment` using a unique key and a patch. */
  updateComment?: Maybe<UpdateCommentPayload>;
  /** Updates a single `Member` using its globally unique id and a patch. */
  updateMemberByNodeId?: Maybe<UpdateMemberPayload>;
  /** Updates a single `Member` using a unique key and a patch. */
  updateMember?: Maybe<UpdateMemberPayload>;
  /** Updates a single `Project` using its globally unique id and a patch. */
  updateProjectByNodeId?: Maybe<UpdateProjectPayload>;
  /** Updates a single `Project` using a unique key and a patch. */
  updateProject?: Maybe<UpdateProjectPayload>;
  /** Updates a single `Role` using its globally unique id and a patch. */
  updateRoleByNodeId?: Maybe<UpdateRolePayload>;
  /** Updates a single `Role` using a unique key and a patch. */
  updateRole?: Maybe<UpdateRolePayload>;
  /** Updates a single `Role` using a unique key and a patch. */
  updateRoleByName?: Maybe<UpdateRolePayload>;
  /** Updates a single `Sprint` using its globally unique id and a patch. */
  updateSprintByNodeId?: Maybe<UpdateSprintPayload>;
  /** Updates a single `Sprint` using a unique key and a patch. */
  updateSprint?: Maybe<UpdateSprintPayload>;
  /** Updates a single `Story` using its globally unique id and a patch. */
  updateStoryByNodeId?: Maybe<UpdateStoryPayload>;
  /** Updates a single `Story` using a unique key and a patch. */
  updateStory?: Maybe<UpdateStoryPayload>;
  /** Updates a single `System` using its globally unique id and a patch. */
  updateSystemByNodeId?: Maybe<UpdateSystemPayload>;
  /** Updates a single `System` using a unique key and a patch. */
  updateSystem?: Maybe<UpdateSystemPayload>;
  /** Updates a single `Tag` using its globally unique id and a patch. */
  updateTagByNodeId?: Maybe<UpdateTagPayload>;
  /** Updates a single `Tag` using a unique key and a patch. */
  updateTag?: Maybe<UpdateTagPayload>;
  /** Updates a single `Tag` using a unique key and a patch. */
  updateTagByLabel?: Maybe<UpdateTagPayload>;
  /** Updates a single `Tagbook` using its globally unique id and a patch. */
  updateTagbookByNodeId?: Maybe<UpdateTagbookPayload>;
  /** Updates a single `Tagbook` using a unique key and a patch. */
  updateTagbook?: Maybe<UpdateTagbookPayload>;
  /** Updates a single `Task` using its globally unique id and a patch. */
  updateTaskByNodeId?: Maybe<UpdateTaskPayload>;
  /** Updates a single `Task` using a unique key and a patch. */
  updateTask?: Maybe<UpdateTaskPayload>;
  /** Updates a single `TimeEntry` using its globally unique id and a patch. */
  updateTimeEntryByNodeId?: Maybe<UpdateTimeEntryPayload>;
  /** Updates a single `TimeEntry` using a unique key and a patch. */
  updateTimeEntry?: Maybe<UpdateTimeEntryPayload>;
  /** Updates a single `UserProfile` using its globally unique id and a patch. */
  updateUserProfileByNodeId?: Maybe<UpdateUserProfilePayload>;
  /** Updates a single `UserProfile` using a unique key and a patch. */
  updateUserProfile?: Maybe<UpdateUserProfilePayload>;
  /** Deletes a single `Comment` using its globally unique id. */
  deleteCommentByNodeId?: Maybe<DeleteCommentPayload>;
  /** Deletes a single `Comment` using a unique key. */
  deleteComment?: Maybe<DeleteCommentPayload>;
  /** Deletes a single `Member` using its globally unique id. */
  deleteMemberByNodeId?: Maybe<DeleteMemberPayload>;
  /** Deletes a single `Member` using a unique key. */
  deleteMember?: Maybe<DeleteMemberPayload>;
  /** Deletes a single `Project` using its globally unique id. */
  deleteProjectByNodeId?: Maybe<DeleteProjectPayload>;
  /** Deletes a single `Project` using a unique key. */
  deleteProject?: Maybe<DeleteProjectPayload>;
  /** Deletes a single `Role` using its globally unique id. */
  deleteRoleByNodeId?: Maybe<DeleteRolePayload>;
  /** Deletes a single `Role` using a unique key. */
  deleteRole?: Maybe<DeleteRolePayload>;
  /** Deletes a single `Role` using a unique key. */
  deleteRoleByName?: Maybe<DeleteRolePayload>;
  /** Deletes a single `Sprint` using its globally unique id. */
  deleteSprintByNodeId?: Maybe<DeleteSprintPayload>;
  /** Deletes a single `Sprint` using a unique key. */
  deleteSprint?: Maybe<DeleteSprintPayload>;
  /** Deletes a single `Story` using its globally unique id. */
  deleteStoryByNodeId?: Maybe<DeleteStoryPayload>;
  /** Deletes a single `Story` using a unique key. */
  deleteStory?: Maybe<DeleteStoryPayload>;
  /** Deletes a single `System` using its globally unique id. */
  deleteSystemByNodeId?: Maybe<DeleteSystemPayload>;
  /** Deletes a single `System` using a unique key. */
  deleteSystem?: Maybe<DeleteSystemPayload>;
  /** Deletes a single `Tag` using its globally unique id. */
  deleteTagByNodeId?: Maybe<DeleteTagPayload>;
  /** Deletes a single `Tag` using a unique key. */
  deleteTag?: Maybe<DeleteTagPayload>;
  /** Deletes a single `Tag` using a unique key. */
  deleteTagByLabel?: Maybe<DeleteTagPayload>;
  /** Deletes a single `Tagbook` using its globally unique id. */
  deleteTagbookByNodeId?: Maybe<DeleteTagbookPayload>;
  /** Deletes a single `Tagbook` using a unique key. */
  deleteTagbook?: Maybe<DeleteTagbookPayload>;
  /** Deletes a single `Task` using its globally unique id. */
  deleteTaskByNodeId?: Maybe<DeleteTaskPayload>;
  /** Deletes a single `Task` using a unique key. */
  deleteTask?: Maybe<DeleteTaskPayload>;
  /** Deletes a single `TimeEntry` using its globally unique id. */
  deleteTimeEntryByNodeId?: Maybe<DeleteTimeEntryPayload>;
  /** Deletes a single `TimeEntry` using a unique key. */
  deleteTimeEntry?: Maybe<DeleteTimeEntryPayload>;
  /** Deletes a single `UserProfile` using its globally unique id. */
  deleteUserProfileByNodeId?: Maybe<DeleteUserProfilePayload>;
  /** Deletes a single `UserProfile` using a unique key. */
  deleteUserProfile?: Maybe<DeleteUserProfilePayload>;
  /** Creates a JWT token to safely identify a user and give them access to the application. */
  authenticate?: Maybe<AuthenticatePayload>;
  checkProjectMembership?: Maybe<CheckProjectMembershipPayload>;
  /** Registers a user creating their private account and hashing the password. */
  registerUser?: Maybe<RegisterUserPayload>;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateCommentArgs = {
  input: CreateCommentInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateMemberArgs = {
  input: CreateMemberInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateProjectArgs = {
  input: CreateProjectInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateRoleArgs = {
  input: CreateRoleInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateSprintArgs = {
  input: CreateSprintInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateStoryArgs = {
  input: CreateStoryInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateSystemArgs = {
  input: CreateSystemInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateTagArgs = {
  input: CreateTagInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateTagbookArgs = {
  input: CreateTagbookInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateTaskArgs = {
  input: CreateTaskInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateTimeEntryArgs = {
  input: CreateTimeEntryInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateUserProfileArgs = {
  input: CreateUserProfileInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCommentByNodeIdArgs = {
  input: UpdateCommentByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateCommentArgs = {
  input: UpdateCommentInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateMemberByNodeIdArgs = {
  input: UpdateMemberByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateMemberArgs = {
  input: UpdateMemberInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateProjectByNodeIdArgs = {
  input: UpdateProjectByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateProjectArgs = {
  input: UpdateProjectInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateRoleByNodeIdArgs = {
  input: UpdateRoleByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateRoleArgs = {
  input: UpdateRoleInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateRoleByNameArgs = {
  input: UpdateRoleByNameInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateSprintByNodeIdArgs = {
  input: UpdateSprintByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateSprintArgs = {
  input: UpdateSprintInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateStoryByNodeIdArgs = {
  input: UpdateStoryByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateStoryArgs = {
  input: UpdateStoryInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateSystemByNodeIdArgs = {
  input: UpdateSystemByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateSystemArgs = {
  input: UpdateSystemInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateTagByNodeIdArgs = {
  input: UpdateTagByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateTagArgs = {
  input: UpdateTagInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateTagByLabelArgs = {
  input: UpdateTagByLabelInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateTagbookByNodeIdArgs = {
  input: UpdateTagbookByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateTagbookArgs = {
  input: UpdateTagbookInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateTaskByNodeIdArgs = {
  input: UpdateTaskByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateTaskArgs = {
  input: UpdateTaskInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateTimeEntryByNodeIdArgs = {
  input: UpdateTimeEntryByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateTimeEntryArgs = {
  input: UpdateTimeEntryInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateUserProfileByNodeIdArgs = {
  input: UpdateUserProfileByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateUserProfileArgs = {
  input: UpdateUserProfileInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCommentByNodeIdArgs = {
  input: DeleteCommentByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteCommentArgs = {
  input: DeleteCommentInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteMemberByNodeIdArgs = {
  input: DeleteMemberByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteMemberArgs = {
  input: DeleteMemberInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteProjectByNodeIdArgs = {
  input: DeleteProjectByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteProjectArgs = {
  input: DeleteProjectInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteRoleByNodeIdArgs = {
  input: DeleteRoleByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteRoleArgs = {
  input: DeleteRoleInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteRoleByNameArgs = {
  input: DeleteRoleByNameInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteSprintByNodeIdArgs = {
  input: DeleteSprintByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteSprintArgs = {
  input: DeleteSprintInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteStoryByNodeIdArgs = {
  input: DeleteStoryByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteStoryArgs = {
  input: DeleteStoryInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteSystemByNodeIdArgs = {
  input: DeleteSystemByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteSystemArgs = {
  input: DeleteSystemInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteTagByNodeIdArgs = {
  input: DeleteTagByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteTagArgs = {
  input: DeleteTagInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteTagByLabelArgs = {
  input: DeleteTagByLabelInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteTagbookByNodeIdArgs = {
  input: DeleteTagbookByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteTagbookArgs = {
  input: DeleteTagbookInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteTaskByNodeIdArgs = {
  input: DeleteTaskByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteTaskArgs = {
  input: DeleteTaskInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteTimeEntryByNodeIdArgs = {
  input: DeleteTimeEntryByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteTimeEntryArgs = {
  input: DeleteTimeEntryInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteUserProfileByNodeIdArgs = {
  input: DeleteUserProfileByNodeIdInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteUserProfileArgs = {
  input: DeleteUserProfileInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationAuthenticateArgs = {
  input: AuthenticateInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationCheckProjectMembershipArgs = {
  input: CheckProjectMembershipInput;
};

/** The root mutation type which contains root level fields which mutate data. */
export type MutationRegisterUserArgs = {
  input: RegisterUserInput;
};

/** An object with a globally unique `ID`. */
export type Node = {
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
};

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean'];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['Cursor']>;
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['Cursor']>;
};

export enum Priority {
  High = 'HIGH',
  Medium = 'MEDIUM',
  Low = 'LOW',
}

/** A project is the abstract representation of a product to develop and deliver. */
export type Project = Node & {
  __typename?: 'Project';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  /** Unique identifier. */
  id: Scalars['UUID'];
  /** Indicates the time of creation. */
  createdAt: Scalars['Datetime'];
  /** Indicates the time of last change. */
  updatedAt: Scalars['Datetime'];
  /** Human readable name. */
  name: Scalars['String'];
  /** Short description. */
  description?: Maybe<Scalars['String']>;
  /** Scheduled start time. */
  start?: Maybe<Scalars['Datetime']>;
  /** Scheduled end of project. */
  deadline?: Maybe<Scalars['Datetime']>;
  /** Actual project finish date. */
  finish?: Maybe<Scalars['Datetime']>;
  /** Project wiki page. */
  wiki?: Maybe<Scalars['String']>;
  /** Configuration object. */
  config?: Maybe<Scalars['JSON']>;
  /** Lifecycle status. */
  state: State;
  /** Priority. */
  priority?: Maybe<Priority>;
  /** URL to the VCS repository. */
  repository?: Maybe<Scalars['String']>;
  /** Weight in an ordered list. */
  weight?: Maybe<Scalars['Int']>;
  sprintDuration?: Maybe<Scalars['Int']>;
  /** Reads and enables pagination through a set of `Member`. */
  membersByProject: MembersConnection;
  /** Reads and enables pagination through a set of `Sprint`. */
  sprintsByProject: SprintsConnection;
  /** Reads and enables pagination through a set of `Story`. */
  storiesByProject: StoriesConnection;
  /** Reads and enables pagination through a set of `Task`. */
  tasksByProject: TasksConnection;
};

/** A project is the abstract representation of a product to develop and deliver. */
export type ProjectMembersByProjectArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<MembersOrderBy>>;
  condition?: Maybe<MemberCondition>;
};

/** A project is the abstract representation of a product to develop and deliver. */
export type ProjectSprintsByProjectArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<SprintsOrderBy>>;
  condition?: Maybe<SprintCondition>;
};

/** A project is the abstract representation of a product to develop and deliver. */
export type ProjectStoriesByProjectArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<StoriesOrderBy>>;
  condition?: Maybe<StoryCondition>;
};

/** A project is the abstract representation of a product to develop and deliver. */
export type ProjectTasksByProjectArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TasksOrderBy>>;
  condition?: Maybe<TaskCondition>;
};

/** A condition to be used against `Project` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type ProjectCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `name` field. */
  name?: Maybe<Scalars['String']>;
  /** Checks for equality with the object’s `weight` field. */
  weight?: Maybe<Scalars['Int']>;
};

/** An input for mutations affecting `Project` */
export type ProjectInput = {
  /** Unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of creation. */
  createdAt?: Maybe<Scalars['Datetime']>;
  /** Indicates the time of last change. */
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Human readable name. */
  name: Scalars['String'];
  /** Short description. */
  description?: Maybe<Scalars['String']>;
  /** Scheduled start time. */
  start?: Maybe<Scalars['Datetime']>;
  /** Scheduled end of project. */
  deadline?: Maybe<Scalars['Datetime']>;
  /** Actual project finish date. */
  finish?: Maybe<Scalars['Datetime']>;
  /** Project wiki page. */
  wiki?: Maybe<Scalars['String']>;
  /** Configuration object. */
  config?: Maybe<Scalars['JSON']>;
  /** Lifecycle status. */
  state?: Maybe<State>;
  /** Priority. */
  priority?: Maybe<Priority>;
  /** URL to the VCS repository. */
  repository?: Maybe<Scalars['String']>;
  /** Weight in an ordered list. */
  weight?: Maybe<Scalars['Int']>;
  sprintDuration?: Maybe<Scalars['Int']>;
};

/** Represents an update to a `Project`. Fields that are set will be updated. */
export type ProjectPatch = {
  /** Unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of creation. */
  createdAt?: Maybe<Scalars['Datetime']>;
  /** Indicates the time of last change. */
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Human readable name. */
  name?: Maybe<Scalars['String']>;
  /** Short description. */
  description?: Maybe<Scalars['String']>;
  /** Scheduled start time. */
  start?: Maybe<Scalars['Datetime']>;
  /** Scheduled end of project. */
  deadline?: Maybe<Scalars['Datetime']>;
  /** Actual project finish date. */
  finish?: Maybe<Scalars['Datetime']>;
  /** Project wiki page. */
  wiki?: Maybe<Scalars['String']>;
  /** Configuration object. */
  config?: Maybe<Scalars['JSON']>;
  /** Lifecycle status. */
  state?: Maybe<State>;
  /** Priority. */
  priority?: Maybe<Priority>;
  /** URL to the VCS repository. */
  repository?: Maybe<Scalars['String']>;
  /** Weight in an ordered list. */
  weight?: Maybe<Scalars['Int']>;
  sprintDuration?: Maybe<Scalars['Int']>;
};

/** A connection to a list of `Project` values. */
export type ProjectsConnection = {
  __typename?: 'ProjectsConnection';
  /** A list of `Project` objects. */
  nodes: Array<Project>;
  /** A list of edges which contains the `Project` and cursor to aid in pagination. */
  edges: Array<ProjectsEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Project` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `Project` edge in the connection. */
export type ProjectsEdge = {
  __typename?: 'ProjectsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `Project` at the end of the edge. */
  node: Project;
};

/** Methods to use when ordering `Project`. */
export enum ProjectsOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  WeightAsc = 'WEIGHT_ASC',
  WeightDesc = 'WEIGHT_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
}

/** The root query type which gives access points into the data universe. */
export type Query = Node & {
  __typename?: 'Query';
  /**
   * Exposes the root query type nested one level down. This is helpful for Relay 1
   * which can only query top level fields if they are in a particular form.
   */
  query: Query;
  /** The root query type must be a `Node` to work well with Relay 1 mutations. This just resolves to `query`. */
  nodeId: Scalars['ID'];
  /** Fetches an object given its globally unique `ID`. */
  node?: Maybe<Node>;
  /** Reads and enables pagination through a set of `Comment`. */
  comments?: Maybe<CommentsConnection>;
  /** Reads and enables pagination through a set of `Member`. */
  members?: Maybe<MembersConnection>;
  /** Reads and enables pagination through a set of `Project`. */
  projects?: Maybe<ProjectsConnection>;
  /** Reads and enables pagination through a set of `Role`. */
  roles?: Maybe<RolesConnection>;
  /** Reads and enables pagination through a set of `Sprint`. */
  sprints?: Maybe<SprintsConnection>;
  /** Reads and enables pagination through a set of `Story`. */
  stories?: Maybe<StoriesConnection>;
  /** Reads and enables pagination through a set of `System`. */
  systems?: Maybe<SystemsConnection>;
  /** Reads and enables pagination through a set of `Tag`. */
  tags?: Maybe<TagsConnection>;
  /** Reads and enables pagination through a set of `Tagbook`. */
  tagbooks?: Maybe<TagbooksConnection>;
  /** Reads and enables pagination through a set of `Task`. */
  tasks?: Maybe<TasksConnection>;
  /** Reads and enables pagination through a set of `TimeEntry`. */
  timeEntries?: Maybe<TimeEntriesConnection>;
  /** Reads and enables pagination through a set of `UserProfile`. */
  userProfiles?: Maybe<UserProfilesConnection>;
  comment?: Maybe<Comment>;
  member?: Maybe<Member>;
  project?: Maybe<Project>;
  role?: Maybe<Role>;
  roleByName?: Maybe<Role>;
  sprint?: Maybe<Sprint>;
  story?: Maybe<Story>;
  system?: Maybe<System>;
  tag?: Maybe<Tag>;
  tagByLabel?: Maybe<Tag>;
  tagbook?: Maybe<Tagbook>;
  task?: Maybe<Task>;
  timeEntry?: Maybe<TimeEntry>;
  userProfile?: Maybe<UserProfile>;
  /** Get the user who is currently authenticated. */
  currentUser?: Maybe<UserProfile>;
  /** Reads a single `Comment` using its globally unique `ID`. */
  commentByNodeId?: Maybe<Comment>;
  /** Reads a single `Member` using its globally unique `ID`. */
  memberByNodeId?: Maybe<Member>;
  /** Reads a single `Project` using its globally unique `ID`. */
  projectByNodeId?: Maybe<Project>;
  /** Reads a single `Role` using its globally unique `ID`. */
  roleByNodeId?: Maybe<Role>;
  /** Reads a single `Sprint` using its globally unique `ID`. */
  sprintByNodeId?: Maybe<Sprint>;
  /** Reads a single `Story` using its globally unique `ID`. */
  storyByNodeId?: Maybe<Story>;
  /** Reads a single `System` using its globally unique `ID`. */
  systemByNodeId?: Maybe<System>;
  /** Reads a single `Tag` using its globally unique `ID`. */
  tagByNodeId?: Maybe<Tag>;
  /** Reads a single `Tagbook` using its globally unique `ID`. */
  tagbookByNodeId?: Maybe<Tagbook>;
  /** Reads a single `Task` using its globally unique `ID`. */
  taskByNodeId?: Maybe<Task>;
  /** Reads a single `TimeEntry` using its globally unique `ID`. */
  timeEntryByNodeId?: Maybe<TimeEntry>;
  /** Reads a single `UserProfile` using its globally unique `ID`. */
  userProfileByNodeId?: Maybe<UserProfile>;
};

/** The root query type which gives access points into the data universe. */
export type QueryNodeArgs = {
  nodeId: Scalars['ID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryCommentsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<CommentsOrderBy>>;
  condition?: Maybe<CommentCondition>;
};

/** The root query type which gives access points into the data universe. */
export type QueryMembersArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<MembersOrderBy>>;
  condition?: Maybe<MemberCondition>;
};

/** The root query type which gives access points into the data universe. */
export type QueryProjectsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<ProjectsOrderBy>>;
  condition?: Maybe<ProjectCondition>;
};

/** The root query type which gives access points into the data universe. */
export type QueryRolesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<RolesOrderBy>>;
  condition?: Maybe<RoleCondition>;
};

/** The root query type which gives access points into the data universe. */
export type QuerySprintsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<SprintsOrderBy>>;
  condition?: Maybe<SprintCondition>;
};

/** The root query type which gives access points into the data universe. */
export type QueryStoriesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<StoriesOrderBy>>;
  condition?: Maybe<StoryCondition>;
};

/** The root query type which gives access points into the data universe. */
export type QuerySystemsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<SystemsOrderBy>>;
  condition?: Maybe<SystemCondition>;
};

/** The root query type which gives access points into the data universe. */
export type QueryTagsArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TagsOrderBy>>;
  condition?: Maybe<TagCondition>;
};

/** The root query type which gives access points into the data universe. */
export type QueryTagbooksArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TagbooksOrderBy>>;
  condition?: Maybe<TagbookCondition>;
};

/** The root query type which gives access points into the data universe. */
export type QueryTasksArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TasksOrderBy>>;
  condition?: Maybe<TaskCondition>;
};

/** The root query type which gives access points into the data universe. */
export type QueryTimeEntriesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TimeEntriesOrderBy>>;
  condition?: Maybe<TimeEntryCondition>;
};

/** The root query type which gives access points into the data universe. */
export type QueryUserProfilesArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<UserProfilesOrderBy>>;
  condition?: Maybe<UserProfileCondition>;
};

/** The root query type which gives access points into the data universe. */
export type QueryCommentArgs = {
  id: Scalars['UUID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryMemberArgs = {
  id: Scalars['UUID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryProjectArgs = {
  id: Scalars['UUID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryRoleArgs = {
  id: Scalars['UUID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryRoleByNameArgs = {
  name: Scalars['String'];
};

/** The root query type which gives access points into the data universe. */
export type QuerySprintArgs = {
  id: Scalars['UUID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryStoryArgs = {
  id: Scalars['UUID'];
};

/** The root query type which gives access points into the data universe. */
export type QuerySystemArgs = {
  id: Scalars['UUID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryTagArgs = {
  id: Scalars['UUID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryTagByLabelArgs = {
  label: Scalars['String'];
};

/** The root query type which gives access points into the data universe. */
export type QueryTagbookArgs = {
  id: Scalars['UUID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryTaskArgs = {
  id: Scalars['UUID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryTimeEntryArgs = {
  id: Scalars['UUID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryUserProfileArgs = {
  id: Scalars['UUID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryCommentByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryMemberByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryProjectByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryRoleByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** The root query type which gives access points into the data universe. */
export type QuerySprintByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryStoryByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** The root query type which gives access points into the data universe. */
export type QuerySystemByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryTagByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryTagbookByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryTaskByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryTimeEntryByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** The root query type which gives access points into the data universe. */
export type QueryUserProfileByNodeIdArgs = {
  nodeId: Scalars['ID'];
};

/** All input for the `registerUser` mutation. */
export type RegisterUserInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  firstNameInput: Scalars['String'];
  lastNameInput: Scalars['String'];
  emailInput: Scalars['String'];
  password: Scalars['String'];
};

/** The output of our `registerUser` mutation. */
export type RegisterUserPayload = {
  __typename?: 'RegisterUserPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  userProfile?: Maybe<UserProfile>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Role` that is related to this `UserProfile`. */
  roleByRole?: Maybe<Role>;
  /** An edge for our `UserProfile`. May be used by Relay 1. */
  userProfileEdge?: Maybe<UserProfilesEdge>;
};

/** The output of our `registerUser` mutation. */
export type RegisterUserPayloadUserProfileEdgeArgs = {
  orderBy?: Maybe<Array<UserProfilesOrderBy>>;
};

/** A role used to fence permissions. */
export type Role = Node & {
  __typename?: 'Role';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  /** Unique identifier. */
  id: Scalars['UUID'];
  /** Indicates the time of creation. */
  createdAt: Scalars['Datetime'];
  /** Indicates the time of last change. */
  updatedAt: Scalars['Datetime'];
  /** Unique name. */
  name: Scalars['String'];
  /** Reads and enables pagination through a set of `UserProfile`. */
  userProfilesByRole: UserProfilesConnection;
};

/** A role used to fence permissions. */
export type RoleUserProfilesByRoleArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<UserProfilesOrderBy>>;
  condition?: Maybe<UserProfileCondition>;
};

/** A condition to be used against `Role` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type RoleCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `name` field. */
  name?: Maybe<Scalars['String']>;
};

/** An input for mutations affecting `Role` */
export type RoleInput = {
  /** Unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of creation. */
  createdAt?: Maybe<Scalars['Datetime']>;
  /** Indicates the time of last change. */
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Unique name. */
  name: Scalars['String'];
};

/** Represents an update to a `Role`. Fields that are set will be updated. */
export type RolePatch = {
  /** Unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of creation. */
  createdAt?: Maybe<Scalars['Datetime']>;
  /** Indicates the time of last change. */
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Unique name. */
  name?: Maybe<Scalars['String']>;
};

/** A connection to a list of `Role` values. */
export type RolesConnection = {
  __typename?: 'RolesConnection';
  /** A list of `Role` objects. */
  nodes: Array<Role>;
  /** A list of edges which contains the `Role` and cursor to aid in pagination. */
  edges: Array<RolesEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Role` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `Role` edge in the connection. */
export type RolesEdge = {
  __typename?: 'RolesEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `Role` at the end of the edge. */
  node: Role;
};

/** Methods to use when ordering `Role`. */
export enum RolesOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
}

/** A sprint specifies a time frame for the delivery of a set amount of story points. */
export type Sprint = Node & {
  __typename?: 'Sprint';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  /** Sprint unique identifier. */
  id: Scalars['UUID'];
  /** Indicates the time of last change. */
  createdAt: Scalars['Datetime'];
  updatedAt: Scalars['Datetime'];
  /** Name. */
  name: Scalars['String'];
  /** Description. */
  description?: Maybe<Scalars['String']>;
  /** Duration in weeks. */
  duration?: Maybe<Scalars['Int']>;
  /** Determines if this is the current working sprint. */
  active: Scalars['Boolean'];
  /** Time of activation. */
  activationTime?: Maybe<Scalars['Datetime']>;
  /** Timestamp when this sprint was closed. */
  closed?: Maybe<Scalars['Datetime']>;
  /** Weight in an ordered list. */
  weight?: Maybe<Scalars['Int']>;
  /** Project the sprint is associated to. */
  project?: Maybe<Scalars['UUID']>;
  /** Reads a single `Project` that is related to this `Sprint`. */
  projectByProject?: Maybe<Project>;
  /** Reads and enables pagination through a set of `Story`. */
  storiesBySprint: StoriesConnection;
  /** Reads and enables pagination through a set of `Task`. */
  tasksBySprint: TasksConnection;
};

/** A sprint specifies a time frame for the delivery of a set amount of story points. */
export type SprintStoriesBySprintArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<StoriesOrderBy>>;
  condition?: Maybe<StoryCondition>;
};

/** A sprint specifies a time frame for the delivery of a set amount of story points. */
export type SprintTasksBySprintArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TasksOrderBy>>;
  condition?: Maybe<TaskCondition>;
};

/** A condition to be used against `Sprint` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type SprintCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `weight` field. */
  weight?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `project` field. */
  project?: Maybe<Scalars['UUID']>;
};

/** An input for mutations affecting `Sprint` */
export type SprintInput = {
  /** Sprint unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of last change. */
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Name. */
  name: Scalars['String'];
  /** Description. */
  description?: Maybe<Scalars['String']>;
  /** Duration in weeks. */
  duration?: Maybe<Scalars['Int']>;
  /** Determines if this is the current working sprint. */
  active?: Maybe<Scalars['Boolean']>;
  /** Time of activation. */
  activationTime?: Maybe<Scalars['Datetime']>;
  /** Timestamp when this sprint was closed. */
  closed?: Maybe<Scalars['Datetime']>;
  /** Weight in an ordered list. */
  weight?: Maybe<Scalars['Int']>;
  /** Project the sprint is associated to. */
  project?: Maybe<Scalars['UUID']>;
};

/** Represents an update to a `Sprint`. Fields that are set will be updated. */
export type SprintPatch = {
  /** Sprint unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of last change. */
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Name. */
  name?: Maybe<Scalars['String']>;
  /** Description. */
  description?: Maybe<Scalars['String']>;
  /** Duration in weeks. */
  duration?: Maybe<Scalars['Int']>;
  /** Determines if this is the current working sprint. */
  active?: Maybe<Scalars['Boolean']>;
  /** Time of activation. */
  activationTime?: Maybe<Scalars['Datetime']>;
  /** Timestamp when this sprint was closed. */
  closed?: Maybe<Scalars['Datetime']>;
  /** Weight in an ordered list. */
  weight?: Maybe<Scalars['Int']>;
  /** Project the sprint is associated to. */
  project?: Maybe<Scalars['UUID']>;
};

/** A connection to a list of `Sprint` values. */
export type SprintsConnection = {
  __typename?: 'SprintsConnection';
  /** A list of `Sprint` objects. */
  nodes: Array<Sprint>;
  /** A list of edges which contains the `Sprint` and cursor to aid in pagination. */
  edges: Array<SprintsEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Sprint` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `Sprint` edge in the connection. */
export type SprintsEdge = {
  __typename?: 'SprintsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `Sprint` at the end of the edge. */
  node: Sprint;
};

/** Methods to use when ordering `Sprint`. */
export enum SprintsOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  WeightAsc = 'WEIGHT_ASC',
  WeightDesc = 'WEIGHT_DESC',
  ProjectAsc = 'PROJECT_ASC',
  ProjectDesc = 'PROJECT_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
}

export enum State {
  New = 'NEW',
  // Pending = 'PENDING',
  Ongoing = 'ONGOING',
  Blocked = 'BLOCKED',
  // Review = 'REVIEW',
  Finished = 'FINISHED',
}

/** A connection to a list of `Story` values. */
export type StoriesConnection = {
  __typename?: 'StoriesConnection';
  /** A list of `Story` objects. */
  nodes: Array<Story>;
  /** A list of edges which contains the `Story` and cursor to aid in pagination. */
  edges: Array<StoriesEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Story` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `Story` edge in the connection. */
export type StoriesEdge = {
  __typename?: 'StoriesEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `Story` at the end of the edge. */
  node: Story;
};

/** Methods to use when ordering `Story`. */
export enum StoriesOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  WeightAsc = 'WEIGHT_ASC',
  WeightDesc = 'WEIGHT_DESC',
  ProjectAsc = 'PROJECT_ASC',
  ProjectDesc = 'PROJECT_DESC',
  SprintAsc = 'SPRINT_ASC',
  SprintDesc = 'SPRINT_DESC',
  AssigneeAsc = 'ASSIGNEE_ASC',
  AssigneeDesc = 'ASSIGNEE_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
}

/** A story defines a use case to implement. */
export type Story = Node & {
  __typename?: 'Story';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  /** Story unique identifier. */
  id: Scalars['UUID'];
  /** Indicates the time of last change. */
  createdAt: Scalars['Datetime'];
  updatedAt: Scalars['Datetime'];
  /** Name. */
  name: Scalars['String'];
  /** Description. */
  description?: Maybe<Scalars['String']>;
  /** Duration in agile points. */
  points?: Maybe<Scalars['Int']>;
  /** Story state. */
  state: State;
  /** Classification of the story by task type. */
  taskType?: Maybe<TaskType>;
  /** Weight in an ordered list. */
  weight?: Maybe<Scalars['Int']>;
  /** Project the story is associated to. */
  project: Scalars['UUID'];
  /** Sprint the story is associated to. */
  sprint?: Maybe<Scalars['UUID']>;
  /** Project member tasked with the story. */
  assignee?: Maybe<Scalars['UUID']>;
  finishedDate?: Maybe<Scalars['Datetime']>;
  /** Reads a single `Project` that is related to this `Story`. */
  projectByProject?: Maybe<Project>;
  /** Reads a single `Sprint` that is related to this `Story`. */
  sprintBySprint?: Maybe<Sprint>;
  /** Reads a single `UserProfile` that is related to this `Story`. */
  userProfileByAssignee?: Maybe<UserProfile>;
  /** Reads and enables pagination through a set of `Task`. */
  tasksByStory: TasksConnection;
  /** Reads and enables pagination through a set of `Tagbook`. */
  tagbooksByStory: TagbooksConnection;
};

/** A story defines a use case to implement. */
export type StoryTasksByStoryArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TasksOrderBy>>;
  condition?: Maybe<TaskCondition>;
};

/** A story defines a use case to implement. */
export type StoryTagbooksByStoryArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TagbooksOrderBy>>;
  condition?: Maybe<TagbookCondition>;
};

/** A condition to be used against `Story` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type StoryCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `weight` field. */
  weight?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `project` field. */
  project?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `sprint` field. */
  sprint?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `assignee` field. */
  assignee?: Maybe<Scalars['UUID']>;
};

/** An input for mutations affecting `Story` */
export type StoryInput = {
  /** Story unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of last change. */
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Name. */
  name: Scalars['String'];
  /** Description. */
  description?: Maybe<Scalars['String']>;
  /** Duration in agile points. */
  points?: Maybe<Scalars['Int']>;
  /** Story state. */
  state?: Maybe<State>;
  /** Classification of the story by task type. */
  taskType?: Maybe<TaskType>;
  /** Weight in an ordered list. */
  weight?: Maybe<Scalars['Int']>;
  /** Project the story is associated to. */
  project: Scalars['UUID'];
  /** Sprint the story is associated to. */
  sprint?: Maybe<Scalars['UUID']>;
  /** Project member tasked with the story. */
  assignee?: Maybe<Scalars['UUID']>;
  finishedDate?: Maybe<Scalars['Datetime']>;
};

/** Represents an update to a `Story`. Fields that are set will be updated. */
export type StoryPatch = {
  /** Story unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of last change. */
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Name. */
  name?: Maybe<Scalars['String']>;
  /** Description. */
  description?: Maybe<Scalars['String']>;
  /** Duration in agile points. */
  points?: Maybe<Scalars['Int']>;
  /** Story state. */
  state?: Maybe<State>;
  /** Classification of the story by task type. */
  taskType?: Maybe<TaskType>;
  /** Weight in an ordered list. */
  weight?: Maybe<Scalars['Int']>;
  /** Project the story is associated to. */
  project?: Maybe<Scalars['UUID']>;
  /** Sprint the story is associated to. */
  sprint?: Maybe<Scalars['UUID']>;
  /** Project member tasked with the story. */
  assignee?: Maybe<Scalars['UUID']>;
  finishedDate?: Maybe<Scalars['Datetime']>;
};

export type System = Node & {
  __typename?: 'System';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  id: Scalars['UUID'];
  createdAt: Scalars['Datetime'];
  updatedAt: Scalars['Datetime'];
  config: Scalars['JSON'];
};

/** A condition to be used against `System` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type SystemCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['UUID']>;
};

/** An input for mutations affecting `System` */
export type SystemInput = {
  id?: Maybe<Scalars['UUID']>;
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  config: Scalars['JSON'];
};

/** Represents an update to a `System`. Fields that are set will be updated. */
export type SystemPatch = {
  id?: Maybe<Scalars['UUID']>;
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  config?: Maybe<Scalars['JSON']>;
};

/** A connection to a list of `System` values. */
export type SystemsConnection = {
  __typename?: 'SystemsConnection';
  /** A list of `System` objects. */
  nodes: Array<System>;
  /** A list of edges which contains the `System` and cursor to aid in pagination. */
  edges: Array<SystemsEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `System` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `System` edge in the connection. */
export type SystemsEdge = {
  __typename?: 'SystemsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `System` at the end of the edge. */
  node: System;
};

/** Methods to use when ordering `System`. */
export enum SystemsOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
}

/** A tag classifies a story. */
export type Tag = Node & {
  __typename?: 'Tag';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  /** Story unique identifier. */
  id: Scalars['UUID'];
  /** Indicates the time of last change. */
  createdAt: Scalars['Datetime'];
  updatedAt: Scalars['Datetime'];
  /** Name. */
  label: Scalars['String'];
  /** Description. */
  description?: Maybe<Scalars['String']>;
  /** Reads and enables pagination through a set of `Tagbook`. */
  tagbooksByTag: TagbooksConnection;
};

/** A tag classifies a story. */
export type TagTagbooksByTagArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TagbooksOrderBy>>;
  condition?: Maybe<TagbookCondition>;
};

/** A registry of stories and tags relationships. */
export type Tagbook = Node & {
  __typename?: 'Tagbook';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  /** Tagbook entry unique identifier. */
  id: Scalars['UUID'];
  /** UUID of the associated tag. */
  tag: Scalars['UUID'];
  /** UUID of the associated story. */
  story: Scalars['UUID'];
  /** Reads a single `Tag` that is related to this `Tagbook`. */
  tagByTag?: Maybe<Tag>;
  /** Reads a single `Story` that is related to this `Tagbook`. */
  storyByStory?: Maybe<Story>;
};

/** A condition to be used against `Tagbook` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type TagbookCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `tag` field. */
  tag?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `story` field. */
  story?: Maybe<Scalars['UUID']>;
};

/** An input for mutations affecting `Tagbook` */
export type TagbookInput = {
  /** Tagbook entry unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** UUID of the associated tag. */
  tag: Scalars['UUID'];
  /** UUID of the associated story. */
  story: Scalars['UUID'];
};

/** Represents an update to a `Tagbook`. Fields that are set will be updated. */
export type TagbookPatch = {
  /** Tagbook entry unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** UUID of the associated tag. */
  tag?: Maybe<Scalars['UUID']>;
  /** UUID of the associated story. */
  story?: Maybe<Scalars['UUID']>;
};

/** A connection to a list of `Tagbook` values. */
export type TagbooksConnection = {
  __typename?: 'TagbooksConnection';
  /** A list of `Tagbook` objects. */
  nodes: Array<Tagbook>;
  /** A list of edges which contains the `Tagbook` and cursor to aid in pagination. */
  edges: Array<TagbooksEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Tagbook` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `Tagbook` edge in the connection. */
export type TagbooksEdge = {
  __typename?: 'TagbooksEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `Tagbook` at the end of the edge. */
  node: Tagbook;
};

/** Methods to use when ordering `Tagbook`. */
export enum TagbooksOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  TagAsc = 'TAG_ASC',
  TagDesc = 'TAG_DESC',
  StoryAsc = 'STORY_ASC',
  StoryDesc = 'STORY_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
}

/** A condition to be used against `Tag` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type TagCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `label` field. */
  label?: Maybe<Scalars['String']>;
};

/** An input for mutations affecting `Tag` */
export type TagInput = {
  /** Story unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of last change. */
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Name. */
  label: Scalars['String'];
  /** Description. */
  description?: Maybe<Scalars['String']>;
};

/** Represents an update to a `Tag`. Fields that are set will be updated. */
export type TagPatch = {
  /** Story unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of last change. */
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Name. */
  label?: Maybe<Scalars['String']>;
  /** Description. */
  description?: Maybe<Scalars['String']>;
};

/** A connection to a list of `Tag` values. */
export type TagsConnection = {
  __typename?: 'TagsConnection';
  /** A list of `Tag` objects. */
  nodes: Array<Tag>;
  /** A list of edges which contains the `Tag` and cursor to aid in pagination. */
  edges: Array<TagsEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Tag` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `Tag` edge in the connection. */
export type TagsEdge = {
  __typename?: 'TagsEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `Tag` at the end of the edge. */
  node: Tag;
};

/** Methods to use when ordering `Tag`. */
export enum TagsOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  LabelAsc = 'LABEL_ASC',
  LabelDesc = 'LABEL_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
}

/** A task defines a specific body of work inside a story. */
export type Task = Node & {
  __typename?: 'Task';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  /** Task unique identifier. */
  id: Scalars['UUID'];
  /** Indicates the time of last change. */
  createdAt: Scalars['Datetime'];
  updatedAt: Scalars['Datetime'];
  /** Name. */
  name: Scalars['String'];
  /** Description. */
  description?: Maybe<Scalars['String']>;
  /** Duration in agile points. */
  points?: Maybe<Scalars['Int']>;
  /** Task state. */
  state: State;
  /** Classification of the task by task type. */
  taskType?: Maybe<TaskType>;
  /** Weight in an ordered list. */
  weight?: Maybe<Scalars['Int']>;
  /** Project the task is associated to. */
  project: Scalars['UUID'];
  /** Sprint the task is associated to. */
  sprint?: Maybe<Scalars['UUID']>;
  /** Story the task is associated to. */
  story: Scalars['UUID'];
  /** Project member tasked with the story. */
  assignee?: Maybe<Scalars['UUID']>;
  /** Reads a single `Project` that is related to this `Task`. */
  projectByProject?: Maybe<Project>;
  /** Reads a single `Sprint` that is related to this `Task`. */
  sprintBySprint?: Maybe<Sprint>;
  /** Reads a single `Story` that is related to this `Task`. */
  storyByStory?: Maybe<Story>;
  /** Reads a single `UserProfile` that is related to this `Task`. */
  userProfileByAssignee?: Maybe<UserProfile>;
  /** Reads and enables pagination through a set of `Comment`. */
  commentsByTask: CommentsConnection;
  /** Reads and enables pagination through a set of `TimeEntry`. */
  timeEntriesByTask: TimeEntriesConnection;
};

/** A task defines a specific body of work inside a story. */
export type TaskCommentsByTaskArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<CommentsOrderBy>>;
  condition?: Maybe<CommentCondition>;
};

/** A task defines a specific body of work inside a story. */
export type TaskTimeEntriesByTaskArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TimeEntriesOrderBy>>;
  condition?: Maybe<TimeEntryCondition>;
};

/** A condition to be used against `Task` object types. All fields are tested for equality and combined with a logical ‘and.’ */
export type TaskCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `createdAt` field. */
  createdAt?: Maybe<Scalars['Datetime']>;
  /** Checks for equality with the object’s `weight` field. */
  weight?: Maybe<Scalars['Int']>;
  /** Checks for equality with the object’s `project` field. */
  project?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `sprint` field. */
  sprint?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `story` field. */
  story?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `assignee` field. */
  assignee?: Maybe<Scalars['UUID']>;
};

/** An input for mutations affecting `Task` */
export type TaskInput = {
  /** Task unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of last change. */
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Name. */
  name: Scalars['String'];
  /** Description. */
  description?: Maybe<Scalars['String']>;
  /** Duration in agile points. */
  points?: Maybe<Scalars['Int']>;
  /** Task state. */
  state?: Maybe<State>;
  /** Classification of the task by task type. */
  taskType?: Maybe<TaskType>;
  /** Weight in an ordered list. */
  weight?: Maybe<Scalars['Int']>;
  /** Project the task is associated to. */
  project: Scalars['UUID'];
  /** Sprint the task is associated to. */
  sprint?: Maybe<Scalars['UUID']>;
  /** Story the task is associated to. */
  story: Scalars['UUID'];
  /** Project member tasked with the story. */
  assignee?: Maybe<Scalars['UUID']>;
};

/** Represents an update to a `Task`. Fields that are set will be updated. */
export type TaskPatch = {
  /** Task unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of last change. */
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Name. */
  name?: Maybe<Scalars['String']>;
  /** Description. */
  description?: Maybe<Scalars['String']>;
  /** Duration in agile points. */
  points?: Maybe<Scalars['Int']>;
  /** Task state. */
  state?: Maybe<State>;
  /** Classification of the task by task type. */
  taskType?: Maybe<TaskType>;
  /** Weight in an ordered list. */
  weight?: Maybe<Scalars['Int']>;
  /** Project the task is associated to. */
  project?: Maybe<Scalars['UUID']>;
  /** Sprint the task is associated to. */
  sprint?: Maybe<Scalars['UUID']>;
  /** Story the task is associated to. */
  story?: Maybe<Scalars['UUID']>;
  /** Project member tasked with the story. */
  assignee?: Maybe<Scalars['UUID']>;
};

/** A connection to a list of `Task` values. */
export type TasksConnection = {
  __typename?: 'TasksConnection';
  /** A list of `Task` objects. */
  nodes: Array<Task>;
  /** A list of edges which contains the `Task` and cursor to aid in pagination. */
  edges: Array<TasksEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `Task` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `Task` edge in the connection. */
export type TasksEdge = {
  __typename?: 'TasksEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `Task` at the end of the edge. */
  node: Task;
};

/** Methods to use when ordering `Task`. */
export enum TasksOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  CreatedAtAsc = 'CREATED_AT_ASC',
  CreatedAtDesc = 'CREATED_AT_DESC',
  WeightAsc = 'WEIGHT_ASC',
  WeightDesc = 'WEIGHT_DESC',
  ProjectAsc = 'PROJECT_ASC',
  ProjectDesc = 'PROJECT_DESC',
  SprintAsc = 'SPRINT_ASC',
  SprintDesc = 'SPRINT_DESC',
  StoryAsc = 'STORY_ASC',
  StoryDesc = 'STORY_DESC',
  AssigneeAsc = 'ASSIGNEE_ASC',
  AssigneeDesc = 'ASSIGNEE_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
}

export enum TaskType {
  Management = 'MANAGEMENT',
  Analysis = 'ANALYSIS',
  Design = 'DESIGN',
  Frontend = 'FRONTEND',
  Backend = 'BACKEND',
  Testing = 'TESTING',
  Systems = 'SYSTEMS',
}

/** A connection to a list of `TimeEntry` values. */
export type TimeEntriesConnection = {
  __typename?: 'TimeEntriesConnection';
  /** A list of `TimeEntry` objects. */
  nodes: Array<TimeEntry>;
  /** A list of edges which contains the `TimeEntry` and cursor to aid in pagination. */
  edges: Array<TimeEntriesEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `TimeEntry` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `TimeEntry` edge in the connection. */
export type TimeEntriesEdge = {
  __typename?: 'TimeEntriesEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `TimeEntry` at the end of the edge. */
  node: TimeEntry;
};

/** Methods to use when ordering `TimeEntry`. */
export enum TimeEntriesOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  AuthorAsc = 'AUTHOR_ASC',
  AuthorDesc = 'AUTHOR_DESC',
  TaskAsc = 'TASK_ASC',
  TaskDesc = 'TASK_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
}

/** Time spent in an specific task inside a story. */
export type TimeEntry = Node & {
  __typename?: 'TimeEntry';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  /** Entry unique identifier. */
  id: Scalars['UUID'];
  /** Indicates the time of last change. */
  createdAt: Scalars['Datetime'];
  updatedAt: Scalars['Datetime'];
  /** Time spent in the task. */
  hours: Scalars['BigFloat'];
  /** Relative percentage of work done for the parent story. */
  progress: Scalars['BigFloat'];
  /** Description of the task. */
  description: Scalars['String'];
  /** The day the work was done. */
  time: Scalars['Datetime'];
  /** User who created the entry. */
  author: Scalars['UUID'];
  /** Task where the time entry is created. */
  task: Scalars['UUID'];
  /** Reads a single `UserProfile` that is related to this `TimeEntry`. */
  userProfileByAuthor?: Maybe<UserProfile>;
  /** Reads a single `Task` that is related to this `TimeEntry`. */
  taskByTask?: Maybe<Task>;
};

/**
 * A condition to be used against `TimeEntry` object types. All fields are tested
 * for equality and combined with a logical ‘and.’
 */
export type TimeEntryCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `author` field. */
  author?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `task` field. */
  task?: Maybe<Scalars['UUID']>;
};

/** An input for mutations affecting `TimeEntry` */
export type TimeEntryInput = {
  /** Entry unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of last change. */
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Time spent in the task. */
  hours: Scalars['BigFloat'];
  /** Relative percentage of work done for the parent story. */
  progress: Scalars['BigFloat'];
  /** Description of the task. */
  description: Scalars['String'];
  /** The day the work was done. */
  time?: Maybe<Scalars['Datetime']>;
  /** User who created the entry. */
  author: Scalars['UUID'];
  /** Task where the time entry is created. */
  task: Scalars['UUID'];
};

/** Represents an update to a `TimeEntry`. Fields that are set will be updated. */
export type TimeEntryPatch = {
  /** Entry unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Indicates the time of last change. */
  createdAt?: Maybe<Scalars['Datetime']>;
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** Time spent in the task. */
  hours?: Maybe<Scalars['BigFloat']>;
  /** Relative percentage of work done for the parent story. */
  progress?: Maybe<Scalars['BigFloat']>;
  /** Description of the task. */
  description?: Maybe<Scalars['String']>;
  /** The day the work was done. */
  time?: Maybe<Scalars['Datetime']>;
  /** User who created the entry. */
  author?: Maybe<Scalars['UUID']>;
  /** Task where the time entry is created. */
  task?: Maybe<Scalars['UUID']>;
};

/** All input for the `updateCommentByNodeId` mutation. */
export type UpdateCommentByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Comment` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `Comment` being updated. */
  patch: CommentPatch;
};

/** All input for the `updateComment` mutation. */
export type UpdateCommentInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `Comment` being updated. */
  patch: CommentPatch;
  /** Comment unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our update `Comment` mutation. */
export type UpdateCommentPayload = {
  __typename?: 'UpdateCommentPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Comment` that was updated by this mutation. */
  comment?: Maybe<Comment>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `UserProfile` that is related to this `Comment`. */
  userProfileByAuthor?: Maybe<UserProfile>;
  /** Reads a single `Task` that is related to this `Comment`. */
  taskByTask?: Maybe<Task>;
  /** An edge for our `Comment`. May be used by Relay 1. */
  commentEdge?: Maybe<CommentsEdge>;
};

/** The output of our update `Comment` mutation. */
export type UpdateCommentPayloadCommentEdgeArgs = {
  orderBy?: Maybe<Array<CommentsOrderBy>>;
};

/** All input for the `updateMemberByNodeId` mutation. */
export type UpdateMemberByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Member` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `Member` being updated. */
  patch: MemberPatch;
};

/** All input for the `updateMember` mutation. */
export type UpdateMemberInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `Member` being updated. */
  patch: MemberPatch;
  id: Scalars['UUID'];
};

/** The output of our update `Member` mutation. */
export type UpdateMemberPayload = {
  __typename?: 'UpdateMemberPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Member` that was updated by this mutation. */
  member?: Maybe<Member>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Project` that is related to this `Member`. */
  projectByProject?: Maybe<Project>;
  /** Reads a single `UserProfile` that is related to this `Member`. */
  userProfileByUserProfile?: Maybe<UserProfile>;
  /** An edge for our `Member`. May be used by Relay 1. */
  memberEdge?: Maybe<MembersEdge>;
};

/** The output of our update `Member` mutation. */
export type UpdateMemberPayloadMemberEdgeArgs = {
  orderBy?: Maybe<Array<MembersOrderBy>>;
};

/** All input for the `updateProjectByNodeId` mutation. */
export type UpdateProjectByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Project` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `Project` being updated. */
  patch: ProjectPatch;
};

/** All input for the `updateProject` mutation. */
export type UpdateProjectInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `Project` being updated. */
  patch: ProjectPatch;
  /** Unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our update `Project` mutation. */
export type UpdateProjectPayload = {
  __typename?: 'UpdateProjectPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Project` that was updated by this mutation. */
  project?: Maybe<Project>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Project`. May be used by Relay 1. */
  projectEdge?: Maybe<ProjectsEdge>;
};

/** The output of our update `Project` mutation. */
export type UpdateProjectPayloadProjectEdgeArgs = {
  orderBy?: Maybe<Array<ProjectsOrderBy>>;
};

/** All input for the `updateRoleByName` mutation. */
export type UpdateRoleByNameInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `Role` being updated. */
  patch: RolePatch;
  /** Unique name. */
  name: Scalars['String'];
};

/** All input for the `updateRoleByNodeId` mutation. */
export type UpdateRoleByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Role` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `Role` being updated. */
  patch: RolePatch;
};

/** All input for the `updateRole` mutation. */
export type UpdateRoleInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `Role` being updated. */
  patch: RolePatch;
  /** Unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our update `Role` mutation. */
export type UpdateRolePayload = {
  __typename?: 'UpdateRolePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Role` that was updated by this mutation. */
  role?: Maybe<Role>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Role`. May be used by Relay 1. */
  roleEdge?: Maybe<RolesEdge>;
};

/** The output of our update `Role` mutation. */
export type UpdateRolePayloadRoleEdgeArgs = {
  orderBy?: Maybe<Array<RolesOrderBy>>;
};

/** All input for the `updateSprintByNodeId` mutation. */
export type UpdateSprintByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Sprint` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `Sprint` being updated. */
  patch: SprintPatch;
};

/** All input for the `updateSprint` mutation. */
export type UpdateSprintInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `Sprint` being updated. */
  patch: SprintPatch;
  /** Sprint unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our update `Sprint` mutation. */
export type UpdateSprintPayload = {
  __typename?: 'UpdateSprintPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Sprint` that was updated by this mutation. */
  sprint?: Maybe<Sprint>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Project` that is related to this `Sprint`. */
  projectByProject?: Maybe<Project>;
  /** An edge for our `Sprint`. May be used by Relay 1. */
  sprintEdge?: Maybe<SprintsEdge>;
};

/** The output of our update `Sprint` mutation. */
export type UpdateSprintPayloadSprintEdgeArgs = {
  orderBy?: Maybe<Array<SprintsOrderBy>>;
};

/** All input for the `updateStoryByNodeId` mutation. */
export type UpdateStoryByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Story` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `Story` being updated. */
  patch: StoryPatch;
};

/** All input for the `updateStory` mutation. */
export type UpdateStoryInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `Story` being updated. */
  patch: StoryPatch;
  /** Story unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our update `Story` mutation. */
export type UpdateStoryPayload = {
  __typename?: 'UpdateStoryPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Story` that was updated by this mutation. */
  story?: Maybe<Story>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Project` that is related to this `Story`. */
  projectByProject?: Maybe<Project>;
  /** Reads a single `Sprint` that is related to this `Story`. */
  sprintBySprint?: Maybe<Sprint>;
  /** Reads a single `UserProfile` that is related to this `Story`. */
  userProfileByAssignee?: Maybe<UserProfile>;
  /** An edge for our `Story`. May be used by Relay 1. */
  storyEdge?: Maybe<StoriesEdge>;
};

/** The output of our update `Story` mutation. */
export type UpdateStoryPayloadStoryEdgeArgs = {
  orderBy?: Maybe<Array<StoriesOrderBy>>;
};

/** All input for the `updateSystemByNodeId` mutation. */
export type UpdateSystemByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `System` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `System` being updated. */
  patch: SystemPatch;
};

/** All input for the `updateSystem` mutation. */
export type UpdateSystemInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `System` being updated. */
  patch: SystemPatch;
  id: Scalars['UUID'];
};

/** The output of our update `System` mutation. */
export type UpdateSystemPayload = {
  __typename?: 'UpdateSystemPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `System` that was updated by this mutation. */
  system?: Maybe<System>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `System`. May be used by Relay 1. */
  systemEdge?: Maybe<SystemsEdge>;
};

/** The output of our update `System` mutation. */
export type UpdateSystemPayloadSystemEdgeArgs = {
  orderBy?: Maybe<Array<SystemsOrderBy>>;
};

/** All input for the `updateTagbookByNodeId` mutation. */
export type UpdateTagbookByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Tagbook` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `Tagbook` being updated. */
  patch: TagbookPatch;
};

/** All input for the `updateTagbook` mutation. */
export type UpdateTagbookInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `Tagbook` being updated. */
  patch: TagbookPatch;
  /** Tagbook entry unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our update `Tagbook` mutation. */
export type UpdateTagbookPayload = {
  __typename?: 'UpdateTagbookPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Tagbook` that was updated by this mutation. */
  tagbook?: Maybe<Tagbook>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Tag` that is related to this `Tagbook`. */
  tagByTag?: Maybe<Tag>;
  /** Reads a single `Story` that is related to this `Tagbook`. */
  storyByStory?: Maybe<Story>;
  /** An edge for our `Tagbook`. May be used by Relay 1. */
  tagbookEdge?: Maybe<TagbooksEdge>;
};

/** The output of our update `Tagbook` mutation. */
export type UpdateTagbookPayloadTagbookEdgeArgs = {
  orderBy?: Maybe<Array<TagbooksOrderBy>>;
};

/** All input for the `updateTagByLabel` mutation. */
export type UpdateTagByLabelInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `Tag` being updated. */
  patch: TagPatch;
  /** Name. */
  label: Scalars['String'];
};

/** All input for the `updateTagByNodeId` mutation. */
export type UpdateTagByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Tag` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `Tag` being updated. */
  patch: TagPatch;
};

/** All input for the `updateTag` mutation. */
export type UpdateTagInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `Tag` being updated. */
  patch: TagPatch;
  /** Story unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our update `Tag` mutation. */
export type UpdateTagPayload = {
  __typename?: 'UpdateTagPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Tag` that was updated by this mutation. */
  tag?: Maybe<Tag>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** An edge for our `Tag`. May be used by Relay 1. */
  tagEdge?: Maybe<TagsEdge>;
};

/** The output of our update `Tag` mutation. */
export type UpdateTagPayloadTagEdgeArgs = {
  orderBy?: Maybe<Array<TagsOrderBy>>;
};

/** All input for the `updateTaskByNodeId` mutation. */
export type UpdateTaskByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `Task` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `Task` being updated. */
  patch: TaskPatch;
};

/** All input for the `updateTask` mutation. */
export type UpdateTaskInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `Task` being updated. */
  patch: TaskPatch;
  /** Task unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our update `Task` mutation. */
export type UpdateTaskPayload = {
  __typename?: 'UpdateTaskPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `Task` that was updated by this mutation. */
  task?: Maybe<Task>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Project` that is related to this `Task`. */
  projectByProject?: Maybe<Project>;
  /** Reads a single `Sprint` that is related to this `Task`. */
  sprintBySprint?: Maybe<Sprint>;
  /** Reads a single `Story` that is related to this `Task`. */
  storyByStory?: Maybe<Story>;
  /** Reads a single `UserProfile` that is related to this `Task`. */
  userProfileByAssignee?: Maybe<UserProfile>;
  /** An edge for our `Task`. May be used by Relay 1. */
  taskEdge?: Maybe<TasksEdge>;
};

/** The output of our update `Task` mutation. */
export type UpdateTaskPayloadTaskEdgeArgs = {
  orderBy?: Maybe<Array<TasksOrderBy>>;
};

/** All input for the `updateTimeEntryByNodeId` mutation. */
export type UpdateTimeEntryByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `TimeEntry` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `TimeEntry` being updated. */
  patch: TimeEntryPatch;
};

/** All input for the `updateTimeEntry` mutation. */
export type UpdateTimeEntryInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `TimeEntry` being updated. */
  patch: TimeEntryPatch;
  /** Entry unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our update `TimeEntry` mutation. */
export type UpdateTimeEntryPayload = {
  __typename?: 'UpdateTimeEntryPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `TimeEntry` that was updated by this mutation. */
  timeEntry?: Maybe<TimeEntry>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `UserProfile` that is related to this `TimeEntry`. */
  userProfileByAuthor?: Maybe<UserProfile>;
  /** Reads a single `Task` that is related to this `TimeEntry`. */
  taskByTask?: Maybe<Task>;
  /** An edge for our `TimeEntry`. May be used by Relay 1. */
  timeEntryEdge?: Maybe<TimeEntriesEdge>;
};

/** The output of our update `TimeEntry` mutation. */
export type UpdateTimeEntryPayloadTimeEntryEdgeArgs = {
  orderBy?: Maybe<Array<TimeEntriesOrderBy>>;
};

/** All input for the `updateUserProfileByNodeId` mutation. */
export type UpdateUserProfileByNodeIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The globally unique `ID` which will identify a single `UserProfile` to be updated. */
  nodeId: Scalars['ID'];
  /** An object where the defined keys will be set on the `UserProfile` being updated. */
  patch: UserProfilePatch;
};

/** All input for the `updateUserProfile` mutation. */
export type UpdateUserProfileInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** An object where the defined keys will be set on the `UserProfile` being updated. */
  patch: UserProfilePatch;
  /** Unique identifier. */
  id: Scalars['UUID'];
};

/** The output of our update `UserProfile` mutation. */
export type UpdateUserProfilePayload = {
  __typename?: 'UpdateUserProfilePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']>;
  /** The `UserProfile` that was updated by this mutation. */
  userProfile?: Maybe<UserProfile>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
  /** Reads a single `Role` that is related to this `UserProfile`. */
  roleByRole?: Maybe<Role>;
  /** An edge for our `UserProfile`. May be used by Relay 1. */
  userProfileEdge?: Maybe<UserProfilesEdge>;
};

/** The output of our update `UserProfile` mutation. */
export type UpdateUserProfilePayloadUserProfileEdgeArgs = {
  orderBy?: Maybe<Array<UserProfilesOrderBy>>;
};

/** Public information of a user. */
export type UserProfile = Node & {
  __typename?: 'UserProfile';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  nodeId: Scalars['ID'];
  /** Unique identifier. */
  id: Scalars['UUID'];
  /** Time of creation. */
  createdAt: Scalars['Datetime'];
  /** Time of last change. */
  updatedAt: Scalars['Datetime'];
  /** First name. */
  firstName?: Maybe<Scalars['String']>;
  /** Surname. */
  lastName: Scalars['String'];
  picture?: Maybe<Scalars['String']>;
  /** Information about the skills of the user. */
  proficiency?: Maybe<Scalars['JSON']>;
  /** User assigned role. */
  role?: Maybe<Scalars['UUID']>;
  /** Reads a single `Role` that is related to this `UserProfile`. */
  roleByRole?: Maybe<Role>;
  /** Reads and enables pagination through a set of `Member`. */
  membersByUserProfile: MembersConnection;
  /** Reads and enables pagination through a set of `Story`. */
  storiesByAssignee: StoriesConnection;
  /** Reads and enables pagination through a set of `Task`. */
  tasksByAssignee: TasksConnection;
  /** Reads and enables pagination through a set of `Comment`. */
  commentsByAuthor: CommentsConnection;
  /** Reads and enables pagination through a set of `TimeEntry`. */
  timeEntriesByAuthor: TimeEntriesConnection;
  /** A user full name which is a concatenation of their first and last names. */
  fullName?: Maybe<Scalars['String']>;
};

/** Public information of a user. */
export type UserProfileMembersByUserProfileArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<MembersOrderBy>>;
  condition?: Maybe<MemberCondition>;
};

/** Public information of a user. */
export type UserProfileStoriesByAssigneeArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<StoriesOrderBy>>;
  condition?: Maybe<StoryCondition>;
};

/** Public information of a user. */
export type UserProfileTasksByAssigneeArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TasksOrderBy>>;
  condition?: Maybe<TaskCondition>;
};

/** Public information of a user. */
export type UserProfileCommentsByAuthorArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<CommentsOrderBy>>;
  condition?: Maybe<CommentCondition>;
};

/** Public information of a user. */
export type UserProfileTimeEntriesByAuthorArgs = {
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['Cursor']>;
  after?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TimeEntriesOrderBy>>;
  condition?: Maybe<TimeEntryCondition>;
};

/**
 * A condition to be used against `UserProfile` object types. All fields are tested
 * for equality and combined with a logical ‘and.’
 */
export type UserProfileCondition = {
  /** Checks for equality with the object’s `id` field. */
  id?: Maybe<Scalars['UUID']>;
  /** Checks for equality with the object’s `role` field. */
  role?: Maybe<Scalars['UUID']>;
};

/** An input for mutations affecting `UserProfile` */
export type UserProfileInput = {
  /** Unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Time of creation. */
  createdAt?: Maybe<Scalars['Datetime']>;
  /** Time of last change. */
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** First name. */
  firstName?: Maybe<Scalars['String']>;
  /** Surname. */
  lastName: Scalars['String'];
  picture?: Maybe<Scalars['String']>;
  /** Information about the skills of the user. */
  proficiency?: Maybe<Scalars['JSON']>;
  /** User assigned role. */
  role?: Maybe<Scalars['UUID']>;
};

/** Represents an update to a `UserProfile`. Fields that are set will be updated. */
export type UserProfilePatch = {
  /** Unique identifier. */
  id?: Maybe<Scalars['UUID']>;
  /** Time of creation. */
  createdAt?: Maybe<Scalars['Datetime']>;
  /** Time of last change. */
  updatedAt?: Maybe<Scalars['Datetime']>;
  /** First name. */
  firstName?: Maybe<Scalars['String']>;
  /** Surname. */
  lastName?: Maybe<Scalars['String']>;
  picture?: Maybe<Scalars['String']>;
  /** Information about the skills of the user. */
  proficiency?: Maybe<Scalars['JSON']>;
  /** User assigned role. */
  role?: Maybe<Scalars['UUID']>;
};

/** A connection to a list of `UserProfile` values. */
export type UserProfilesConnection = {
  __typename?: 'UserProfilesConnection';
  /** A list of `UserProfile` objects. */
  nodes: Array<UserProfile>;
  /** A list of edges which contains the `UserProfile` and cursor to aid in pagination. */
  edges: Array<UserProfilesEdge>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `UserProfile` you could get from the connection. */
  totalCount: Scalars['Int'];
};

/** A `UserProfile` edge in the connection. */
export type UserProfilesEdge = {
  __typename?: 'UserProfilesEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']>;
  /** The `UserProfile` at the end of the edge. */
  node: UserProfile;
};

/** Methods to use when ordering `UserProfile`. */
export enum UserProfilesOrderBy {
  Natural = 'NATURAL',
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  RoleAsc = 'ROLE_ASC',
  RoleDesc = 'ROLE_DESC',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC',
}

export type AuthenticateMutationVariables = Exact<{
  item: AuthenticateInput;
}>;

export type AuthenticateMutation = { __typename?: 'Mutation' } & {
  authenticate?: Maybe<{ __typename?: 'AuthenticatePayload' } & { token: AuthenticatePayload['jwtToken'] }>;
};

export type GetCurrentUserQueryVariables = Exact<{ [key: string]: never }>;

export type GetCurrentUserQuery = { __typename?: 'Query' } & {
  currentUser?: Maybe<
    { __typename?: 'UserProfile' } & Pick<
      UserProfile,
      'id' | 'createdAt' | 'updatedAt' | 'firstName' | 'lastName' | 'fullName' | 'picture' | 'proficiency'
    > & { roleByRole?: Maybe<{ __typename?: 'Role' } & Pick<Role, 'id' | 'name'>> }
  >;
};

export type CreateCommentMutationVariables = Exact<{
  item: CommentInput;
}>;

export type CreateCommentMutation = { __typename?: 'Mutation' } & {
  createComment?: Maybe<
    { __typename?: 'CreateCommentPayload' } & Pick<CreateCommentPayload, 'clientMutationId'> & {
        comment?: Maybe<
          { __typename?: 'Comment' } & Pick<
            Comment,
            'id' | 'createdAt' | 'updatedAt' | 'content' | 'author' | 'task'
          > & {
              userProfileByAuthor?: Maybe<
                { __typename?: 'UserProfile' } & Pick<UserProfile, 'id' | 'fullName' | 'picture'>
              >;
              taskByTask?: Maybe<{ __typename?: 'Task' } & Pick<Task, 'id' | 'name'>>;
            }
        >;
      }
  >;
};

export type DeleteCommentMutationVariables = Exact<{
  id: Scalars['UUID'];
}>;

export type DeleteCommentMutation = { __typename?: 'Mutation' } & {
  deleteComment?: Maybe<
    { __typename?: 'DeleteCommentPayload' } & Pick<DeleteCommentPayload, 'clientMutationId' | 'deletedCommentNodeId'>
  >;
};

export type GetCommentsQueryVariables = Exact<{
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['Cursor']>;
  before?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<CommentsOrderBy>>;
}>;

export type GetCommentsQuery = { __typename?: 'Query' } & {
  comments?: Maybe<
    { __typename?: 'CommentsConnection' } & Pick<CommentsConnection, 'totalCount'> & {
        edges: Array<
          { __typename?: 'CommentsEdge' } & Pick<CommentsEdge, 'cursor'> & {
              node: { __typename?: 'Comment' } & Pick<
                Comment,
                'id' | 'createdAt' | 'updatedAt' | 'content' | 'author' | 'task'
              > & {
                  userProfileByAuthor?: Maybe<
                    { __typename?: 'UserProfile' } & Pick<UserProfile, 'id' | 'fullName' | 'picture'>
                  >;
                  taskByTask?: Maybe<{ __typename?: 'Task' } & Pick<Task, 'id' | 'name'>>;
                };
            }
        >;
        pageInfo: { __typename?: 'PageInfo' } & Pick<
          PageInfo,
          'hasNextPage' | 'hasPreviousPage' | 'startCursor' | 'endCursor'
        >;
      }
  >;
};

export type CreateMemberMutationVariables = Exact<{
  item: MemberInput;
}>;

export type CreateMemberMutation = { __typename?: 'Mutation' } & {
  createMember?: Maybe<
    { __typename?: 'CreateMemberPayload' } & Pick<CreateMemberPayload, 'clientMutationId'> & {
        member?: Maybe<{ __typename?: 'Member' } & Pick<Member, 'project' | 'userProfile'>>;
      }
  >;
};

export type DeleteMemberMutationVariables = Exact<{
  id: Scalars['UUID'];
}>;

export type DeleteMemberMutation = { __typename?: 'Mutation' } & {
  deleteMember?: Maybe<
    { __typename?: 'DeleteMemberPayload' } & Pick<DeleteMemberPayload, 'clientMutationId' | 'deletedMemberNodeId'>
  >;
};

export type CreateProjectMutationVariables = Exact<{
  item: ProjectInput;
}>;

export type CreateProjectMutation = { __typename?: 'Mutation' } & {
  createProject?: Maybe<
    { __typename?: 'CreateProjectPayload' } & {
      project?: Maybe<
        { __typename?: 'Project' } & Pick<
          Project,
          | 'id'
          | 'name'
          | 'config'
          | 'createdAt'
          | 'deadline'
          | 'description'
          | 'finish'
          | 'priority'
          | 'repository'
          | 'start'
          | 'state'
          | 'weight'
          | 'wiki'
          | 'sprintDuration'
        >
      >;
    }
  >;
};

export type DeleteProjectMutationVariables = Exact<{
  id: Scalars['UUID'];
}>;

export type DeleteProjectMutation = { __typename?: 'Mutation' } & {
  deleteProject?: Maybe<{ __typename?: 'DeleteProjectPayload' } & Pick<DeleteProjectPayload, 'deletedProjectNodeId'>>;
};

export type GetProjectQueryVariables = Exact<{
  id: Scalars['UUID'];
  storiesOrderBy?: Maybe<Array<StoriesOrderBy>>;
}>;

export type GetProjectQuery = { __typename?: 'Query' } & {
  project?: Maybe<
    { __typename?: 'Project' } & Pick<
      Project,
      | 'id'
      | 'name'
      | 'createdAt'
      | 'updatedAt'
      | 'start'
      | 'deadline'
      | 'finish'
      | 'description'
      | 'priority'
      | 'state'
      | 'repository'
      | 'wiki'
      | 'weight'
      | 'config'
      | 'sprintDuration'
    > & {
        membersByProject: { __typename?: 'MembersConnection' } & Pick<MembersConnection, 'totalCount'> & {
            nodes: Array<
              { __typename?: 'Member' } & Pick<Member, 'id'> & {
                  userProfileByUserProfile?: Maybe<
                    { __typename?: 'UserProfile' } & Pick<
                      UserProfile,
                      'id' | 'firstName' | 'lastName' | 'fullName' | 'picture' | 'proficiency' | 'createdAt'
                    > & {
                        roleByRole?: Maybe<
                          { __typename?: 'Role' } & Pick<Role, 'id' | 'name' | 'createdAt' | 'updatedAt'>
                        >;
                        timeEntriesByAuthor: { __typename?: 'TimeEntriesConnection' } & Pick<
                          TimeEntriesConnection,
                          'totalCount'
                        > & {
                            nodes: Array<
                              { __typename?: 'TimeEntry' } & Pick<
                                TimeEntry,
                                | 'createdAt'
                                | 'updatedAt'
                                | 'id'
                                | 'description'
                                | 'hours'
                                | 'progress'
                                | 'time'
                                | 'task'
                              > & {
                                  taskByTask?: Maybe<
                                    { __typename?: 'Task' } & Pick<
                                      Task,
                                      | 'id'
                                      | 'createdAt'
                                      | 'updatedAt'
                                      | 'name'
                                      | 'description'
                                      | 'points'
                                      | 'state'
                                      | 'taskType'
                                      | 'project'
                                      | 'sprint'
                                      | 'story'
                                    >
                                  >;
                                }
                            >;
                          };
                      }
                  >;
                }
            >;
          };
        sprintsByProject: { __typename?: 'SprintsConnection' } & Pick<SprintsConnection, 'totalCount'> & {
            nodes: Array<
              { __typename?: 'Sprint' } & Pick<
                Sprint,
                | 'id'
                | 'name'
                | 'description'
                | 'createdAt'
                | 'updatedAt'
                | 'duration'
                | 'active'
                | 'activationTime'
                | 'closed'
                | 'weight'
              > & {
                  tasksBySprint: { __typename?: 'TasksConnection' } & Pick<TasksConnection, 'totalCount'> & {
                      nodes: Array<
                        { __typename?: 'Task' } & Pick<
                          Task,
                          | 'id'
                          | 'createdAt'
                          | 'updatedAt'
                          | 'name'
                          | 'description'
                          | 'points'
                          | 'state'
                          | 'project'
                          | 'sprint'
                          | 'story'
                          | 'assignee'
                          | 'taskType'
                        > & {
                            userProfileByAssignee?: Maybe<
                              { __typename?: 'UserProfile' } & Pick<
                                UserProfile,
                                'id' | 'firstName' | 'lastName' | 'fullName' | 'picture'
                              >
                            >;
                            timeEntriesByTask: { __typename?: 'TimeEntriesConnection' } & Pick<
                              TimeEntriesConnection,
                              'totalCount'
                            > & {
                                nodes: Array<
                                  { __typename?: 'TimeEntry' } & Pick<
                                    TimeEntry,
                                    | 'createdAt'
                                    | 'updatedAt'
                                    | 'id'
                                    | 'description'
                                    | 'hours'
                                    | 'progress'
                                    | 'time'
                                    | 'author'
                                  > & {
                                      userProfileByAuthor?: Maybe<
                                        { __typename?: 'UserProfile' } & Pick<
                                          UserProfile,
                                          | 'id'
                                          | 'picture'
                                          | 'firstName'
                                          | 'lastName'
                                          | 'fullName'
                                          | 'role'
                                          | 'proficiency'
                                        >
                                      >;
                                    }
                                >;
                              };
                          }
                      >;
                    };
                  storiesBySprint: { __typename?: 'StoriesConnection' } & Pick<StoriesConnection, 'totalCount'> & {
                      nodes: Array<
                        { __typename?: 'Story' } & Pick<
                          Story,
                          | 'id'
                          | 'name'
                          | 'description'
                          | 'createdAt'
                          | 'updatedAt'
                          | 'points'
                          | 'state'
                          | 'taskType'
                          | 'finishedDate'
                          | 'weight'
                          | 'sprint'
                          | 'assignee'
                        > & {
                            tasksByStory: { __typename?: 'TasksConnection' } & Pick<TasksConnection, 'totalCount'> & {
                                nodes: Array<
                                  { __typename?: 'Task' } & Pick<
                                    Task,
                                    | 'id'
                                    | 'createdAt'
                                    | 'updatedAt'
                                    | 'name'
                                    | 'description'
                                    | 'points'
                                    | 'state'
                                    | 'taskType'
                                    | 'project'
                                    | 'sprint'
                                    | 'story'
                                    | 'assignee'
                                  > & {
                                      sprintBySprint?: Maybe<{ __typename?: 'Sprint' } & Pick<Sprint, 'id' | 'name'>>;
                                      storyByStory?: Maybe<{ __typename?: 'Story' } & Pick<Story, 'id' | 'name'>>;
                                      userProfileByAssignee?: Maybe<
                                        { __typename?: 'UserProfile' } & Pick<
                                          UserProfile,
                                          'id' | 'firstName' | 'lastName' | 'fullName' | 'picture'
                                        >
                                      >;
                                      timeEntriesByTask: { __typename?: 'TimeEntriesConnection' } & Pick<
                                        TimeEntriesConnection,
                                        'totalCount'
                                      > & {
                                          nodes: Array<
                                            { __typename?: 'TimeEntry' } & Pick<
                                              TimeEntry,
                                              | 'createdAt'
                                              | 'updatedAt'
                                              | 'id'
                                              | 'description'
                                              | 'hours'
                                              | 'progress'
                                              | 'time'
                                              | 'author'
                                            > & {
                                                userProfileByAuthor?: Maybe<
                                                  { __typename?: 'UserProfile' } & Pick<
                                                    UserProfile,
                                                    | 'id'
                                                    | 'picture'
                                                    | 'firstName'
                                                    | 'lastName'
                                                    | 'fullName'
                                                    | 'role'
                                                    | 'proficiency'
                                                  >
                                                >;
                                              }
                                          >;
                                        };
                                    }
                                >;
                              };
                          }
                      >;
                    };
                }
            >;
          };
        storiesByProject: { __typename?: 'StoriesConnection' } & Pick<StoriesConnection, 'totalCount'> & {
            nodes: Array<
              { __typename?: 'Story' } & Pick<
                Story,
                | 'id'
                | 'name'
                | 'description'
                | 'createdAt'
                | 'updatedAt'
                | 'points'
                | 'state'
                | 'taskType'
                | 'finishedDate'
                | 'weight'
                | 'sprint'
                | 'assignee'
              > & {
                  sprintBySprint?: Maybe<
                    { __typename?: 'Sprint' } & Pick<
                      Sprint,
                      | 'id'
                      | 'name'
                      | 'description'
                      | 'createdAt'
                      | 'updatedAt'
                      | 'duration'
                      | 'active'
                      | 'activationTime'
                      | 'closed'
                      | 'weight'
                    >
                  >;
                  userProfileByAssignee?: Maybe<
                    { __typename?: 'UserProfile' } & Pick<
                      UserProfile,
                      'id' | 'firstName' | 'lastName' | 'fullName' | 'picture'
                    >
                  >;
                  tagbooksByStory: { __typename?: 'TagbooksConnection' } & Pick<TagbooksConnection, 'totalCount'> & {
                      nodes: Array<
                        { __typename?: 'Tagbook' } & Pick<Tagbook, 'id'> & {
                            tagByTag?: Maybe<{ __typename?: 'Tag' } & Pick<Tag, 'id' | 'label'>>;
                          }
                      >;
                    };
                  tasksByStory: { __typename?: 'TasksConnection' } & Pick<TasksConnection, 'totalCount'> & {
                      nodes: Array<
                        { __typename?: 'Task' } & Pick<
                          Task,
                          | 'id'
                          | 'createdAt'
                          | 'updatedAt'
                          | 'name'
                          | 'description'
                          | 'points'
                          | 'state'
                          | 'taskType'
                          | 'project'
                          | 'sprint'
                          | 'story'
                          | 'assignee'
                        > & {
                            sprintBySprint?: Maybe<{ __typename?: 'Sprint' } & Pick<Sprint, 'id' | 'name'>>;
                            storyByStory?: Maybe<{ __typename?: 'Story' } & Pick<Story, 'id' | 'name'>>;
                            userProfileByAssignee?: Maybe<
                              { __typename?: 'UserProfile' } & Pick<
                                UserProfile,
                                'id' | 'firstName' | 'lastName' | 'fullName' | 'picture' | 'proficiency'
                              >
                            >;
                            timeEntriesByTask: { __typename?: 'TimeEntriesConnection' } & Pick<
                              TimeEntriesConnection,
                              'totalCount'
                            > & {
                                nodes: Array<
                                  { __typename?: 'TimeEntry' } & Pick<
                                    TimeEntry,
                                    | 'createdAt'
                                    | 'updatedAt'
                                    | 'id'
                                    | 'description'
                                    | 'hours'
                                    | 'progress'
                                    | 'time'
                                    | 'author'
                                  > & {
                                      userProfileByAuthor?: Maybe<
                                        { __typename?: 'UserProfile' } & Pick<
                                          UserProfile,
                                          | 'id'
                                          | 'picture'
                                          | 'firstName'
                                          | 'lastName'
                                          | 'fullName'
                                          | 'role'
                                          | 'proficiency'
                                        >
                                      >;
                                    }
                                >;
                              };
                            commentsByTask: { __typename?: 'CommentsConnection' } & Pick<
                              CommentsConnection,
                              'totalCount'
                            > & {
                                nodes: Array<
                                  { __typename?: 'Comment' } & Pick<
                                    Comment,
                                    'id' | 'createdAt' | 'updatedAt' | 'content' | 'author'
                                  > & {
                                      userProfileByAuthor?: Maybe<
                                        { __typename?: 'UserProfile' } & Pick<
                                          UserProfile,
                                          'id' | 'fullName' | 'picture'
                                        >
                                      >;
                                    }
                                >;
                              };
                          }
                      >;
                    };
                }
            >;
          };
      }
  >;
};

export type GetProjectsQueryVariables = Exact<{
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['Cursor']>;
  before?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<ProjectsOrderBy>>;
}>;

export type GetProjectsQuery = { __typename?: 'Query' } & {
  projects?: Maybe<
    { __typename?: 'ProjectsConnection' } & Pick<ProjectsConnection, 'totalCount'> & {
        edges: Array<
          { __typename?: 'ProjectsEdge' } & Pick<ProjectsEdge, 'cursor'> & {
              node: { __typename?: 'Project' } & Pick<
                Project,
                | 'id'
                | 'name'
                | 'createdAt'
                | 'updatedAt'
                | 'start'
                | 'deadline'
                | 'finish'
                | 'description'
                | 'priority'
                | 'state'
                | 'repository'
                | 'wiki'
                | 'weight'
                | 'config'
                | 'sprintDuration'
              >;
            }
        >;
        pageInfo: { __typename?: 'PageInfo' } & Pick<
          PageInfo,
          'hasNextPage' | 'hasPreviousPage' | 'startCursor' | 'endCursor'
        >;
      }
  >;
};

export type GetProjectsInDepthQueryVariables = Exact<{
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['Cursor']>;
  before?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<ProjectsOrderBy>>;
}>;

export type GetProjectsInDepthQuery = { __typename?: 'Query' } & {
  projects?: Maybe<
    { __typename?: 'ProjectsConnection' } & Pick<ProjectsConnection, 'totalCount'> & {
        edges: Array<
          { __typename?: 'ProjectsEdge' } & Pick<ProjectsEdge, 'cursor'> & {
              node: { __typename?: 'Project' } & Pick<
                Project,
                | 'id'
                | 'name'
                | 'createdAt'
                | 'updatedAt'
                | 'start'
                | 'deadline'
                | 'finish'
                | 'description'
                | 'priority'
                | 'state'
                | 'repository'
                | 'wiki'
                | 'weight'
                | 'config'
                | 'sprintDuration'
              > & {
                  membersByProject: { __typename?: 'MembersConnection' } & Pick<MembersConnection, 'totalCount'> & {
                      nodes: Array<
                        { __typename?: 'Member' } & Pick<Member, 'id' | 'userProfile'> & {
                            userProfileByUserProfile?: Maybe<
                              { __typename?: 'UserProfile' } & Pick<UserProfile, 'fullName'> & {
                                  roleByRole?: Maybe<{ __typename?: 'Role' } & Pick<Role, 'id' | 'name'>>;
                                }
                            >;
                          }
                      >;
                    };
                  sprintsByProject: { __typename?: 'SprintsConnection' } & Pick<SprintsConnection, 'totalCount'> & {
                      nodes: Array<
                        { __typename?: 'Sprint' } & Pick<
                          Sprint,
                          | 'id'
                          | 'name'
                          | 'description'
                          | 'createdAt'
                          | 'updatedAt'
                          | 'duration'
                          | 'active'
                          | 'activationTime'
                          | 'closed'
                          | 'weight'
                        > & {
                            tasksBySprint: { __typename?: 'TasksConnection' } & Pick<TasksConnection, 'totalCount'> & {
                                nodes: Array<
                                  { __typename?: 'Task' } & Pick<
                                    Task,
                                    | 'id'
                                    | 'createdAt'
                                    | 'updatedAt'
                                    | 'name'
                                    | 'description'
                                    | 'points'
                                    | 'state'
                                    | 'project'
                                    | 'sprint'
                                    | 'story'
                                    | 'assignee'
                                    | 'taskType'
                                  > & {
                                      sprintBySprint?: Maybe<{ __typename?: 'Sprint' } & Pick<Sprint, 'id' | 'name'>>;
                                      storyByStory?: Maybe<{ __typename?: 'Story' } & Pick<Story, 'id' | 'name'>>;
                                      userProfileByAssignee?: Maybe<
                                        { __typename?: 'UserProfile' } & Pick<
                                          UserProfile,
                                          'id' | 'firstName' | 'lastName' | 'fullName' | 'picture'
                                        >
                                      >;
                                      timeEntriesByTask: { __typename?: 'TimeEntriesConnection' } & Pick<
                                        TimeEntriesConnection,
                                        'totalCount'
                                      > & {
                                          nodes: Array<
                                            { __typename?: 'TimeEntry' } & Pick<
                                              TimeEntry,
                                              | 'createdAt'
                                              | 'updatedAt'
                                              | 'id'
                                              | 'description'
                                              | 'hours'
                                              | 'progress'
                                              | 'time'
                                              | 'author'
                                            > & {
                                                userProfileByAuthor?: Maybe<
                                                  { __typename?: 'UserProfile' } & Pick<
                                                    UserProfile,
                                                    | 'id'
                                                    | 'picture'
                                                    | 'firstName'
                                                    | 'lastName'
                                                    | 'fullName'
                                                    | 'role'
                                                    | 'proficiency'
                                                  >
                                                >;
                                              }
                                          >;
                                        };
                                    }
                                >;
                              };
                            storiesBySprint: { __typename?: 'StoriesConnection' } & Pick<
                              StoriesConnection,
                              'totalCount'
                            > & {
                                nodes: Array<
                                  { __typename?: 'Story' } & Pick<
                                    Story,
                                    | 'id'
                                    | 'name'
                                    | 'description'
                                    | 'createdAt'
                                    | 'updatedAt'
                                    | 'points'
                                    | 'state'
                                    | 'taskType'
                                    | 'finishedDate'
                                    | 'assignee'
                                    | 'weight'
                                  > & {
                                      tasksByStory: { __typename?: 'TasksConnection' } & Pick<
                                        TasksConnection,
                                        'totalCount'
                                      > & {
                                          nodes: Array<
                                            { __typename?: 'Task' } & Pick<
                                              Task,
                                              | 'id'
                                              | 'createdAt'
                                              | 'updatedAt'
                                              | 'name'
                                              | 'description'
                                              | 'points'
                                              | 'state'
                                              | 'taskType'
                                              | 'project'
                                              | 'sprint'
                                              | 'story'
                                              | 'assignee'
                                            > & {
                                                sprintBySprint?: Maybe<
                                                  { __typename?: 'Sprint' } & Pick<Sprint, 'id' | 'name'>
                                                >;
                                                storyByStory?: Maybe<
                                                  { __typename?: 'Story' } & Pick<Story, 'id' | 'name'>
                                                >;
                                                userProfileByAssignee?: Maybe<
                                                  { __typename?: 'UserProfile' } & Pick<
                                                    UserProfile,
                                                    'id' | 'firstName' | 'lastName' | 'fullName' | 'picture'
                                                  >
                                                >;
                                                timeEntriesByTask: { __typename?: 'TimeEntriesConnection' } & Pick<
                                                  TimeEntriesConnection,
                                                  'totalCount'
                                                > & {
                                                    nodes: Array<
                                                      { __typename?: 'TimeEntry' } & Pick<
                                                        TimeEntry,
                                                        | 'createdAt'
                                                        | 'updatedAt'
                                                        | 'id'
                                                        | 'description'
                                                        | 'hours'
                                                        | 'progress'
                                                        | 'time'
                                                        | 'author'
                                                      > & {
                                                          userProfileByAuthor?: Maybe<
                                                            { __typename?: 'UserProfile' } & Pick<
                                                              UserProfile,
                                                              | 'id'
                                                              | 'picture'
                                                              | 'firstName'
                                                              | 'lastName'
                                                              | 'fullName'
                                                              | 'role'
                                                              | 'proficiency'
                                                            >
                                                          >;
                                                        }
                                                    >;
                                                  };
                                              }
                                          >;
                                        };
                                    }
                                >;
                              };
                          }
                      >;
                    };
                  storiesByProject: { __typename?: 'StoriesConnection' } & Pick<StoriesConnection, 'totalCount'> & {
                      nodes: Array<
                        { __typename?: 'Story' } & Pick<
                          Story,
                          | 'id'
                          | 'name'
                          | 'description'
                          | 'createdAt'
                          | 'updatedAt'
                          | 'points'
                          | 'state'
                          | 'taskType'
                          | 'finishedDate'
                          | 'assignee'
                          | 'weight'
                          | 'sprint'
                        > & {
                            tasksByStory: { __typename?: 'TasksConnection' } & Pick<TasksConnection, 'totalCount'> & {
                                nodes: Array<
                                  { __typename?: 'Task' } & Pick<
                                    Task,
                                    | 'id'
                                    | 'createdAt'
                                    | 'updatedAt'
                                    | 'name'
                                    | 'description'
                                    | 'points'
                                    | 'state'
                                    | 'project'
                                    | 'sprint'
                                    | 'story'
                                    | 'assignee'
                                    | 'taskType'
                                  > & {
                                      userProfileByAssignee?: Maybe<
                                        { __typename?: 'UserProfile' } & Pick<
                                          UserProfile,
                                          'id' | 'firstName' | 'lastName' | 'fullName' | 'picture' | 'proficiency'
                                        >
                                      >;
                                      timeEntriesByTask: { __typename?: 'TimeEntriesConnection' } & Pick<
                                        TimeEntriesConnection,
                                        'totalCount'
                                      > & {
                                          nodes: Array<
                                            { __typename?: 'TimeEntry' } & Pick<
                                              TimeEntry,
                                              | 'createdAt'
                                              | 'updatedAt'
                                              | 'id'
                                              | 'description'
                                              | 'hours'
                                              | 'progress'
                                              | 'time'
                                              | 'author'
                                            > & {
                                                userProfileByAuthor?: Maybe<
                                                  { __typename?: 'UserProfile' } & Pick<
                                                    UserProfile,
                                                    | 'id'
                                                    | 'picture'
                                                    | 'firstName'
                                                    | 'lastName'
                                                    | 'fullName'
                                                    | 'role'
                                                    | 'proficiency'
                                                  >
                                                >;
                                              }
                                          >;
                                        };
                                      commentsByTask: { __typename?: 'CommentsConnection' } & Pick<
                                        CommentsConnection,
                                        'totalCount'
                                      > & {
                                          nodes: Array<
                                            { __typename?: 'Comment' } & Pick<
                                              Comment,
                                              'id' | 'createdAt' | 'updatedAt' | 'content' | 'author'
                                            > & {
                                                userProfileByAuthor?: Maybe<
                                                  { __typename?: 'UserProfile' } & Pick<
                                                    UserProfile,
                                                    'id' | 'fullName' | 'picture'
                                                  >
                                                >;
                                              }
                                          >;
                                        };
                                    }
                                >;
                              };
                          }
                      >;
                    };
                };
            }
        >;
        pageInfo: { __typename?: 'PageInfo' } & Pick<
          PageInfo,
          'hasNextPage' | 'hasPreviousPage' | 'startCursor' | 'endCursor'
        >;
      }
  >;
};

export type UpdateProjectMutationVariables = Exact<{
  id: Scalars['UUID'];
  patch: ProjectPatch;
}>;

export type UpdateProjectMutation = { __typename?: 'Mutation' } & {
  updateProject?: Maybe<
    { __typename?: 'UpdateProjectPayload' } & {
      project?: Maybe<
        { __typename?: 'Project' } & Pick<
          Project,
          | 'id'
          | 'name'
          | 'description'
          | 'createdAt'
          | 'updatedAt'
          | 'start'
          | 'deadline'
          | 'finish'
          | 'priority'
          | 'state'
          | 'wiki'
          | 'weight'
          | 'config'
          | 'repository'
          | 'sprintDuration'
        >
      >;
    }
  >;
};

export type GetRolesQueryVariables = Exact<{
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['Cursor']>;
  before?: Maybe<Scalars['Cursor']>;
}>;

export type GetRolesQuery = { __typename?: 'Query' } & {
  roles?: Maybe<
    { __typename?: 'RolesConnection' } & Pick<RolesConnection, 'totalCount'> & {
        edges: Array<
          { __typename?: 'RolesEdge' } & Pick<RolesEdge, 'cursor'> & {
              node: { __typename?: 'Role' } & Pick<Role, 'id' | 'name'>;
            }
        >;
        pageInfo: { __typename?: 'PageInfo' } & Pick<
          PageInfo,
          'hasNextPage' | 'hasPreviousPage' | 'startCursor' | 'endCursor'
        >;
      }
  >;
};

export type CreateSprintMutationVariables = Exact<{
  item: SprintInput;
}>;

export type CreateSprintMutation = { __typename?: 'Mutation' } & {
  createSprint?: Maybe<
    { __typename?: 'CreateSprintPayload' } & {
      sprint?: Maybe<
        { __typename?: 'Sprint' } & Pick<
          Sprint,
          | 'id'
          | 'name'
          | 'description'
          | 'createdAt'
          | 'updatedAt'
          | 'duration'
          | 'active'
          | 'activationTime'
          | 'closed'
          | 'project'
          | 'weight'
        >
      >;
    }
  >;
};

export type DeleteSprintMutationVariables = Exact<{
  id: Scalars['UUID'];
}>;

export type DeleteSprintMutation = { __typename?: 'Mutation' } & {
  deleteSprint?: Maybe<{ __typename?: 'DeleteSprintPayload' } & Pick<DeleteSprintPayload, 'deletedSprintNodeId'>>;
};

export type GetSprintsQueryVariables = Exact<{
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['Cursor']>;
  before?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<SprintsOrderBy>>;
}>;

export type GetSprintsQuery = { __typename?: 'Query' } & {
  sprints?: Maybe<
    { __typename?: 'SprintsConnection' } & Pick<SprintsConnection, 'totalCount'> & {
        edges: Array<
          { __typename?: 'SprintsEdge' } & Pick<SprintsEdge, 'cursor'> & {
              node: { __typename?: 'Sprint' } & Pick<
                Sprint,
                | 'id'
                | 'createdAt'
                | 'updatedAt'
                | 'name'
                | 'description'
                | 'duration'
                | 'active'
                | 'activationTime'
                | 'closed'
                | 'weight'
                | 'project'
              >;
            }
        >;
        pageInfo: { __typename?: 'PageInfo' } & Pick<
          PageInfo,
          'hasNextPage' | 'hasPreviousPage' | 'startCursor' | 'endCursor'
        >;
      }
  >;
};

export type UpdateSprintMutationVariables = Exact<{
  id: Scalars['UUID'];
  patch: SprintPatch;
}>;

export type UpdateSprintMutation = { __typename?: 'Mutation' } & {
  updateSprint?: Maybe<
    { __typename?: 'UpdateSprintPayload' } & {
      sprint?: Maybe<
        { __typename?: 'Sprint' } & Pick<
          Sprint,
          'id' | 'name' | 'description' | 'duration' | 'active' | 'activationTime' | 'closed' | 'project' | 'weight'
        >
      >;
    }
  >;
};

export type CreateStoryMutationVariables = Exact<{
  item: StoryInput;
}>;

export type CreateStoryMutation = { __typename?: 'Mutation' } & {
  createStory?: Maybe<
    { __typename?: 'CreateStoryPayload' } & {
      story?: Maybe<
        { __typename?: 'Story' } & Pick<
          Story,
          | 'id'
          | 'name'
          | 'description'
          | 'createdAt'
          | 'updatedAt'
          | 'points'
          | 'assignee'
          | 'project'
          | 'sprint'
          | 'state'
          | 'taskType'
          | 'finishedDate'
          | 'weight'
        > & {
            sprintBySprint?: Maybe<{ __typename?: 'Sprint' } & Pick<Sprint, 'id' | 'name'>>;
            tasksByStory: { __typename?: 'TasksConnection' } & Pick<TasksConnection, 'totalCount'> & {
                nodes: Array<
                  { __typename?: 'Task' } & Pick<
                    Task,
                    | 'id'
                    | 'createdAt'
                    | 'updatedAt'
                    | 'name'
                    | 'description'
                    | 'points'
                    | 'state'
                    | 'project'
                    | 'sprint'
                    | 'story'
                    | 'assignee'
                    | 'taskType'
                  > & {
                      userProfileByAssignee?: Maybe<
                        { __typename?: 'UserProfile' } & Pick<
                          UserProfile,
                          'id' | 'firstName' | 'lastName' | 'fullName' | 'picture' | 'proficiency'
                        >
                      >;
                      timeEntriesByTask: { __typename?: 'TimeEntriesConnection' } & Pick<
                        TimeEntriesConnection,
                        'totalCount'
                      > & {
                          nodes: Array<
                            { __typename?: 'TimeEntry' } & Pick<
                              TimeEntry,
                              | 'createdAt'
                              | 'updatedAt'
                              | 'id'
                              | 'description'
                              | 'hours'
                              | 'progress'
                              | 'time'
                              | 'author'
                            > & {
                                userProfileByAuthor?: Maybe<
                                  { __typename?: 'UserProfile' } & Pick<
                                    UserProfile,
                                    'id' | 'picture' | 'firstName' | 'lastName' | 'fullName' | 'role' | 'proficiency'
                                  >
                                >;
                              }
                          >;
                        };
                      commentsByTask: { __typename?: 'CommentsConnection' } & Pick<CommentsConnection, 'totalCount'> & {
                          nodes: Array<
                            { __typename?: 'Comment' } & Pick<
                              Comment,
                              'author' | 'content' | 'createdAt' | 'id' | 'nodeId' | 'updatedAt'
                            >
                          >;
                        };
                    }
                >;
              };
            tagbooksByStory: { __typename?: 'TagbooksConnection' } & Pick<TagbooksConnection, 'totalCount'> & {
                nodes: Array<{ __typename?: 'Tagbook' } & Pick<Tagbook, 'id' | 'tag'>>;
              };
          }
      >;
    }
  >;
};

export type DeleteStoryMutationVariables = Exact<{
  id: Scalars['UUID'];
}>;

export type DeleteStoryMutation = { __typename?: 'Mutation' } & {
  deleteStory?: Maybe<{ __typename?: 'DeleteStoryPayload' } & Pick<DeleteStoryPayload, 'deletedStoryNodeId'>>;
};

export type GetStoriesQueryVariables = Exact<{
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['Cursor']>;
  before?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<StoriesOrderBy>>;
}>;

export type GetStoriesQuery = { __typename?: 'Query' } & {
  stories?: Maybe<
    { __typename?: 'StoriesConnection' } & Pick<StoriesConnection, 'totalCount'> & {
        edges: Array<
          { __typename?: 'StoriesEdge' } & Pick<StoriesEdge, 'cursor'> & {
              node: { __typename?: 'Story' } & Pick<
                Story,
                | 'id'
                | 'name'
                | 'description'
                | 'createdAt'
                | 'updatedAt'
                | 'points'
                | 'assignee'
                | 'project'
                | 'sprint'
                | 'state'
                | 'taskType'
                | 'finishedDate'
                | 'weight'
              > & {
                  tagbooksByStory: { __typename?: 'TagbooksConnection' } & Pick<TagbooksConnection, 'totalCount'> & {
                      nodes: Array<
                        { __typename?: 'Tagbook' } & Pick<Tagbook, 'id'> & {
                            tagByTag?: Maybe<{ __typename?: 'Tag' } & Pick<Tag, 'id' | 'label'>>;
                          }
                      >;
                    };
                };
            }
        >;
        pageInfo: { __typename?: 'PageInfo' } & Pick<
          PageInfo,
          'hasNextPage' | 'hasPreviousPage' | 'startCursor' | 'endCursor'
        >;
      }
  >;
};

export type GetStoriesInDepthQueryVariables = Exact<{
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['Cursor']>;
  before?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<StoriesOrderBy>>;
}>;

export type GetStoriesInDepthQuery = { __typename?: 'Query' } & {
  stories?: Maybe<
    { __typename?: 'StoriesConnection' } & Pick<StoriesConnection, 'totalCount'> & {
        edges: Array<
          { __typename?: 'StoriesEdge' } & Pick<StoriesEdge, 'cursor'> & {
              node: { __typename?: 'Story' } & Pick<
                Story,
                | 'id'
                | 'name'
                | 'description'
                | 'createdAt'
                | 'updatedAt'
                | 'points'
                | 'assignee'
                | 'project'
                | 'sprint'
                | 'state'
                | 'taskType'
                | 'finishedDate'
                | 'weight'
              > & {
                  tagbooksByStory: { __typename?: 'TagbooksConnection' } & Pick<TagbooksConnection, 'totalCount'> & {
                      nodes: Array<{ __typename?: 'Tagbook' } & Pick<Tagbook, 'id' | 'tag'>>;
                    };
                  tasksByStory: { __typename?: 'TasksConnection' } & Pick<TasksConnection, 'totalCount'> & {
                      nodes: Array<
                        { __typename?: 'Task' } & Pick<
                          Task,
                          | 'id'
                          | 'createdAt'
                          | 'updatedAt'
                          | 'name'
                          | 'description'
                          | 'points'
                          | 'state'
                          | 'project'
                          | 'sprint'
                          | 'story'
                          | 'assignee'
                          | 'taskType'
                        > & {
                            userProfileByAssignee?: Maybe<
                              { __typename?: 'UserProfile' } & Pick<
                                UserProfile,
                                'id' | 'firstName' | 'lastName' | 'fullName' | 'picture' | 'proficiency'
                              >
                            >;
                            timeEntriesByTask: { __typename?: 'TimeEntriesConnection' } & Pick<
                              TimeEntriesConnection,
                              'totalCount'
                            > & {
                                nodes: Array<
                                  { __typename?: 'TimeEntry' } & Pick<
                                    TimeEntry,
                                    | 'createdAt'
                                    | 'updatedAt'
                                    | 'id'
                                    | 'description'
                                    | 'hours'
                                    | 'progress'
                                    | 'time'
                                    | 'author'
                                  > & {
                                      userProfileByAuthor?: Maybe<
                                        { __typename?: 'UserProfile' } & Pick<
                                          UserProfile,
                                          | 'id'
                                          | 'picture'
                                          | 'firstName'
                                          | 'lastName'
                                          | 'fullName'
                                          | 'role'
                                          | 'proficiency'
                                        >
                                      >;
                                    }
                                >;
                              };
                            commentsByTask: { __typename?: 'CommentsConnection' } & Pick<
                              CommentsConnection,
                              'totalCount'
                            > & {
                                nodes: Array<
                                  { __typename?: 'Comment' } & Pick<
                                    Comment,
                                    'author' | 'content' | 'createdAt' | 'id' | 'nodeId' | 'updatedAt'
                                  >
                                >;
                              };
                          }
                      >;
                    };
                };
            }
        >;
        pageInfo: { __typename?: 'PageInfo' } & Pick<
          PageInfo,
          'hasNextPage' | 'hasPreviousPage' | 'startCursor' | 'endCursor'
        >;
      }
  >;
};

export type GetStoryQueryVariables = Exact<{
  id: Scalars['UUID'];
}>;

export type GetStoryQuery = { __typename?: 'Query' } & {
  story?: Maybe<
    { __typename?: 'Story' } & Pick<
      Story,
      | 'id'
      | 'name'
      | 'description'
      | 'createdAt'
      | 'updatedAt'
      | 'points'
      | 'assignee'
      | 'project'
      | 'sprint'
      | 'state'
      | 'taskType'
      | 'finishedDate'
      | 'weight'
    > & {
        tagbooksByStory: { __typename?: 'TagbooksConnection' } & Pick<TagbooksConnection, 'totalCount'> & {
            nodes: Array<{ __typename?: 'Tagbook' } & Pick<Tagbook, 'id' | 'tag'>>;
          };
        tasksByStory: { __typename?: 'TasksConnection' } & Pick<TasksConnection, 'totalCount'> & {
            nodes: Array<
              { __typename?: 'Task' } & Pick<
                Task,
                | 'id'
                | 'createdAt'
                | 'updatedAt'
                | 'name'
                | 'description'
                | 'points'
                | 'state'
                | 'project'
                | 'sprint'
                | 'story'
                | 'assignee'
                | 'taskType'
              > & {
                  userProfileByAssignee?: Maybe<
                    { __typename?: 'UserProfile' } & Pick<
                      UserProfile,
                      'id' | 'firstName' | 'lastName' | 'fullName' | 'picture' | 'proficiency'
                    >
                  >;
                  timeEntriesByTask: { __typename?: 'TimeEntriesConnection' } & Pick<
                    TimeEntriesConnection,
                    'totalCount'
                  > & {
                      nodes: Array<
                        { __typename?: 'TimeEntry' } & Pick<
                          TimeEntry,
                          'createdAt' | 'updatedAt' | 'id' | 'description' | 'hours' | 'progress' | 'time' | 'author'
                        > & {
                            userProfileByAuthor?: Maybe<
                              { __typename?: 'UserProfile' } & Pick<
                                UserProfile,
                                'id' | 'picture' | 'firstName' | 'lastName' | 'fullName' | 'role' | 'proficiency'
                              >
                            >;
                          }
                      >;
                    };
                  commentsByTask: { __typename?: 'CommentsConnection' } & Pick<CommentsConnection, 'totalCount'> & {
                      nodes: Array<
                        { __typename?: 'Comment' } & Pick<
                          Comment,
                          'author' | 'content' | 'createdAt' | 'id' | 'nodeId' | 'updatedAt'
                        >
                      >;
                    };
                }
            >;
          };
      }
  >;
};

export type UpdateStoryMutationVariables = Exact<{
  id: Scalars['UUID'];
  patch: StoryPatch;
}>;

export type UpdateStoryMutation = { __typename?: 'Mutation' } & {
  updateStory?: Maybe<
    { __typename?: 'UpdateStoryPayload' } & {
      story?: Maybe<
        { __typename?: 'Story' } & Pick<
          Story,
          | 'id'
          | 'name'
          | 'description'
          | 'createdAt'
          | 'updatedAt'
          | 'points'
          | 'state'
          | 'taskType'
          | 'finishedDate'
          | 'weight'
          | 'sprint'
          | 'assignee'
        > & {
            sprintBySprint?: Maybe<
              { __typename?: 'Sprint' } & Pick<
                Sprint,
                'id' | 'name' | 'description' | 'createdAt' | 'updatedAt' | 'duration' | 'active' | 'closed' | 'weight'
              >
            >;
            userProfileByAssignee?: Maybe<
              { __typename?: 'UserProfile' } & Pick<
                UserProfile,
                'id' | 'firstName' | 'lastName' | 'fullName' | 'picture' | 'proficiency'
              >
            >;
            tagbooksByStory: { __typename?: 'TagbooksConnection' } & Pick<TagbooksConnection, 'totalCount'> & {
                nodes: Array<
                  { __typename?: 'Tagbook' } & Pick<Tagbook, 'id'> & {
                      tagByTag?: Maybe<{ __typename?: 'Tag' } & Pick<Tag, 'id' | 'label'>>;
                    }
                >;
              };
            tasksByStory: { __typename?: 'TasksConnection' } & Pick<TasksConnection, 'totalCount'> & {
                nodes: Array<
                  { __typename?: 'Task' } & Pick<
                    Task,
                    | 'id'
                    | 'createdAt'
                    | 'updatedAt'
                    | 'name'
                    | 'description'
                    | 'points'
                    | 'state'
                    | 'project'
                    | 'sprint'
                    | 'story'
                    | 'assignee'
                    | 'taskType'
                  > & {
                      userProfileByAssignee?: Maybe<
                        { __typename?: 'UserProfile' } & Pick<
                          UserProfile,
                          'id' | 'firstName' | 'lastName' | 'fullName' | 'picture' | 'proficiency'
                        >
                      >;
                      timeEntriesByTask: { __typename?: 'TimeEntriesConnection' } & Pick<
                        TimeEntriesConnection,
                        'totalCount'
                      > & {
                          nodes: Array<
                            { __typename?: 'TimeEntry' } & Pick<
                              TimeEntry,
                              | 'createdAt'
                              | 'updatedAt'
                              | 'id'
                              | 'description'
                              | 'hours'
                              | 'progress'
                              | 'time'
                              | 'author'
                            > & {
                                userProfileByAuthor?: Maybe<
                                  { __typename?: 'UserProfile' } & Pick<
                                    UserProfile,
                                    'id' | 'picture' | 'firstName' | 'lastName' | 'fullName' | 'role' | 'proficiency'
                                  >
                                >;
                              }
                          >;
                        };
                      commentsByTask: { __typename?: 'CommentsConnection' } & Pick<CommentsConnection, 'totalCount'> & {
                          nodes: Array<
                            { __typename?: 'Comment' } & Pick<
                              Comment,
                              'author' | 'content' | 'createdAt' | 'id' | 'nodeId' | 'updatedAt'
                            >
                          >;
                        };
                    }
                >;
              };
          }
      >;
    }
  >;
};

export type CreateTagbookMutationVariables = Exact<{
  item: TagbookInput;
}>;

export type CreateTagbookMutation = { __typename?: 'Mutation' } & {
  createTagbook?: Maybe<
    { __typename?: 'CreateTagbookPayload' } & {
      tagbook?: Maybe<
        { __typename?: 'Tagbook' } & Pick<Tagbook, 'id'> & {
            tagByTag?: Maybe<{ __typename?: 'Tag' } & Pick<Tag, 'id' | 'label'>>;
          }
      >;
    }
  >;
};

export type DeleteTagbookMutationVariables = Exact<{
  id: Scalars['UUID'];
}>;

export type DeleteTagbookMutation = { __typename?: 'Mutation' } & {
  deleteTagbook?: Maybe<{ __typename?: 'DeleteTagbookPayload' } & Pick<DeleteTagbookPayload, 'deletedTagbookNodeId'>>;
};

export type UpdateTagbookMutationVariables = Exact<{
  id: Scalars['UUID'];
  patch: TagbookPatch;
}>;

export type UpdateTagbookMutation = { __typename?: 'Mutation' } & {
  updateTagbook?: Maybe<
    { __typename?: 'UpdateTagbookPayload' } & {
      tagbook?: Maybe<{ __typename?: 'Tagbook' } & Pick<Tagbook, 'id' | 'story' | 'tag'>>;
    }
  >;
};

export type GetTagsInDepthQueryVariables = Exact<{
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['Cursor']>;
  before?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TagsOrderBy>>;
}>;

export type GetTagsInDepthQuery = { __typename?: 'Query' } & {
  tags?: Maybe<
    { __typename?: 'TagsConnection' } & Pick<TagsConnection, 'totalCount'> & {
        edges: Array<
          { __typename?: 'TagsEdge' } & Pick<TagsEdge, 'cursor'> & {
              node: { __typename?: 'Tag' } & Pick<Tag, 'id' | 'createdAt' | 'updatedAt' | 'label' | 'description'>;
            }
        >;
        pageInfo: { __typename?: 'PageInfo' } & Pick<
          PageInfo,
          'hasNextPage' | 'hasPreviousPage' | 'startCursor' | 'endCursor'
        >;
      }
  >;
};

export type CreateTaskMutationVariables = Exact<{
  item: TaskInput;
}>;

export type CreateTaskMutation = { __typename?: 'Mutation' } & {
  createTask?: Maybe<
    { __typename?: 'CreateTaskPayload' } & {
      task?: Maybe<
        { __typename?: 'Task' } & Pick<
          Task,
          | 'id'
          | 'name'
          | 'description'
          | 'createdAt'
          | 'updatedAt'
          | 'points'
          | 'assignee'
          | 'project'
          | 'sprint'
          | 'story'
          | 'state'
          | 'taskType'
          | 'weight'
        > & {
            userProfileByAssignee?: Maybe<
              { __typename?: 'UserProfile' } & Pick<UserProfile, 'id' | 'fullName' | 'picture' | 'proficiency'>
            >;
            sprintBySprint?: Maybe<{ __typename?: 'Sprint' } & Pick<Sprint, 'id' | 'name'>>;
            storyByStory?: Maybe<{ __typename?: 'Story' } & Pick<Story, 'id' | 'name'>>;
          }
      >;
    }
  >;
};

export type DeleteTaskMutationVariables = Exact<{
  id: Scalars['UUID'];
}>;

export type DeleteTaskMutation = { __typename?: 'Mutation' } & {
  deleteTask?: Maybe<{ __typename?: 'DeleteTaskPayload' } & Pick<DeleteTaskPayload, 'deletedTaskNodeId'>>;
};

export type GetTaskQueryVariables = Exact<{
  id: Scalars['UUID'];
}>;

export type GetTaskQuery = { __typename?: 'Query' } & {
  task?: Maybe<
    { __typename?: 'Task' } & Pick<
      Task,
      | 'id'
      | 'name'
      | 'description'
      | 'createdAt'
      | 'updatedAt'
      | 'points'
      | 'assignee'
      | 'project'
      | 'sprint'
      | 'story'
      | 'state'
      | 'taskType'
      | 'weight'
    > & {
        commentsByTask: { __typename?: 'CommentsConnection' } & Pick<CommentsConnection, 'totalCount'> & {
            nodes: Array<
              { __typename?: 'Comment' } & Pick<Comment, 'id' | 'createdAt' | 'updatedAt' | 'content' | 'author'> & {
                  userProfileByAuthor?: Maybe<
                    { __typename?: 'UserProfile' } & Pick<UserProfile, 'id' | 'fullName' | 'picture'>
                  >;
                }
            >;
          };
      }
  >;
};

export type GetTasksQueryVariables = Exact<{
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['Cursor']>;
  before?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TasksOrderBy>>;
}>;

export type GetTasksQuery = { __typename?: 'Query' } & {
  tasks?: Maybe<
    { __typename?: 'TasksConnection' } & Pick<TasksConnection, 'totalCount'> & {
        edges: Array<
          { __typename?: 'TasksEdge' } & Pick<TasksEdge, 'cursor'> & {
              node: { __typename?: 'Task' } & Pick<
                Task,
                | 'id'
                | 'name'
                | 'description'
                | 'createdAt'
                | 'updatedAt'
                | 'points'
                | 'assignee'
                | 'project'
                | 'sprint'
                | 'story'
                | 'state'
                | 'taskType'
                | 'weight'
              >;
            }
        >;
        pageInfo: { __typename?: 'PageInfo' } & Pick<
          PageInfo,
          'hasNextPage' | 'hasPreviousPage' | 'startCursor' | 'endCursor'
        >;
      }
  >;
};

export type GetTasksInDepthQueryVariables = Exact<{
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['Cursor']>;
  before?: Maybe<Scalars['Cursor']>;
  orderBy?: Maybe<Array<TasksOrderBy>>;
}>;

export type GetTasksInDepthQuery = { __typename?: 'Query' } & {
  tasks?: Maybe<
    { __typename?: 'TasksConnection' } & Pick<TasksConnection, 'totalCount'> & {
        edges: Array<
          { __typename?: 'TasksEdge' } & Pick<TasksEdge, 'cursor'> & {
              node: { __typename?: 'Task' } & Pick<
                Task,
                | 'id'
                | 'name'
                | 'description'
                | 'createdAt'
                | 'updatedAt'
                | 'points'
                | 'assignee'
                | 'project'
                | 'sprint'
                | 'state'
                | 'taskType'
                | 'weight'
              > & {
                  timeEntriesByTask: { __typename?: 'TimeEntriesConnection' } & Pick<
                    TimeEntriesConnection,
                    'totalCount'
                  > & {
                      nodes: Array<
                        { __typename?: 'TimeEntry' } & Pick<
                          TimeEntry,
                          'id' | 'description' | 'hours' | 'progress' | 'time' | 'task' | 'author'
                        >
                      >;
                    };
                  commentsByTask: { __typename?: 'CommentsConnection' } & Pick<CommentsConnection, 'totalCount'> & {
                      nodes: Array<
                        { __typename?: 'Comment' } & Pick<Comment, 'id' | 'content' | 'author'> & {
                            userProfileByAuthor?: Maybe<
                              { __typename?: 'UserProfile' } & Pick<UserProfile, 'id' | 'fullName' | 'picture'>
                            >;
                          }
                      >;
                    };
                  userProfileByAssignee?: Maybe<
                    { __typename?: 'UserProfile' } & Pick<UserProfile, 'id' | 'fullName' | 'picture' | 'proficiency'>
                  >;
                };
            }
        >;
        pageInfo: { __typename?: 'PageInfo' } & Pick<
          PageInfo,
          'hasNextPage' | 'hasPreviousPage' | 'startCursor' | 'endCursor'
        >;
      }
  >;
};

export type UpdateTaskMutationVariables = Exact<{
  id: Scalars['UUID'];
  patch: TaskPatch;
}>;

export type UpdateTaskMutation = { __typename?: 'Mutation' } & {
  updateTask?: Maybe<
    { __typename?: 'UpdateTaskPayload' } & {
      task?: Maybe<
        { __typename?: 'Task' } & Pick<
          Task,
          | 'id'
          | 'name'
          | 'description'
          | 'createdAt'
          | 'updatedAt'
          | 'points'
          | 'assignee'
          | 'project'
          | 'sprint'
          | 'story'
          | 'state'
          | 'taskType'
          | 'weight'
        > & {
            sprintBySprint?: Maybe<{ __typename?: 'Sprint' } & Pick<Sprint, 'id' | 'name'>>;
            storyByStory?: Maybe<{ __typename?: 'Story' } & Pick<Story, 'id' | 'name'>>;
            timeEntriesByTask: { __typename?: 'TimeEntriesConnection' } & Pick<TimeEntriesConnection, 'totalCount'> & {
                nodes: Array<
                  { __typename?: 'TimeEntry' } & Pick<
                    TimeEntry,
                    'id' | 'description' | 'hours' | 'progress' | 'time' | 'task' | 'author'
                  >
                >;
              };
            commentsByTask: { __typename?: 'CommentsConnection' } & Pick<CommentsConnection, 'totalCount'> & {
                nodes: Array<
                  { __typename?: 'Comment' } & Pick<Comment, 'id' | 'content' | 'author'> & {
                      userProfileByAuthor?: Maybe<
                        { __typename?: 'UserProfile' } & Pick<UserProfile, 'id' | 'fullName' | 'picture'>
                      >;
                    }
                >;
              };
            userProfileByAssignee?: Maybe<
              { __typename?: 'UserProfile' } & Pick<UserProfile, 'id' | 'fullName' | 'picture' | 'proficiency'>
            >;
          }
      >;
    }
  >;
};

export type CreateTimeEntryMutationVariables = Exact<{
  item: TimeEntryInput;
}>;

export type CreateTimeEntryMutation = { __typename?: 'Mutation' } & {
  createTimeEntry?: Maybe<
    { __typename?: 'CreateTimeEntryPayload' } & {
      timeEntry?: Maybe<
        { __typename?: 'TimeEntry' } & Pick<
          TimeEntry,
          'id' | 'description' | 'hours' | 'progress' | 'time' | 'task' | 'author'
        > & { userProfileByAuthor?: Maybe<{ __typename?: 'UserProfile' } & Pick<UserProfile, 'id' | 'fullName'>> }
      >;
    }
  >;
};

export type DeleteTimeEntryMutationVariables = Exact<{
  id: Scalars['UUID'];
}>;

export type DeleteTimeEntryMutation = { __typename?: 'Mutation' } & {
  deleteTimeEntry?: Maybe<
    { __typename?: 'DeleteTimeEntryPayload' } & Pick<DeleteTimeEntryPayload, 'deletedTimeEntryNodeId'>
  >;
};

export type UpdateTimeEntryMutationVariables = Exact<{
  id: Scalars['UUID'];
  patch: TimeEntryPatch;
}>;

export type UpdateTimeEntryMutation = { __typename?: 'Mutation' } & {
  updateTimeEntry?: Maybe<
    { __typename?: 'UpdateTimeEntryPayload' } & {
      timeEntry?: Maybe<
        { __typename?: 'TimeEntry' } & Pick<
          TimeEntry,
          'id' | 'description' | 'hours' | 'progress' | 'time' | 'task' | 'author'
        > & { userProfileByAuthor?: Maybe<{ __typename?: 'UserProfile' } & Pick<UserProfile, 'id' | 'fullName'>> }
      >;
    }
  >;
};

export type DeleteUserProfileMutationVariables = Exact<{
  id: Scalars['UUID'];
}>;

export type DeleteUserProfileMutation = { __typename?: 'Mutation' } & {
  deleteUserProfile?: Maybe<
    { __typename?: 'DeleteUserProfilePayload' } & Pick<DeleteUserProfilePayload, 'deletedUserProfileNodeId'>
  >;
};

export type GetUserProfilesQueryVariables = Exact<{
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['Cursor']>;
  before?: Maybe<Scalars['Cursor']>;
}>;

export type GetUserProfilesQuery = { __typename?: 'Query' } & {
  userProfiles?: Maybe<
    { __typename?: 'UserProfilesConnection' } & Pick<UserProfilesConnection, 'totalCount'> & {
        edges: Array<
          { __typename?: 'UserProfilesEdge' } & Pick<UserProfilesEdge, 'cursor'> & {
              node: { __typename?: 'UserProfile' } & Pick<
                UserProfile,
                'id' | 'firstName' | 'lastName' | 'fullName' | 'picture' | 'role' | 'proficiency'
              > & {
                  roleByRole?: Maybe<{ __typename?: 'Role' } & Pick<Role, 'id' | 'name'>>;
                  storiesByAssignee: { __typename?: 'StoriesConnection' } & Pick<StoriesConnection, 'totalCount'> & {
                      nodes: Array<{ __typename?: 'Story' } & Pick<Story, 'id' | 'state'>>;
                    };
                  tasksByAssignee: { __typename?: 'TasksConnection' } & Pick<TasksConnection, 'totalCount'> & {
                      nodes: Array<{ __typename?: 'Task' } & Pick<Task, 'id' | 'state'>>;
                    };
                };
            }
        >;
        pageInfo: { __typename?: 'PageInfo' } & Pick<
          PageInfo,
          'hasNextPage' | 'hasPreviousPage' | 'startCursor' | 'endCursor'
        >;
      }
  >;
};

export type RegisterUserMutationVariables = Exact<{
  item: RegisterUserInput;
}>;

export type RegisterUserMutation = { __typename?: 'Mutation' } & {
  registerUser?: Maybe<
    { __typename?: 'RegisterUserPayload' } & {
      userProfile?: Maybe<
        { __typename?: 'UserProfile' } & Pick<
          UserProfile,
          'id' | 'firstName' | 'lastName' | 'fullName' | 'picture' | 'proficiency' | 'role'
        > & { roleByRole?: Maybe<{ __typename?: 'Role' } & Pick<Role, 'id' | 'name'>> }
      >;
    }
  >;
};

export type UpdateUserProfileMutationVariables = Exact<{
  id: Scalars['UUID'];
  patch: UserProfilePatch;
}>;

export type UpdateUserProfileMutation = { __typename?: 'Mutation' } & {
  updateUserProfile?: Maybe<
    { __typename?: 'UpdateUserProfilePayload' } & {
      userProfile?: Maybe<
        { __typename?: 'UserProfile' } & Pick<
          UserProfile,
          'id' | 'firstName' | 'lastName' | 'fullName' | 'picture' | 'proficiency' | 'role'
        > & { roleByRole?: Maybe<{ __typename?: 'Role' } & Pick<Role, 'id' | 'name'>> }
      >;
    }
  >;
};

export const AuthenticateDocument = gql`
  mutation Authenticate($item: AuthenticateInput!) {
    authenticate(input: $item) {
      token: jwtToken
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class AuthenticateGQL extends Apollo.Mutation<AuthenticateMutation, AuthenticateMutationVariables> {
  document = AuthenticateDocument;
}
export const GetCurrentUserDocument = gql`
  query GetCurrentUser {
    currentUser {
      id
      createdAt
      updatedAt
      firstName
      lastName
      fullName
      picture
      proficiency
      roleByRole {
        id
        name
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetCurrentUserGQL extends Apollo.Query<GetCurrentUserQuery, GetCurrentUserQueryVariables> {
  document = GetCurrentUserDocument;
}
export const CreateCommentDocument = gql`
  mutation CreateComment($item: CommentInput!) {
    createComment(input: { comment: $item }) {
      clientMutationId
      comment {
        id
        createdAt
        updatedAt
        content
        author
        userProfileByAuthor {
          id
          fullName
          picture
        }
        task
        taskByTask {
          id
          name
        }
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class CreateCommentGQL extends Apollo.Mutation<CreateCommentMutation, CreateCommentMutationVariables> {
  document = CreateCommentDocument;
}
export const DeleteCommentDocument = gql`
  mutation DeleteComment($id: UUID!) {
    deleteComment(input: { id: $id }) {
      clientMutationId
      deletedCommentNodeId
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class DeleteCommentGQL extends Apollo.Mutation<DeleteCommentMutation, DeleteCommentMutationVariables> {
  document = DeleteCommentDocument;
}
export const GetCommentsDocument = gql`
  query GetComments($first: Int, $after: Cursor, $before: Cursor, $orderBy: [CommentsOrderBy!]) {
    comments(first: $first, after: $after, before: $before, orderBy: $orderBy) {
      edges {
        node {
          id
          createdAt
          updatedAt
          content
          author
          userProfileByAuthor {
            id
            fullName
            picture
          }
          task
          taskByTask {
            id
            name
          }
        }
        cursor
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetCommentsGQL extends Apollo.Query<GetCommentsQuery, GetCommentsQueryVariables> {
  document = GetCommentsDocument;
}
export const CreateMemberDocument = gql`
  mutation CreateMember($item: MemberInput!) {
    createMember(input: { member: $item }) {
      clientMutationId
      member {
        project
        userProfile
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class CreateMemberGQL extends Apollo.Mutation<CreateMemberMutation, CreateMemberMutationVariables> {
  document = CreateMemberDocument;
}
export const DeleteMemberDocument = gql`
  mutation DeleteMember($id: UUID!) {
    deleteMember(input: { id: $id }) {
      clientMutationId
      deletedMemberNodeId
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class DeleteMemberGQL extends Apollo.Mutation<DeleteMemberMutation, DeleteMemberMutationVariables> {
  document = DeleteMemberDocument;
}
export const CreateProjectDocument = gql`
  mutation CreateProject($item: ProjectInput!) {
    createProject(input: { project: $item }) {
      project {
        id
        name
        config
        createdAt
        deadline
        description
        finish
        priority
        repository
        start
        state
        weight
        wiki
        sprintDuration
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class CreateProjectGQL extends Apollo.Mutation<CreateProjectMutation, CreateProjectMutationVariables> {
  document = CreateProjectDocument;
}
export const DeleteProjectDocument = gql`
  mutation DeleteProject($id: UUID!) {
    deleteProject(input: { id: $id }) {
      deletedProjectNodeId
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class DeleteProjectGQL extends Apollo.Mutation<DeleteProjectMutation, DeleteProjectMutationVariables> {
  document = DeleteProjectDocument;
}
export const GetProjectDocument = gql`
  query GetProject($id: UUID!, $storiesOrderBy: [StoriesOrderBy!]) {
    project(id: $id) {
      id
      name
      createdAt
      updatedAt
      start
      deadline
      finish
      description
      priority
      state
      repository
      wiki
      weight
      config
      sprintDuration
      membersByProject {
        nodes {
          id
          userProfileByUserProfile {
            id
            firstName
            lastName
            fullName
            picture
            proficiency
            roleByRole {
              id
              name
              createdAt
              updatedAt
            }
            timeEntriesByAuthor {
              nodes {
                createdAt
                updatedAt
                id
                description
                hours
                progress
                time
                task
                taskByTask {
                  id
                  createdAt
                  updatedAt
                  name
                  description
                  points
                  state
                  taskType
                  project
                  sprint
                  story
                }
              }
              totalCount
            }
            createdAt
          }
        }
        totalCount
      }
      sprintsByProject {
        nodes {
          id
          name
          description
          createdAt
          updatedAt
          duration
          active
          activationTime
          closed
          weight
          tasksBySprint {
            nodes {
              id
              createdAt
              updatedAt
              name
              description
              points
              state
              project
              sprint
              story
              assignee
              userProfileByAssignee {
                id
                firstName
                lastName
                fullName
                picture
              }
              taskType
              timeEntriesByTask {
                nodes {
                  createdAt
                  updatedAt
                  id
                  description
                  hours
                  progress
                  time
                  author
                  userProfileByAuthor {
                    id
                    picture
                    firstName
                    lastName
                    fullName
                    role
                    proficiency
                  }
                }
                totalCount
              }
            }
            totalCount
          }
          storiesBySprint {
            totalCount
            nodes {
              id
              name
              description
              createdAt
              updatedAt
              points
              state
              taskType
              finishedDate
              weight
              sprint
              assignee
              tasksByStory {
                nodes {
                  id
                  createdAt
                  updatedAt
                  name
                  description
                  points
                  state
                  taskType
                  project
                  sprint
                  sprintBySprint {
                    id
                    name
                  }
                  story
                  storyByStory {
                    id
                    name
                  }
                  assignee
                  userProfileByAssignee {
                    id
                    firstName
                    lastName
                    fullName
                    picture
                  }
                  timeEntriesByTask {
                    nodes {
                      createdAt
                      updatedAt
                      id
                      description
                      hours
                      progress
                      time
                      author
                      userProfileByAuthor {
                        id
                        picture
                        firstName
                        lastName
                        fullName
                        role
                        proficiency
                      }
                    }
                    totalCount
                  }
                }
                totalCount
              }
            }
          }
        }
        totalCount
      }
      storiesByProject(orderBy: $storiesOrderBy) {
        nodes {
          id
          name
          description
          createdAt
          updatedAt
          points
          state
          taskType
          finishedDate
          weight
          sprint
          sprintBySprint {
            id
            name
            description
            createdAt
            updatedAt
            duration
            active
            activationTime
            closed
            weight
          }
          assignee
          userProfileByAssignee {
            id
            firstName
            lastName
            fullName
            picture
          }
          tagbooksByStory {
            nodes {
              id
              tagByTag {
                id
                label
              }
            }
            totalCount
          }
          tasksByStory {
            nodes {
              id
              createdAt
              updatedAt
              name
              description
              points
              state
              taskType
              project
              sprint
              sprintBySprint {
                id
                name
              }
              story
              storyByStory {
                id
                name
              }
              assignee
              userProfileByAssignee {
                id
                firstName
                lastName
                fullName
                picture
                proficiency
              }
              timeEntriesByTask {
                nodes {
                  createdAt
                  updatedAt
                  id
                  description
                  hours
                  progress
                  time
                  author
                  userProfileByAuthor {
                    id
                    picture
                    firstName
                    lastName
                    fullName
                    role
                    proficiency
                  }
                }
                totalCount
              }
              commentsByTask {
                totalCount
                nodes {
                  id
                  createdAt
                  updatedAt
                  content
                  author
                  userProfileByAuthor {
                    id
                    fullName
                    picture
                  }
                }
              }
            }
            totalCount
          }
        }
        totalCount
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetProjectGQL extends Apollo.Query<GetProjectQuery, GetProjectQueryVariables> {
  document = GetProjectDocument;
}
export const GetProjectsDocument = gql`
  query GetProjects($first: Int, $after: Cursor, $before: Cursor, $orderBy: [ProjectsOrderBy!]) {
    projects(first: $first, after: $after, before: $before, orderBy: $orderBy) {
      edges {
        node {
          id
          name
          createdAt
          updatedAt
          start
          deadline
          finish
          description
          priority
          state
          repository
          wiki
          weight
          config
          sprintDuration
        }
        cursor
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetProjectsGQL extends Apollo.Query<GetProjectsQuery, GetProjectsQueryVariables> {
  document = GetProjectsDocument;
}
export const GetProjectsInDepthDocument = gql`
  query GetProjectsInDepth($first: Int, $after: Cursor, $before: Cursor, $orderBy: [ProjectsOrderBy!]) {
    projects(first: $first, after: $after, before: $before, orderBy: $orderBy) {
      edges {
        node {
          id
          name
          createdAt
          updatedAt
          start
          deadline
          finish
          description
          priority
          state
          repository
          wiki
          weight
          config
          sprintDuration
          membersByProject {
            totalCount
            nodes {
              id
              userProfile
              userProfileByUserProfile {
                fullName
                roleByRole {
                  id
                  name
                }
              }
            }
          }
          sprintsByProject {
            nodes {
              id
              name
              description
              createdAt
              updatedAt
              duration
              active
              activationTime
              closed
              weight
              tasksBySprint {
                nodes {
                  id
                  createdAt
                  updatedAt
                  name
                  description
                  points
                  state
                  project
                  sprint
                  sprintBySprint {
                    id
                    name
                  }
                  story
                  storyByStory {
                    id
                    name
                  }
                  assignee
                  userProfileByAssignee {
                    id
                    firstName
                    lastName
                    fullName
                    picture
                  }
                  taskType
                  timeEntriesByTask {
                    nodes {
                      createdAt
                      updatedAt
                      id
                      description
                      hours
                      progress
                      time
                      author
                      userProfileByAuthor {
                        id
                        picture
                        firstName
                        lastName
                        fullName
                        role
                        proficiency
                      }
                    }
                    totalCount
                  }
                }
                totalCount
              }
              storiesBySprint {
                nodes {
                  id
                  name
                  description
                  createdAt
                  updatedAt
                  points
                  state
                  taskType
                  finishedDate
                  assignee
                  weight
                  tasksByStory {
                    nodes {
                      id
                      createdAt
                      updatedAt
                      name
                      description
                      points
                      state
                      taskType
                      project
                      sprint
                      sprintBySprint {
                        id
                        name
                      }
                      story
                      storyByStory {
                        id
                        name
                      }
                      assignee
                      userProfileByAssignee {
                        id
                        firstName
                        lastName
                        fullName
                        picture
                      }
                      timeEntriesByTask {
                        nodes {
                          createdAt
                          updatedAt
                          id
                          description
                          hours
                          progress
                          time
                          author
                          userProfileByAuthor {
                            id
                            picture
                            firstName
                            lastName
                            fullName
                            role
                            proficiency
                          }
                        }
                        totalCount
                      }
                    }
                    totalCount
                  }
                }
                totalCount
              }
            }
            totalCount
          }
          storiesByProject {
            nodes {
              id
              name
              description
              createdAt
              updatedAt
              points
              state
              taskType
              finishedDate
              assignee
              weight
              sprint
              tasksByStory {
                nodes {
                  id
                  createdAt
                  updatedAt
                  name
                  description
                  points
                  state
                  project
                  sprint
                  story
                  assignee
                  userProfileByAssignee {
                    id
                    firstName
                    lastName
                    fullName
                    picture
                    proficiency
                  }
                  taskType
                  timeEntriesByTask {
                    nodes {
                      createdAt
                      updatedAt
                      id
                      description
                      hours
                      progress
                      time
                      author
                      userProfileByAuthor {
                        id
                        picture
                        firstName
                        lastName
                        fullName
                        role
                        proficiency
                      }
                    }
                    totalCount
                  }
                  commentsByTask {
                    totalCount
                    nodes {
                      id
                      createdAt
                      updatedAt
                      content
                      author
                      userProfileByAuthor {
                        id
                        fullName
                        picture
                      }
                    }
                  }
                }
                totalCount
              }
            }
            totalCount
          }
        }
        cursor
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetProjectsInDepthGQL extends Apollo.Query<GetProjectsInDepthQuery, GetProjectsInDepthQueryVariables> {
  document = GetProjectsInDepthDocument;
}
export const UpdateProjectDocument = gql`
  mutation UpdateProject($id: UUID!, $patch: ProjectPatch!) {
    updateProject(input: { patch: $patch, id: $id }) {
      project {
        id
        name
        description
        createdAt
        updatedAt
        start
        deadline
        finish
        priority
        state
        wiki
        weight
        config
        repository
        sprintDuration
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class UpdateProjectGQL extends Apollo.Mutation<UpdateProjectMutation, UpdateProjectMutationVariables> {
  document = UpdateProjectDocument;
}
export const GetRolesDocument = gql`
  query GetRoles($first: Int, $after: Cursor, $before: Cursor) {
    roles(first: $first, after: $after, before: $before) {
      edges {
        node {
          id
          name
        }
        cursor
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetRolesGQL extends Apollo.Query<GetRolesQuery, GetRolesQueryVariables> {
  document = GetRolesDocument;
}
export const CreateSprintDocument = gql`
  mutation CreateSprint($item: SprintInput!) {
    createSprint(input: { sprint: $item }) {
      sprint {
        id
        name
        description
        createdAt
        updatedAt
        duration
        active
        activationTime
        closed
        project
        weight
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class CreateSprintGQL extends Apollo.Mutation<CreateSprintMutation, CreateSprintMutationVariables> {
  document = CreateSprintDocument;
}
export const DeleteSprintDocument = gql`
  mutation DeleteSprint($id: UUID!) {
    deleteSprint(input: { id: $id }) {
      deletedSprintNodeId
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class DeleteSprintGQL extends Apollo.Mutation<DeleteSprintMutation, DeleteSprintMutationVariables> {
  document = DeleteSprintDocument;
}
export const GetSprintsDocument = gql`
  query GetSprints($first: Int, $after: Cursor, $before: Cursor, $orderBy: [SprintsOrderBy!]) {
    sprints(first: $first, after: $after, before: $before, orderBy: $orderBy) {
      edges {
        node {
          id
          createdAt
          updatedAt
          name
          description
          duration
          active
          activationTime
          closed
          weight
          project
        }
        cursor
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetSprintsGQL extends Apollo.Query<GetSprintsQuery, GetSprintsQueryVariables> {
  document = GetSprintsDocument;
}
export const UpdateSprintDocument = gql`
  mutation UpdateSprint($id: UUID!, $patch: SprintPatch!) {
    updateSprint(input: { patch: $patch, id: $id }) {
      sprint {
        id
        name
        description
        duration
        active
        activationTime
        closed
        project
        weight
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class UpdateSprintGQL extends Apollo.Mutation<UpdateSprintMutation, UpdateSprintMutationVariables> {
  document = UpdateSprintDocument;
}
export const CreateStoryDocument = gql`
  mutation CreateStory($item: StoryInput!) {
    createStory(input: { story: $item }) {
      story {
        id
        name
        description
        createdAt
        updatedAt
        points
        assignee
        project
        sprint
        sprintBySprint {
          id
          name
        }
        state
        taskType
        finishedDate
        weight
        tasksByStory {
          nodes {
            id
            createdAt
            updatedAt
            name
            description
            points
            state
            project
            sprint
            story
            assignee
            userProfileByAssignee {
              id
              firstName
              lastName
              fullName
              picture
              proficiency
            }
            taskType
            timeEntriesByTask {
              nodes {
                createdAt
                updatedAt
                id
                description
                hours
                progress
                time
                author
                userProfileByAuthor {
                  id
                  picture
                  firstName
                  lastName
                  fullName
                  role
                  proficiency
                }
              }
              totalCount
            }
            commentsByTask {
              nodes {
                author
                content
                createdAt
                id
                nodeId
                updatedAt
              }
              totalCount
            }
          }
          totalCount
        }
        tagbooksByStory {
          nodes {
            id
            tag
          }
          totalCount
        }
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class CreateStoryGQL extends Apollo.Mutation<CreateStoryMutation, CreateStoryMutationVariables> {
  document = CreateStoryDocument;
}
export const DeleteStoryDocument = gql`
  mutation DeleteStory($id: UUID!) {
    deleteStory(input: { id: $id }) {
      deletedStoryNodeId
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class DeleteStoryGQL extends Apollo.Mutation<DeleteStoryMutation, DeleteStoryMutationVariables> {
  document = DeleteStoryDocument;
}
export const GetStoriesDocument = gql`
  query GetStories($first: Int, $after: Cursor, $before: Cursor, $orderBy: [StoriesOrderBy!]) {
    stories(first: $first, after: $after, before: $before, orderBy: $orderBy) {
      edges {
        node {
          id
          name
          description
          createdAt
          updatedAt
          points
          assignee
          project
          sprint
          state
          taskType
          finishedDate
          weight
          tagbooksByStory {
            nodes {
              id
              tagByTag {
                id
                label
              }
            }
            totalCount
          }
        }
        cursor
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetStoriesGQL extends Apollo.Query<GetStoriesQuery, GetStoriesQueryVariables> {
  document = GetStoriesDocument;
}
export const GetStoriesInDepthDocument = gql`
  query GetStoriesInDepth($first: Int, $after: Cursor, $before: Cursor, $orderBy: [StoriesOrderBy!]) {
    stories(first: $first, after: $after, before: $before, orderBy: $orderBy) {
      edges {
        node {
          id
          name
          description
          createdAt
          updatedAt
          points
          assignee
          project
          sprint
          state
          taskType
          finishedDate
          weight
          tagbooksByStory {
            nodes {
              id
              tag
            }
            totalCount
          }
          tasksByStory {
            nodes {
              id
              createdAt
              updatedAt
              name
              description
              points
              state
              project
              sprint
              story
              assignee
              userProfileByAssignee {
                id
                firstName
                lastName
                fullName
                picture
                proficiency
              }
              taskType
              timeEntriesByTask {
                nodes {
                  createdAt
                  updatedAt
                  id
                  description
                  hours
                  progress
                  time
                  author
                  userProfileByAuthor {
                    id
                    picture
                    firstName
                    lastName
                    fullName
                    role
                    proficiency
                  }
                }
                totalCount
              }
              commentsByTask {
                nodes {
                  author
                  content
                  createdAt
                  id
                  nodeId
                  updatedAt
                }
                totalCount
              }
            }
            totalCount
          }
        }
        cursor
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetStoriesInDepthGQL extends Apollo.Query<GetStoriesInDepthQuery, GetStoriesInDepthQueryVariables> {
  document = GetStoriesInDepthDocument;
}
export const GetStoryDocument = gql`
  query GetStory($id: UUID!) {
    story(id: $id) {
      id
      name
      description
      createdAt
      updatedAt
      points
      assignee
      project
      sprint
      state
      taskType
      finishedDate
      weight
      tagbooksByStory {
        nodes {
          id
          tag
        }
        totalCount
      }
      tasksByStory {
        nodes {
          id
          createdAt
          updatedAt
          name
          description
          points
          state
          project
          sprint
          story
          assignee
          userProfileByAssignee {
            id
            firstName
            lastName
            fullName
            picture
            proficiency
          }
          taskType
          timeEntriesByTask {
            nodes {
              createdAt
              updatedAt
              id
              description
              hours
              progress
              time
              author
              userProfileByAuthor {
                id
                picture
                firstName
                lastName
                fullName
                role
                proficiency
              }
            }
            totalCount
          }
          commentsByTask {
            nodes {
              author
              content
              createdAt
              id
              nodeId
              updatedAt
            }
            totalCount
          }
        }
        totalCount
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetStoryGQL extends Apollo.Query<GetStoryQuery, GetStoryQueryVariables> {
  document = GetStoryDocument;
}
export const UpdateStoryDocument = gql`
  mutation UpdateStory($id: UUID!, $patch: StoryPatch!) {
    updateStory(input: { patch: $patch, id: $id }) {
      story {
        id
        name
        description
        createdAt
        updatedAt
        points
        state
        taskType
        finishedDate
        weight
        sprint
        sprintBySprint {
          id
          name
          description
          createdAt
          updatedAt
          duration
          active
          closed
          weight
        }
        assignee
        userProfileByAssignee {
          id
          firstName
          lastName
          fullName
          picture
          proficiency
        }
        tagbooksByStory {
          nodes {
            id
            tagByTag {
              id
              label
            }
          }
          totalCount
        }
        tasksByStory {
          nodes {
            id
            createdAt
            updatedAt
            name
            description
            points
            state
            project
            sprint
            story
            assignee
            userProfileByAssignee {
              id
              firstName
              lastName
              fullName
              picture
              proficiency
            }
            taskType
            timeEntriesByTask {
              nodes {
                createdAt
                updatedAt
                id
                description
                hours
                progress
                time
                author
                userProfileByAuthor {
                  id
                  picture
                  firstName
                  lastName
                  fullName
                  role
                  proficiency
                }
              }
              totalCount
            }
            commentsByTask {
              nodes {
                author
                content
                createdAt
                id
                nodeId
                updatedAt
              }
              totalCount
            }
          }
          totalCount
        }
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class UpdateStoryGQL extends Apollo.Mutation<UpdateStoryMutation, UpdateStoryMutationVariables> {
  document = UpdateStoryDocument;
}
export const CreateTagbookDocument = gql`
  mutation CreateTagbook($item: TagbookInput!) {
    createTagbook(input: { tagbook: $item }) {
      tagbook {
        id
        tagByTag {
          id
          label
        }
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class CreateTagbookGQL extends Apollo.Mutation<CreateTagbookMutation, CreateTagbookMutationVariables> {
  document = CreateTagbookDocument;
}
export const DeleteTagbookDocument = gql`
  mutation DeleteTagbook($id: UUID!) {
    deleteTagbook(input: { id: $id }) {
      deletedTagbookNodeId
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class DeleteTagbookGQL extends Apollo.Mutation<DeleteTagbookMutation, DeleteTagbookMutationVariables> {
  document = DeleteTagbookDocument;
}
export const UpdateTagbookDocument = gql`
  mutation UpdateTagbook($id: UUID!, $patch: TagbookPatch!) {
    updateTagbook(input: { patch: $patch, id: $id }) {
      tagbook {
        id
        story
        tag
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class UpdateTagbookGQL extends Apollo.Mutation<UpdateTagbookMutation, UpdateTagbookMutationVariables> {
  document = UpdateTagbookDocument;
}
export const GetTagsInDepthDocument = gql`
  query GetTagsInDepth($first: Int, $after: Cursor, $before: Cursor, $orderBy: [TagsOrderBy!]) {
    tags(first: $first, after: $after, before: $before, orderBy: $orderBy) {
      edges {
        node {
          id
          createdAt
          updatedAt
          label
          description
        }
        cursor
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetTagsInDepthGQL extends Apollo.Query<GetTagsInDepthQuery, GetTagsInDepthQueryVariables> {
  document = GetTagsInDepthDocument;
}
export const CreateTaskDocument = gql`
  mutation CreateTask($item: TaskInput!) {
    createTask(input: { task: $item }) {
      task {
        id
        name
        description
        createdAt
        updatedAt
        points
        assignee
        userProfileByAssignee {
          id
          fullName
          picture
          proficiency
        }
        project
        sprint
        sprintBySprint {
          id
          name
        }
        story
        storyByStory {
          id
          name
        }
        state
        taskType
        weight
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class CreateTaskGQL extends Apollo.Mutation<CreateTaskMutation, CreateTaskMutationVariables> {
  document = CreateTaskDocument;
}
export const DeleteTaskDocument = gql`
  mutation DeleteTask($id: UUID!) {
    deleteTask(input: { id: $id }) {
      deletedTaskNodeId
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class DeleteTaskGQL extends Apollo.Mutation<DeleteTaskMutation, DeleteTaskMutationVariables> {
  document = DeleteTaskDocument;
}
export const GetTaskDocument = gql`
  query GetTask($id: UUID!) {
    task(id: $id) {
      id
      name
      description
      createdAt
      updatedAt
      points
      assignee
      project
      sprint
      story
      state
      taskType
      weight
      commentsByTask {
        totalCount
        nodes {
          id
          createdAt
          updatedAt
          content
          author
          userProfileByAuthor {
            id
            fullName
            picture
          }
        }
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetTaskGQL extends Apollo.Query<GetTaskQuery, GetTaskQueryVariables> {
  document = GetTaskDocument;
}
export const GetTasksDocument = gql`
  query GetTasks($first: Int, $after: Cursor, $before: Cursor, $orderBy: [TasksOrderBy!]) {
    tasks(first: $first, after: $after, before: $before, orderBy: $orderBy) {
      edges {
        node {
          id
          name
          description
          createdAt
          updatedAt
          points
          assignee
          project
          sprint
          story
          state
          taskType
          weight
        }
        cursor
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetTasksGQL extends Apollo.Query<GetTasksQuery, GetTasksQueryVariables> {
  document = GetTasksDocument;
}
export const GetTasksInDepthDocument = gql`
  query GetTasksInDepth($first: Int, $after: Cursor, $before: Cursor, $orderBy: [TasksOrderBy!]) {
    tasks(first: $first, after: $after, before: $before, orderBy: $orderBy) {
      edges {
        node {
          id
          name
          description
          createdAt
          updatedAt
          points
          assignee
          project
          sprint
          state
          taskType
          weight
          timeEntriesByTask {
            totalCount
            nodes {
              id
              description
              hours
              progress
              time
              task
              author
            }
          }
          commentsByTask {
            totalCount
            nodes {
              id
              content
              author
              userProfileByAuthor {
                id
                fullName
                picture
              }
            }
          }
          userProfileByAssignee {
            id
            fullName
            picture
            proficiency
          }
        }
        cursor
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetTasksInDepthGQL extends Apollo.Query<GetTasksInDepthQuery, GetTasksInDepthQueryVariables> {
  document = GetTasksInDepthDocument;
}
export const UpdateTaskDocument = gql`
  mutation UpdateTask($id: UUID!, $patch: TaskPatch!) {
    updateTask(input: { patch: $patch, id: $id }) {
      task {
        id
        name
        description
        createdAt
        updatedAt
        points
        assignee
        project
        sprint
        sprintBySprint {
          id
          name
        }
        story
        storyByStory {
          id
          name
        }
        state
        taskType
        weight
        timeEntriesByTask {
          totalCount
          nodes {
            id
            description
            hours
            progress
            time
            task
            author
          }
        }
        commentsByTask {
          totalCount
          nodes {
            id
            content
            author
            userProfileByAuthor {
              id
              fullName
              picture
            }
          }
        }
        userProfileByAssignee {
          id
          fullName
          picture
          proficiency
        }
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class UpdateTaskGQL extends Apollo.Mutation<UpdateTaskMutation, UpdateTaskMutationVariables> {
  document = UpdateTaskDocument;
}
export const CreateTimeEntryDocument = gql`
  mutation CreateTimeEntry($item: TimeEntryInput!) {
    createTimeEntry(input: { timeEntry: $item }) {
      timeEntry {
        id
        description
        hours
        progress
        time
        task
        author
        userProfileByAuthor {
          id
          fullName
        }
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class CreateTimeEntryGQL extends Apollo.Mutation<CreateTimeEntryMutation, CreateTimeEntryMutationVariables> {
  document = CreateTimeEntryDocument;
}
export const DeleteTimeEntryDocument = gql`
  mutation DeleteTimeEntry($id: UUID!) {
    deleteTimeEntry(input: { id: $id }) {
      deletedTimeEntryNodeId
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class DeleteTimeEntryGQL extends Apollo.Mutation<DeleteTimeEntryMutation, DeleteTimeEntryMutationVariables> {
  document = DeleteTimeEntryDocument;
}
export const UpdateTimeEntryDocument = gql`
  mutation UpdateTimeEntry($id: UUID!, $patch: TimeEntryPatch!) {
    updateTimeEntry(input: { patch: $patch, id: $id }) {
      timeEntry {
        id
        description
        hours
        progress
        time
        task
        author
        userProfileByAuthor {
          id
          fullName
        }
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class UpdateTimeEntryGQL extends Apollo.Mutation<UpdateTimeEntryMutation, UpdateTimeEntryMutationVariables> {
  document = UpdateTimeEntryDocument;
}
export const DeleteUserProfileDocument = gql`
  mutation DeleteUserProfile($id: UUID!) {
    deleteUserProfile(input: { id: $id }) {
      deletedUserProfileNodeId
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class DeleteUserProfileGQL extends Apollo.Mutation<
  DeleteUserProfileMutation,
  DeleteUserProfileMutationVariables
> {
  document = DeleteUserProfileDocument;
}
export const GetUserProfilesDocument = gql`
  query GetUserProfiles($first: Int, $after: Cursor, $before: Cursor) {
    userProfiles(first: $first, after: $after, before: $before) {
      edges {
        node {
          id
          firstName
          lastName
          fullName
          picture
          role
          proficiency
          roleByRole {
            id
            name
          }
          storiesByAssignee {
            nodes {
              id
              state
            }
            totalCount
          }
          tasksByAssignee {
            nodes {
              id
              state
            }
            totalCount
          }
        }
        cursor
      }
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class GetUserProfilesGQL extends Apollo.Query<GetUserProfilesQuery, GetUserProfilesQueryVariables> {
  document = GetUserProfilesDocument;
}
export const RegisterUserDocument = gql`
  mutation RegisterUser($item: RegisterUserInput!) {
    registerUser(input: $item) {
      userProfile {
        id
        firstName
        lastName
        fullName
        picture
        proficiency
        role
        roleByRole {
          id
          name
        }
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class RegisterUserGQL extends Apollo.Mutation<RegisterUserMutation, RegisterUserMutationVariables> {
  document = RegisterUserDocument;
}
export const UpdateUserProfileDocument = gql`
  mutation UpdateUserProfile($id: UUID!, $patch: UserProfilePatch!) {
    updateUserProfile(input: { patch: $patch, id: $id }) {
      userProfile {
        id
        firstName
        lastName
        fullName
        picture
        proficiency
        role
        roleByRole {
          id
          name
        }
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class UpdateUserProfileGQL extends Apollo.Mutation<
  UpdateUserProfileMutation,
  UpdateUserProfileMutationVariables
> {
  document = UpdateUserProfileDocument;
}
