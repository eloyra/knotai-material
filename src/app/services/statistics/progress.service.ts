import { Injectable } from '@angular/core';
import { Sprint, State, Story, Task, TimeEntry } from 'src/app/services/api';

@Injectable({
  providedIn: 'root',
})
export class ProgressService {
  constructor() {}

  /**
   * Calculates story progress based on tasks finished divided by total story tasks.
   *
   * @param story
   * @param tasks
   */
  public static forStory({ story, tasks }: { story?: Story; tasks?: Task[] }): number {
    let data = [];
    let totalElements = 0;

    if (story && story?.tasksByStory?.totalCount) {
      data = story.tasksByStory.nodes;
      totalElements = story.tasksByStory.totalCount;
    } else if (tasks?.length) {
      data = tasks;
      totalElements = tasks.length;
    }

    if (data.length && totalElements) {
      return (data.filter((task: Task) => task.state === State.Finished).length / data.length) * 100;
    }

    return 0;
  }

  public static getSprintDeadlineMilliseconds({ sprint }: { sprint: Sprint }): number {
    if (sprint?.duration != null) {
      const createdAtMilliseconds = new Date(sprint.createdAt).getTime();
      const durationMilliseconds = sprint.duration * 7 * 24 * 60 * 60 * 1000;

      return createdAtMilliseconds + durationMilliseconds;
    }

    return null;
  }

  /**
   * Calculate progress for a Sprint.
   *
   * The progress % is calculated based on the % progress of each story in the sprint, divided by the number of stories.
   *
   * The progress color is explained inside the function.
   *
   * @param sprint
   * @param velocity
   */
  public static forSprint({
    sprint,
    velocity,
  }: {
    sprint: Sprint;
    velocity: number;
  }): { progress: number; color: string } {
    const {
      progress,
      totalUnfinishedStoryPoints,
      totalUnfinishedStoriesWorkedHours,
    } = ProgressService.extractSprintMetrics(sprint);

    /**
     * Color represents if we are on track with development.
     *
     * We calculate color based on multiple metrics:
     * - totalUnfinishedStoryPoints: addition of the story points of all unfinished stories
     * - totalUnfinishedStoriesWorkedHours: addition of all worked hours (obtained through tasks) in all unfinished stories
     * - velocity: statistical project metric that defines average hours required per story point
     * - deadline: calculated adding the createdAd date and the duration
     *
     * With this metrics we calculate available time to finish and statistical time required to finished, and return colors
     * based on the results:
     * - blue: not enough data
     * - green: on track
     * - yellow: slightly delayed
     * - red: delayed
     */
    const deadlineMilliseconds = ProgressService.getSprintDeadlineMilliseconds({ sprint });

    const color = ProgressService.getStatusColor(
      totalUnfinishedStoryPoints,
      totalUnfinishedStoriesWorkedHours,
      velocity,
      deadlineMilliseconds
    );

    return {
      progress,
      color,
    };
  }

  public static getHoursInTask(task: Task): number | null {
    if (task?.timeEntriesByTask?.totalCount) {
      return task.timeEntriesByTask.nodes.reduce(
        (hours: number, currentEntry: TimeEntry) => hours + +currentEntry?.hours,
        0
      );
    }

    return null;
  }

  private static extractSprintMetrics(
    sprint: Sprint
  ): { progress: number; totalUnfinishedStoryPoints: number; totalUnfinishedStoriesWorkedHours: number } {
    // Progress is calculated based on the addition of all the stories' progress divided by the number of stories.
    let progress = 0;

    // This two metrics will be used to estimate if the sprint will finish on time
    let totalUnfinishedStoryPoints = 0;
    let totalUnfinishedStoriesWorkedHours = 0;

    if (sprint?.storiesBySprint?.totalCount) {
      sprint.storiesBySprint.nodes.forEach((story: Story) => {
        // Get the progress per story
        progress += ProgressService.forStory({ story });

        if (story.state !== State.Finished) {
          // Iteratively add up the unfinished story points of the sprint
          totalUnfinishedStoryPoints += story.points;

          // Add up the unfinished stories' dedicated times
          if (story?.tasksByStory?.totalCount) {
            story.tasksByStory.nodes.forEach((task: Task) => {
              if (task?.timeEntriesByTask?.totalCount) {
                totalUnfinishedStoriesWorkedHours += task.timeEntriesByTask.nodes.reduce(
                  (accumulatedTime: number, currentEntry: TimeEntry) => accumulatedTime + +currentEntry?.hours,
                  0
                );
              }
            });
          }
        }
      });

      progress /= sprint.storiesBySprint.totalCount;
    }

    return {
      progress,
      totalUnfinishedStoryPoints,
      totalUnfinishedStoriesWorkedHours,
    };
  }

  private static getStatusColor(
    totalUnfinishedStoryPoints: number,
    totalUnfinishedStoriesWorkedHours: number,
    velocity: number,
    deadlineMilliseconds: number
  ): string {
    const estimatedTimeToFinish = totalUnfinishedStoryPoints * velocity - totalUnfinishedStoriesWorkedHours;

    if (estimatedTimeToFinish < 0 || typeof deadlineMilliseconds !== 'number') {
      return 'blue';
    }

    const estimatedTimeToFinishMilliseconds = estimatedTimeToFinish * 60 * 60 * 1000;
    const availableTime = deadlineMilliseconds - Date.now();
    const availableTimeGenerous = deadlineMilliseconds * 1.00015 - Date.now();

    if (availableTime > estimatedTimeToFinishMilliseconds) {
      return 'green';
    } else if (availableTimeGenerous > estimatedTimeToFinishMilliseconds) {
      return 'yellow';
    } else {
      return 'red';
    }
  }

  public static calculateSprintVelocity(sprint: Sprint): number {
    const durationInHours = sprint?.duration * 5 * 8;
    let finishedStoryPoints = 0;

    if (sprint?.storiesBySprint?.totalCount) {
      finishedStoryPoints = sprint.storiesBySprint.nodes
        .filter((story: Story) => story?.state === State.Finished)
        .reduce((points: number, currentStory: Story) => points + +currentStory?.points, 0);
    }

    if (!durationInHours || !finishedStoryPoints) return 0;

    return durationInHours / finishedStoryPoints;
  }
}
