import { Injectable } from '@angular/core';
import { Project, Sprint, State, Story } from 'src/app/services/api';
import { ProgressService } from 'src/app/services/statistics/progress.service';
import { isWeekend } from 'src/app/shared/utils';

type BurndownPlotLine = Array<{ t: number; y: number }>;

type BurndownDataset = {
  ideal?: BurndownPlotLine;
  real: BurndownPlotLine;
  activeSprint?: BurndownPlotLine;
} | null;

@Injectable({
  providedIn: 'root',
})
export class BurndownService {
  constructor() {}

  /**
   * Ideal burndown for sprint is based on ideal burndown completion of story points per day taking
   * into consideration 5-day work weeks.
   *
   * Real burndown for sprint is calculated plotting the real completion times of stories and their
   * story points.
   *
   * @param sprint
   */
  public static forSprint({ sprint }: { sprint: Sprint }): BurndownDataset {
    const sprintStoryPoints = BurndownService.getTotalSprintStoryPoints(sprint);

    if (!sprintStoryPoints) {
      return null;
    }

    return {
      ideal: [...(BurndownService.extractSprintIdealBurndown(sprint, sprintStoryPoints) ?? [])],
      real: [...(BurndownService.extractRealSprintBurndown(sprint, sprintStoryPoints) ?? [])],
    };
  }

  /*public static forProject({ project }: { project: Project }): BurndownDataset {
    if (!project?.sprintsByProject?.totalCount) {
      return null;
    }

    const orderedSprints = project.sprintsByProject.nodes.sort((a: Sprint, b: Sprint) => {
      return a.createdAt - b.createdAt;
    });

    const activeSprintId = project.sprintsByProject.nodes.find((sprint: Sprint) => sprint?.active)?.id;

    let idealBurndown: BurndownPlotLine = [];
    let realBurndown: BurndownPlotLine = [];
    let activeSprintBurndown: BurndownPlotLine = [];

    orderedSprints.forEach((sprint: Sprint) => {
      const currentSprintStoryPoints = BurndownService.getTotalSprintStoryPoints(sprint);

      if (currentSprintStoryPoints) {
        const localIdealBurndown = [...BurndownService.extractSprintIdealBurndown(sprint, currentSprintStoryPoints)];

        const localRealBurndown =
          sprint?.closed || sprint.id === activeSprintId
            ? [...BurndownService.extractRealSprintBurndown(sprint, currentSprintStoryPoints)]
            : null;

        if (localIdealBurndown?.length) {
          idealBurndown = [...idealBurndown, ...localIdealBurndown];
        }

        if (localRealBurndown?.length) {
          if (sprint?.id === activeSprintId) {
            activeSprintBurndown = [...localRealBurndown];
          } else {
            realBurndown = [...realBurndown, ...localRealBurndown];
          }
        }
      }
    });

    return {
      ideal: idealBurndown,
      real: realBurndown,
      activeSprint: activeSprintBurndown,
    };
  }*/

  public static forProject({ project }: { project: Project }): BurndownDataset {
    const projectStoryPoints = BurndownService.getTotalProjectStoryPoints(project);
    let projectStoryPointsBuffer = projectStoryPoints;

    if (!projectStoryPoints) {
      return null;
    }

    const orderedSprints = project.sprintsByProject.nodes.sort((a: Sprint, b: Sprint) => {
      return a.createdAt - b.createdAt;
    });

    const activeSprintId = project.sprintsByProject.nodes.find((sprint: Sprint) => sprint?.active)?.id;

    let idealBurndown: BurndownPlotLine = [];
    let realBurndown: BurndownPlotLine = [];
    let activeSprintBurndown: BurndownPlotLine = [];

    orderedSprints.forEach((sprint: Sprint) => {
      const currentSprintStoryPoints = BurndownService.getTotalSprintStoryPoints(sprint);

      if (currentSprintStoryPoints) {
        const localIdealBurndown = [
          ...(BurndownService.extractSprintIdealBurndown(sprint, currentSprintStoryPoints, projectStoryPointsBuffer) ??
            []),
        ];

        const localRealBurndown =
          sprint?.closed || sprint.id === activeSprintId
            ? [
                ...(BurndownService.extractRealSprintBurndown(
                  sprint,
                  currentSprintStoryPoints,
                  projectStoryPointsBuffer
                ) ?? []),
              ]
            : null;

        projectStoryPointsBuffer -= currentSprintStoryPoints;

        if (localIdealBurndown?.length) {
          idealBurndown = [...idealBurndown, ...localIdealBurndown];
        }

        if (localRealBurndown?.length) {
          if (sprint?.id === activeSprintId) {
            activeSprintBurndown = [...localRealBurndown];
          } else {
            realBurndown = [...realBurndown, ...localRealBurndown];
          }
        }
      }
    });

    return {
      ideal: idealBurndown,
      real: realBurndown,
      activeSprint: activeSprintBurndown,
    };
  }

  private static getTotalSprintStoryPoints(sprint: Sprint): number {
    if (sprint?.storiesBySprint?.totalCount) {
      return sprint.storiesBySprint.nodes.reduce(
        (accumulatedPoints: number, currentStory: Story) => accumulatedPoints + +currentStory?.points,
        0
      );
    }

    return 0;
  }

  private static getTotalProjectStoryPoints(project: Project): number {
    let points = 0;

    if (project?.sprintsByProject?.totalCount) {
      project.sprintsByProject.nodes.forEach((sprint: Sprint) => {
        points += BurndownService.getTotalSprintStoryPoints(sprint);
      });

      return points;
    }

    return 0;
  }

  /**
   * Ideal sprint burndown is based on ideal completion of story points per day.
   *
   * @param sprint
   * @param sprintStoryPoints
   * @param mockTotalPoints
   *
   * @private
   */
  private static extractSprintIdealBurndown(
    sprint: Sprint,
    sprintStoryPoints: number,
    mockTotalPoints?: number
  ): BurndownPlotLine | null {
    const startDate = new Date(sprint.createdAt).getTime();
    const deadline = ProgressService.getSprintDeadlineMilliseconds({ sprint });
    const dayMilliseconds = 24 * 60 * 60 * 1000;
    const naturalDaysInSprint = Math.round((deadline - startDate) / dayMilliseconds);
    const realDaysInSprint = naturalDaysInSprint - Math.floor(naturalDaysInSprint / 7) * 2;

    if (realDaysInSprint <= 0) {
      return null;
    }

    const pointsPerDay = Math.max(sprintStoryPoints / realDaysInSprint, 1);
    const loopDateIncrement = pointsPerDay >= 1 ? dayMilliseconds : (realDaysInSprint / pointsPerDay) * dayMilliseconds;

    const idealBurndown = [
      {
        t: startDate,
        y: mockTotalPoints || sprintStoryPoints,
      },
    ];
    let storyPointBuffer = (mockTotalPoints || sprintStoryPoints) - pointsPerDay;
    for (let i = startDate + loopDateIncrement; i <= deadline; i += loopDateIncrement) {
      if (!isWeekend(new Date(i))) {
        idealBurndown.push({
          t: i,
          y: Math.round(storyPointBuffer),
        });

        storyPointBuffer -= pointsPerDay;
      }

      if (storyPointBuffer <= 0) {
        break;
      }
    }

    if (!mockTotalPoints && idealBurndown[idealBurndown.length - 1].y > 0) {
      idealBurndown.push({ ...idealBurndown.pop(), y: 0 });
    }

    return idealBurndown;
  }

  private static extractRealSprintBurndown(
    sprint: Sprint,
    sprintStoryPoints: number,
    mockTotalPoints?: number
  ): BurndownPlotLine {
    const stories = sprint.storiesBySprint.nodes
      .filter((story: Story) => story.state === State.Finished && !!story?.finishedDate)
      .sort((a: Story, b: Story) => {
        return new Date(a.finishedDate).getTime() - new Date(b.finishedDate).getTime();
      });

    const realBurndown = [
      {
        t: sprint.createdAt,
        y: mockTotalPoints || sprintStoryPoints,
      },
    ];
    let storyPointBuffer = mockTotalPoints || sprintStoryPoints;
    stories.forEach((story: Story) => {
      storyPointBuffer -= story.points;

      realBurndown.push({
        t: story.finishedDate,
        y: storyPointBuffer,
      });
    });

    return realBurndown;
  }
}
