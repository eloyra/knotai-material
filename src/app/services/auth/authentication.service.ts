import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { AuthenticateGQL, AuthenticatePayload, GetCurrentUserGQL, UserProfile } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';

const JWT_TOKEN = 'token';

// noinspection JSUnusedGlobalSymbols
export class JwtToken {
  aud: string;
  exp: number;
  iat: number;
  iss: string;
  role_id: string;
  role_name: string;
  user_id: string;
  user_name: string;

  constructor(object: any) {
    Object.assign(this, object);
  }
}

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(
    private graphql: GraphqlService,
    private login: AuthenticateGQL,
    private currentUser: GetCurrentUserGQL,
    private apollo: Apollo
  ) {}

  getToken(decode: boolean = true): string | JwtToken | null {
    const token = localStorage.getItem(JWT_TOKEN);
    if (!token) return null;
    return decode ? new JwtToken(jwtDecode(token)) : token;
  }

  isAuthenticated(): boolean {
    return this.isTokenValid();
  }

  getCurrentUser(): Observable<UserProfile> {
    return this.graphql
      .query(this.currentUser)
      .pipe((response: Observable<unknown>) => response as Observable<UserProfile>);
  }

  authenticate(credentials: any): Promise<boolean> {
    this.logout();
    return this.graphql
      .mutate(this.login, { item: credentials })
      .toPromise()
      .then((response: AuthenticatePayload) => {
        if (response !== null) {
          AuthenticationService.storeToken(response as string);
          return true;
        }
        return false;
      })
      .catch((reason) => {
        console.error(reason);
        return false;
      });
  }

  logout(): void {
    this.apollo.getClient().resetStore();
    AuthenticationService.deleteToken();
  }

  private isTokenValid(token?: string | JwtToken): boolean {
    let content = token;
    if (!content) {
      content = this.getToken(true);
    }
    if (!content) {
      return false;
    }
    if (typeof token === 'string') {
      content = new JwtToken(jwtDecode(token));
    }
    return Math.floor(Date.now() / 1000) < (content as JwtToken).exp;
  }

  private static storeToken(jwtToken: string): void {
    localStorage.setItem(JWT_TOKEN, jwtToken);
  }

  private static deleteToken(): void {
    localStorage.removeItem(JWT_TOKEN);
  }
}
