import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad, Router } from '@angular/router';
import { AuthenticationService, JwtToken } from 'src/app/services/auth/authentication.service';
import { environment } from 'src/environments';

@Injectable({
  providedIn: 'root',
})
export class AdminAuthGuardService implements CanActivate, CanLoad, CanActivateChild {
  constructor(private auth: AuthenticationService, private router: Router) {}

  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['login']);
      return false;
    }

    if ((this.auth.getToken() as JwtToken)?.role_name !== environment.adminRole) {
      this.router.navigate(['login']);
      return false;
    }

    return true;
  }

  canActivateChild(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['login']);
      return false;
    }

    if ((this.auth.getToken() as JwtToken)?.role_name !== environment.adminRole) {
      this.router.navigate(['login']);
      return false;
    }

    return true;
  }

  canLoad(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['login']);
      return false;
    }

    if ((this.auth.getToken() as JwtToken)?.role_name !== environment.adminRole) {
      this.router.navigate(['login']);
      return false;
    }

    return true;
  }
}
