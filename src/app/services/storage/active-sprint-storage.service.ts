import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Project, Sprint } from 'src/app/services/api';
import { ProjectStorageService } from 'src/app/services/storage/project-storage.service';

@Injectable({
  providedIn: 'root',
})
export class ActiveSprintStorageService {
  private sprint: Sprint = null;
  private sprint$: BehaviorSubject<Sprint> = new BehaviorSubject<Sprint>(this.sprint);

  constructor(private projectStorage: ProjectStorageService) {
    if (!this.sprint) {
      this.sprint = this.recover();
      this.sprint$.next(this.sprint);
    }
  }

  public getSprintSnapshot(): Sprint {
    return this.recover();
  }

  public getListener(project: Project = null): Observable<Sprint> {
    if (!this.sprint && project) {
      this.loadFromProject(project).then(this.sprint$.next).catch(this.sprint$.next);
    }
    return this.sprint$;
  }

  public loadFromProject(project: Project = null): Promise<any> {
    return new Promise((resolve, reject) => {
      if (project) {
        this.sprint = this.findActiveSprintInProject(project);
        this.store(this.sprint);
        resolve(this.sprint);
      } else {
        this.projectStorage.getListener().subscribe((project: Project) => {
          if (project) {
            this.sprint = this.findActiveSprintInProject(project);
            this.store(this.sprint);
            resolve(this.sprint);
          }
          reject('loadFromProject failed to load sprint.');
        }, reject);
      }
    });
  }

  public findActiveSprintInProject(project: Project): Sprint {
    let activeSprint: Sprint = null;
    if (project && project?.sprintsByProject?.totalCount) {
      activeSprint = project.sprintsByProject.nodes.find((sprint: Sprint) => sprint.active);
    }
    return activeSprint;
  }

  public store(sprint: Sprint) {
    if (sprint) {
      this.sprint = sprint;
      this.sprint$.next(this.sprint);
      this.persist(this.sprint);
    }
  }

  public flush(sprint: Sprint = this.sprint) {
    this.store(sprint);
  }

  public invalidate() {
    this.sprint = null;
    this.delete();
  }

  private persist = (sprint: Sprint) => {
    localStorage.setItem('activeSprint', JSON.stringify(sprint));
  };

  private recover = (): Sprint => {
    const data = localStorage.getItem('activeSprint');
    return data ? JSON.parse(data) : null;
  };

  private delete = () => {
    localStorage.removeItem('activeSprint');
  };
}
