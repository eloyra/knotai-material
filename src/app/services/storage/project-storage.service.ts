import { Injectable } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { GetProjectGQL, Project } from 'src/app/services/api';
import { GraphqlService } from 'src/app/services/api/graphql.service';

@Injectable({
  providedIn: 'root',
})
export class ProjectStorageService {
  private project: Project = null;
  private project$: BehaviorSubject<Project> = new BehaviorSubject<Project>(this.project);
  private projects: Project[] = [];

  constructor(private graphql: GraphqlService, private getProject: GetProjectGQL) {
    if (!this.project) {
      this.project = this.recover();
      this.project$.next(this.project);
    }
  }

  public getProjectSnapshot(): Project {
    return this.recover();
  }

  public getListener(currentRoute: ActivatedRoute = null): Observable<Project> {
    if (!this.project && currentRoute) {
      this.loadFromRoute(currentRoute).catch(this.project$.next);
    }
    return this.project$;
  }

  public store(project: Project) {
    if (project) {
      this.project = project;
      this.project$.next(this.project);
      this.persist(this.project);
    }
  }

  public flush(project: Project = this.project) {
    this.store(project);
  }

  public invalidate() {
    this.project = null;
    this.delete();
  }

  public loadFromRoute(route: ActivatedRoute): Promise<any> {
    return new Promise((resolve, reject) => {
      route.paramMap
        .pipe(
          switchMap((params: ParamMap) => {
            if (params.get('project')) {
              return this.graphql.query(this.getProject, { id: params.get('project'), storiesOrderBy: 'WEIGHT_ASC' });
            } else {
              return Promise.reject('loadFromRoute failed to load project.');
            }
          })
        )
        .subscribe((project: Project) => {
          this.store(project);
          resolve(project);
        }, reject);
    });
  }

  public storeProjects(projects: Project[]): void {
    this.projects = projects;
  }

  public getStoredProjects(): Project[] {
    return this.projects;
  }

  private persist = (project: Project) => {
    localStorage.setItem('project', JSON.stringify(project));
  };

  private recover = (): Project => {
    const data = localStorage.getItem('project');
    return data ? JSON.parse(data) : null;
  };

  private delete = () => {
    localStorage.removeItem('project');
  };
}
