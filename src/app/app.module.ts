import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APOLLO_OPTIONS, ApolloModule } from 'apollo-angular';
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ContextMenuModule } from 'ngx-contextmenu';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { SimplemdeModule } from 'ngx-simplemde';
import { createApollo } from 'src/app/graphql.module';
import { LoginPageComponent } from 'src/app/login/login-page/login-page.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { ManagementModule } from './modules/management/management.module';
import { FormlyWrapperAddonsComponent } from './shared/forms/formly/formly-wrapper-addons/formly-wrapper-addons.component';
import { addonsExtension } from './shared/forms/formly/addons.extension';
import { TimeagoModule } from 'ngx-timeago';

@NgModule({
  declarations: [AppComponent, LoginPageComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    ApolloModule,
    HttpLinkModule,
    ManagementModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      wrappers: [{ name: 'addons', component: FormlyWrapperAddonsComponent }],
      extensions: [{ name: 'addons', extension: { onPopulate: addonsExtension } }],
    }),
    FormlyMaterialModule,
    FlexLayoutModule,
    CommonModule,
    ContextMenuModule.forRoot(),
    TimeagoModule.forRoot(),
    SimplemdeModule.forRoot({}),
  ],
  providers: [
    Title,
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
